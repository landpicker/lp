export interface Area {
	code: string;
	type: AreaType;
	name: string;
	parentAreaCode: string;
	subAreas: Area[];
}

export enum AreaType {
	REGION = 'REGION',
	DISTRICT = 'DISTRICT',
	CITY = 'CITY',
	CADASTRAL_TERRITORY = 'CADASTRAL_TERRITORY'
}

export interface AreaGroup {
	type: AreaType;
	areas: Area[];
}
