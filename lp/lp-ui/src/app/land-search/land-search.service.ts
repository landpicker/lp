import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LandSearchFilter } from './land-search-filter/land-search-filter.interface';
import { Observable } from 'rxjs';
import { Land, LandSearchResult } from './land-search.interface';
import { delay } from 'rxjs/operators';
import { DEFAULT_PAGE_SIZE } from './land-search-result/land-search-table/land-search-table.component';

@Injectable({
  providedIn: 'root'
})
export class LandSearchService {

  constructor(private _http: HttpClient) {}

  geExpectedCountOftLands(filter: LandSearchFilter): Observable<number> {
		return this._http.post<number>('api/lands/count', filter).pipe(delay(500));
	}

  getLandsBySearchId(searchId: string): Observable<LandSearchResult> {
		return this._http.post<LandSearchResult>(`api/lands/search/${searchId}?page=0&size=${DEFAULT_PAGE_SIZE}`, {}).pipe(delay(500));
	}

	getLandsByFilter(filter: LandSearchFilter, page: number, pageSize: number): Observable<LandSearchResult> {
		return this._http.post<LandSearchResult>(`api/lands/search?page=${page}&size=${pageSize}`, filter).pipe(delay(500));
	}

	getLand(id: string): Observable<Land> {
		return this._http.get<Land>(`api/lands/${id}`).pipe(delay(500));
	}
}
