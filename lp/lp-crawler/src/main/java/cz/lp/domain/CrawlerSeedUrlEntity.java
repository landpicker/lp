package cz.lp.domain;

import cz.lp.enumerate.SeedUrlCondition;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Crawler SEED URL entity represents URL object in database table and contains default URL of crawler.
 */
@Entity
@Table(name = "crawler_seed_url")
public class CrawlerSeedUrlEntity implements Serializable {

    @Id
    @SequenceGenerator(name = "id", sequenceName = "crawler_seed_url_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "crawler_id", nullable = false)
    private CrawlerEntity crawler;

    @Column(name = "url", unique = true, nullable = false)
    private String url;

    @Column(name = "enabled", nullable = false)
    private Boolean enabled;

    @Enumerated(EnumType.STRING)
    @Column(name = "condition", nullable = false, length = 100)
    private SeedUrlCondition condition;

    /**
     * Creates default constructor.
     */
    public CrawlerSeedUrlEntity() {
    }

    /**
     * Creates instance of crawler seed URL entity.
     *
     * @param id        identifier
     * @param crawler   crawler
     * @param url       URL
     * @param enabled   enabled
     * @param condition condition
     */
    public CrawlerSeedUrlEntity(Long id, CrawlerEntity crawler, String url, Boolean enabled, SeedUrlCondition condition) {
        this.id = id;
        this.crawler = crawler;
        this.url = url;
        this.enabled = enabled;
        this.condition = condition;
    }

    /**
     * Gets identifier.
     *
     * @return identifier
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets crawler.
     *
     * @return crawler
     */
    public CrawlerEntity getCrawler() {
        return crawler;
    }

    /**
     * Gets URL.
     *
     * @return URL
     */
    public String getUrl() {
        return url;
    }

    /**
     * Check if is enabled.
     *
     * @return enabled
     */
    public Boolean isEnabled() {
        return enabled;
    }

    /**
     * Gets condition.
     *
     * @return condition
     */
    public SeedUrlCondition getCondition() {
        return condition;
    }
}
