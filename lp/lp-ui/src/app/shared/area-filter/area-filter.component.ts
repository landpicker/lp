import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl } from '../../../../node_modules/@angular/forms';
import { MatAutocompleteSelectedEvent } from '../../../../node_modules/@angular/material';
import { Area, AreaGroup, AreaType } from './area.interface';
import { Observable, fromEvent, merge, of } from '../../../../node_modules/rxjs';
import { map } from 'rxjs/operators';
import { AreaFilterService } from './area-filter.service';

@Component({
	selector: 'lp-area-filter',
	templateUrl: './area-filter.component.html',
	styleUrls: ['./area-filter.component.scss']
})
export class AreaFilterComponent implements OnInit, AfterViewInit {
	@Input()
	areaForm: FormGroup = new FormGroup({ areas: new FormControl([]) });
	areaAutocompleteControl = new FormControl();
	filteredAreaGroups: Observable<AreaGroup[]>;
	@ViewChild('areaInput')
	areaInput: ElementRef;
	areaTrees: Area[] = [];
	selectedAreas: Area[] = [];

	constructor(private _areaFilterService: AreaFilterService) {}

	ngOnInit() {
		// Load area trees
		this._areaFilterService.getAreaTree().subscribe((areaTrees) => {
			this.areaTrees = areaTrees;

			// Init selected areas from search id
			this.selectedAreas = this._getAreaListFromTreesWithoutSubAreas(this.areaTrees).filter((area) => this.areaForm.get('areas').value.findIndex((areaCode) => areaCode === area.code) !== -1);
		});
	}

	ngAfterViewInit(): void {
		// Init autocomplete handler for focus and change value
		this.filteredAreaGroups = merge(
			fromEvent(this.areaInput.nativeElement, 'focus').pipe(map((event: any) => (event.target.value && event.target.value.length > 2 ? this._filterAreaGroups(event.target.value) : []))),
			this.areaAutocompleteControl.valueChanges.pipe(
				// Check if autocomplete contains string by user or area object after user selection
				map((value) => (typeof value === 'string' ? value : undefined)),
				// If the value was selected by user (value === undefined) return empty array
				// If the value was filled by user and it has more then 2 characters return filtered area groups, othrerwiser return empty array
				map((value) => (value && value.length > 2 ? this._filterAreaGroups(value) : []))
			)
		);
	}

	display(area?: Area): string | undefined {
		return '';
	}

	onAreaSelect(event: MatAutocompleteSelectedEvent): void {
		this._selectArea(event.option.value.code);
		this._updateSelectedAreas();
	}

	onAreaRemove(area: Area): void {
		const areaIndexForRemove = this.selectedAreas.findIndex((selectedArea) => selectedArea.code === area.code);
		if (areaIndexForRemove !== -1) {
			this.selectedAreas.splice(areaIndexForRemove, 1);
		}
		this._updateSelectedAreas();
	}

	private _filterAreaGroups(val: string): AreaGroup[] {
		let areaGroups: AreaGroup[] = this._getAreaGroups(this.areaTrees);
		if (val) {
			return areaGroups
				.map((group) => {
					return {
						type: group.type,
						areas: this._filterAreas(group.areas, val)
					};
				})
				.filter((group) => group.areas.length > 0);
		}
		return areaGroups;
	}

	private _filterAreas(areaGroup: Area[], val: string): Area[] {
		return areaGroup.filter((area) => !this._isAreaSelected(area) && !this._isParentAreaSelected(area) && this._replaceAccents(area.name.toLocaleLowerCase()).startsWith(this._replaceAccents(val.toLowerCase())));
	}

	private _isAreaSelected(area: Area): boolean {
		return !!this.selectedAreas.find((selectedArea) => selectedArea.code === area.code);
	}

	private _isParentAreaSelected(childArea: Area): boolean {
		const parentAreas = this._getParentAreas(childArea, this.areaTrees);
		const selectedParentAreas = parentAreas.filter((parentArea) => this._isAreaSelected(parentArea));
		return selectedParentAreas.length > 0;
	}

	private _getParentAreas(childArea: Area, areaTrees: Area[]): Area[] {
		let parentAreas = [];
		if (childArea.parentAreaCode) {
			areaTrees.forEach((areaTree) => {
				if (areaTree.code === childArea.parentAreaCode) {
					parentAreas.push(<Area>{ code: areaTree.code, type: areaTree.type, name: areaTree.name, parentAreaCode: areaTree.parentAreaCode, subAreas: [] });
				} else if (areaTree.subAreas.length > 0) {
					let returnedParentAreas = this._getParentAreas(childArea, areaTree.subAreas);
					returnedParentAreas.forEach((returnedParentArea) => {
						parentAreas.push(returnedParentArea);
					});
					parentAreas.push(areaTree);
				}
			});
		}
		return parentAreas;
	}

	private _selectArea(areaCode: string) {
		// Get selected area from tree
		const selectedAreaSubTree = this._getAreaSubTreeFromTrees(areaCode, this.areaTrees);
		if (!selectedAreaSubTree) {
			console.log('Area with code ' + areaCode + ' cannot be found.');
			return;
		}
		const selectedArea = <Area>{ code: selectedAreaSubTree.code, type: selectedAreaSubTree.type, name: selectedAreaSubTree.name, parentAreaCode: selectedAreaSubTree.parentAreaCode, subAreas: [] };

		// Remove sub areas from selected areas
		this._removeSelectedSubAreas(selectedAreaSubTree);

		// Push selected area
		this.selectedAreas.push(selectedArea);
	}

	private _removeSelectedSubAreas(areaSubTree: Area) {
		let areasForRemove = [];
		const self = this;
		this.selectedAreas.forEach((selectedArea) => {
			if (self._isChildOf(selectedArea, areaSubTree)) areasForRemove.push(selectedArea);
		});
		areasForRemove.forEach((areaForRemove) => {
			const areaIndex = this.selectedAreas.findIndex((selectedArea) => selectedArea.code === areaForRemove.code);
			this.selectedAreas.splice(areaIndex, 1);
		});
	}

	private _isChildOf(childArea: Area, parentAreaSubTree: Area) {
		let status = false;
		parentAreaSubTree.subAreas.forEach((subAreaTree) => {
			if (subAreaTree.code === childArea.code) {
				status = true;
			} else if (subAreaTree.subAreas.length !== 0) {
				const returnedStatus = this._isChildOf(childArea, subAreaTree);
				if (returnedStatus) {
					status = true;
				}
			}
		});
		return status;
	}

	private _getAreaSubTreeFromTrees(areaCode: string, areaTrees: Area[]) {
		let areaSubTree: Area;
		areaTrees.forEach((areaSubTreeFromParentTree) => {
			if (areaSubTreeFromParentTree.code === areaCode) {
				areaSubTree = areaSubTreeFromParentTree;
			} else if (areaSubTreeFromParentTree.subAreas.length !== 0) {
				const returnedAreaSubTree = this._getAreaSubTreeFromTrees(areaCode, areaSubTreeFromParentTree.subAreas);
				if (returnedAreaSubTree) {
					areaSubTree = returnedAreaSubTree;
				}
			}
		});
		return areaSubTree;
	}

	private _getAreaGroups(areaTree: Area[]): AreaGroup[] {
		let areaGroups: Array<AreaGroup> = new Array<AreaGroup>();
		let regionAreaGroup: Array<Area> = [];
		let districAreaGroup: Array<Area> = [];
		let cityAreaGroup: Array<Area> = [];
		let cadastralTerritoryAreaGroup: Array<Area> = [];

		this._getAreaListFromTreesWithoutSubAreas(areaTree).forEach((area) => {
			switch (area.type) {
				case AreaType.REGION:
					regionAreaGroup.push(area);
					break;
				case AreaType.DISTRICT:
					districAreaGroup.push(area);
					break;
				case AreaType.CITY:
					cityAreaGroup.push(area);
					break;
				case AreaType.CADASTRAL_TERRITORY:
					cadastralTerritoryAreaGroup.push(area);
					break;
				default:
					break;
			}
		});

		if (regionAreaGroup.length !== 0) areaGroups.push(<AreaGroup>{ type: AreaType.REGION, areas: regionAreaGroup });
		if (districAreaGroup.length !== 0) areaGroups.push(<AreaGroup>{ type: AreaType.DISTRICT, areas: districAreaGroup });
		if (cityAreaGroup.length !== 0) areaGroups.push(<AreaGroup>{ type: AreaType.CITY, areas: cityAreaGroup });
		if (cadastralTerritoryAreaGroup.length !== 0) areaGroups.push(<AreaGroup>{ type: AreaType.CADASTRAL_TERRITORY, areas: cadastralTerritoryAreaGroup });
		return areaGroups;
	}

	private _getAreaListFromTreesWithoutSubAreas(areaTrees: Area[]): Area[] {
		let areaList: Array<Area> = [];
		areaTrees.forEach((area) => {
			areaList.push(<Area>{ code: area.code, type: area.type, name: area.name, parentAreaCode: area.parentAreaCode, subAreas: [] });
			if (area.subAreas.length !== 0) {
				const subAreaList = this._getAreaListFromTreesWithoutSubAreas(area.subAreas);
				subAreaList.forEach((subArea) => areaList.push(subArea));
			}
		});
		return areaList;
	}

	private _replaceAccents(text: string) {
		return text
			.replace(/á/g, 'a')
			.replace(/č/g, 'c')
			.replace(/ď/g, 'd')
			.replace(/é/g, 'e')
			.replace(/ě/g, 'e')
			.replace(/í/g, 'i')
			.replace(/ó/g, 'o')
			.replace(/ň/g, 'n')
			.replace(/ř/g, 'r')
			.replace(/š/g, 's')
			.replace(/ť/g, 't')
			.replace(/ú/g, 'u')
			.replace(/ů/g, 'u')
			.replace(/ý/g, 'y')
			.replace(/ž/g, 'z');
	}

	private _updateSelectedAreas() {
		this.areaForm.controls['areas'].setValue(this.selectedAreas.map((area) => area.code));
	}
}

export function createAreaFilterFormGroup(areaCodes?: string[]): FormGroup {
	return new FormGroup({
		areas: new FormControl(areaCodes ? areaCodes : [], [validateAreas()])
	});
}

function validateAreas() {
	return (control: FormControl): { [key: string]: any } | null => {
		return control.value.length === 0 ? { invalid: true } : null;
	};
}

export function getFilterAreaCodes(areasForm: FormGroup): string[] {
	return areasForm.get('areas').value;
}
