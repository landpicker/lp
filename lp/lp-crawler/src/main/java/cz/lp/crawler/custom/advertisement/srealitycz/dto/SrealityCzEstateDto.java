package cz.lp.crawler.custom.advertisement.srealitycz.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class SrealityCzEstateDto {

    private final Long hashId;

    @JsonCreator
    public SrealityCzEstateDto(@JsonProperty("hash_id") Long hashId) {
        this.hashId = hashId;
    }
}
