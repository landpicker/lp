package cz.lp.crawler.custom.advertisement.bazoscz;

import cz.lp.CrawlerRunner;
import cz.lp.CrawlerScheduler;
import cz.lp.CrawlerWorkerService;
import cz.lp.StandaloneProcessorScheduler;
import cz.lp.exception.CrawlerClassException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

/**
 * This class run only BazosCzCrawlerRunner {@link BazosCzCrawlerRunner}
 */

@ComponentScan(basePackages = "cz.lp", excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {CrawlerRunner.class, CrawlerScheduler.class, StandaloneProcessorScheduler.class}))
public class BazosCzCrawlerRunner {

    private static final Logger log = LoggerFactory.getLogger(BazosCzCrawlerRunner.class);

    private static final Long ID = 3l;

    public static void main(String[] args) throws CrawlerClassException {

        log.info("Starting Bazos CZ advertisements crawler runner ...");
        ApplicationContext ctx = new AnnotationConfigApplicationContext(BazosCzCrawlerRunner.class);
        CrawlerWorkerService crawlerWorkerService = ctx.getBean(CrawlerWorkerService.class);
        BazosCzCrawler crawler = (BazosCzCrawler) crawlerWorkerService.getCrawler(ID);
        if (crawler.checkCondition()) {
            crawler.crawl();
        } else {
            log.warn("Bazos CZ advertisement crawler does not meet startup conditions");
        }

    }
}
