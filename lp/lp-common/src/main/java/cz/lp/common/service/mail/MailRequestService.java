package cz.lp.common.service.mail;

import cz.lp.common.dto.ad.Ad;
import cz.lp.common.dto.mail.MailRequest;
import cz.lp.common.enumerate.MailRequestStatus;

import java.util.List;
import java.util.Optional;

public interface MailRequestService {

    Long createMailFromAd(Ad ad);

    Long createMailRequest(MailRequest mailRequest);

    /**
     * Gets next mail request which is ready to send and contains subscribers.
     * @return mail request
     */
    Optional<MailRequest> getNextMailRequestReadyToExecute();

    void changeStatus(Long mailRequestId, MailRequestStatus malRequestStatus);

}
