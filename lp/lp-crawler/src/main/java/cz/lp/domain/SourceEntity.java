package cz.lp.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Source entity represents source object in database table and contains information about data source.
 */
@Entity
@Table(name = "source")
public class SourceEntity implements Serializable {

    @Id
    @SequenceGenerator(name = "id", sequenceName = "source_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "provider", nullable = false, length = 100)
    private String provider;

    @Column(name = "website_url", nullable = false)
    private String websiteUrl;

    @OneToMany(mappedBy = "source", fetch = FetchType.LAZY)
    private List<CrawlerEntity> crawlers;

    /**
     * Creates default constructor.
     */
    public SourceEntity() {
    }

    /**
     * Create instance of source entity.
     *
     * @param id         identifier
     * @param provider   provider
     * @param websiteUrl website URL
     * @param crawlers   crawlers
     */
    public SourceEntity(Long id, String provider, String websiteUrl, List<CrawlerEntity> crawlers) {
        this.id = id;
        this.provider = provider;
        this.websiteUrl = websiteUrl;
        this.crawlers = crawlers;
    }

    /**
     * Gets identifier.
     *
     * @return identifier
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets provider.
     *
     * @return provider
     */
    public String getProvider() {
        return provider;
    }

    /**
     * Gets website URL.
     *
     * @return website URL
     */
    public String getWebsiteUrl() {
        return websiteUrl;
    }

    /**
     * Gets crawlers.
     *
     * @return crawlers
     */
    public List<CrawlerEntity> getCrawlers() {
        return crawlers;
    }
}
