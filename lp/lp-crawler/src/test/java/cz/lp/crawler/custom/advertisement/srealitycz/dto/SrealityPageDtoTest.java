package cz.lp.crawler.custom.advertisement.srealitycz.dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import static org.junit.Assert.assertEquals;

public class SrealityPageDtoTest {

    @Test
    public void mapResponse() throws IOException {

        try(InputStream is = getClass().getClassLoader().getResourceAsStream("crawler/srealitycz/page.json")){

            byte[] content = IOUtils.toByteArray(is);
            ObjectMapper mapper = new ObjectMapper();
            SrealityCzPageDto pageDto = mapper.readValue(new String(content, Charset.forName("utf-8")), SrealityCzPageDto.class);
            assertEquals(159, (int)pageDto.getResultSize());
            assertEquals(1, (int)pageDto.getPage());
            assertEquals(10, (int)pageDto.getPerPage());
            assertEquals(10, pageDto.getEmbedded().getEstates().size());
            assertEquals(2428632668l, (long)pageDto.getEmbedded().getEstates().get(0).getHashId());
        }
    }

}