package cz.lp.repository;

import cz.lp.domain.CrawlerEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Crawler repository provides access to crawler table.
 */
public interface CrawlerRepository extends CrudRepository<CrawlerEntity, Long> {

    /**
     * Gets all enabled crawler that be can executing (recrawl_on = NULL OR recrawl_on < NOW)
     *
     * @return crawler entities
     */
    @Query(value = "SELECT * FROM crawler WHERE enabled IS TRUE AND (recrawl_on IS NULL OR recrawl_on < NOW())", nativeQuery = true)
    List<CrawlerEntity> findAllEnabled();
}
