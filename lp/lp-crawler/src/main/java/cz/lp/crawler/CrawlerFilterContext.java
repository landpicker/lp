package cz.lp.crawler;

import com.google.common.collect.Maps;
import cz.lp.crawler.custom.vdparea.filter.VdpAreaCrawlerFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Crawler filter context holds filters for specific crawlers.
 */
@Component
public class CrawlerFilterContext {

    private Map<String, CrawlerFilter> filters = Maps.newHashMap();

    @Autowired()
    public CrawlerFilterContext(VdpAreaCrawlerFilter vdpAreaCrawlerFilter) {

        filters.put("VdpAreaCrawler", vdpAreaCrawlerFilter);
    }

    public CrawlerFilter getFilter(String simpleClassName) {
        return filters.get(simpleClassName);
    }
}
