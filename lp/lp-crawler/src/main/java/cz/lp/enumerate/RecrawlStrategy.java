package cz.lp.enumerate;

/**
 * Recrawl strategy provides enumerate of strategies. These strategies are used in step of
 */
public enum RecrawlStrategy {

        /**
     * RECRAWL_ALL strategy selects and fetches all already fetched URLs and new ones
     */
    RECRAWL_ALL,

    /**
     * RECRAWL_BY_PATTERN selects and fetches all already fetched URLs by REGEX pattern and all not fetched
     */
    RECRAWL_BY_PATTERN,
}
