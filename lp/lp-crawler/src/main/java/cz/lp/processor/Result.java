package cz.lp.processor;

import cz.lp.enumerate.ResultStatus;

public class Result {

    private final Long id;
    private final ResultStatus resultStatus;
    private final String url;
    private final byte[] content;


    public Result(Long id, ResultStatus resultStatus, String url, byte[] content) {
        this.id = id;
        this.resultStatus = resultStatus;
        this.url = url;
        this.content = content;
    }

    public Long getId() {
        return id;
    }

    public ResultStatus getResultStatus() {
        return resultStatus;
    }

    public String getUrl() {
        return url;
    }

    public byte[] getContent() {
        return content;
    }

    public String getContentAsString() {
        return new String(content);
    }
}
