CREATE TABLE public.ad
(
  url text NOT NULL,
  source character varying(100) NOT NULL,
  title text NOT NULL,
  content text NOT NULL,
  published_on timestamp without time zone NOT NULL,
  CONSTRAINT ad_url_pk PRIMARY KEY (url)
);

CREATE TABLE public.ad_image
(
  url text NOT NULL,
  ad_id text NOT NULL,
  file_name character varying(255) NOT NULL,
  content_type character varying(255) NOT NULL,
  data bytea NOT NULL,
  CONSTRAINT ad_image_url_pk PRIMARY KEY (url),
  CONSTRAINT ad_image_ad_id_fk FOREIGN KEY (ad_id)
    REFERENCES public.ad (url) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE public.ad_subscriber
(
  subscriber_email character varying(100) NOT NULL,
  ad_url_pattern text NOT NULL,
  CONSTRAINT ad_subscriber_pk PRIMARY KEY (subscriber_email, ad_url_pattern)
);

CREATE TABLE public.ad_mail_request
(
  ad_url text NOT NULL,
  mail_request_id bigint NOT NULL,
  CONSTRAINT ad_mail_request_pk PRIMARY KEY (ad_url, mail_request_id),
  CONSTRAINT ad_mail_request_ad_url_fk FOREIGN KEY (ad_url)
    REFERENCES public.ad (url) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT ad_mail_request_mail_request_id_fk FOREIGN KEY (mail_request_id)
    REFERENCES public.mail_request (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
);