package cz.lp.processor.custom.areabase.filter;

import com.google.common.collect.Sets;
import cz.lp.processor.ProcessorFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class AreaBaseProcessorFilter extends ProcessorFilter {

    private Set<String> allowedRegionAreaCodes = Sets.newHashSet();
    private Set<String> allowedDistrictAreaCodes = Sets.newHashSet();
    private Set<String> allowedCityAreaCodes = Sets.newHashSet();
    private Set<String> allowedCadastralTerritoryAreaCodes = Sets.newHashSet();

    @Profile("district-sumperk")
    @Bean
    private boolean initDistrictSumperkAreaCodes() {

        return this.allowedRegionAreaCodes.addAll(Sets.newHashSet("124"))
                && this.allowedDistrictAreaCodes.addAll(Sets.newHashSet("3809"))
                && this.allowedCityAreaCodes.addAll(Sets.newHashSet());
    }

    /**
     * Gets information if the processed region area is allowed in case that the filter was defined.
     *
     * @param regionAreaCode processed district area code
     * @return true if the region area is allowed, otherwise false
     */
    public boolean isRegionAreaAllowed(String regionAreaCode) {
        if (this.allowedRegionAreaCodes.isEmpty()) return true;
        return this.allowedRegionAreaCodes.contains(regionAreaCode);
    }

    /**
     * Gets information if the processed district area is allowed in case that the filter was defined.
     *
     * @param districtAreaCode processed district area code
     * @return true if the district area is allowed, otherwise false
     */
    public boolean isDistrictAreaAllowed(String districtAreaCode) {
        if (this.allowedDistrictAreaCodes.isEmpty()) return true;
        return this.allowedDistrictAreaCodes.contains(districtAreaCode);
    }

    /**
     * Gets information if the processed city area is allowed in case that the filter was defined.
     *
     * @param cityAreaCode processed city area code
     * @return true if the city area is allowed, otherwise false
     */
    public boolean isCityAreaAllowed(String cityAreaCode) {
        if (this.allowedCityAreaCodes.isEmpty()) return true;
        return this.allowedCityAreaCodes.contains(cityAreaCode);
    }

    /**
     * Gets information if the processed cadastral territory area is allowed in case that the filter was defined.
     *
     * @param cadastralTerritoryAreaCode processed cadastral territory area code
     * @return true if the cadastral territory area is allowed, otherwise false
     */
    public boolean isCadastralTerritoryAreaAllowed(String cadastralTerritoryAreaCode) {
        if (this.allowedCadastralTerritoryAreaCodes.isEmpty()) return true;
        return this.allowedCadastralTerritoryAreaCodes.contains(cadastralTerritoryAreaCode);
    }
}
