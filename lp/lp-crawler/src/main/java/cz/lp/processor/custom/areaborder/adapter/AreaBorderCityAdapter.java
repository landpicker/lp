package cz.lp.processor.custom.areaborder.adapter;

import cz.lp.common.dto.Epsg5514Coordinate;
import cz.lp.processor.custom.areaborder.AreaBorder;
import cz.lp.util.CoordinateUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;

import java.util.List;
import java.util.Optional;

public class AreaBorderCityAdapter {

    private final static String CITY_CODE_SELECTOR = "vf|Obce obi|Kod";
    private final static String BORDER_POLYGON_SELECTOR = "vf|Obce obi|Geometrie obi|OriginalniHranice gml|posList";

    private AreaBorder cityAreaBorder;

    public AreaBorderCityAdapter(String content, String url) {

        Document document = Jsoup.parse(content, url, Parser.xmlParser());
        extractCityBorder(document);
    }

    private void extractCityBorder(Document document) {

        Element cityCodeElement = document.selectFirst(CITY_CODE_SELECTOR);
        if (cityCodeElement == null) return;
        String areaCode = cityCodeElement.text();

        Element posListElement = document.selectFirst(BORDER_POLYGON_SELECTOR);
        if (posListElement == null) return;
        List<Epsg5514Coordinate> epsg5514Coordinates = CoordinateUtil.extractEpsg5514Coordinates(posListElement.text());
        if(epsg5514Coordinates.isEmpty()) return;
        this.cityAreaBorder = new AreaBorder(areaCode, epsg5514Coordinates);
    }

    /**
     * Gets city area border.
     *
     * @return city area border
     */
    public Optional<AreaBorder> getCityAreaBorder() {

        return Optional.ofNullable(cityAreaBorder);
    }
}
