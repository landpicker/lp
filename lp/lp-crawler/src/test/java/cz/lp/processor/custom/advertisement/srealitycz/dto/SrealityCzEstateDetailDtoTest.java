package cz.lp.processor.custom.advertisement.srealitycz.dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SrealityCzEstateDetailDtoTest {

    @Test
    public void mapResponse() throws IOException {

        try (InputStream is = getClass().getClassLoader().getResourceAsStream("processor/srealitycz/ad.json")) {

            byte[] content = IOUtils.toByteArray(is);
            ObjectMapper mapper = new ObjectMapper();
            SrealityCzEstateDetailDto detailDto = mapper.readValue(new String(content, Charset.forName("utf-8")), SrealityCzEstateDetailDto.class);
            assertEquals("Dražba  louky 26 733 m²", detailDto.getName().getValue());
            assertTrue(detailDto.getText().getValue().startsWith("Jedná se o nemovitosti"));
            assertEquals("Bušín, okres Šumperk", detailDto.getLocality().getValue());
            assertEquals(3, detailDto.getEmbedded().getImages().size());
            assertEquals(251568639, (long) detailDto.getEmbedded().getImages().get(0).getId());
            assertEquals("https://d18-a.sdn.szn.cz/d_18/c_img_H_BH/7u1Vvk.jpeg?fl=res,749,562,3|shr,,20|jpg,90", detailDto.getEmbedded().getImages().get(0).getLinks().getView().getHref());
        }
    }

}