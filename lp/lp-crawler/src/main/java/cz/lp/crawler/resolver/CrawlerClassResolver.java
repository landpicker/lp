package cz.lp.crawler.resolver;

import cz.lp.CrawlerWorkerService;
import cz.lp.crawler.Crawler;
import cz.lp.crawler.CrawlerFilter;
import cz.lp.crawler.CrawlerFilterContext;
import cz.lp.crawler.SeedUrl;
import cz.lp.crawler.service.CrawlerService;
import cz.lp.domain.CrawlerEntity;
import cz.lp.enumerate.CrawlerCondition;
import cz.lp.enumerate.RecrawlStrategy;
import cz.lp.exception.CrawlerClassException;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDateTime;
import java.util.Set;

import static java.util.Objects.isNull;

/**
 * Crawler class resolver provides method for transforming crawler entity to custom crawler class by reflection.
 */
public class CrawlerClassResolver {

    private CrawlerClassResolver() {
    }

    /**
     * Creates instance of crawler from crawler entity by class name (reflections)
     *
     * @param crawlerEntity crawler entity
     * @return crawler
     * @throws CrawlerClassException when something during creating instance of crawler by reflection failed
     */
    public static Crawler createCrawlerInstance(CrawlerEntity crawlerEntity, CrawlerService crawlerService, CrawlerConditionResolver crawlerConditionResolver, CrawlerFilterContext crawlerFilterContext) throws CrawlerClassException {

        try {
            Class<?> type = Class.forName(crawlerEntity.getCrawlerClass());
            Constructor<?> constructor = type.getConstructor(String.class, Long.class, CrawlerCondition.class, Integer.class, RecrawlStrategy.class, String.class, Long.class, LocalDateTime.class, Set.class, CrawlerService.class, CrawlerConditionResolver.class, CrawlerFilter.class);
            Set<SeedUrl> seedUrls = CrawlerWorkerService.convertCrawlerSeedUrlEntitiesToSeedUrls(crawlerEntity.getCrawlerSeedUrls());
            return (Crawler) constructor.newInstance(type.getSimpleName(), crawlerEntity.getId(), crawlerEntity.getCondition(), crawlerEntity.getDelay(), crawlerEntity.getRecrawlStrategy(), crawlerEntity.getRecrawlUrlPattern(), crawlerEntity.getRecrawlInterval(), (!isNull(crawlerEntity.getRecrawlOn())) ? crawlerEntity.getRecrawlOn().toLocalDateTime() : null, seedUrls, crawlerService, crawlerConditionResolver, crawlerFilterContext.getFilter(type.getSimpleName()));
        } catch (ClassNotFoundException ex) {
            throw new CrawlerClassException(String.format("Unable to find crawler class: %s", crawlerEntity.getCrawlerClass()), ex);
        } catch (NoSuchMethodException ex) {
            throw new CrawlerClassException(String.format("Unable to find constructor with defined arguments for class: %s", crawlerEntity.getCrawlerClass()), ex);
        } catch (IllegalAccessException ex) {
            throw new CrawlerClassException(String.format("Invalid arguments for creating class: %s", crawlerEntity.getCrawlerClass()), ex);
        } catch (InstantiationException ex) {
            throw new CrawlerClassException(String.format("Unable to create instance of constructor of abstract class for class: %s", crawlerEntity.getCrawlerClass()), ex);
        } catch (InvocationTargetException ex) {
            throw new CrawlerClassException(String.format("Constructor exception for class: %s", crawlerEntity.getCrawlerClass()), ex);
        }
    }
}
