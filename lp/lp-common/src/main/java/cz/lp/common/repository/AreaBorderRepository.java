package cz.lp.common.repository;

import cz.lp.common.domain.AreaBorderEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 * Area border repository provides methods for access to area border table.
 */
public interface AreaBorderRepository extends CrudRepository<AreaBorderEntity, Long> {

    @Query(value = "SELECT * FROM area_border WHERE area_code = :area_code", nativeQuery = true)
    AreaBorderEntity findOneByAreaCode(@Param("area_code") String areaCode);
}
