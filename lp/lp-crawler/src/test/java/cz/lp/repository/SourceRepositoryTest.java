package cz.lp.repository;

import com.google.common.collect.Lists;
import cz.lp.domain.CrawlerEntity;
import cz.lp.domain.SourceEntity;
import cz.lp.enumerate.CrawlerCondition;
import cz.lp.enumerate.RecrawlStrategy;
import cz.lp.repository.configuration.RepositoryTemplate;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import java.util.List;

public class SourceRepositoryTest extends RepositoryTemplate {

    @Autowired
    SourceRepository sourceRepository;

    private static final Long ID = 1l;
    private static final String PROVIDER = "CUZK";
    private static final String WEBSITE_URL = "http://cuzk.cz";
    private static final List<CrawlerEntity> CRAWLER_ENTITIES = Lists.newArrayList();

    @Test
    public void insert() {

        SourceEntity sourceEntity = new SourceEntity(ID, PROVIDER, WEBSITE_URL, CRAWLER_ENTITIES);
        Long id = sourceRepository.save(sourceEntity).getId();
        SourceEntity savedSourceEntity = sourceRepository.findOne(ID);
        Assert.assertNotNull(savedSourceEntity);
        Assert.assertEquals(id, savedSourceEntity.getId());
        Assert.assertEquals(PROVIDER, savedSourceEntity.getProvider());
        Assert.assertEquals(WEBSITE_URL, savedSourceEntity.getWebsiteUrl());
        Assert.assertEquals(CRAWLER_ENTITIES, savedSourceEntity.getCrawlers());
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void findOne() {

        SourceEntity sourceEntity = sourceRepository.findOne(ID);
        Assert.assertNotNull(sourceEntity);
        Assert.assertEquals(ID, sourceEntity.getId());
        Assert.assertEquals(PROVIDER, sourceEntity.getProvider());
        Assert.assertEquals(WEBSITE_URL, sourceEntity.getWebsiteUrl());
        Assert.assertEquals(CRAWLER_ENTITIES, sourceEntity.getCrawlers());
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler/INSERT_crawler_WITH_id_=_1_AND_recrawl_on_=_NULL_AND_ENABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void findOne_withCrawlers() {

        SourceEntity sourceEntity = sourceRepository.findOne(ID);
        Assert.assertNotNull(sourceEntity);
        Assert.assertEquals(ID, sourceEntity.getId());
        Assert.assertEquals(PROVIDER, sourceEntity.getProvider());
        Assert.assertEquals(WEBSITE_URL, sourceEntity.getWebsiteUrl());
        Assert.assertEquals(1, sourceEntity.getCrawlers().size());

        CrawlerEntity crawlerEntity = sourceEntity.getCrawlers().get(0);
        Assert.assertNotNull(crawlerEntity);
        Assert.assertEquals(1l, crawlerEntity.getId().longValue());
        Assert.assertEquals("CrawlerClass 1", crawlerEntity.getCrawlerClass());
        Assert.assertEquals(CrawlerCondition.EXIST_FIRST_LEVEL_AREA, crawlerEntity.getCondition());
        Assert.assertEquals(1, crawlerEntity.getDelay().intValue());
        Assert.assertEquals(true, crawlerEntity.isEnabled());
        Assert.assertEquals(RecrawlStrategy.RECRAWL_ALL, crawlerEntity.getRecrawlStrategy());
        Assert.assertEquals(1000, crawlerEntity.getRecrawlInterval().longValue());
        Assert.assertEquals(null, crawlerEntity.getRecrawlOn());
        Assert.assertEquals(null, crawlerEntity.getLastRecrawledOn());
        Assert.assertEquals(Lists.newArrayList(), crawlerEntity.getCrawlerSeedUrls());
        Assert.assertEquals(Lists.newArrayList(), crawlerEntity.getCrawlerResults());
        Assert.assertEquals(Lists.newArrayList(), crawlerEntity.getProcessors());
    }
}