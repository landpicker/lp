import { Injectable } from '@angular/core';
import { MatPaginatorIntl } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
	providedIn: 'root'
})
export class LandSearchTablePaginationService extends MatPaginatorIntl {
	ofLabel: string;

	constructor(private _translateService: TranslateService) {
		super();

		this._translateService
			.get([
				'land-search.result.table.pagination.items-per-page',
				'land-search.result.table.pagination.first-page',
				'land-search.result.table.pagination.next-page',
				'land-search.result.table.pagination.last-page',
				'land-search.result.table.pagination.previous-page',
				'land-search.result.table.pagination.of'
			])
			.subscribe((res: string[]) => {
				this.itemsPerPageLabel = res['land-search.result.table.pagination.items-per-page'];
				this.firstPageLabel = res['land-search.result.table.pagination.first-page'];
				this.nextPageLabel = res['land-search.result.table.pagination.next-page'];
				this.lastPageLabel = res['land-search.result.table.pagination.last-page'];
				this.previousPageLabel = res['land-search.result.table.pagination.previous-page'];
				this.ofLabel = res['land-search.result.table.pagination.of'];
			});
	}

	getRangeLabel = (page: number, pageSize: number, length: number) => {
		if (length === 0 || pageSize === 0) {
			return `0 ${this.ofLabel} ${length}`;
		}
		length = Math.max(length, 0);
		const startIndex = page * pageSize;
		const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
		return `${startIndex + 1} - ${endIndex} ${this.ofLabel}  ${length}`;
	};
}
