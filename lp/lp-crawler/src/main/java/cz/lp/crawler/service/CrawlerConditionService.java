package cz.lp.crawler.service;

import cz.lp.common.repository.AreaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CrawlerConditionService {

    private final AreaRepository areaRepository;

    @Autowired
    public CrawlerConditionService(AreaRepository areaRepository) {
        this.areaRepository = areaRepository;
    }

    /**
     * Gets information if first level areas are existing.
     *
     * @return true when first level areas are existing, otherwise false
     */
    public boolean existFirstLevelAreas() {

        return areaRepository.existFirstLevelAreas();
    }

    /**
     * Gets information if city areas are existing.
     *
     * @return true when fcity areas are existing, otherwise false
     */
    public boolean existCityAreas() {

        return areaRepository.existCityAreas();
    }

}
