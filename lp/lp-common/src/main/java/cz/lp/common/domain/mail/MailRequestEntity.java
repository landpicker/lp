package cz.lp.common.domain.mail;

import cz.lp.common.enumerate.MailRequestStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

/**
 * Mail request represents description of email like subject, content, recipients to execute.
 */

@Entity
@Table(name = "mail_request")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MailRequestEntity {

    @Id
    @SequenceGenerator(name = "id", sequenceName = "mail_request_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false, length = 100)
    private MailRequestStatus status;

    @Column(name = "subject")
    private String subject;

    @Column(name = "content")
    private String content;

    @Type(type = "JsonSet", parameters = {
            @org.hibernate.annotations.Parameter(name = "type", value = "java.lang.String")
    })
    @Column(name = "subscribers")
    private Set<String> subscribers;

    @Column(name = "processed_on")
    private Timestamp processedOn;

    @Column(name = "created_on")
    private Timestamp createdOn;

    @OneToMany(mappedBy = "request", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<MailRequestAttachmentEntity> attachments;
}
