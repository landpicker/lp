package cz.lp.crawler.custom.vdparea.adapter;

import cz.lp.crawler.custom.vdparea.filter.VdpAreaCrawlerFilter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

/**
 * Vdp district area document adapter provides method for converting content to links.
 */
public class VdpDistrictAreaDocumentLinkAdapter {

    private VdpAreaCrawlerFilter filter;

    private static final String CITY_URL = "http://vdp.cuzk.cz/vdp/ruian/obce/export?vc.kod=&op.kod=&ok.kod=%s&pu.kod=&ob.nazev=&ob.statusKod=&ob.kod=&ohrada.id=&obg.sort=KOD&export=XML";
    private static final String DISTRICT_ITEM_ELEMENT_SELECTOR = "Okres";
    private static final String DISTRICT_CODE_ELEMENT_SELECTOR = "Kod";
    private LinkedHashSet<String> links;

    public VdpDistrictAreaDocumentLinkAdapter(String content, String url, VdpAreaCrawlerFilter filter) {

        this.filter = filter;
        Document document = Jsoup.parse(content, url, Parser.xmlParser());
        extractLinks(document);
    }

    /**
     * Extracts links.
     *
     * @param document document
     */
    private void extractLinks(Document document) {

        links = document.select(DISTRICT_ITEM_ELEMENT_SELECTOR).stream()
                .map(element -> element.select(DISTRICT_CODE_ELEMENT_SELECTOR).first())
                .filter(Objects::nonNull)
                .map(element -> String.format(CITY_URL, element.text()))
                .filter(link -> nonNull(filter) ? filter.isLinkAllowed(link) : true)
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    /**
     * Gets links.
     *
     * @return links
     */
    public Optional<Set<String>> getLinks() {

        if (Objects.isNull(links) || links.isEmpty()) {
            Optional.empty();
        }

        return Optional.of(links);
    }
}
