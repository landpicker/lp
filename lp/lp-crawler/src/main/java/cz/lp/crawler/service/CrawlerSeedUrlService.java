package cz.lp.crawler.service;

import com.google.common.collect.Sets;
import cz.lp.common.domain.AreaEntity;
import cz.lp.common.enumerate.AreaType;
import cz.lp.common.repository.AreaRepository;
import cz.lp.crawler.SeedUrl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CrawlerSeedUrlService {

    private final AreaRepository areaRepository;

    @Autowired
    public CrawlerSeedUrlService(AreaRepository areaRepository) {
        this.areaRepository = areaRepository;
    }

    /**
     * Gets URLs with input data by condition.
     *
     * @param crawlerSeedUrl crawler seed URL
     * @return URLs with input data
     */
    public Set<String> getUrlsWithInputDataByCondition(SeedUrl crawlerSeedUrl) {

        Set<String> urlsWithInputData = Sets.newHashSet();
        switch (crawlerSeedUrl.getCondition()) {

            case PUT_AREA_CITY_CODE:
                urlsWithInputData.addAll(getUrlsWithAreaCityCode(crawlerSeedUrl.getUrl()));
                break;
            default:
                urlsWithInputData.add(crawlerSeedUrl.getUrl());
                break;
        }

        return urlsWithInputData;
    }

    /**
     * Gets URLs with putted area city code.
     *
     * @param url URL
     * @return URL with area city code
     */
    private Set<String> getUrlsWithAreaCityCode(String url) {

        List<AreaEntity> areaEntities = areaRepository.getAllAreasByType(AreaType.CITY.name());
        return areaEntities.stream().map(areaEntity -> String.format(url, areaEntity.getCode())).collect(Collectors.toSet());
    }
}
