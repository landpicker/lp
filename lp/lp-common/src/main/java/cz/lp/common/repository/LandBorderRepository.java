package cz.lp.common.repository;

import cz.lp.common.domain.LandBorderEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 * Land border repository provides methods for access to land border table.
 */
public interface LandBorderRepository extends CrudRepository<LandBorderEntity, Long> {

    @Query(value = "SELECT * FROM land_border WHERE land_id = :land_id", nativeQuery = true)
    LandBorderEntity findOneByLandId(@Param("land_id") String landId);
}
