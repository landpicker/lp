package cz.lp.common.domain.mail;

import cz.lp.common.domain.ad.AdEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Mail request attachment represents attachment of mail request.
 */

@Entity
@Table(name = "mail_request_attachment")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MailRequestAttachmentEntity {

    @Id
    @SequenceGenerator(name = "id", sequenceName = "mail_request_attachment_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "file_name", nullable = false)
    private String fileName;

    @Column(name = "content_type", nullable = false)
    private String contentType;

    @Column(name = "data", nullable = false)
    private byte[] data;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mail_request_id")
    private MailRequestEntity request;
}
