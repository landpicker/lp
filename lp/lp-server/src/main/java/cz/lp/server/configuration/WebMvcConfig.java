package cz.lp.server.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ResourceProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.http.CacheControl;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.resource.PathResourceResolver;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Configuration
@EnableConfigurationProperties({ResourceProperties.class})
public class WebMvcConfig implements WebMvcConfigurer {

    @Autowired
    private ResourceProperties resourceProperties = new ResourceProperties();

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        Integer cachePeriod = resourceProperties.getCachePeriod();

        final String[] staticLocations = resourceProperties.getStaticLocations();
        final String[] indexLocations = new String[staticLocations.length];
        for (int i = 0; i < staticLocations.length; i++) {
            indexLocations[i] = staticLocations[i] + "index.html";
        }

        registry.addResourceHandler(
                "/**/*.css",
                "/**/*.js",
                "/**/Roboto*.ttf",
                "/**/Roboto*.eot",
                "/**/Roboto*.woff",
                "/**/Roboto*.woff2",
                "/**/cl-icons*.svg",
                "/**/cl-icons*.eot",
                "/**/cl-icons*.woff",
                "/**/cl-icons*.woff2")
                .addResourceLocations(staticLocations)
                .setCacheControl(CacheControl.maxAge(cachePeriod, TimeUnit.SECONDS));

        registry.addResourceHandler(
                "/**/*.html",
                "/**/*.json",
                "/**/*.bmp",
                "/**/*.jpeg",
                "/**/*.jpg",
                "/**/*.png",
                "/**/*.ttf",
                "/**/*.svg",
                "/**/*.pdf")
                .addResourceLocations(staticLocations)
                .setCacheControl(CacheControl.maxAge(60, TimeUnit.SECONDS));

        registry.addResourceHandler("/**")
                .addResourceLocations(indexLocations)
                .resourceChain(true)
                .addResolver(new PathResourceResolver() {
                    @Override
                    protected Resource getResource(String resourcePath, Resource location) throws IOException {
                        return location.exists() && location.isReadable() ? location : null;
                    }
                });

        registry.addResourceHandler("/api")
                .addResourceLocations("classpath:/static/docs/api-guide.html")
                .resourceChain(true)
                .addResolver(new PathResourceResolver() {
                    @Override
                    protected Resource getResource(String resourcePath, Resource location) throws IOException {
                        return location.exists() && location.isReadable() ? location : null;
                    }
                });
    }
}
