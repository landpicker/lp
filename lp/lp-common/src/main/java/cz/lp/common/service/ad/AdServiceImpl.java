package cz.lp.common.service.ad;

import cz.lp.common.domain.ad.AdEntity;
import cz.lp.common.domain.ad.AdMailRequestEntity;
import cz.lp.common.dto.ad.Ad;
import cz.lp.common.repository.AdMailRequestRepository;
import cz.lp.common.repository.AdRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

@Service
@Transactional
public class AdServiceImpl implements AdService {

    private final AdRepository adRepository;
    private final AdMailRequestRepository adMailRequestRepository;

    public AdServiceImpl(AdRepository adRepository,
                         AdMailRequestRepository adMailRequestRepository) {
        this.adRepository = adRepository;
        this.adMailRequestRepository = adMailRequestRepository;
    }


    @Override
    public Optional<Ad> getNextAdReadyToCreateMailRequest() {
        AdEntity adEntity = adRepository.findOneReadyToCreateMailRequest();
        if(isNull(adEntity)){
            return Optional.empty();
        }
        return Optional.of(Ad.from(adEntity));
    }

    @Override
    public void markAdWithCreatedMailRequest(String adUrl, Long mailRequestId) {

        AdMailRequestEntity mailRequestEntity = new AdMailRequestEntity();
        AdMailRequestEntity.AdMailRequestId adMailRequestId = new AdMailRequestEntity.AdMailRequestId(adUrl, mailRequestId);
        mailRequestEntity.setAdMailRequestId(adMailRequestId);
        adMailRequestRepository.save(mailRequestEntity);
    }
}
