import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
	selector: 'lp-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {

	constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, translate: TranslateService) {
		iconRegistry.addSvgIcon('filter', sanitizer.bypassSecurityTrustResourceUrl('assets/icon/icon_filter.svg'));
		iconRegistry.addSvgIcon('map', sanitizer.bypassSecurityTrustResourceUrl('assets/icon/icon_map.svg'));
    iconRegistry.addSvgIcon('share', sanitizer.bypassSecurityTrustResourceUrl('assets/icon/icon_share.svg'));
    iconRegistry.addSvgIcon('left-chevron', sanitizer.bypassSecurityTrustResourceUrl('assets/icon/icon_chevron_left.svg'));
		iconRegistry.addSvgIcon('right-chevron', sanitizer.bypassSecurityTrustResourceUrl('assets/icon/icon_chevron_right.svg'));
    translate.setDefaultLang('cs');
  }
}
