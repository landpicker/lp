package cz.lp.common.dto.mail;

import cz.lp.common.domain.mail.MailRequestAttachmentEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class MailRequestAttachment {

    private final String fileName;
    private final String contentType;
    private final byte[] data;

    public static MailRequestAttachment from(MailRequestAttachmentEntity mailRequestAttachmentEntity) {

        return new MailRequestAttachment(
                mailRequestAttachmentEntity.getFileName(),
                mailRequestAttachmentEntity.getContentType(),
                mailRequestAttachmentEntity.getData()
        );
    }
}
