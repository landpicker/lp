package cz.lp.repository;

import cz.lp.domain.ProcessorEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProcessorRepository extends CrudRepository<ProcessorEntity, Long> {

    /**
     * Gets all processors by crawler identifier.
     *
     * @return processor entities
     */
    @Query(value = "SELECT * FROM processor WHERE crawler_id = :crawler_id", nativeQuery = true)
    List<ProcessorEntity> findAllByCrawlerId(@Param("crawler_id") Long crawlerId);
}
