package cz.lp.crawler.service;

import cz.lp.crawler.SeedUrl;
import cz.lp.crawler.context.ResultContext;
import cz.lp.domain.CrawlerEntity;
import cz.lp.domain.CrawlerResultEntity;
import cz.lp.enumerate.ResultStatus;
import cz.lp.repository.CrawlerRepository;
import cz.lp.repository.CrawlerResultRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;

import static java.util.Objects.isNull;

@Service
public class CrawlerService {

    private final CrawlerRepository crawlerRepository;
    private final CrawlerResultRepository crawlerResultRepository;
    private final CrawlerSeedUrlService crawlerSeedUrlService;

    @Autowired
    public CrawlerService(CrawlerRepository crawlerRepository,
                          CrawlerResultRepository crawlerResultRepository,
                          CrawlerSeedUrlService crawlerSeedUrlService) {
        this.crawlerRepository = crawlerRepository;
        this.crawlerResultRepository = crawlerResultRepository;
        this.crawlerSeedUrlService = crawlerSeedUrlService;
    }

    /**
     * Creates NOT_FETCHED result from seed URLs.
     *
     * @param crawlerId       crawler identifier
     * @param crawlerSeedUrls crawler seed URLs
     */
    @Transactional
    public void createNotFetchedResultFromSeedUrls(Long crawlerId, Set<SeedUrl> crawlerSeedUrls) {

        CrawlerEntity crawlerEntity = crawlerRepository.findOne(crawlerId);
        crawlerSeedUrls.stream()
                .filter(SeedUrl::isEnabled)
                .forEach(crawlerSeedUrl -> {

                    Set<String> urls = crawlerSeedUrlService.getUrlsWithInputDataByCondition(crawlerSeedUrl);
                    urls.forEach(url -> {

                        if (!crawlerResultRepository.existByUrl(url)) {
                            CrawlerResultEntity crawlerResultEntity = new CrawlerResultEntity(
                                    crawlerEntity,
                                    ResultStatus.NOT_FETCHED,
                                    url,
                                    0,
                                    Timestamp.valueOf(LocalDateTime.now()));
                            crawlerResultRepository.save(crawlerResultEntity);
                        }
                    });

                });
    }

    /**
     * Gets following result by RECRAWL_ALL strategy.
     *
     * @param crawlerId          crawler identifier
     * @param newLastRecrawledOn new last recrawled on
     * @return result context
     */
    public Optional<ResultContext> getFollowingResultByRecrawlAllStrategy(Long crawlerId, LocalDateTime newLastRecrawledOn) {

        CrawlerResultEntity resultEntity = crawlerResultRepository.findOneByCrawlerIdAndLastRecrawledOnOrderByProcessingOrder(crawlerId, Timestamp.valueOf(newLastRecrawledOn));
        if (isNull(resultEntity)) {
            return Optional.empty();
        }
        return Optional.of(ResultContext.of(resultEntity, newLastRecrawledOn));
    }

    /**
     * Gets following result by ONLY_ONCE strategy.
     *
     * @param crawlerId          crawler identifier
     * @param recrawlUrlPattern  recrawl URL pattern
     * @param newLastRecrawledOn new last recrawled on
     * @return result context
     */
    public Optional<ResultContext> getFollowingResultByRecrawlByUrlPatternStrategy(Long crawlerId, String recrawlUrlPattern, LocalDateTime newLastRecrawledOn) {

        CrawlerResultEntity resultEntity = crawlerResultRepository.findOneByCrawlerIdByRecrawlUrlPatternAndLastRecrawledOnOrNotFetchedOrderByProcessingOrder(crawlerId, recrawlUrlPattern, Timestamp.valueOf(newLastRecrawledOn));
        if (isNull(resultEntity)) {
            return Optional.empty();
        }
        return Optional.of(ResultContext.of(resultEntity, newLastRecrawledOn));
    }

    /**
     * Saves result.
     *
     * @param context result context
     */
    @Transactional
    public void saveResult(ResultContext context) {

        CrawlerResultEntity resultEntity = crawlerResultRepository.findOne(context.getResultId());
        resultEntity.setStatus(context.getStatus());

        if (isNull(resultEntity.getLastModifiedOn()) || !StringUtils.equals(resultEntity.getHash(), context.getHash())) {
            resultEntity.setContentType(context.getContentType());
            resultEntity.setContent(context.getContent());
            resultEntity.setHash(context.getHash());
            resultEntity.setLastModifiedOn(Timestamp.valueOf(context.getLastCrawledOn()));
        }

        resultEntity.setMessage(context.getMessage());
        resultEntity.setLastCrawledOn(Timestamp.valueOf(context.getLastCrawledOn()));
        crawlerResultRepository.save(resultEntity);
    }

    /**
     * Save extracted links as NOT FETCHED results.
     *
     * @param context   result context
     * @param crawlerId crawler identifier
     * @param createdOn created on
     */
    @Transactional
    public void saveExtractedLinks(ResultContext context, Long crawlerId, LocalDateTime createdOn) {

        if (!isNull(context.getExtractedLinks())) {

            CrawlerEntity crawlerEntity = crawlerRepository.findOne(crawlerId);
            context.getExtractedLinks().stream()
                    .filter(extractedLink -> !crawlerResultRepository.existByUrl(extractedLink))
                    .forEach(extractedLink -> {
                        CrawlerResultEntity resultEntity = new CrawlerResultEntity(crawlerEntity, ResultStatus.NOT_FETCHED, extractedLink, context.getProcessingOrder() + 1, Timestamp.valueOf(createdOn));
                        crawlerResultRepository.save(resultEntity);
                    });
        }
    }

    /**
     * Updated crawler after success finish.
     *
     * @param crawlerId       crawler identifier
     * @param recrawlOn       recrawl on
     * @param lastRecrawledOn last recrawled on
     */
    public void updateCrawlerAfterSuccessFinish(Long crawlerId, LocalDateTime recrawlOn, LocalDateTime lastRecrawledOn) {

        CrawlerEntity crawlerEntity = crawlerRepository.findOne(crawlerId);
        crawlerEntity.setEnabled(true);
        crawlerEntity.setRecrawlOn(Timestamp.valueOf(recrawlOn));
        crawlerEntity.setLastRecrawledOn(Timestamp.valueOf(lastRecrawledOn));
        crawlerRepository.save(crawlerEntity);
    }

    /**
     * Updated crawler after failed.
     *
     * @param crawlerId       crawler identifier
     * @param lastRecrawledOn last recrawled on
     */
    public void updateCrawlerAfterFailed(Long crawlerId, LocalDateTime lastRecrawledOn) {

        CrawlerEntity crawlerEntity = crawlerRepository.findOne(crawlerId);
        crawlerEntity.setEnabled(false);
        crawlerEntity.setRecrawlOn(null);
        crawlerEntity.setLastRecrawledOn(Timestamp.valueOf(lastRecrawledOn));
        crawlerRepository.save(crawlerEntity);
    }
}
