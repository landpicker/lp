package cz.lp.crawler.custom.vdparea.filter;

import com.google.common.collect.Lists;
import cz.lp.crawler.CrawlerFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
public class VdpAreaCrawlerFilter extends CrawlerFilter {

    @Profile("district-sumperk")
    @Bean
    private boolean initDistrictSumperkLinkRegexPatterns() {

        String olomoucRegionRegexPattern = "http://vdp\\.cuzk\\.cz/vdp/ruian/okresy/export\\?vc\\.kod=(124)&kr\\.kod=&ok\\.nazev=&ok\\.kod=&ohrada\\.id=&okg\\.sort=KOD&export=XML";
        String sumperkDistrictRegexPattern = "http://vdp\\.cuzk\\.cz/vdp/ruian/obce/export\\?vc\\.kod=&op\\.kod=&ok\\.kod=(3809)&pu\\.kod=&ob\\.nazev=&ob\\.statusKod=&ob\\.kod=&ohrada\\.id=&obg\\.sort=KOD&export=XML";
        String allCitiesRegexPattern = "http://vdp\\.cuzk\\.cz/vdp/ruian/katastralniuzemi/export\\?ob\\.kod=\\d+&ku\\.nazev=&ku\\.kod=&ohrada\\.id=&kug\\.sort=KOD&export=XML";
        return this.allowedLinkRegexPatterns.addAll(Lists.newArrayList(olomoucRegionRegexPattern, sumperkDistrictRegexPattern, allCitiesRegexPattern));
    }
}
