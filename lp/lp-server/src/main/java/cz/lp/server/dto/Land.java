package cz.lp.server.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.base.MoreObjects;
import com.google.common.collect.Lists;
import cz.lp.common.domain.LandEntity;
import cz.lp.common.dto.Epsg5514Coordinate;
import cz.lp.common.enumerate.LandType;

import java.util.List;

/**
 * Land data transfer object represents land.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Land {

    private final String id;
    private final String cadastralTerritoryArea;
    private final String cityArea;
    private final LandType type;
    private final Integer trunkNumber;
    private final Integer underDivisionNumber;
    private final Integer acreage;
    private final Epsg5514Coordinate definitionPointCoordinate;
    private final List<Epsg5514Coordinate> borderCoordinates;

    /**
     * Creates instance of land data transfer object.
     *
     * @param id                        unique land identifier
     * @param cadastralTerritoryArea    cadastral territory area
     * @param cityArea                  city area
     * @param type                      land type
     * @param trunkNumber               trunk number
     * @param underDivisionNumber       under division number
     * @param acreage                   acreage
     * @param definitionPointCoordinate definition point coordinate
     * @param borderCoordinates         border coordinates
     */
    public Land(String id, String cadastralTerritoryArea, String cityArea, LandType type, Integer trunkNumber, Integer underDivisionNumber, Integer acreage, Epsg5514Coordinate definitionPointCoordinate, List<Epsg5514Coordinate> borderCoordinates) {
        this.id = id;
        this.cadastralTerritoryArea = cadastralTerritoryArea;
        this.cityArea = cityArea;
        this.type = type;
        this.trunkNumber = trunkNumber;
        this.underDivisionNumber = underDivisionNumber;
        this.acreage = acreage;
        this.definitionPointCoordinate = definitionPointCoordinate;
        this.borderCoordinates = borderCoordinates;
    }

    /**
     * Creates instance from land entity
     *
     * @param landEntity land entity
     */
    public static Land assembleOfEntity(LandEntity landEntity) {
        return new Land(landEntity.getId(), landEntity.getArea().getName(), landEntity.getArea().getParentArea().getName(), landEntity.getType(), landEntity.getTrunkNumber(), landEntity.getUnderDivisionNumber(), landEntity.getAcreage(), landEntity.getDefinitionPointEpsg5514(), landEntity.getLandBorder().getEpsg5514Coordinates());
    }

    /**
     * Creates instance from land entity
     *
     * @param landEntity land entity
     */
    public static Land assembleOfEntityWithoutBorder(LandEntity landEntity) {
        return new Land(landEntity.getId(),  landEntity.getArea().getName(), landEntity.getArea().getParentArea().getName(), landEntity.getType(), landEntity.getTrunkNumber(), landEntity.getUnderDivisionNumber(), landEntity.getAcreage(), null, Lists.newArrayList());
    }

    /**
     * Gets unique land identifier.
     *
     * @return unique land identifier
     */
    public String getId() {
        return id;
    }

    /**
     * Gets cadastral territory area.
     * @return cadastral territory area
     */
    public String getCadastralTerritoryArea() {
        return cadastralTerritoryArea;
    }

    /**
     * Gets city area.
     *
     * @return city area
     */
    public String getCityArea() {
        return cityArea;
    }

    /**
     * Gets land type.
     *
     * @return land type
     */
    public LandType getType() {
        return type;
    }

    /**
     * Gets trunk number.
     *
     * @return trunk number
     */
    public Integer getTrunkNumber() {
        return trunkNumber;
    }

    /**
     * Gets under division number.
     *
     * @return under division number
     */
    public Integer getUnderDivisionNumber() {
        return underDivisionNumber;
    }

    /**
     * Gets acreage.
     *
     * @return acreage
     */
    public Integer getAcreage() {
        return acreage;
    }

    /**
     * Get definition point coordinate.
     *
     * @return definition point coordinate
     */
    public Epsg5514Coordinate getDefinitionPointCoordinate() {
        return definitionPointCoordinate;
    }

    /**
     * Gets border coordinates.
     *
     * @return border coordinates
     */
    public List<Epsg5514Coordinate> getBorderCoordinates() {
        return borderCoordinates;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("type", type)
                .add("trunkNumber", trunkNumber)
                .add("underDivisionNumber", underDivisionNumber)
                .add("acreage", acreage)
                .omitNullValues()
                .toString();
    }
}
