package cz.lp.common.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * EPSG 5411 coordinate.
 */
public class Epsg5514Coordinate implements Serializable {

    public final Float x;
    public final Float y;

    /**
     * Creates instance of x and y positions
     *
     * @param x x position
     * @param y y position
     * @return EPSG 5514 coordinate
     */
    @JsonCreator
    public Epsg5514Coordinate(@JsonProperty("x") Float x, @JsonProperty("y") Float y) {
        this.x = x;
        this.y = y;
    }

    public Float getX() {
        return x;
    }

    public Float getY() {
        return y;
    }

    /**
     * Creates instance of x and y positions
     *
     * @param x x position
     * @param y y position
     * @return EPSG 5514 coordinate
     */
    public static Epsg5514Coordinate of(Float x, Float y) {

        return new Epsg5514Coordinate(x, y);
    }
}
