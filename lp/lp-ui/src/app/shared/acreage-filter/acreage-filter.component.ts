import { Component, Input } from '@angular/core';
import { FormGroup, FormControl } from '../../../../node_modules/@angular/forms';

@Component({
	selector: 'lp-acreage-filter',
	templateUrl: './acreage-filter.component.html',
	styleUrls: ['./acreage-filter.component.scss']
})
export class AcreageFilterComponent {
	@Input()
	acreageForm: FormGroup;

  constructor() {}

  get acreageFrom(): FormControl {
		return this.acreageForm ? this.acreageForm.get('acreageFrom') as FormControl : undefined;
	}

	get acreageTo(): FormControl {
		return this.acreageForm ? this.acreageForm.get('acreageTo') as FormControl : undefined;
	}
}

export function createAcreageFilterFormGroup(fromInitValue: number, toInitValue: number): FormGroup {
	let acreageFrom = new FormControl(fromInitValue);
	let acreageTo = new FormControl(toInitValue);
	acreageFrom.setValidators([validateAcreageFrom(acreageTo)]);
	acreageTo.setValidators([validateAcreageTo(acreageFrom)]);
	return new FormGroup({ acreageFrom, acreageTo });
}

function validateAcreageFrom(acreageTo: FormControl) {
	return (control: FormControl): { [key: string]: any } | null => {
		if (control.value < 0) return { lessThanZero: true };
		if (+control.value > +acreageTo.value) return { invalid: true };
		return null;
	};
}

function validateAcreageTo(acreageFrom: FormControl) {
	return (control: FormControl): { [key: string]: any } | null => {
		if (control.value < 0) return { lessThanZero: true };
		if (+control.value < +acreageFrom.value) return { invalid: true };
		return null;
	};
}

export function getFilterAcreageFrom(acreageForm: FormGroup): number {
	return acreageForm.get('acreageFrom').value;
}

export function getFilterAcreageTo(acreageForm: FormGroup): number {
	return acreageForm.get('acreageTo').value;
}
