package cz.lp.processor.custom.advertisement.srealitycz;

import cz.lp.CrawlerRunner;
import cz.lp.CrawlerScheduler;
import cz.lp.CrawlerWorkerService;
import cz.lp.StandaloneProcessorScheduler;
import cz.lp.exception.ProcessorClassException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

@ComponentScan(basePackages = "cz.lp", excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {CrawlerRunner.class, CrawlerScheduler.class, StandaloneProcessorScheduler.class}))
public class SrealityCzProcessorRunner {

    private static final Logger log = LoggerFactory.getLogger(SrealityCzProcessorRunner.class);

    private static final Long ID = 5l;

    public static void main(String[] args) throws ProcessorClassException {

        log.info("Starting Sreality CZ processor runner ...");
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(SrealityCzProcessorRunner.class);
        CrawlerWorkerService crawlerWorkerService = ctx.getBean(CrawlerWorkerService.class);
        SrealityCzProcessor processor = (SrealityCzProcessor) crawlerWorkerService.getProcessor(ID);
        if (processor.checkCondition()) {
            processor.process();
        } else {
            log.warn("Sreality CZ processor does not meet startup conditions");
        }
    }
}
