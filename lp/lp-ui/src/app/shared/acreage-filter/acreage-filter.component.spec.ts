import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcreageFilterComponent, createAcreageFilterFormGroup } from './acreage-filter.component';
import { TranslateModule } from '../../../../node_modules/@ngx-translate/core';
import { MatFormFieldModule, MatInputModule } from '../../../../node_modules/@angular/material';
import { FormsModule, ReactiveFormsModule } from '../../../../node_modules/@angular/forms';
import { BrowserAnimationsModule } from '../../../../node_modules/@angular/platform-browser/animations';

describe('AcreageFilterComponent', () => {
	let component: AcreageFilterComponent;
	let fixture: ComponentFixture<AcreageFilterComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [BrowserAnimationsModule, TranslateModule.forRoot(), FormsModule, ReactiveFormsModule, MatFormFieldModule, MatInputModule],
			declarations: [AcreageFilterComponent]
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(AcreageFilterComponent);
		component = fixture.componentInstance;
	});

	it('init without form group as input - should create instance', () => {
		expect(component).toBeTruthy();
		expect(component.acreageForm).toBeUndefined();
		expect(component.acreageFrom).toBeUndefined();
		expect(component.acreageTo).toBeUndefined();
	});

	it('init with form group as input - should create instance', () => {
		component.acreageForm = createAcreageFilterFormGroup(500, 1000);

		expect(component).toBeTruthy();
		expect(component.acreageForm.get('acreageFrom').value).toEqual(500);
		expect(component.acreageFrom.value).toEqual(500);
		expect(component.acreageForm.get('acreageTo').value).toEqual(1000);
		expect(component.acreageTo.value).toEqual(1000);
		expect(component.acreageForm.valid).toBeTruthy();
	});

	it('change acreage from to 200 - should return 200 without error', () => {
		component.acreageForm = createAcreageFilterFormGroup(500, 1000);
		component.acreageForm.get('acreageFrom').setValue(200);

		expect(component.acreageForm.get('acreageFrom').value).toEqual(200);
		expect(component.acreageFrom.value).toEqual(200);
		expect(component.acreageForm.valid).toBeTruthy();
	});

	it('change acreage to to 1200 - should return 1200 without error', () => {
		component.acreageForm = createAcreageFilterFormGroup(500, 1000);
		component.acreageForm.get('acreageTo').setValue(1200);

		expect(component.acreageForm.get('acreageTo').value).toEqual(1200);
		expect(component.acreageTo.value).toEqual(1200);
		expect(component.acreageForm.valid).toBeTruthy();
	});

	it('change acreage from to 2000 (from is greater than to) - should return invalid error', () => {
		component.acreageForm = createAcreageFilterFormGroup(500, 1000);
		component.acreageForm.get('acreageFrom').setValue(2000);

		expect(component.acreageForm.invalid).toBeTruthy();
		expect(component.acreageForm.get('acreageFrom').errors.invalid).toBeTruthy();
		expect(component.acreageFrom.errors.invalid).toBeTruthy();
		expect(component.acreageForm.get('acreageFrom').hasError('invalid')).toBeTruthy();
		expect(component.acreageFrom.hasError('invalid')).toBeTruthy();
	});

	it('change acreage to to 100 (from is less than to) - should return invalid error', () => {
		component.acreageForm = createAcreageFilterFormGroup(500, 1000);
		component.acreageForm.get('acreageTo').setValue(100);

		expect(component.acreageForm.invalid).toBeTruthy();
		expect(component.acreageForm.get('acreageTo').errors.invalid).toBeTruthy();
		expect(component.acreageTo.errors.invalid).toBeTruthy();
		expect(component.acreageForm.get('acreageTo').hasError('invalid')).toBeTruthy();
		expect(component.acreageTo.hasError('invalid')).toBeTruthy();
	});

	it('change acreage from to -100 - should return lessThanZero error', () => {
		component.acreageForm = createAcreageFilterFormGroup(500, 1000);
		component.acreageForm.get('acreageFrom').setValue(-100);

		expect(component.acreageForm.invalid).toBeTruthy();
		expect(component.acreageForm.get('acreageFrom').errors.lessThanZero).toBeTruthy();
		expect(component.acreageFrom.errors.lessThanZero).toBeTruthy();
		expect(component.acreageForm.get('acreageFrom').hasError('lessThanZero')).toBeTruthy();
		expect(component.acreageFrom.hasError('lessThanZero')).toBeTruthy();
	});

	it('change acreage to to -100 - should return lessThanZero error', () => {
		component.acreageForm = createAcreageFilterFormGroup(500, 1000);
		component.acreageForm.get('acreageTo').setValue(-100);

		expect(component.acreageForm.invalid).toBeTruthy();
		expect(component.acreageForm.get('acreageTo').errors.lessThanZero).toBeTruthy();
		expect(component.acreageTo.errors.lessThanZero).toBeTruthy();
		expect(component.acreageForm.get('acreageTo').hasError('lessThanZero')).toBeTruthy();
		expect(component.acreageTo.hasError('lessThanZero')).toBeTruthy();
	});
});
