package cz.lp.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Land Picker Server Runner.
 */

@SpringBootApplication(scanBasePackages = {"cz.lp.server", "cz.lp.common"})
public class LandPickerServerRunner {

    public static void main(String[] args) {
        SpringApplication.run(LandPickerServerRunner.class, args);
    }
}
