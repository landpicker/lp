package cz.lp.exception;

/**
 * Crawler class exception is generated if something during creating instance of crawler by reflection failed.
 */
public class CrawlerClassException extends Exception {

    public CrawlerClassException(String message) {
        super(message);
    }

    public CrawlerClassException(String message, Throwable cause) {
        super(message, cause);
    }
}
