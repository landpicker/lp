package cz.lp.repository;

import cz.lp.domain.SourceEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 * Source repository provides access to source table.
 */
public interface SourceRepository extends CrudRepository<SourceEntity, Long> {

    /**
     * Gets source by crawler identifier.
     *
     * @return source entity
     */
    @Query(value = "SELECT * FROM source s INNER JOIN crawler c ON c.source_id = s.id WHERE c.id = :crawler_id LIMIT 1", nativeQuery = true)
    SourceEntity findOneByCrawlerId(@Param("crawler_id") Long crawlerId);
}
