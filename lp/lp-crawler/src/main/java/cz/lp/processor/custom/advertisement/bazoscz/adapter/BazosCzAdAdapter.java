package cz.lp.processor.custom.advertisement.bazoscz.adapter;

import cz.lp.common.dto.ad.Ad;
import cz.lp.common.dto.ad.AdImage;
import cz.lp.util.DownloadUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

public class BazosCzAdAdapter {

    private static final String TITLE_SELECTOR = "h1[class=nadpis]";
    private static final String CONTENT_SELECTOR = "div[class=popis]";
    private static final String PUBLISHED_ON_SELECTOR = "table[class=listainzerat]";
    private static final Pattern PUBLISHED_ON_REGEX_PATTERN = Pattern.compile("\\d{1,2}\\.\\d{1,2}\\. \\d{4}");
    private static final String PUBLISHED_ON_DATE_PATTERN = "d.M. yyyy";
    private static final Pattern IMAGE_FILE_NAME_REGEX_PATTERN = Pattern.compile(".*/(\\d+.jpg).*");

    private Ad ad;

    public BazosCzAdAdapter(String content, String url, String source) {

        Document document = Jsoup.parse(content, url);
        Element titleElement = document.select(TITLE_SELECTOR).first();
        if (isNull(titleElement)) {
            return;
        }
        Element publishedOnElement = document.select(PUBLISHED_ON_SELECTOR).first();
        if (isNull(publishedOnElement)) {
            return;
        }
        Matcher publishedOnMatcher = PUBLISHED_ON_REGEX_PATTERN.matcher(publishedOnElement.text());
        LocalDate publishedOn;
        if (publishedOnMatcher.find()) {
            publishedOn = LocalDate.parse(publishedOnMatcher.group(0), DateTimeFormatter.ofPattern(PUBLISHED_ON_DATE_PATTERN));
        } else {
            return;
        }

        Element contentElement = document.select(CONTENT_SELECTOR).first();
        if (isNull(contentElement)) {
            return;
        }

        List<AdImage> images = document.select("div[class=flinavigace] img").stream()
                .map(element -> element.attr("abs:src").replace("t/", "/"))
                .map(imageUrl -> {

                    try {
                        Optional<String> fileName = extractImageFileName(imageUrl);
                        if (fileName.isPresent()) {
                            DownloadUtil.DownloadedData data = DownloadUtil.getData(imageUrl);
                            return new AdImage(imageUrl, fileName.get(), data.getContentType(), data.getData());
                        } else {
                            // TODO
                            return null;
                        }
                    } catch (IOException e) {
                        // TODO
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        ad = new Ad(url, source, titleElement.text(), contentElement.text(), publishedOn, images);
    }

    /**
     * Gets ad.
     *
     * @return ad
     */
    public Optional<Ad> getAd() {

        return Optional.of(ad);
    }

    /**
     * Extracts file name from image URL.
     *
     * @param imageUrl image URL
     * @return image file name
     */
    private Optional<String> extractImageFileName(String imageUrl) {

        Matcher fileNameMatcher = IMAGE_FILE_NAME_REGEX_PATTERN.matcher(imageUrl);
        if (fileNameMatcher.find()) {
            return Optional.of(fileNameMatcher.group(1));
        }
        return Optional.empty();
    }
}
