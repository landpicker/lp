package cz.lp.processor.custom.advertisement.srealitycz.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class SrealityCzEstateDetailDto {

    private SrealityCzNameDto name;
    private SrealityCzTextDto text;
    private SrealityCzLocalityDto locality;
    private SrealityCzEstateDetailEmbeddedDto embedded;

    public SrealityCzEstateDetailDto(
            @JsonProperty("name") SrealityCzNameDto name,
            @JsonProperty("text") SrealityCzTextDto text,
            @JsonProperty("locality") SrealityCzLocalityDto locality,
            @JsonProperty("_embedded") SrealityCzEstateDetailEmbeddedDto embedded) {
        this.name = name;
        this.text = text;
        this.locality = locality;
        this.embedded = embedded;
    }
}
