package cz.lp.crawler.resolver;

import cz.lp.crawler.service.CrawlerConditionService;
import cz.lp.enumerate.CrawlerCondition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Crawler condition resolver provides method for obtaining information about meeting of condition.
 */
@Component
public class CrawlerConditionResolver {

    private final CrawlerConditionService conditionService;

    @Autowired
    public CrawlerConditionResolver(CrawlerConditionService conditionService) {
        this.conditionService = conditionService;
    }

    /**
     * Gets information about meeting the condition.
     *
     * @param crawlerCondition crawler condition
     * @return true if the condition is met, otherwise false
     */
    public boolean meetsCondition(CrawlerCondition crawlerCondition) {

        boolean meetsCondition;

        switch (crawlerCondition) {
            case NONE:
                meetsCondition = true;
                break;
            case EXIST_FIRST_LEVEL_AREA:
                meetsCondition = conditionService.existFirstLevelAreas();
                break;
            case EXIST_CITY_LEVEL_AREAS:
                meetsCondition = conditionService.existCityAreas();
                break;
            default:
                meetsCondition = true;
                break;
        }

        return meetsCondition;
    }
}
