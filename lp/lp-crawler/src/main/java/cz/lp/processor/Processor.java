package cz.lp.processor;

import cz.lp.enumerate.ProcessorCondition;
import cz.lp.enumerate.ResultStatus;
import cz.lp.exception.ProcessorExtractDataException;
import cz.lp.processor.resolver.ProcessorConditionResolver;
import cz.lp.processor.service.ProcessorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;

public abstract class Processor<D> {

    protected static Logger log = LoggerFactory.getLogger(Processor.class);

    protected ProcessorService processorService;
    protected ProcessorConditionResolver processorConditionResolver;
    protected ProcessorFilter processorFilter;

    private final String className;
    private final Long id;
    protected final Long crawlerId;
    protected final ProcessorCondition condition;
    protected final String urlPattern;
    protected final LocalDateTime newLastProcessedOn;

    public Processor(String className, Long id, Long crawlerId,  ProcessorCondition condition, String urlPattern, ProcessorService processorService, ProcessorConditionResolver processorConditionResolver, ProcessorFilter processorFilter) {
        this.className = className;
        this.id = id;
        this.crawlerId = crawlerId;
        this.condition = condition;
        this.urlPattern = urlPattern;
        this.newLastProcessedOn = LocalDateTime.now();
        this.processorService = processorService;
        this.processorConditionResolver = processorConditionResolver;
        this.processorFilter = processorFilter;
    }

    /**
     * Gets class name.
     *
     * @return class name
     */
    public String getClassName() {
        return className;
    }

    /**
     * Gets information about result of condition.
     *
     * @return true if the condition is true, otherwise false
     */
    public final boolean checkCondition() {

        boolean result = processorConditionResolver.meetsCondition(condition, crawlerId);
        if (!result) {
            log.warn("{} - Processor does not meet condition", className);
        }
        return result;
    }

    /**
     * Runs processor.
     */
    public final void process() {

        log.info("{} - Starting processing...", className);
        int processingOrder = 0;
        Optional<Set<Long>> optionalResultIdentifiers;

        while ((optionalResultIdentifiers = processorService.getResultIdentifiersByProcessingOrder(crawlerId, id, processingOrder)).isPresent()) {

            try {
                optionalResultIdentifiers.get().forEach(resultIdentifier -> {

                    Result result = processorService.getResultForExtract(resultIdentifier);
                    // Filter by URL pattern
                    if(!result.getUrl().matches(urlPattern)){
                        return;
                    }

                    if (result.getResultStatus() == ResultStatus.FAILED) {
                        setDataAsRemoved(result, newLastProcessedOn);
                        return;
                    }
                    log.info("{} - Processing result with URL: {}", className, result.getUrl());
                    try {
                        List<D> data = extractData(result);
                        saveData(data, result.getId(), newLastProcessedOn);
                        processorService.markResultAsProcessed(id, result.getId());
                    } catch (ProcessorExtractDataException e) {
                        log.error("{} - Unable to extract data from result with url {}", className, result.getUrl());
                        updateProcessorAfterFailed();
                        throw new RuntimeException(e);
                    }
                });
                processingOrder++;
            } catch (RuntimeException ex) {
                log.error("Processor failed", ex);
                return;
            }
        }
        log.info("{} - No crawler results for processing.", className);
        updateProcessorAfterSuccessFinish();
    }

    /**
     * Extracts data.
     *
     * @param result result
     * @return data
     * @throws ProcessorExtractDataException if extraction failed
     */
    protected abstract List<D> extractData(Result result) throws ProcessorExtractDataException;

    protected abstract void setDataAsRemoved(Result result, LocalDateTime removedOn);

    /**
     * Saves results.
     *
     * @param data            data
     * @param crawlerResultId crawler result identifier
     * @param lastUpdatedOn   last updated on
     */
    protected abstract void saveData(List<D> data, Long crawlerResultId, LocalDateTime lastUpdatedOn);

    /**
     * Updates processor after success finish.
     */
    private void updateProcessorAfterSuccessFinish() {

        processorService.updateProcessorAfterSuccessFinish(id, newLastProcessedOn);
    }

    /**
     * Updates processor after failed.
     */
    private void updateProcessorAfterFailed() {

        processorService.updateProcessorAfterSuccessFinish(id, newLastProcessedOn);
    }
}
