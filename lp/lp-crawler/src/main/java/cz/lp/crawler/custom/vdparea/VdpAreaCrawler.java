package cz.lp.crawler.custom.vdparea;

import com.google.common.collect.Sets;
import cz.lp.crawler.Crawler;
import cz.lp.crawler.CrawlerFilter;
import cz.lp.crawler.SeedUrl;
import cz.lp.crawler.custom.vdparea.adapter.VdpCityAreaDocumentLinkAdapter;
import cz.lp.crawler.custom.vdparea.adapter.VdpDistrictAreaDocumentLinkAdapter;
import cz.lp.crawler.custom.vdparea.adapter.VdpRegionAreaDocumentLinkAdapter;
import cz.lp.crawler.custom.vdparea.filter.VdpAreaCrawlerFilter;
import cz.lp.crawler.resolver.CrawlerConditionResolver;
import cz.lp.crawler.service.CrawlerService;
import cz.lp.enumerate.CrawlerCondition;
import cz.lp.enumerate.RecrawlStrategy;
import org.apache.commons.codec.digest.DigestUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;

/**
 * VDP area crawler provides crawling areas from http://vdp.cuzk.cz/vdp/ruian/vusc/vyhledej.
 */
public class VdpAreaCrawler extends Crawler {

    private static final String REGION_URL_PATTERN = "(?i)https?://(www.)?vdp\\.cuzk\\.cz/vdp/ruian/vusc/.*";
    private static final String DISTRICT_URL_PATTERN = "(?i)https?://(www.)?vdp\\.cuzk\\.cz/vdp/ruian/okresy/.*";
    private static final String CITY_URL_PATTERN = "(?i)https?://(www.)?vdp\\.cuzk\\.cz/vdp/ruian/obce/.*";
    private static final String HASH_SUB_CONTENT_SELECTOR = "Zaznamy";


    public VdpAreaCrawler(String className, Long id, CrawlerCondition condition, Integer delay, RecrawlStrategy recrawlStrategy, String recrawlUrlPattern, Long recrawlInterval, LocalDateTime recrawlOn, Set<SeedUrl> seedUrls, CrawlerService crawlerService, CrawlerConditionResolver crawlerConditionResolver, CrawlerFilter crawlerFilter) {
        super(className, id, condition, delay, recrawlStrategy, recrawlUrlPattern, recrawlInterval, recrawlOn, seedUrls, crawlerService, crawlerConditionResolver, crawlerFilter);
    }

    @Override
    protected Optional<Set<String>> parseLinks(String content, String url) {

        if (url.matches(REGION_URL_PATTERN)) {
            return new VdpRegionAreaDocumentLinkAdapter(content, url, (VdpAreaCrawlerFilter) crawlerFilter).getLinks();
        } else if (url.matches(DISTRICT_URL_PATTERN)) {
            return new VdpDistrictAreaDocumentLinkAdapter(content, url, (VdpAreaCrawlerFilter) crawlerFilter).getLinks();
        } else if (url.matches(CITY_URL_PATTERN)) {
            return new VdpCityAreaDocumentLinkAdapter(content, url, (VdpAreaCrawlerFilter) crawlerFilter).getLinks();
        } else {
            return Optional.of(Sets.newHashSet());
        }
    }

    @Override
    protected String generateContentHash(byte[] content, String url) {

        Document document = Jsoup.parse(new String(content), url, Parser.xmlParser());
        Elements subContentElements = document.select(HASH_SUB_CONTENT_SELECTOR);
        if (subContentElements.isEmpty()) {
            return null;
        }
        String subContent = document.select(HASH_SUB_CONTENT_SELECTOR).html();
        return DigestUtils.md5Hex(subContent).toUpperCase();
    }
}
