import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AcreageFilterComponent } from './acreage-filter.component';
import { FormsModule, ReactiveFormsModule } from '../../../../node_modules/@angular/forms';
import { MatFormFieldModule, MatInputModule } from '../../../../node_modules/@angular/material';
import { TranslateModule } from '../../../../node_modules/@ngx-translate/core';
import { BrowserAnimationsModule } from '../../../../node_modules/@angular/platform-browser/animations';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule
  ],
  exports: [AcreageFilterComponent],
  declarations: [AcreageFilterComponent]
})
export class AcreageFilterModule { }
