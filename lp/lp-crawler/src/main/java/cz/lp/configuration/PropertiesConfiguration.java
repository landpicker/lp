package cz.lp.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@Configuration
public class PropertiesConfiguration {

    @Configuration
    @Profile("!dev")
    @PropertySource("classpath:application.properties")
    static class Defaults {
    }

    @Configuration
    @Profile("dev")
    @PropertySource({"classpath:application.properties", "classpath:application-dev.properties"})
    static class Dev {
    }

    @Configuration
    @Profile("prod")
    @PropertySource({"classpath:application.properties", "classpath:application-prod.properties"})
    static class Prod {
    }
}
