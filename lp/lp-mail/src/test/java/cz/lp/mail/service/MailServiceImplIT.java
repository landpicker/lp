package cz.lp.mail.service;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import cz.lp.common.dto.mail.MailRequestAttachment;
import cz.lp.common.service.mail.MailRequestService;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.util.ReflectionUtils;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Set;

public class MailServiceImplIT {

    @Test
    public void send() throws IOException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        Environment env = Mockito.mock(Environment.class);
        Mockito.when(env.getProperty("mail.host")).thenReturn("smtp.zoner.com");
        Mockito.when(env.getProperty("mail.port")).thenReturn("587");
        Mockito.when(env.getProperty("mail.username")).thenReturn("news@landpicker.cz");
        Mockito.when(env.getProperty("mail.password")).thenReturn("NfSYdxYh");
        Mockito.when(env.getProperty("mail.sender")).thenReturn("news@landpicker.cz");

        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setDefaultEncoding("utf-8");
        mailSender.setHost(env.getProperty("mail.host"));
        mailSender.setPort(Integer.valueOf(env.getProperty("mail.port")));
        mailSender.setUsername(env.getProperty("mail.username"));
        mailSender.setPassword(env.getProperty("mail.password"));

        MailService mailService = new MailServiceImpl(mailSender, env, Mockito.mock(MailRequestService.class));
        Method sendMethod = mailService.getClass().getDeclaredMethod("send",
                String.class,
                String.class,
                List.class,
                Set.class);
        sendMethod.setAccessible(true);

        try (InputStream is = getClass().getClassLoader().getResourceAsStream("105315857.jpg")) {
            ReflectionUtils.invokeMethod(sendMethod, mailService,
                    "Integration test",
                    "Content",
                    Lists.newArrayList(new MailRequestAttachment("test.jpg", "image/jpeg", IOUtils.toByteArray(is))),
                    Sets.newHashSet("honza.sipek@gmail.com"));
        }
    }
}