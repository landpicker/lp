package cz.lp.processor.custom.areabase;

import cz.lp.common.domain.AreaEntity;
import cz.lp.common.repository.AreaBorderRepository;
import cz.lp.common.repository.AreaRepository;
import cz.lp.common.repository.LandBorderRepository;
import cz.lp.common.repository.LandRepository;
import cz.lp.enumerate.ProcessorCondition;
import cz.lp.exception.ProcessorExtractDataException;
import cz.lp.processor.Processor;
import cz.lp.processor.ProcessorFilter;
import cz.lp.processor.Result;
import cz.lp.processor.custom.areabase.adapter.AreaBaseDataAdapter;
import cz.lp.processor.custom.areabase.filter.AreaBaseProcessorFilter;
import cz.lp.processor.resolver.ProcessorConditionResolver;
import cz.lp.processor.service.ProcessorRepositoryService;
import cz.lp.processor.service.ProcessorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.isNull;

public class AreaBaseProcessor extends Processor<Area> {

    private AreaRepository areaRepository;

    public AreaBaseProcessor(String className, Long id, Long crawlerId, ProcessorCondition condition, String urlPattern, ProcessorService processorService, ProcessorConditionResolver processorConditionResolver,
                             ProcessorRepositoryService processorRepositoryService, ProcessorFilter processorFilter) {
        super(className, id, crawlerId, condition, urlPattern, processorService, processorConditionResolver, processorFilter);
        this.areaRepository = processorRepositoryService.getAreaRepository();
    }

    @Override
    protected List<Area> extractData(Result result) throws ProcessorExtractDataException {

        AreaBaseDataAdapter areaAdapter = new AreaBaseDataAdapter(result.getContentAsString(), result.getUrl(), (AreaBaseProcessorFilter) this.processorFilter);
        Optional<List<Area>> areas = areaAdapter.getAreas();
        if (!areas.isPresent()) {
            throw new ProcessorExtractDataException();
        }
        return areas.get();
    }

    @Override
    protected void saveData(List<Area> data, Long crawlerResultId, LocalDateTime lastUpdatedOn) {

        setAllAreasAsRemoved(crawlerResultId, lastUpdatedOn);

        data.forEach(area -> {

            AreaEntity areaEntity = areaRepository.findOne(area.getCode());
            if (isNull(areaEntity)) {
                createArea(area, crawlerResultId, lastUpdatedOn);
            } else {
                updateArea(areaEntity, area, lastUpdatedOn);
            }
        });
    }

    @Override
    protected void setDataAsRemoved(Result result, LocalDateTime removedOn) {

        setAllAreasAsRemoved(result.getId(), removedOn);
    }

    /**
     * Sets all areas as removed for specific crawler result identifier.
     *
     * @param crawlerResultId crawler result identifier
     * @param removedOn       removed on
     */
    @Transactional
    private void setAllAreasAsRemoved(Long crawlerResultId, LocalDateTime removedOn) {

        areaRepository.updateAllAreasAsRemovedByCrawlerResultId(Timestamp.valueOf(removedOn), crawlerResultId);
    }

    /**
     * Creates area.
     *
     * @param area            area
     * @param crawlerResultId crawler result identifier
     * @param createdOn       created on
     */
    private void createArea(Area area, Long crawlerResultId, LocalDateTime createdOn) {

        AreaEntity parentAreaEntity = !isNull(area.getParentAreaCode()) ? areaRepository.findOne(area.getParentAreaCode()) : null;
        AreaEntity areaEntity = new AreaEntity(area.getCode(), area.getType(), area.getName(), parentAreaEntity, null, crawlerResultId, Timestamp.valueOf(createdOn), Timestamp.valueOf(createdOn));
        areaRepository.save(areaEntity);
    }

    /**
     * Updated area.
     *
     * @param areaEntity    area entity
     * @param area          area
     * @param lastUpdatedOn last updated on
     */
    private void updateArea(AreaEntity areaEntity, Area area, LocalDateTime lastUpdatedOn) {

        areaEntity.setName(area.getName());
        areaEntity.setLastUpdatedOn(Timestamp.valueOf(lastUpdatedOn));
        areaEntity.setRemovedOn(null);
        areaRepository.save(areaEntity);
    }
}
