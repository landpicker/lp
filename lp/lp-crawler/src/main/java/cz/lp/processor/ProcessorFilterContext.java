package cz.lp.processor;

import com.google.common.collect.Maps;
import cz.lp.processor.custom.areabase.filter.AreaBaseProcessorFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Processor filter context holds filters for specific processors.
 */
@Component
public class ProcessorFilterContext {

    private Map<String, ProcessorFilter> filters = Maps.newHashMap();

    @Autowired()
    public ProcessorFilterContext(AreaBaseProcessorFilter areaBaseProcessorFilter) {

        filters.put("AreaBaseProcessor", areaBaseProcessorFilter);
    }

    public ProcessorFilter getFilter(String simpleClassName) {
        return filters.get(simpleClassName);
    }
}
