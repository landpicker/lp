package cz.lp.crawler.custom.vdparea;

import com.google.common.collect.Sets;
import cz.lp.crawler.SeedUrl;
import cz.lp.crawler.resolver.CrawlerConditionResolver;
import cz.lp.crawler.service.CrawlerService;
import cz.lp.enumerate.CrawlerCondition;
import cz.lp.enumerate.RecrawlStrategy;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;

public class VdpAreaCrawlerTest {

    private static final String CLASS_NAME = "VdpAreaCrawler";
    private static final Long ID = 1l;
    private static final CrawlerCondition CRAWLER_CONDITION = CrawlerCondition.NONE;
    private static final Integer DELAY = 1000;
    private static final RecrawlStrategy RECRAWL_STRATEGY = RecrawlStrategy.RECRAWL_ALL;
    private static final String RECRAWL_URL_PATTERN = ".*";
    private static final Long RECRAWL_INTERVAL = 1000l;
    private static final LocalDateTime RECRAWL_ON = LocalDateTime.now();
    private static final LocalDateTime LAST_CRAWLED_ON = LocalDateTime.now();
    private static final Set<SeedUrl> SEED_URLS = Sets.newHashSet(Mockito.mock(SeedUrl.class));
    private static final CrawlerService CRAWLER_SERVICE = Mockito.mock(CrawlerService.class);
    private static final CrawlerConditionResolver CRAWLER_CONDITION_RESOLVER = Mockito.mock(CrawlerConditionResolver.class);

    private static final String DISTRICTS_URL = "http://vdp.cuzk.cz/vdp/ruian/okresy/export?vc.kod=141&kr.kod=&ok.nazev=&ok.kod=&ohrada.id=&okg.sort=KOD&export=XML";
    private static final String CITIES_URL = "http://vdp.cuzk.cz/vdp/ruian/obce/export?vc.kod=&op.kod=&ok.kod=1234&pu.kod=&ob.nazev=&ob.statusKod=&ob.kod=&ohrada.id=&obg.sort=KOD&export=XML";
    private static final String CADASTRAL_TERRITORY_URL = "http://vdp.cuzk.cz/vdp/ruian/katastralniuzemi/export?ob.kod=585068&ku.nazev=&ku.kod=&ohrada.id=&kug.sort=KOD&export=XML";

    @Test
    public void parseLinks_parseDistricts() throws Exception {

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("crawler/vdparea/district_valid_sample.xml");
        String content = IOUtils.toString(inputStream, StandardCharsets.UTF_8.name());
        VdpAreaCrawler crawler = new VdpAreaCrawler(CLASS_NAME, ID, CRAWLER_CONDITION, DELAY, RECRAWL_STRATEGY, RECRAWL_URL_PATTERN, RECRAWL_INTERVAL, RECRAWL_ON, SEED_URLS, CRAWLER_SERVICE, CRAWLER_CONDITION_RESOLVER, null);
        Optional<Set<String>> links = crawler.parseLinks(content, DISTRICTS_URL);
        Assert.assertTrue(links.isPresent());
        Assert.assertEquals(4, links.get().size());
        Iterator<String> iterator = links.get().iterator();
        Assert.assertTrue(iterator.hasNext());
        Assert.assertEquals("http://vdp.cuzk.cz/vdp/ruian/obce/export?vc.kod=&op.kod=&ok.kod=3705&pu.kod=&ob.nazev=&ob.statusKod=&ob.kod=&ohrada.id=&obg.sort=KOD&export=XML", iterator.next());
    }

    @Test
    public void parseLinks_parseCities() throws Exception {

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("crawler/vdparea/city_valid_sample.xml");
        String content = IOUtils.toString(inputStream, StandardCharsets.UTF_8.name());
        VdpAreaCrawler crawler = new VdpAreaCrawler(CLASS_NAME, ID, CRAWLER_CONDITION, DELAY, RECRAWL_STRATEGY, RECRAWL_URL_PATTERN, RECRAWL_INTERVAL, RECRAWL_ON, SEED_URLS, CRAWLER_SERVICE, CRAWLER_CONDITION_RESOLVER, null);
        Optional<Set<String>> links = crawler.parseLinks(content, CITIES_URL);
        Assert.assertTrue(links.isPresent());
        Assert.assertNotNull(links);
        Assert.assertFalse(links.get().isEmpty());
        Assert.assertEquals(89, links.get().size());
        Iterator<String> iterator = links.get().iterator();
        Assert.assertTrue(iterator.hasNext());
        Assert.assertEquals("http://vdp.cuzk.cz/vdp/ruian/katastralniuzemi/export?ob.kod=500011&ku.nazev=&ku.kod=&ohrada.id=&kug.sort=KOD&export=XML", iterator.next());
    }

    @Test
    public void parseLinks_parseCadastralTerritory() throws Exception {

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("crawler/vdparea/cadastral_territory_valid_sample.xml");
        String content = IOUtils.toString(inputStream, StandardCharsets.UTF_8.name());
        VdpAreaCrawler crawler = new VdpAreaCrawler(CLASS_NAME, ID, CRAWLER_CONDITION, DELAY, RECRAWL_STRATEGY, RECRAWL_URL_PATTERN, RECRAWL_INTERVAL, RECRAWL_ON, SEED_URLS, CRAWLER_SERVICE, CRAWLER_CONDITION_RESOLVER, null);
        Optional<Set<String>> links = crawler.parseLinks(content, CADASTRAL_TERRITORY_URL);
        Assert.assertTrue(links.isPresent());
        Assert.assertNotNull(links);
        Assert.assertTrue(links.get().isEmpty());
    }
}