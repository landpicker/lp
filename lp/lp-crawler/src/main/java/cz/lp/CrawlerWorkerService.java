package cz.lp;

import cz.lp.common.repository.AreaBorderRepository;
import cz.lp.common.repository.AreaRepository;
import cz.lp.common.repository.LandBorderRepository;
import cz.lp.common.repository.LandRepository;
import cz.lp.crawler.Crawler;
import cz.lp.crawler.CrawlerFilterContext;
import cz.lp.crawler.SeedUrl;
import cz.lp.crawler.custom.vdparea.filter.VdpAreaCrawlerFilter;
import cz.lp.crawler.resolver.CrawlerClassResolver;
import cz.lp.crawler.resolver.CrawlerConditionResolver;
import cz.lp.crawler.service.CrawlerService;
import cz.lp.domain.CrawlerEntity;
import cz.lp.domain.CrawlerSeedUrlEntity;
import cz.lp.domain.ProcessorEntity;
import cz.lp.exception.CrawlerClassException;
import cz.lp.exception.ProcessorClassException;
import cz.lp.processor.Processor;
import cz.lp.processor.ProcessorFilterContext;
import cz.lp.processor.resolver.ProcessorClassResolver;
import cz.lp.processor.resolver.ProcessorConditionResolver;
import cz.lp.processor.service.ProcessorRepositoryService;
import cz.lp.processor.service.ProcessorService;
import cz.lp.repository.CrawlerRepository;
import cz.lp.repository.ProcessorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CrawlerWorkerService {

    private static final Logger log = LoggerFactory.getLogger(CrawlerWorkerService.class);

    private final CrawlerRepository crawlerRepository;
    private final ProcessorRepository processorRepository;
    private final CrawlerService crawlerService;
    private final ProcessorService processorService;
    private final CrawlerConditionResolver crawlerConditionResolver;
    private final ProcessorConditionResolver processorConditionResolver;
    private final ProcessorRepositoryService processorRepositoryService;
    private final CrawlerFilterContext crawlerFilterContext;
    private final ProcessorFilterContext processorFilterContext;

    @Autowired
    public CrawlerWorkerService(CrawlerRepository crawlerRepository,
                                ProcessorRepository processorRepository,
                                CrawlerService crawlerService,
                                ProcessorService processorService,
                                CrawlerConditionResolver crawlerConditionResolver,
                                ProcessorConditionResolver processorConditionResolver,
                                ProcessorRepositoryService processorRepositoryService,
                                CrawlerFilterContext crawlerFilterContext,
                                ProcessorFilterContext processorFilterContext) {
        this.crawlerRepository = crawlerRepository;
        this.processorRepository = processorRepository;
        this.crawlerService = crawlerService;
        this.processorService = processorService;
        this.crawlerConditionResolver = crawlerConditionResolver;
        this.processorConditionResolver = processorConditionResolver;
        this.processorRepositoryService = processorRepositoryService;
        this.crawlerFilterContext = crawlerFilterContext;
        this.processorFilterContext = processorFilterContext;
    }

    /**
     * Gets executable crawlers.
     * <p>
     * <strong>Crawler is executable when:</strong>
     * <ul>
     * <li>crawler.enabled = true</li>
     * <li>crawler.recrawl_on < NOW() OR recrawl_on IS NULL</li>
     * <li>crawler.condition = true</li>
     * </ul>
     * </p>
     *
     * @return crawlers
     */
    @Transactional
    public List<Crawler> getExecutableCrawlers() {

        List<CrawlerEntity> crawlerEntities = crawlerRepository.findAllEnabled();
        List<Crawler> crawlers = convertCrawlerEntitiesToCrawlers(crawlerEntities, crawlerService, crawlerConditionResolver, crawlerFilterContext);
        return crawlers.stream().filter(crawler -> crawler.checkCondition()).collect(Collectors.toList());
    }

    @Transactional
    public Crawler getCrawler(Long crawlerId) {

        CrawlerEntity crawlerEntity = crawlerRepository.findOne(crawlerId);
        Crawler crawler = convertCrawlerEntityToCrawler(crawlerEntity, crawlerService, crawlerConditionResolver, crawlerFilterContext);
        return crawler;
    }

    /**
     * Gets executable processors.
     * <p>
     * <strong>Processors is executable when:</strong>
     * <ul>
     * <li>processor.condition = true</li>
     * </ul>
     * </p>
     *
     * @param crawlerId crawler identifier
     * @return processors
     */
    @Transactional
    public List<Processor> getExecutableProcessors(Long crawlerId) {

        List<ProcessorEntity> processorEntities = processorRepository.findAllByCrawlerId(crawlerId);
        List<Processor> processors = convertProcessorEntitiesToProcessors(processorEntities, processorService, processorConditionResolver, processorRepositoryService, processorFilterContext);
        return processors.stream().filter(processor -> processor.checkCondition()).collect(Collectors.toList());
    }

    @Transactional
    public Processor getProcessor(Long processorId) {

        ProcessorEntity processorEntity = processorRepository.findOne(processorId);
        Processor processor = convertProcessorEntityToProcessor(processorEntity, processorService, processorConditionResolver, processorRepositoryService, processorFilterContext);
        return processor;
    }

    /**
     * Converts crawler entities to crawlers.
     *
     * @param crawlers crawler entities
     * @return crawlers
     */
    public static List<Crawler> convertCrawlerEntitiesToCrawlers(List<CrawlerEntity> crawlers, CrawlerService crawlerService, CrawlerConditionResolver crawlerConditionResolver, CrawlerFilterContext crawlerFilterContext) {

        return crawlers.stream()
                .map(crawlerEntity -> {
                    try {
                        return CrawlerClassResolver.createCrawlerInstance(crawlerEntity, crawlerService, crawlerConditionResolver, crawlerFilterContext);
                    } catch (CrawlerClassException ex) {
                        log.error("Unable to create instance of crawler from entity: {}", ex.getMessage());
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    /**
     * Converts crawler entity to crawler.
     *
     * @param crawlerEntity crawler entity
     * @return crawler
     */
    public static Crawler convertCrawlerEntityToCrawler(CrawlerEntity crawlerEntity, CrawlerService crawlerService, CrawlerConditionResolver crawlerConditionResolver, CrawlerFilterContext crawlerFilterContext) {

        try {
            return CrawlerClassResolver.createCrawlerInstance(crawlerEntity, crawlerService, crawlerConditionResolver, crawlerFilterContext);
        } catch (CrawlerClassException ex) {
            log.error("Unable to create instance of crawler from entity: {}", ex.getMessage());
            return null;
        }
    }

    /**
     * Converts crawler seed URL entities to seed URLs.
     *
     * @param crawlerSeedUrlEntities crawler seed URL entities
     * @return seed URLs
     */
    public static Set<SeedUrl> convertCrawlerSeedUrlEntitiesToSeedUrls(List<CrawlerSeedUrlEntity> crawlerSeedUrlEntities) {

        return crawlerSeedUrlEntities.stream()
                .map(crawlerSeedUrlEntity -> SeedUrl.of(crawlerSeedUrlEntity.getUrl(), crawlerSeedUrlEntity.isEnabled(), crawlerSeedUrlEntity.getCondition()))
                .collect(Collectors.toSet());
    }

    /**
     * Converts processor entities to processors.
     *
     * @param processors processor entities
     * @return processors
     */
    public static List<Processor> convertProcessorEntitiesToProcessors(List<ProcessorEntity> processors, ProcessorService processorService, ProcessorConditionResolver processorConditionResolver,
                                                                       ProcessorRepositoryService processorRepositoryService, ProcessorFilterContext processorFilterContext) {

        return processors.stream()
                .map(processorEntity -> {
                    try {
                        return ProcessorClassResolver.createProcessorInstance(processorEntity, processorService, processorConditionResolver, processorRepositoryService, processorFilterContext);
                    } catch (ProcessorClassException ex) {
                        log.error("Unable to create instance of processor from entity: {}", ex.getMessage());
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    /**
     * Converts processor entity to processor.
     *
     * @param processor processor entity
     * @return processor
     */
    public static Processor convertProcessorEntityToProcessor(ProcessorEntity processor, ProcessorService processorService, ProcessorConditionResolver processorConditionResolver,
                                                              ProcessorRepositoryService processorRepositoryService, ProcessorFilterContext processorFilterContext) {

        try {
            return ProcessorClassResolver.createProcessorInstance(processor, processorService, processorConditionResolver, processorRepositoryService, processorFilterContext);
        } catch (ProcessorClassException ex) {
            log.error("Unable to create instance of processor from entity: {}", ex.getMessage());
            return null;
        }
    }
}
