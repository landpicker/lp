package cz.lp.common.service.mail;

import cz.lp.common.domain.mail.MailTemplateEntity;
import cz.lp.common.dto.mail.MailTemplate;
import cz.lp.common.repository.MailTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static java.util.Objects.isNull;

@Service
public class MailTemplateServiceImpl implements MailTemplateService {

    private final static long MAIL_TEMPLATE_AD = 1l;

    private final MailTemplateRepository mailTemplateRepository;

    @Autowired
    public MailTemplateServiceImpl(MailTemplateRepository mailTemplateRepository) {
        this.mailTemplateRepository = mailTemplateRepository;
    }


    @Override
    public Optional<MailTemplate> getAdTemplate() {
        return getTemplate(MAIL_TEMPLATE_AD);
    }


    @Override
    public Optional<MailTemplate> getTemplate(long id) {

        MailTemplateEntity mailTemplateEntity = mailTemplateRepository.findOne(id);
        if (isNull(mailTemplateEntity)) {
            return Optional.empty();
        }
        return Optional.of(new MailTemplate(mailTemplateEntity.getSubjectPattern(), mailTemplateEntity.getContentPattern()));
    }
}
