package cz.lp.common.domain;

import cz.lp.common.dto.Epsg5514Coordinate;
import cz.lp.common.enumerate.LandType;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Land entity represents land object in database table.
 */

@Entity
@Table(name = "land")
public class LandEntity implements Serializable {

    @Id
    @Column(name = "id", unique = true, nullable = false, length = 30)
    private String id;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false, length = 50)
    private LandType type;

    @Column(name = "trunk_number", nullable = false)
    private Integer trunkNumber;

    @Column(name = "under_division_number")
    private Integer underDivisionNumber;

    @Column(name = "acreage", nullable = false)
    private Integer acreage;

    @Column(name = "contains_building")
    private Boolean containsBuilding;

    @Type(type = "Json", parameters = {
            @org.hibernate.annotations.Parameter(name = "type", value = "cz.lp.common.dto.Epsg5514Coordinate")
    })
    @Column(name = "definition_point", nullable = false)
    private Epsg5514Coordinate definitionPointEpsg5514;

    @Column(name = "created_on", nullable = false)
    private Timestamp createdOn;

    @Column(name = "last_updated_on")
    private Timestamp lastUpdatedOn;

    @Column(name = "removed_on")
    private Timestamp removedOn;

    @Column(name = "crawler_result_id")
    private Long crawlerResultId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "area_code", nullable = false)
    private AreaEntity area;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "land_border_id")
    private LandBorderEntity landBorder;

    /**
     * Creates instance of default constructor.
     */
    public LandEntity() {

    }


    /**
     * Creates instance of land entity.
     *
     * @param id                      land unique identifier
     * @param type                    land type
     * @param trunkNumber             trunk number
     * @param underDivisionNumber     under division number
     * @param acreage                 acreage
     * @param containsBuilding        contains building
     * @param definitionPointEpsg5514 definition point json in EPSG 5514
     * @param createdOn               created on
     * @param lastUpdatedOn           last updated on
     * @param removedOn               removed on
     * @param crawlerResultId         crawler result identifier
     * @param area                    area
     * @param landBorder              land border
     */
    public LandEntity(String id, LandType type, Integer trunkNumber, Integer underDivisionNumber,
                      Integer acreage, Boolean containsBuilding, Epsg5514Coordinate definitionPointEpsg5514,
                      Timestamp createdOn, Timestamp lastUpdatedOn, Timestamp removedOn,
                      Long crawlerResultId, AreaEntity area, LandBorderEntity landBorder) {
        this.id = id;
        this.type = type;
        this.trunkNumber = trunkNumber;
        this.underDivisionNumber = underDivisionNumber;
        this.acreage = acreage;
        this.containsBuilding = containsBuilding;
        this.definitionPointEpsg5514 = definitionPointEpsg5514;
        this.createdOn = createdOn;
        this.lastUpdatedOn = lastUpdatedOn;
        this.removedOn = removedOn;
        this.crawlerResultId = crawlerResultId;
        this.area = area;
        this.landBorder = landBorder;
    }

    /**
     * Gets land unique identifier.
     *
     * @return land unique identifier
     */
    public String getId() {
        return id;
    }

    /**
     * Gets land type
     *
     * @return land type
     */
    public LandType getType() {
        return type;
    }

    /**
     * Gets trunk number
     *
     * @return trunk number
     */
    public Integer getTrunkNumber() {
        return trunkNumber;
    }

    /**
     * Gets under division number
     *
     * @return under division number
     */
    public Integer getUnderDivisionNumber() {
        return underDivisionNumber;
    }

    /**
     * Gets acreage.
     *
     * @return acreage
     */
    public Integer getAcreage() {
        return acreage;
    }

    /**
     * Gets contains building.
     *
     * @return contains building
     */
    public Boolean containsBuilding() {
        return containsBuilding;
    }

    /**
     * Gets definition point EPSG 5514.
     *
     * @return definition point EPSG 5514
     */
    public Epsg5514Coordinate getDefinitionPointEpsg5514() {
        return definitionPointEpsg5514;
    }

    /**
     * Gets created on.
     *
     * @return created on
     */
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    /**
     * Gets last updated on.
     *
     * @return last updated on
     */
    public Timestamp getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    /**
     * Sets last updated on.
     *
     * @param lastUpdatedOn last updated on
     */
    public void setLastUpdatedOn(Timestamp lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    /**
     * Gets removed on.
     *
     * @return removed on
     */
    public Timestamp getRemovedOn() {
        return removedOn;
    }

    /**
     * Sets removed on.
     *
     * @param removedOn removed on
     */
    public void setRemovedOn(Timestamp removedOn) {
        this.removedOn = removedOn;
    }

    /**
     * Gets crawler result identifier.
     *
     * @return crawler result identifier
     */
    public Long getCrawlerResultId() {
        return crawlerResultId;
    }

    /**
     * Gets area.
     *
     * @return area
     */
    public AreaEntity getArea() {
        return area;
    }

    /**
     * Gets land border.
     *
     * @return land border
     */
    public LandBorderEntity getLandBorder() {
        return landBorder;
    }
}
