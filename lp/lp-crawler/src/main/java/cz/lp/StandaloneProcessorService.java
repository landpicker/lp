package cz.lp;

import com.google.common.collect.Lists;
import cz.lp.processor.standalone.StandaloneProcessor;
import cz.lp.processor.standalone.custom.AdMailRequestStandaloneProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Standalone processor service provides method for executing all registered (added into list) standalone processors.
 */
@Service
public class StandaloneProcessorService {

    private final List<StandaloneProcessor> standaloneProcessors;

    @Autowired
    public StandaloneProcessorService(AdMailRequestStandaloneProcessor adMailRequestStandaloneProcessor) {
        standaloneProcessors = Lists.newArrayList(adMailRequestStandaloneProcessor);
    }

    /**
     * Runs standalone processors.
     */
    public void runStandaloneProcessors() {

        standaloneProcessors.forEach(standaloneProcessor -> standaloneProcessor.execute());
    }
}
