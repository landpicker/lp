package cz.lp.enumerate;

/**
 * Crawler condition provides enumerate of conditions for execute crawler.
 */

public enum CrawlerCondition {

    NONE, EXIST_FIRST_LEVEL_AREA, EXIST_CITY_LEVEL_AREAS
}
