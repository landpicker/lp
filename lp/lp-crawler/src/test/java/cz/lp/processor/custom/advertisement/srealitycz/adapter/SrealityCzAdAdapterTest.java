package cz.lp.processor.custom.advertisement.srealitycz.adapter;

import com.google.common.collect.Sets;
import cz.lp.common.dto.ad.Ad;
import cz.lp.processor.custom.advertisement.bazoscz.adapter.BazosCzAdAdapter;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.time.LocalDate;

import static org.junit.Assert.*;

public class SrealityCzAdAdapterTest {

    private final static String SOURCE = "Sreality CZ";
    private final static String AD_URL = "https://www.sreality.cz/api/cs/v2/estates/2428632668";
    private final static String TITLE = "Dražba  louky 26 733 m², Bušín, okres Šumperk";
    private final static String CONTENT_START = "Jedná se o nemovitosti";
    private final static String CONTENT_END = "789 62 Bušín.";
    private final static LocalDate PUBLISHED_ON = LocalDate.now();
    private final static String FIRST_IMAGE_FILE_NAME = "251568639.jpg";
    private final static String FIRST_IMAGE_CONTENT_TYPE = "image/jpeg";


    @Test
    public void getAd() throws IOException {

        try(InputStream is = getClass().getClassLoader().getResourceAsStream("processor/srealitycz/ad.json")){

            byte[] content = IOUtils.toByteArray(is);
            SrealityCzAdAdapter adapter = new SrealityCzAdAdapter(new String(content, Charset.forName("utf-8")), AD_URL, SOURCE, Sets.newHashSet());
            assertTrue(adapter.getAd().isPresent());

            Ad ad = adapter.getAd().get();
            assertEquals(SOURCE, ad.getSource());
            assertEquals(AD_URL, ad.getUrl());
            assertEquals(TITLE, ad.getTitle());
            assertTrue(ad.getContent().startsWith(CONTENT_START));
            assertTrue(ad.getContent().endsWith(CONTENT_END));
            assertEquals(PUBLISHED_ON, ad.getPublishedOn());
            assertEquals(3, ad.getImages().size());
            assertEquals(FIRST_IMAGE_FILE_NAME, ad.getImages().get(0).getFileName());
            assertEquals(FIRST_IMAGE_CONTENT_TYPE, ad.getImages().get(0).getContentType());
        }
    }

}