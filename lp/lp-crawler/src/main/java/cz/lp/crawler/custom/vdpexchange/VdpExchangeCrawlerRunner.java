package cz.lp.crawler.custom.vdpexchange;

import cz.lp.CrawlerRunner;
import cz.lp.CrawlerScheduler;
import cz.lp.CrawlerWorkerService;
import cz.lp.StandaloneProcessorScheduler;
import cz.lp.crawler.custom.vdparea.VdpAreaCrawler;
import cz.lp.exception.CrawlerClassException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

/**
 * This class run only VdpAreaCrawler {@link VdpAreaCrawler}
 */

@ComponentScan(basePackages = "cz.lp", excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {CrawlerRunner.class, CrawlerScheduler.class, StandaloneProcessorScheduler.class}))
public class VdpExchangeCrawlerRunner {

    private static final Logger log = LoggerFactory.getLogger(VdpExchangeCrawlerRunner.class);

    private static final Long ID = 2l;

    public static void main(String[] args) throws CrawlerClassException {

        log.info("Starting VDP exchange crawler runner ...");
        ApplicationContext ctx = new AnnotationConfigApplicationContext(VdpExchangeCrawlerRunner.class);
        CrawlerWorkerService crawlerWorkerService = ctx.getBean(CrawlerWorkerService.class);
        VdpExchangeCrawler crawler = (VdpExchangeCrawler) crawlerWorkerService.getCrawler(ID);
        if (crawler.checkCondition()) {
            crawler.crawl();
        }

    }
}
