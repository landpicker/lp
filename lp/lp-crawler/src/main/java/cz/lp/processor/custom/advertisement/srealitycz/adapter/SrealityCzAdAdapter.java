package cz.lp.processor.custom.advertisement.srealitycz.adapter;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.lp.common.dto.ad.Ad;
import cz.lp.common.dto.ad.AdImage;
import cz.lp.processor.custom.advertisement.srealitycz.dto.SrealityCzEstateDetailDto;
import cz.lp.util.DownloadUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URLEncoder;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class SrealityCzAdAdapter {

    private static Logger log = LoggerFactory.getLogger(SrealityCzAdAdapter.class);

    private static final String TITLE_PATTERN = "%s, %s";
    private static final String IMAGE_FILE_NAME_PATTERN = "%d.jpg";
    private static final String IMAGE_URL_REPLACEMENT_PATTERN = "(?<=.*\\?fl=)(.*)$";
    private static final Pattern DETAIL_URL_ID_PATTERN = Pattern.compile("(?!(?i)https?://(?!www.)?sreality\\.cz/api/cs/v2/estates/)\\d+$");

    private Ad ad;

    public SrealityCzAdAdapter(String content, String url, String source, Set<String> siteMap) {

        ObjectMapper mapper = new ObjectMapper();
        try {
            SrealityCzEstateDetailDto detail = mapper.readValue(content, SrealityCzEstateDetailDto.class);
            String title = String.format(TITLE_PATTERN, detail.getName().getValue(), detail.getLocality().getValue());
            List<AdImage> images = detail.getEmbedded().getImages().stream()
                    .map(srealityCzImageDto -> {

                        try {
                            String imageHref = srealityCzImageDto.getLinks().getView().getHref();
                            Matcher matcher = Pattern.compile(IMAGE_URL_REPLACEMENT_PATTERN).matcher(imageHref);
                            if (matcher.find()) {
                                String urlWithEncodedParameter = imageHref.replaceFirst(IMAGE_URL_REPLACEMENT_PATTERN, URLEncoder.encode(matcher.group(0), "utf-8"));
                                DownloadUtil.DownloadedData data = DownloadUtil.getData(urlWithEncodedParameter);
                                return new AdImage(
                                        imageHref,
                                        String.format(IMAGE_FILE_NAME_PATTERN, srealityCzImageDto.getId()),
                                        data.getContentType(),
                                        data.getData());
                            } else {
                                return null;
                            }

                        } catch (IOException e) {
                            // TODO
                            return null;
                        }
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            ad = new Ad(mapToPrettyUrl(url, siteMap).orElse(url), source, title, detail.getText().getValue(), LocalDate.now(), images);
        } catch (IOException ex) {
            log.error("Unable to map response to SrealityCzEstateDetailDto object", ex);
        }

    }

    /**
     * Gets ad.
     *
     * @return ad
     */
    public Optional<Ad> getAd() {

        return Optional.ofNullable(ad);
    }

    /**
     * Map API URL to pretty (user friendly URL)
     * @param url API detail URL
     * @param siteMap site map
     * @return pretty URL
     */
    private Optional<String> mapToPrettyUrl(String url, Set<String> siteMap) {

        Matcher matcher = DETAIL_URL_ID_PATTERN.matcher(url);
        if (matcher.find()) {
            String id = matcher.group(0);
            return siteMap.stream().filter(s -> s.endsWith(id)).findFirst();
        }

        return Optional.empty();
    }

}
