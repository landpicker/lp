package cz.lp.common.repository;

import cz.lp.common.domain.ad.AdEntity;
import cz.lp.common.dto.ad.Ad;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Ad repository provides access to ad table.
 */
public interface AdRepository extends CrudRepository<AdEntity, Long> {

    @Query(value = "SELECT ad.* FROM ad " +
            "LEFT JOIN ad_mail_request ON ad.url = ad_mail_request.ad_url " +
            "WHERE ad_mail_request.mail_request_id IS NULL " +
            "ORDER BY ad.published_on ASC LIMIT 1", nativeQuery = true)
    AdEntity findOneReadyToCreateMailRequest();
}
