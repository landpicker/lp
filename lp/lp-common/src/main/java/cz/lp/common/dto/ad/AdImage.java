package cz.lp.common.dto.ad;

import cz.lp.common.domain.ad.AdImageEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class AdImage {

    private final String url;
    private final String fileName;
    private final String contentType;
    private final byte[] data;

    public static AdImage from(AdImageEntity adImageEntity) {
        return new AdImage(adImageEntity.getUrl(), adImageEntity.getFileName(), adImageEntity.getContentType(), adImageEntity.getData());
    }
}
