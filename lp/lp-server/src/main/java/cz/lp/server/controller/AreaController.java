package cz.lp.server.controller;

import cz.lp.server.dto.Area;
import cz.lp.server.service.AreaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static cz.lp.server.controller.ControllerUtil.setJsonResponseHeaders;

/**
 * Area REST API controller provides data about areas.
 */
@RestController
@RequestMapping("/api/areas")
public class AreaController {

    private static final Logger LOG = LoggerFactory.getLogger(AreaController.class);

    private final AreaService areaService;

    @Autowired
    public AreaController(AreaService areaService) {
        this.areaService = areaService;
    }

    @RequestMapping(value = "/tree", method = RequestMethod.GET)
    public List<Area> getAreaTree(HttpServletResponse response) {

        LOG.info("Request: Get areas tree.");
        setJsonResponseHeaders(response);
        final List<Area> areas = areaService.getAreaTree();
        LOG.info("Response: Get areas tree: {} areas were found.", areaService.getAreaOneLevelListFromTree(areas).size());
        return areas;
    }

    @RequestMapping(value = "/{code}", method = RequestMethod.GET)
    public Area getArea(@PathVariable("code") String code, HttpServletResponse response) {

        LOG.info("Request: Get get area by code: {}.", code);
        setJsonResponseHeaders(response);
        final Area area = areaService.getArea(code);
        LOG.info("Response: Get get area by code: {}: {}.", code, area);
        return area;
    }
}
