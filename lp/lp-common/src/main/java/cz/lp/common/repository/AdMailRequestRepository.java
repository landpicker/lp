package cz.lp.common.repository;

import cz.lp.common.domain.ad.AdMailRequestEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Ad mail request provides access to ad_mail_request table.
 */
public interface AdMailRequestRepository extends CrudRepository<AdMailRequestEntity, AdMailRequestEntity.AdMailRequestId> {

}
