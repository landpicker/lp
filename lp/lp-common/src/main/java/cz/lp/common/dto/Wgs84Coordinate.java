package cz.lp.common.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * WGS84 coordinate (EPSG4326).
 */
public class Wgs84Coordinate implements Serializable {

    public final Float lat;
    public final Float lon;

    /**
     * Creates instance of latitude and longitude
     *
     * @param lat latitude
     * @param lon longitude
     * @return WGS84 coordinate
     */
    @JsonCreator
    public Wgs84Coordinate(@JsonProperty("lat") Float lat, @JsonProperty("lon") Float lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public Float getLat() {
        return lat;
    }

    public Float getLon() {
        return lon;
    }

    /**
     * Creates instance of latitude and longitude
     *
     * @param lat latitude
     * @param lon longitude
     * @return WGS84 coordinate
     */
    public static Wgs84Coordinate of(Float lat, Float lon) {

        return new Wgs84Coordinate(lat, lon);
    }
}
