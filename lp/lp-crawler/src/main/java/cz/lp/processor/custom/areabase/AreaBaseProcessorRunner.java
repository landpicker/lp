package cz.lp.processor.custom.areabase;

import cz.lp.CrawlerRunner;
import cz.lp.CrawlerScheduler;
import cz.lp.CrawlerWorkerService;
import cz.lp.StandaloneProcessorScheduler;
import cz.lp.exception.ProcessorClassException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
/**
 * This class run only AreaBaseProcessor {@link AreaBaseProcessor}
 */
@ComponentScan(basePackages = "cz.lp", excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {CrawlerRunner.class, CrawlerScheduler.class, StandaloneProcessorScheduler.class}))
public class AreaBaseProcessorRunner {

    private static final Logger log = LoggerFactory.getLogger(AreaBaseProcessorRunner.class);

    private static final Long ID = 1l;

    public static void main(String[] args) throws ProcessorClassException {

        log.info("Starting Area base processor runner ...");
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(AreaBaseProcessorRunner.class);
        CrawlerWorkerService crawlerWorkerService = ctx.getBean(CrawlerWorkerService.class);
        AreaBaseProcessor processor = (AreaBaseProcessor) crawlerWorkerService.getProcessor(ID);
        if (processor.checkCondition()) {
            processor.process();
        } else {
            log.warn("Area base processor does not meet startup conditions");
        }
    }
}
