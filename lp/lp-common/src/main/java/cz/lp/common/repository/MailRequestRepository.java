package cz.lp.common.repository;

import cz.lp.common.domain.mail.MailRequestEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface MailRequestRepository extends CrudRepository<MailRequestEntity, Long>, JpaRepository<MailRequestEntity, Long> {

    @Query(value = "SELECT * FROM mail_request WHERE json_array_length(subscribers) <> 0 AND status = 'READY' ORDER BY created_on LIMIT 1", nativeQuery = true)
    MailRequestEntity findOneReadyToSendOrderByCreatedOn();
}
