package cz.lp.processor.custom.areabase.adapter;

import cz.lp.processor.custom.areabase.Area;
import cz.lp.processor.custom.areabase.filter.AreaBaseProcessorFilter;

import java.util.List;
import java.util.Optional;

public class AreaBaseDataAdapter {

    private final AreaBaseProcessorFilter filter;

    private static final String REGION_URL_PATTERN = "(?i)https?://(www.)?vdp\\.cuzk\\.cz/vdp/ruian/vusc/.*";
    private static final String DISTRICT_URL_PATTERN = "(?i)https?://(www.)?vdp\\.cuzk\\.cz/vdp/ruian/okresy/.*";
    private static final String CITY_URL_PATTERN = "(?i)https?://(www.)?vdp\\.cuzk\\.cz/vdp/ruian/obce/.*";
    private static final String CADASTRAL_TERRITORY_URL_PATTERN = "(?i)https?://(www.)?vdp\\.cuzk\\.cz/vdp/ruian/katastralniuzemi/.*";
    private Optional<List<Area>> areas;

    public AreaBaseDataAdapter(String content, String url, AreaBaseProcessorFilter filter) {

        this.filter = filter;

        if (url.matches(REGION_URL_PATTERN)) {
            AreaBaseRegionAdapter regionAdapter = new AreaBaseRegionAdapter(content, url, this.filter);
            areas = regionAdapter.getRegions();
        } else if (url.matches(DISTRICT_URL_PATTERN)) {
            AreaBaseDistrictAdapter districtAdapter = new AreaBaseDistrictAdapter(content, url, this.filter);
            areas = districtAdapter.getDistricts();
        } else if (url.matches(CITY_URL_PATTERN)) {
            AreaBaseCityAdapter districtAdapter = new AreaBaseCityAdapter(content, url, this.filter);
            areas = districtAdapter.getCities();
        } else if (url.matches(CADASTRAL_TERRITORY_URL_PATTERN)) {
            AreaBaseCadastralTerritoryAdapter districtAdapter = new AreaBaseCadastralTerritoryAdapter(content, url, this.filter);
            areas = districtAdapter.getCadastralTerritories();
        } else {
            System.out.println();
        }
    }

    /**
     * Gets areas.
     *
     * @return areas
     */
    public Optional<List<Area>> getAreas() {

        return areas;
    }
}
