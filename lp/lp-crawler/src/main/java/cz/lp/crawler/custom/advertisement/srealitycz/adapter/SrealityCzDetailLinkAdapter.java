package cz.lp.crawler.custom.advertisement.srealitycz.adapter;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.lp.crawler.custom.advertisement.srealitycz.dto.SrealityCzPageDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Sreality CZ detail link adapter provides method for converting content to links.
 */
public class SrealityCzDetailLinkAdapter {

    private static Logger log = LoggerFactory.getLogger(SrealityCzDetailLinkAdapter.class);

    private static final String DETAIL_LINK_PATTERN = "https://www.sreality.cz/api/cs/v2/estates/%d";

    private LinkedHashSet<String> links;

    public SrealityCzDetailLinkAdapter(String content) {

        ObjectMapper mapper = new ObjectMapper();
        try {
            extractLinks(mapper.readValue(content, SrealityCzPageDto.class));
        } catch (IOException ex) {
            log.error("Unable to map response to SrealityCzPageDto object", ex);
        }
    }

    /**
     * Extracts links.
     *
     * @param page page response object
     */
    private void extractLinks(SrealityCzPageDto page) {

        links = page.getEmbedded().getEstates().stream()
                .map(srealityEstateDto -> String.format(DETAIL_LINK_PATTERN, srealityEstateDto.getHashId()))
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    /**
     * Gets links.
     *
     * @return links
     */
    public Optional<Set<String>> getLinks() {

        if (Objects.isNull(links) || links.isEmpty()) {
            Optional.empty();
        }
        return Optional.of(links);
    }
}
