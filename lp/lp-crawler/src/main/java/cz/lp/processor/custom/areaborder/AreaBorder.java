package cz.lp.processor.custom.areaborder;

import cz.lp.common.dto.Epsg5514Coordinate;
import cz.lp.common.dto.Wgs84Coordinate;

import java.util.List;

public class AreaBorder {

    private final String code;
    private final List<Epsg5514Coordinate> epsg5514Coordinates;
    private final List<Wgs84Coordinate> wgs84Coordinates;

    public AreaBorder(String code, List<Epsg5514Coordinate> epsg5514Coordinates) {
        this.code = code;
        this.epsg5514Coordinates = epsg5514Coordinates;
        this.wgs84Coordinates = null;
    }

    /**
     * Gets area code.
     *
     * @return area code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets list of EPSG 5514 coordinates.
     *
     * @return EPSG 5514 coordinates
     */
    public List<Epsg5514Coordinate> getEpsg5514Coordinates() {
        return epsg5514Coordinates;
    }

    /**
     * Gets WGS 84 coordinates.
     *
     * @return WGS 84 coordinates
     */
    public List<Wgs84Coordinate> getWgs84Coordinates() {
        return wgs84Coordinates;
    }
}
