package cz.lp.processor.custom.areaborder;

import cz.lp.CrawlerRunner;
import cz.lp.CrawlerScheduler;
import cz.lp.CrawlerWorkerService;
import cz.lp.StandaloneProcessorScheduler;
import cz.lp.exception.ProcessorClassException;
import cz.lp.processor.custom.areabase.AreaBaseProcessor;
import cz.lp.processor.custom.areabase.AreaBaseProcessorRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

@ComponentScan(basePackages = "cz.lp", excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {CrawlerRunner.class, CrawlerScheduler.class, StandaloneProcessorScheduler.class}))
public class AreaBorderProcessorRunner {

    private static final Logger log = LoggerFactory.getLogger(AreaBorderProcessorRunner.class);

    private static final Long ID = 2l;

    public static void main(String[] args) throws ProcessorClassException {

        log.info("Starting Area border processor runner ...");
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(AreaBorderProcessorRunner.class);
        CrawlerWorkerService crawlerWorkerService = ctx.getBean(CrawlerWorkerService.class);
        AreaBorderProcessor processor = (AreaBorderProcessor) crawlerWorkerService.getProcessor(ID);
        if (processor.checkCondition()) {
            processor.process();
        } else {
            log.warn("Area border processor does not meet startup conditions");
        }
    }
}
