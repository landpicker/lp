package cz.lp.server.service;

import com.google.common.collect.Lists;
import cz.lp.common.domain.AreaEntity;
import cz.lp.common.enumerate.AreaType;
import cz.lp.common.repository.AreaRepository;
import cz.lp.server.dto.Area;
import cz.lp.server.exception.AreaNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/**
 * Area service provides functions for areas.
 */
@Service
public class AreaService {

    private final AreaRepository areaRepository;

    @Autowired
    public AreaService(AreaRepository areaRepository) {
        this.areaRepository = areaRepository;
    }

    /**
     * Gets tree areas.
     *
     * @return tree areas
     */
    public List<Area> getAreaTree() {

        return this.areaRepository.getAllFirstLevelAreas().stream()
                .map(Area::ofEntityWithoutBorder)
                .collect(Collectors.toList());
    }

    /**
     * Gets area one level list from area tree without sub areas.
     *
     * @param areaTree area tree
     * @return area one level list
     */
    public List<Area> getAreaOneLevelListFromTree(List<Area> areaTree) {

        List<Area> areaOneLevelList = Lists.newArrayList();
        areaTree.stream().forEach(area -> {
            areaOneLevelList.add(Area.ofWithoutSubAreas(area));
            if (nonNull(area.getSubAreas())) {
                areaOneLevelList.addAll(getAreaOneLevelListFromTree(area.getSubAreas()));
            }
        });
        return areaOneLevelList;
    }

    /**
     * Gets area.
     *
     * @param areaCode area code
     * @return area
     */
    public Area getArea(String areaCode) {

        final AreaEntity areaEntity = this.areaRepository.findOne(areaCode);
        if (isNull(areaEntity))
            throw new AreaNotFoundException(String.format("Area with code: %s not found", areaCode));
        return Area.ofEntityWithoutSubAreas(areaEntity);
    }

    /**
     * Gets only cadastral territory area codes
     *
     * @param areaCodes area codes
     * @return cadstral territory area codes
     */
    public Set<String> getOnlyCadastralTerritoryAreaCodes(Set<String> areaCodes) {

        final List<AreaEntity> areaEntities = areaRepository.getAreasByCodes(areaCodes);
        return extractCadastralTerritoryAreaEntities(areaEntities).stream()
                .map(AreaEntity::getCode)
                .collect(Collectors.toSet());
    }

    /**
     * Extracts cadastral territory area entities.
     *
     * @param areaEntities area entities
     * @return cadastral territory area entities
     */
    private List<AreaEntity> extractCadastralTerritoryAreaEntities(List<AreaEntity> areaEntities) {

        List<AreaEntity> cadastralTerritoryAreaEntities = Lists.newArrayList();
        areaEntities.forEach(areaEntity -> {
            if (areaEntity.getType() == AreaType.CADASTRAL_TERRITORY) {
                cadastralTerritoryAreaEntities.add(areaEntity);
            } else if (!areaEntity.getSubAreas().isEmpty()) {
                cadastralTerritoryAreaEntities.addAll(extractCadastralTerritoryAreaEntities(areaEntity.getSubAreas()));
            }
        });
        return cadastralTerritoryAreaEntities;
    }
}
