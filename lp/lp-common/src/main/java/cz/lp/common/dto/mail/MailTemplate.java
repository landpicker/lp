package cz.lp.common.dto.mail;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class MailTemplate {

    private final String subjectPattern;
    private final String contentPattern;
}
