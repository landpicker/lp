package cz.lp.common.repository.configuration;

import org.junit.Ignore;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

/**
 * Repository template provides configuration of test data source and configuration of SQL for calling SQL scripts before methods.
 */
@Ignore
@TestPropertySource(locations = "classpath:application-it.properties")
@ContextConfiguration(classes = {DataSourceIntegrationTestConfiguration.class})
@SqlConfig(dataSource = DataSourceIntegrationTestConfiguration.DATA_SOURCE, transactionManager = DataSourceIntegrationTestConfiguration.TRANSACTION_MANAGER, transactionMode = SqlConfig.TransactionMode.ISOLATED)
public class RepositoryTemplate extends AbstractTransactionalJUnit4SpringContextTests {

}