package cz.lp.server.repository;

import cz.lp.server.domain.LandSearchEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Land search repository provides methods for access to land search table.
 */
public interface LandSearchRepository extends JpaRepository<LandSearchEntity, String> {

    Optional<LandSearchEntity> findOneByLandSearchFilterHash(Integer landSearchFilterHash);
}
