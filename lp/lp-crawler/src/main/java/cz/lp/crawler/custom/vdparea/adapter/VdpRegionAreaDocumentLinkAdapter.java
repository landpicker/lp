package cz.lp.crawler.custom.vdparea.adapter;

import cz.lp.crawler.custom.vdparea.filter.VdpAreaCrawlerFilter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

/**
 * Vdp region area document adapter provides method for converting content to links.
 */
public class VdpRegionAreaDocumentLinkAdapter {

    private VdpAreaCrawlerFilter filter;

    private static final String DISTRICT_URL = "http://vdp.cuzk.cz/vdp/ruian/okresy/export?vc.kod=%s&kr.kod=&ok.nazev=&ok.kod=&ohrada.id=&okg.sort=KOD&export=XML";
    private static final String REGION_ITEM_ELEMENT_SELECTOR = "Vusc";
    private static final String REGION_CODE_ELEMENT_SELECTOR = "Kod";
    private LinkedHashSet<String> links;

    public VdpRegionAreaDocumentLinkAdapter(String content, String url, VdpAreaCrawlerFilter filter) {

        this.filter = filter;
        Document document = Jsoup.parse(content, url, Parser.xmlParser());
        extractLinks(document);
    }

    /**
     * Extracts links.
     *
     * @param document document
     */
    private void extractLinks(Document document) {

        links = document.select(REGION_ITEM_ELEMENT_SELECTOR).stream()
                .map(element -> element.select(REGION_CODE_ELEMENT_SELECTOR).first())
                .filter(Objects::nonNull)
                .map(element -> String.format(DISTRICT_URL, element.text()))
                .filter(link -> nonNull(filter) ? filter.isLinkAllowed(link) : true)
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    /**
     * Gets links.
     *
     * @return links
     */
    public Optional<Set<String>> getLinks() {

        if (Objects.isNull(links) || links.isEmpty()) {
            Optional.empty();
        }

        return Optional.of(links);
    }
}
