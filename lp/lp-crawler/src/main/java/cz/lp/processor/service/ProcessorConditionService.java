package cz.lp.processor.service;

import cz.lp.repository.CrawlerResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProcessorConditionService {

    private final CrawlerResultRepository crawlerResultRepository;

    @Autowired
    public ProcessorConditionService(CrawlerResultRepository crawlerResultRepository) {
        this.crawlerResultRepository = crawlerResultRepository;
    }

    /**
     * Gets information if results of Vdp area crawler are existing.
     *
     * @param crawlerId crawler identifier
     * @return true when results of Vdp area crawler are existing., otherwise false
     */
    public boolean existResultsOfVdpAreaCrawler(Long crawlerId) {

        return crawlerResultRepository.existResultsOfCrawler(crawlerId);
    }

    /**
     * Gets information if results of Vdp exchange crawler are existing.
     *
     * @param crawlerId crawler identifier
     * @return true when results of Vdp exchange crawler are existing., otherwise false
     */
    public boolean existResultsOfVdpExchangeCrawler(Long crawlerId) {

        return crawlerResultRepository.existResultsOfCrawler(crawlerId);
    }

    /**
     * Gets information if results of Bazos CZ crawler are existing.
     *
     * @param crawlerId crawler identifier
     * @return true when results of Bazos CZ crawler are existing., otherwise false
     */
    public boolean existResultsOfBazosCzCrawler(Long crawlerId) {

        return crawlerResultRepository.existResultsOfCrawler(crawlerId);
    }

    /**
     * Gets information if results of Sreality CZ crawler are existing.
     *
     * @param crawlerId crawler identifier
     * @return true when results of Sreality CZ crawler are existing., otherwise false
     */
    public boolean existResultsOfSrealityCzCrawler(Long crawlerId) {

        return crawlerResultRepository.existResultsOfCrawler(crawlerId);
    }
}
