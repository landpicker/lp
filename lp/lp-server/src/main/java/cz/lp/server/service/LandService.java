package cz.lp.server.service;

import com.google.common.collect.Lists;
import cz.lp.common.domain.LandEntity;
import cz.lp.common.repository.LandRepository;
import cz.lp.server.dto.Land;
import cz.lp.server.dto.LandSearchResult;
import cz.lp.server.dto.filter.LandSearchFilter;
import cz.lp.server.exception.LandNotFoundException;
import cz.lp.server.exception.LandSearchIdNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

/**
 * Land service provides functions for lands.
 */
@Service
public class LandService {

    private final LandRepository landRepository;
    private final AreaService areaService;
    private final LandSearchFilterService landSearchFilterService;

    @Autowired
    public LandService(LandRepository landRepository, AreaService areaService, LandSearchFilterService landSearchFilterService) {
        this.landRepository = landRepository;
        this.areaService = areaService;
        this.landSearchFilterService = landSearchFilterService;
    }

    /**
     * Gets expected count of lands by filter.
     *
     * @param landSearchFilter land search filter
     * @return expected count of lands
     */
    public long getExpectedCountOfLands(LandSearchFilter landSearchFilter) {

        final Set<String> cadastralTerritoryAreaCodes = areaService.getOnlyCadastralTerritoryAreaCodes(
                landSearchFilter.getAreaFilter().getCodes());
        if (cadastralTerritoryAreaCodes.isEmpty()) return 0;
        return landRepository.getCountOfLandsByFilter(cadastralTerritoryAreaCodes,
                landSearchFilter.getLandFilter().getLandTypeFilter().getLandTypes(),
                landSearchFilter.getLandFilter().getAcreageFilter().getFrom(),
                landSearchFilter.getLandFilter().getAcreageFilter().getTo());
    }

    /**
     * Gets lands by search identifier.
     *
     * @param searchId search identifier.
     * @param page     page number of land list
     * @param size     size of page (count of land per page)
     * @param searchId search identifier.
     * @return land search result
     */
    public LandSearchResult getLands(String searchId, int page, int size) {

        Optional<LandSearchFilter> landSearchFilter = landSearchFilterService.getLandSearchFilter(searchId);
        if (landSearchFilter.isPresent()) return getLands(landSearchFilter.get(), page, size);
        throw new LandSearchIdNotFoundException(String.format("Land search with id: %s not found", searchId));
    }

    /**
     * Gets lands by search land search filter.
     *
     * @param landSearchFilter search search filter.
     * @param page             page number of land list
     * @param size             size of page (count of land per page)
     * @return land search result
     */
    public LandSearchResult getLands(LandSearchFilter landSearchFilter, int page, int size) {

        final String searchId = landSearchFilterService.getSearchId(landSearchFilter);
        final Set<String> cadastralTerritoryAreaCodes = areaService.getOnlyCadastralTerritoryAreaCodes(
                landSearchFilter.getAreaFilter().getCodes());
        if(cadastralTerritoryAreaCodes.isEmpty()) return LandSearchResult.of(searchId, landSearchFilter, Lists.newArrayList(), 0, 0, 0l);
        final Page<LandEntity> landPage = landRepository.getLandsByFilter(cadastralTerritoryAreaCodes,
                landSearchFilter.getLandFilter().getLandTypeFilter().getLandTypes(),
                landSearchFilter.getLandFilter().getAcreageFilter().getFrom(),
                landSearchFilter.getLandFilter().getAcreageFilter().getTo(), new PageRequest(page, size));
        final List<Land> lands = landPage.getContent().stream().map(Land::assembleOfEntityWithoutBorder)
                .collect(Collectors.toList());
        return LandSearchResult.of(searchId, landSearchFilter, lands, landPage.getNumber(), landPage.getTotalPages(), landPage.getTotalElements());
    }


    /**
     * Gets land.
     *
     * @param id unique land identifier
     * @return land
     */
    public Land getLand(String id) {

        final LandEntity landEntity = landRepository.findOne(id);
        if (isNull(landEntity)) throw new LandNotFoundException(String.format("Land with id: %s not found", id));
        return Land.assembleOfEntity(landEntity);
    }
}
