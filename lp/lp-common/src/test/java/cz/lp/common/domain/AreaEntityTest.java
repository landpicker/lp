package cz.lp.common.domain;

import com.google.common.collect.Lists;
import cz.lp.common.enumerate.AreaType;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.sql.Timestamp;
import java.util.List;

public class AreaEntityTest {

    private static final String CODE = "1234";
    private static final AreaType TYPE = AreaType.REGION;
    private static final String NAME = "Zlinsky";
    private static final AreaEntity PARENT_AREA = Mockito.mock(AreaEntity.class);
    private static final List<AreaEntity> SUB_AREAS = Lists.newArrayList(Mockito.mock(AreaEntity.class));
    private static final Long CRAWLER_RESULT_ID = null;
    private static final Timestamp CREATED_ON = Timestamp.valueOf("2018-01-01 00:00:01.000000");
    private static final Timestamp LAST_UPDATED_ON = Timestamp.valueOf("2018-02-01 00:00:01.000000");
    private static final Timestamp REMOVED_ON = Timestamp.valueOf("2018-01-03 00:00:01.000000");
    private static final AreaBorderEntity AREA_BORDER = Mockito.mock(AreaBorderEntity.class);

    @Test
    public void constructor_empty() throws Exception {

        AreaEntity area = new AreaEntity();
        Assert.assertNotNull(area);
        Assert.assertNull(area.getCode());
        Assert.assertNull(area.getType());
        Assert.assertNull(area.getName());
        Assert.assertNull(area.getParentArea());
        Assert.assertNull(area.getSubAreas());
        Assert.assertNull(area.getCrawlerResultId());
        Assert.assertNull(area.getCreatedOn());
        Assert.assertNull(area.getLastUpdatedOn());
        Assert.assertNull(area.getRemovedOn());
    }

    @Test
    public void constructor() throws Exception {

        AreaEntity area = new AreaEntity(CODE, TYPE, NAME, PARENT_AREA, SUB_AREAS, CRAWLER_RESULT_ID, CREATED_ON, LAST_UPDATED_ON, REMOVED_ON, AREA_BORDER);
        Assert.assertNotNull(area);
        Assert.assertEquals(CODE, area.getCode());
        Assert.assertEquals(TYPE, area.getType());
        Assert.assertEquals(NAME, area.getName());
        Assert.assertEquals(PARENT_AREA, area.getParentArea());
        Assert.assertEquals(SUB_AREAS, area.getSubAreas());
        Assert.assertEquals(CRAWLER_RESULT_ID, area.getCrawlerResultId());
        Assert.assertEquals(CREATED_ON, area.getCreatedOn());
        Assert.assertEquals(LAST_UPDATED_ON, area.getLastUpdatedOn());
        Assert.assertEquals(REMOVED_ON, area.getRemovedOn());
        Assert.assertEquals(AREA_BORDER, area.getAreaBorder());
    }

    @Test
    public void constructor_withoutAreaBorder() throws Exception {

        AreaEntity area = new AreaEntity(CODE, TYPE, NAME, PARENT_AREA, SUB_AREAS, CRAWLER_RESULT_ID, CREATED_ON, LAST_UPDATED_ON, REMOVED_ON);
        Assert.assertNotNull(area);
        Assert.assertEquals(CODE, area.getCode());
        Assert.assertEquals(TYPE, area.getType());
        Assert.assertEquals(NAME, area.getName());
        Assert.assertEquals(PARENT_AREA, area.getParentArea());
        Assert.assertEquals(SUB_AREAS, area.getSubAreas());
        Assert.assertEquals(CRAWLER_RESULT_ID, area.getCrawlerResultId());
        Assert.assertEquals(CREATED_ON, area.getCreatedOn());
        Assert.assertEquals(LAST_UPDATED_ON, area.getLastUpdatedOn());
        Assert.assertEquals(REMOVED_ON, area.getRemovedOn());
    }

    @Test
    public void constructor_withoutRemovedOnAndAreaBorder() throws Exception {

        AreaEntity area = new AreaEntity(CODE, TYPE, NAME, PARENT_AREA, SUB_AREAS, CRAWLER_RESULT_ID, CREATED_ON, LAST_UPDATED_ON);
        Assert.assertNotNull(area);
        Assert.assertEquals(CODE, area.getCode());
        Assert.assertEquals(TYPE, area.getType());
        Assert.assertEquals(NAME, area.getName());
        Assert.assertEquals(PARENT_AREA, area.getParentArea());
        Assert.assertEquals(SUB_AREAS, area.getSubAreas());
        Assert.assertEquals(CRAWLER_RESULT_ID, area.getCrawlerResultId());
        Assert.assertEquals(CREATED_ON, area.getCreatedOn());
        Assert.assertEquals(LAST_UPDATED_ON, area.getLastUpdatedOn());
        Assert.assertNull(area.getRemovedOn());
    }

    @Test
    public void getCode() throws Exception {

        AreaEntity area = new AreaEntity(CODE, TYPE, NAME, PARENT_AREA, SUB_AREAS, CRAWLER_RESULT_ID, CREATED_ON, LAST_UPDATED_ON, REMOVED_ON, AREA_BORDER);
        Assert.assertNotNull(area);
        Assert.assertEquals(CODE, area.getCode());
    }

    @Test
    public void getType() throws Exception {

        AreaEntity area = new AreaEntity(CODE, TYPE, NAME, PARENT_AREA, SUB_AREAS, CRAWLER_RESULT_ID, CREATED_ON, LAST_UPDATED_ON, REMOVED_ON, AREA_BORDER);
        Assert.assertNotNull(area);
        Assert.assertEquals(TYPE, area.getType());
    }

    @Test
    public void getName() throws Exception {

        AreaEntity area = new AreaEntity(CODE, TYPE, NAME, PARENT_AREA, SUB_AREAS, CRAWLER_RESULT_ID, CREATED_ON, LAST_UPDATED_ON, REMOVED_ON, AREA_BORDER);
        Assert.assertNotNull(area);
        Assert.assertEquals(NAME, area.getName());
    }

    @Test
    public void setName() throws Exception {

        AreaEntity area = new AreaEntity(CODE, TYPE, NAME, PARENT_AREA, SUB_AREAS, CRAWLER_RESULT_ID, CREATED_ON, LAST_UPDATED_ON, REMOVED_ON, AREA_BORDER);
        Assert.assertNotNull(area);
        Assert.assertEquals(NAME, area.getName());
        String newName = "name";
        area.setName(newName);
        Assert.assertEquals(newName, area.getName());
    }

    @Test
    public void getParentArea() throws Exception {

        AreaEntity area = new AreaEntity(CODE, TYPE, NAME, PARENT_AREA, SUB_AREAS, CRAWLER_RESULT_ID, CREATED_ON, LAST_UPDATED_ON, REMOVED_ON, AREA_BORDER);
        Assert.assertNotNull(area);
        Assert.assertEquals(PARENT_AREA, area.getParentArea());
    }

    @Test
    public void getSubAreas() throws Exception {

        AreaEntity area = new AreaEntity(CODE, TYPE, NAME, PARENT_AREA, SUB_AREAS, CRAWLER_RESULT_ID, CREATED_ON, LAST_UPDATED_ON, REMOVED_ON, AREA_BORDER);
        Assert.assertNotNull(area);
        Assert.assertEquals(SUB_AREAS, area.getSubAreas());
    }

    @Test
    public void getCrawlerResultId() throws Exception {

        AreaEntity area = new AreaEntity(CODE, TYPE, NAME, PARENT_AREA, SUB_AREAS, CRAWLER_RESULT_ID, CREATED_ON, LAST_UPDATED_ON, REMOVED_ON, AREA_BORDER);
        Assert.assertNotNull(area);
        Assert.assertEquals(CRAWLER_RESULT_ID, area.getCrawlerResultId());
    }

    @Test
    public void getCreatedOn() throws Exception {

        AreaEntity area = new AreaEntity(CODE, TYPE, NAME, PARENT_AREA, SUB_AREAS, CRAWLER_RESULT_ID, CREATED_ON, LAST_UPDATED_ON, REMOVED_ON, AREA_BORDER);
        Assert.assertNotNull(area);
        Assert.assertEquals(CREATED_ON, area.getCreatedOn());
    }

    @Test
    public void getLastUpdatedOn() throws Exception {

        AreaEntity area = new AreaEntity(CODE, TYPE, NAME, PARENT_AREA, SUB_AREAS, CRAWLER_RESULT_ID, CREATED_ON, LAST_UPDATED_ON, REMOVED_ON, AREA_BORDER);
        Assert.assertNotNull(area);
        Assert.assertEquals(LAST_UPDATED_ON, area.getLastUpdatedOn());
    }

    @Test
    public void setLastUpdatedOn() throws Exception {

        AreaEntity area = new AreaEntity(CODE, TYPE, NAME, PARENT_AREA, SUB_AREAS, CRAWLER_RESULT_ID, CREATED_ON, LAST_UPDATED_ON, REMOVED_ON, AREA_BORDER);
        Assert.assertNotNull(area);
        Assert.assertEquals(LAST_UPDATED_ON, area.getLastUpdatedOn());
        Timestamp newLastUpdatedOn = Timestamp.valueOf("2021-02-01 00:00:01.000000");
        area.setLastUpdatedOn(newLastUpdatedOn);
        Assert.assertEquals(newLastUpdatedOn, area.getLastUpdatedOn());
    }

    @Test
    public void getRemovedOn() throws Exception {

        AreaEntity area = new AreaEntity(CODE, TYPE, NAME, PARENT_AREA, SUB_AREAS, CRAWLER_RESULT_ID, CREATED_ON, LAST_UPDATED_ON, REMOVED_ON, AREA_BORDER);
        Assert.assertNotNull(area);
        Assert.assertEquals(REMOVED_ON, area.getRemovedOn());
    }

    @Test
    public void setRemovedOn() throws Exception {

        AreaEntity area = new AreaEntity(CODE, TYPE, NAME, PARENT_AREA, SUB_AREAS, CRAWLER_RESULT_ID, CREATED_ON, LAST_UPDATED_ON, REMOVED_ON, AREA_BORDER);
        Assert.assertNotNull(area);
        Assert.assertEquals(REMOVED_ON, area.getRemovedOn());
        Timestamp newRemovedUpdatedOn = Timestamp.valueOf("2021-02-01 00:00:01.000000");
        area.setRemovedOn(newRemovedUpdatedOn);
        Assert.assertEquals(newRemovedUpdatedOn, area.getRemovedOn());
    }

    @Test
    public void getAreaBorder() throws Exception {

        AreaEntity area = new AreaEntity(CODE, TYPE, NAME, PARENT_AREA, SUB_AREAS, CRAWLER_RESULT_ID, CREATED_ON, LAST_UPDATED_ON, REMOVED_ON, AREA_BORDER);
        Assert.assertNotNull(area);
        Assert.assertEquals(AREA_BORDER, area.getAreaBorder());
    }
}