package cz.lp.processor.service;

import cz.lp.common.repository.AreaBorderRepository;
import cz.lp.common.repository.AreaRepository;
import cz.lp.common.repository.LandBorderRepository;
import cz.lp.common.repository.LandRepository;
import cz.lp.common.repository.AdRepository;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Processor repository service aggregates repositories.
 */
@Service
@Getter
public class ProcessorRepositoryService {

    private final AreaRepository areaRepository;
    private final AreaBorderRepository areaBorderRepository;
    private final LandRepository landRepository;
    private final LandBorderRepository landBorderRepository;
    private final AdRepository adRepository;

    @Autowired
    public ProcessorRepositoryService(AreaRepository areaRepository, AreaBorderRepository areaBorderRepository, LandRepository landRepository, LandBorderRepository landBorderRepository, AdRepository adRepository) {
        this.areaRepository = areaRepository;
        this.areaBorderRepository = areaBorderRepository;
        this.landRepository = landRepository;
        this.landBorderRepository = landBorderRepository;
        this.adRepository = adRepository;
    }
}
