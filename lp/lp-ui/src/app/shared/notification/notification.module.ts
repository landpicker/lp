import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationSnackbarViewComponent } from './notification-snackbar-view/notification-snackbar-view.component';
import { MatSnackBarModule } from '@angular/material';

@NgModule({
	imports: [CommonModule, MatSnackBarModule],
	entryComponents: [NotificationSnackbarViewComponent],
	declarations: [NotificationSnackbarViewComponent]
})
export class NotificationModule {}
