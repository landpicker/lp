import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationSnackbarViewComponent } from './notification-snackbar-view.component';
import { MatSnackBarModule, MatSnackBarRef, MAT_SNACK_BAR_DATA } from '@angular/material';

describe('NotificationViewComponent', () => {
  let component: NotificationSnackbarViewComponent;
  let fixture: ComponentFixture<NotificationSnackbarViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MatSnackBarModule],
      providers: [{
        provide: MatSnackBarRef,
        useValue: {}
        }, {
        provide: MAT_SNACK_BAR_DATA,
        useValue: {} // Add any data you wish to test if it is passed/used correctly
        }],
      declarations: [ NotificationSnackbarViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationSnackbarViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
