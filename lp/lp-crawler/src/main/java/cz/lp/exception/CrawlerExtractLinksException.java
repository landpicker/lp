package cz.lp.exception;

/**
 * Crawler extract links exception is generated when extraction of links failed.
 */
public class CrawlerExtractLinksException extends Exception {

    public CrawlerExtractLinksException() {
        super();
    }

    public CrawlerExtractLinksException(String message) {
        super(message);
    }

    public CrawlerExtractLinksException(String message, Throwable cause) {
        super(message, cause);
    }

    public CrawlerExtractLinksException(Throwable cause) {
        super(cause);
    }
}
