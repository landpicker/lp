package cz.lp.crawler.custom.vdpexchange.adapter;

import java.io.*;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VdpExchangeGzLinkAdapter {

    private static final Pattern GZ_LINK_PATTERN = Pattern.compile("https?://(?:www.)?vdp\\.cuzk\\.cz/vymenny_format/soucasna/\\d+_OB_\\d+\\w+\\.xml\\.gz");

    private LinkedHashSet<String> links;

    public VdpExchangeGzLinkAdapter(String content, String url) {

        extractLinks(content);
    }

    /**
     * Extracts links.
     *
     * @param content content
     */
    private void extractLinks(String content) {

        links = new LinkedHashSet<>();

        Matcher matcher = GZ_LINK_PATTERN.matcher(content);
        while(matcher.find()){
            links.add(matcher.group(0));
        }
    }

    /**
     * Gets links.
     *
     * @return links
     */
    public Optional<Set<String>> getLinks() {

        if (Objects.isNull(links) || links.isEmpty()) {
            Optional.empty();
        }
        return Optional.of(links);
    }
}
