import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AreaFilterComponent } from './area-filter.component';
import { BrowserAnimationsModule } from '../../../../node_modules/@angular/platform-browser/animations';
import { TranslateModule } from '../../../../node_modules/@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '../../../../node_modules/@angular/forms';
import { MatFormFieldModule, MatInputModule, MatAutocompleteModule, MatChipsModule, MatIconModule } from '../../../../node_modules/@angular/material';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    MatChipsModule,
    MatIconModule
  ],
  exports: [AreaFilterComponent],
  declarations: [AreaFilterComponent]
})
export class AreaFilterModule { }
