package cz.lp.domain;

import cz.lp.enumerate.ProcessorCondition;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.sql.Timestamp;

public class ProcessorEntityTest {

    private static final Long ID = 1l;
    private static final CrawlerEntity CRAWLER = Mockito.mock(CrawlerEntity.class);
    private static final String PROCESSING_CLASS = "CLASS";
    private static final ProcessorCondition CONDITION = ProcessorCondition.NONE;
    private static final String URL_PATTERN = ".*";
    private static final Boolean ENABLED = Boolean.TRUE;
    private static final Timestamp LAST_PROCESSED_ON = Mockito.mock(Timestamp.class);

    @Test
    public void constructor_empty() throws Exception {

        ProcessorEntity processor = new ProcessorEntity();
        Assert.assertNotNull(processor);
        Assert.assertNull(processor.getId());
        Assert.assertNull(processor.getCrawler());
        Assert.assertNull(processor.getProcessorClass());
        Assert.assertNull(processor.getCondition());
        Assert.assertNull(processor.getLastProcessedOn());
    }

    @Test
    public void constructor() throws Exception {

        ProcessorEntity processor = new ProcessorEntity(ID, CRAWLER, PROCESSING_CLASS, CONDITION, URL_PATTERN, ENABLED, LAST_PROCESSED_ON);
        Assert.assertNotNull(processor);
        Assert.assertEquals(ID, processor.getId());
        Assert.assertEquals(CRAWLER, processor.getCrawler());
        Assert.assertEquals(PROCESSING_CLASS, processor.getProcessorClass());
        Assert.assertEquals(CONDITION, processor.getCondition());
        Assert.assertEquals(ENABLED, processor.isEnabled());
        Assert.assertEquals(LAST_PROCESSED_ON, processor.getLastProcessedOn());
    }

    @Test
    public void getId() throws Exception {

        ProcessorEntity processor = new ProcessorEntity(ID, CRAWLER, PROCESSING_CLASS, CONDITION, URL_PATTERN, ENABLED, LAST_PROCESSED_ON);
        Assert.assertNotNull(processor);
        Assert.assertEquals(ID, processor.getId());
    }

    @Test
    public void getCrawler() throws Exception {

        ProcessorEntity processor = new ProcessorEntity(ID, CRAWLER, PROCESSING_CLASS, CONDITION, URL_PATTERN, ENABLED, LAST_PROCESSED_ON);
        Assert.assertNotNull(processor);
        Assert.assertEquals(CRAWLER, processor.getCrawler());
    }

    @Test
    public void getProcessorClass() throws Exception {

        ProcessorEntity processor = new ProcessorEntity(ID, CRAWLER, PROCESSING_CLASS, CONDITION, URL_PATTERN, ENABLED, LAST_PROCESSED_ON);
        Assert.assertNotNull(processor);
        Assert.assertEquals(PROCESSING_CLASS, processor.getProcessorClass());
    }

    @Test
    public void getCondition() throws Exception {

        ProcessorEntity processor = new ProcessorEntity(ID, CRAWLER, PROCESSING_CLASS, CONDITION, URL_PATTERN, ENABLED, LAST_PROCESSED_ON);
        Assert.assertNotNull(processor);
        Assert.assertEquals(CONDITION, processor.getCondition());
    }

    @Test
    public void getEnabled() throws Exception {

        ProcessorEntity processor = new ProcessorEntity(ID, CRAWLER, PROCESSING_CLASS, CONDITION, URL_PATTERN, ENABLED, LAST_PROCESSED_ON);
        Assert.assertNotNull(processor);
        Assert.assertEquals(ENABLED, processor.isEnabled());
    }

    @Test
    public void setEnabled() throws Exception {

        ProcessorEntity processor = new ProcessorEntity(ID, CRAWLER, PROCESSING_CLASS, CONDITION, URL_PATTERN, ENABLED, LAST_PROCESSED_ON);
        Assert.assertNotNull(processor);
        Assert.assertEquals(true, processor.isEnabled());
        Boolean newEnabled = false;
        processor.setEnabled(newEnabled);
        Assert.assertEquals(newEnabled, processor.isEnabled());
    }

    @Test
    public void getLastProcessedOn() throws Exception {

        ProcessorEntity processor = new ProcessorEntity(ID, CRAWLER, PROCESSING_CLASS, CONDITION, URL_PATTERN, ENABLED, LAST_PROCESSED_ON);
        Assert.assertNotNull(processor);
        Assert.assertEquals(LAST_PROCESSED_ON, processor.getLastProcessedOn());
    }

    @Test
    public void setLastProcessedOn() throws Exception {

        ProcessorEntity processor = new ProcessorEntity();
        Assert.assertNotNull(processor);
        Assert.assertNull(processor.getLastProcessedOn());
        processor.setLastProcessedOn(LAST_PROCESSED_ON);
        Assert.assertEquals(LAST_PROCESSED_ON, processor.getLastProcessedOn());
    }
}