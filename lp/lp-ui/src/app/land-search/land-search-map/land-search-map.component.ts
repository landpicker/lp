import { Component, OnInit, Input } from '@angular/core';
import { Land } from '../land-search.interface';
import { MapPolygon } from 'src/app/shared/map/map.interface';

@Component({
	selector: 'lp-land-search-map',
	templateUrl: './land-search-map.component.html',
	styleUrls: ['./land-search-map.component.scss']
})
export class LandSearchMapComponent implements OnInit {
	polygons: MapPolygon[];
	private _lands: Land[];

	@Input()
	set lands(lands: Land[]) {
		this._lands = lands;
    this.polygons = this.getPolygons();
	}
	get lands() {
		return this._lands;
	}

	constructor() {}

	ngOnInit() {}

	getPolygons(): MapPolygon[] {
		if (this.lands) {
			return this.lands.map((land) => {
				return { id: land.id, definitionPoint: { coordinate: land.definitionPointCoordinate }, border: { coordinates: land.borderCoordinates } } as MapPolygon;
			});
		} else {
			return undefined;
		}
	}
}
