package cz.lp.crawler.custom.advertisement.srealitycz;

import com.google.common.collect.Sets;
import cz.lp.crawler.Crawler;
import cz.lp.crawler.CrawlerFilter;
import cz.lp.crawler.SeedUrl;
import cz.lp.crawler.custom.advertisement.srealitycz.adapter.SrealityCzDetailLinkAdapter;
import cz.lp.crawler.custom.advertisement.srealitycz.adapter.SrealityCzPageLinkAdapter;
import cz.lp.crawler.resolver.CrawlerConditionResolver;
import cz.lp.crawler.service.CrawlerService;
import cz.lp.enumerate.CrawlerCondition;
import cz.lp.enumerate.RecrawlStrategy;
import org.apache.commons.codec.digest.DigestUtils;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;

/**
 * Sreality cz crawler provides crawling advertisement of lands and buildings from https://sreality.cz.
 */
public class SrealityCzCrawler extends Crawler {

    private static final String PAGE_URL_PATTERN = "(?i)https?://(www.)?sreality\\.cz/api/cs/v2/estates\\?.*&page=.*";
    private static final String PAGE_FIRST_PATTERN = ".*page=1.*";

    public SrealityCzCrawler(String className, Long id, CrawlerCondition condition, Integer delay, RecrawlStrategy recrawlStrategy, String recrawlUrlPattern, Long recrawlInterval, LocalDateTime recrawlOn, Set<SeedUrl> seedUrls, CrawlerService crawlerService, CrawlerConditionResolver crawlerConditionResolver, CrawlerFilter crawlerFilter) {
        super(className, id, condition, delay, recrawlStrategy, recrawlUrlPattern, recrawlInterval, recrawlOn, seedUrls, crawlerService, crawlerConditionResolver, crawlerFilter);
    }

    @Override
    protected Optional<Set<String>> parseLinks(String content, String url) {

        Set<String> links = Sets.newHashSet();
        if (url.matches(PAGE_URL_PATTERN)) {
            new SrealityCzDetailLinkAdapter(content).getLinks().ifPresent(links::addAll);
            if (url.matches(PAGE_FIRST_PATTERN)) {
                new SrealityCzPageLinkAdapter(content, url).getLinks().ifPresent(links::addAll);
            }
        }
        return Optional.of(links);
    }

    @Override
    protected String generateContentHash(byte[] content, String url) {

        return DigestUtils.md5Hex(content).toUpperCase();
    }
}
