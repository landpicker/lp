package cz.lp.crawler.custom.advertisement.srealitycz.adapter;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.lp.crawler.custom.advertisement.srealitycz.dto.SrealityCzPageDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Sreality CZ page link adapter provides method for converting content to links.
 */
public class SrealityCzPageLinkAdapter {

    private static Logger log = LoggerFactory.getLogger(SrealityCzDetailLinkAdapter.class);

    private static String PAGE_REPLACEMENT_PATTERN = "(?<=.*&page=)([\\d]+)(?<=.*)";
    private LinkedHashSet<String> links;

    public SrealityCzPageLinkAdapter(String content, String url) {

        ObjectMapper mapper = new ObjectMapper();
        try {
            extractLinks(mapper.readValue(content, SrealityCzPageDto.class), url);
        } catch (IOException ex) {
            log.error("Unable to map response to SrealityCzPageDto object", ex);
        }
    }

    /**
     * Extracts links.
     *
     * @param page page response object
     * @param url url
     */
    private void extractLinks(SrealityCzPageDto page, String url) {

        int pageCount = Math.floorDiv(page.getResultSize(), page.getPerPage()) + 1;
        links = IntStream.rangeClosed(2, pageCount)
                .mapToObj(pageNumber -> url.replaceFirst(PAGE_REPLACEMENT_PATTERN, String.valueOf(pageNumber)))
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    /**
     * Gets links.
     *
     * @return links
     */
    public Optional<Set<String>> getLinks() {

        if (Objects.isNull(links) || links.isEmpty()) {
            Optional.empty();
        }
        return Optional.of(links);
    }
}
