package cz.lp.common.service.ad;

import cz.lp.common.dto.ad.Ad;

import java.util.List;
import java.util.Optional;

public interface AdService {

    Optional<Ad> getNextAdReadyToCreateMailRequest();

    void markAdWithCreatedMailRequest(String adUrl, Long mailRequestId);
}
