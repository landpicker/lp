package cz.lp.exception;

/**
 * Crawler fetch exception is generated when something during fetching URL failed.
 */
public class CrawlerFetchException extends Exception {

    public CrawlerFetchException(String message) {
        super(message);
    }

    public CrawlerFetchException(String message, Throwable cause) {
        super(message, cause);
    }

    public CrawlerFetchException(Throwable cause) {
        super(cause);
    }
}
