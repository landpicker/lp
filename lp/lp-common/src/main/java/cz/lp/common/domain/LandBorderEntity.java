package cz.lp.common.domain;

import cz.lp.common.dto.Epsg5514Coordinate;
import cz.lp.common.dto.Wgs84Coordinate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Land border entity represents land border with EPSG5514 and WGS84 coordinates in database table.
 */

@Entity
@Table(name = "land_border")
public class LandBorderEntity implements Serializable {

    @Id
    @SequenceGenerator(name = "id", sequenceName = "land_border_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id")
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Type(type = "JsonList", parameters = {
            @org.hibernate.annotations.Parameter(name = "type", value = "cz.lp.common.dto.Epsg5514Coordinate")
    })
    @Column(name = "epsg5514", nullable = false)
    private List<Epsg5514Coordinate> epsg5514Coordinates;

    @Type(type = "JsonList", parameters = {
            @org.hibernate.annotations.Parameter(name = "type", value = "cz.lp.common.dto.Wgs84Coordinate")
    })
    @Column(name = "wgs84")
    private List<Wgs84Coordinate> wgs84Coordinates;


    /**
     * Creates instance of land border entity.
     */
    public LandBorderEntity() {
    }

    /**
     * Creates instance of land border entity
     *
     * @param id                  identifier
     * @param epsg5514Coordinates WPSG5514 coordinates in JSON
     * @param wgs84Coordinates    WGS84 coordinates in JSON
     */
    public LandBorderEntity(Long id, List<Epsg5514Coordinate> epsg5514Coordinates, List<Wgs84Coordinate> wgs84Coordinates) {
        this.id = id;
        this.epsg5514Coordinates = epsg5514Coordinates;
        this.wgs84Coordinates = wgs84Coordinates;
    }

    /**
     * Creates instance of land border entity
     *
     * @param epsg5514Coordinates WPSG5514 coordinates in JSON
     * @param wgs84Coordinates    WGS84 coordinates in JSON
     */
    public LandBorderEntity(List<Epsg5514Coordinate> epsg5514Coordinates, List<Wgs84Coordinate> wgs84Coordinates) {
        this.epsg5514Coordinates = epsg5514Coordinates;
        this.wgs84Coordinates = wgs84Coordinates;
    }

    /**
     * Gets identifier.
     *
     * @return identifier
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets EPSG5514 coordinates in JSON format.
     *
     * @return EPSG5514 coordinates
     */
    public List<Epsg5514Coordinate> getEpsg5514Coordinates() {
        return epsg5514Coordinates;
    }

    /**
     * Sets EPSG 5514 coordinates in JSON format.
     *
     * @param epsg5514Coordinates EPSG 5514 coordinates
     */
    public void setEpsg5514Coordinates(List<Epsg5514Coordinate> epsg5514Coordinates) {
        this.epsg5514Coordinates = epsg5514Coordinates;
    }

    /**
     * Gets WGS84 coordinates inf JSON format.
     *
     * @return WGS84 coordinates
     */
    public List<Wgs84Coordinate> getWgs84Coordinates() {
        return wgs84Coordinates;
    }

    /**
     * Sets WGS84 coordinates.
     *
     * @param wgs84Coordinates WGS84 coordinates
     */
    public void setWgs84Coordinates(List<Wgs84Coordinate> wgs84Coordinates) {
        this.wgs84Coordinates = wgs84Coordinates;
    }
}
