package cz.lp.exception;

/**
 * CrawlerEntity HTTP client communication exception is generated if something during sending HTTP request failed.
 */
public class CrawlerHttpClientCommunicationException extends Exception {

    private final Integer status;

    public CrawlerHttpClientCommunicationException(String message) {
        super(message);
        this.status = null;
    }

    public CrawlerHttpClientCommunicationException(String message, int status) {
        super(message);
        this.status = status;
    }

    public CrawlerHttpClientCommunicationException(String message, Throwable cause) {
        super(message, cause);
        this.status = null;
    }

    public CrawlerHttpClientCommunicationException(String message, Throwable cause, int status) {
        super(message, cause);
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }
}
