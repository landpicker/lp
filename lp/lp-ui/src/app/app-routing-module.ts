import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandSearchComponent } from './land-search/land-search.component';
import { PageNotFoundComponent } from './app-config/page-not-found/page-not-found.component';

const routes: Routes = [
	{
		path: '',
		redirectTo: '/land/search',
		pathMatch: 'full'
  },
  {
		path: 'land/search',
		component: LandSearchComponent
  },
	{
		path: '**',
		component: PageNotFoundComponent
	}
];
@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {}
