package cz.lp.processor.custom.advertisement.bazoscz.adapter;

import cz.lp.common.dto.ad.Ad;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.time.LocalDate;

import static org.junit.Assert.*;

public class BazosCzAdAdapterTest {

    private final static String SOURCE = "Bazos CZ";
    private final static String AD_URL = "https://www.bazos.cz/img/1/212/105474212.jpg?t=1559738050";
    private final static String TITLE = "Dům v centru Postřelmova";
    private final static String CONTENT_START = "Výborná občanská vybavenost";
    private final static String CONTENT_END = "Měření rozděleno na provozovnu a obytné části s garáží.";
    private final static LocalDate PUBLISHED_ON = LocalDate.of(2019, 6, 6);
    private final static String FIRST_IMAGE_FILE_NAME = "105474212.jpg";
    private final static String FIRST_IMAGE_CONTENT_TYPE = "image/jpeg";


    @Test
    public void getAd() throws IOException {

        try(InputStream is = getClass().getClassLoader().getResourceAsStream("processor/bazoscz/ad.html")){

            byte[] content = IOUtils.toByteArray(is);
            BazosCzAdAdapter adapter = new BazosCzAdAdapter(new String(content, Charset.forName("utf-8")), AD_URL, SOURCE);
            assertTrue(adapter.getAd().isPresent());

            Ad ad = adapter.getAd().get();
            assertEquals(SOURCE, ad.getSource());
            assertEquals(AD_URL, ad.getUrl());
            assertEquals(TITLE, ad.getTitle());
            assertTrue(ad.getContent().startsWith(CONTENT_START));
            assertTrue(ad.getContent().endsWith(CONTENT_END));
            assertEquals(PUBLISHED_ON, ad.getPublishedOn());
            assertEquals(10, ad.getImages().size());
            assertEquals(FIRST_IMAGE_FILE_NAME, ad.getImages().get(0).getFileName());
            assertEquals(FIRST_IMAGE_CONTENT_TYPE, ad.getImages().get(0).getContentType());
        }
    }
}