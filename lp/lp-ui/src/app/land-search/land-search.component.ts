import { Component, OnInit, ViewChild } from '@angular/core';
import { LandSearchService } from './land-search.service';
import { LandSearchFilter } from './land-search-filter/land-search-filter.interface';
import { Subscription } from 'rxjs';
import { Land, LandSearchResult } from './land-search.interface';
import { FilterChangeEvent, LandSearchFilterComponent } from './land-search-filter/land-search-filter.component';
import { PaginationChangeEvent, DEFAULT_PAGE_SIZE, ShowOnMapEvent } from './land-search-result/land-search-table/land-search-table.component';
import { LandSearchResultComponent } from './land-search-result/land-search-result.component';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
	selector: 'lp-land-search',
	templateUrl: './land-search.component.html',
	styleUrls: ['./land-search.component.scss']
})
export class LandSearchComponent implements OnInit {
	isFilterButtonDisplayed: boolean = false;
	isFilterOpened: boolean = false;
	isFilterVisibled: boolean = false;
	isMapButtonDisplayed: boolean = false;
	isMapOpened: boolean = false;
	isLoading: boolean = false;

	lastLandSearchFilter: LandSearchFilter;
	lastPage: number = 0;
	lastPageSize: number = DEFAULT_PAGE_SIZE;
	landSearchFilter: LandSearchFilter;
	landSearchResult: LandSearchResult;
	landsForMap: Land[];
	@ViewChild(LandSearchResultComponent)
	landSearchResultComponent: LandSearchResultComponent;
	private _getLandSubscription: Subscription;
	private _getLandsSubscription: Subscription;

	constructor(private _landSearchService: LandSearchService, private _router: Router, private _route: ActivatedRoute) {}

	ngOnInit() {
		// Check if shareId param exists in url
		const shareId = this._route.snapshot.queryParams.shareId;
		if (shareId) {
			this._runInitialLoading();
			if (this._getLandsSubscription) this._getLandsSubscription.unsubscribe();
			this._getLandsSubscription = this._landSearchService.getLandsBySearchId(shareId).subscribe((landSearchResult) => {
				this.landSearchResult = landSearchResult;
				this.landSearchFilter = landSearchResult.landSearchFilter;
				this.lastLandSearchFilter = landSearchResult.landSearchFilter;
				this._stopInitialLoading();
				this._showFilterButton();
				this._openFilter();
			});
		} else {
			this._openFilter();
		}
	}

	onFilterChange(event: FilterChangeEvent) {
		this.search(event.filter, 0, this.lastPageSize);
	}

	onPaginationChange(event: PaginationChangeEvent) {
		this.lastPage = event.page;
		this.lastPageSize = event.size;
		this.search(this.lastLandSearchFilter, this.lastPage, this.lastPageSize);
	}

	onShowOnMap(event: ShowOnMapEvent) {
		if (this._getLandSubscription) this._getLandSubscription.unsubscribe();
		this._getLandSubscription = this._landSearchService.getLand(event.landId).subscribe((land) => {
			this.landsForMap = [land];
			this._openMap();
		});
	}
	search(filter: LandSearchFilter, page: number, pageSize: number) {
		this._runLoading();
		if (this._getLandsSubscription) this._getLandsSubscription.unsubscribe();
		this._getLandsSubscription = this._landSearchService.getLandsByFilter(filter, page, pageSize).subscribe((landSearchResult) => {
			this.landSearchResult = landSearchResult;
			if (this.lastLandSearchFilter !== filter) {
				this._replaceShareIdInUrl(landSearchResult.id);
			}
			this._stopLoading();
			if (landSearchResult.lands.length > 0) {
				this._showMapButton();
			} else {
				this._hideMapButton();
      }
      this.lastLandSearchFilter = filter;
		});
	}

	onFilterOpen() {
		this._openFilter();
	}

	onFilterClose() {
		this._closeFilter();
	}

	onMapOpen() {
		this._openMap();
	}

	onMapClose() {
		this._closeMap();
	}

	private _replaceShareIdInUrl(id: string) {
		this._router.navigate([], {
			queryParams: {
				shareId: id
			}
		});
	}

	private _runLoading() {
		if (this.landSearchResultComponent && this.landSearchResultComponent.landSearchResult.lands.length > 0) {
			this.landSearchResultComponent.startLoading();
		} else {
			this._runInitialLoading();
		}
	}

	private _stopLoading() {
		if (this.landSearchResultComponent) {
			this.landSearchResultComponent.stopLoading();
		} else {
			this._stopInitialLoading();
		}
	}

	private _runInitialLoading() {
		this.isLoading = true;
	}

	private _stopInitialLoading() {
		this.isLoading = false;
	}

	private _showFilterButton() {
		this.isFilterButtonDisplayed = true;
	}

	private _hideFilterButton() {
		this.isFilterButtonDisplayed = false;
	}

	private _openFilter() {
		this.isFilterVisibled = true;
		this.isFilterOpened = true;
	}

	private _closeFilter() {
		this.isFilterOpened = false;
	}

	private _showMapButton() {
		this.isMapButtonDisplayed = true;
	}

	private _hideMapButton() {
		this.isMapButtonDisplayed = false;
	}

	private _openMap() {
		this.isMapOpened = true;
	}

	private _closeMap() {
		this.isMapOpened = false;
	}
}
