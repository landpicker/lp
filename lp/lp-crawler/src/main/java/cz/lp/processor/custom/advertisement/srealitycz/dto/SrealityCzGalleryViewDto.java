package cz.lp.processor.custom.advertisement.srealitycz.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class SrealityCzGalleryViewDto {

    private final String href;

    @JsonCreator
    public SrealityCzGalleryViewDto(
            @JsonProperty("href") String href) {
        this.href = href;
    }
}
