package cz.lp.processor.custom.advertisement.bazoscz;

import com.google.common.collect.Lists;
import cz.lp.common.domain.ad.AdEntity;
import cz.lp.common.domain.ad.AdImageEntity;
import cz.lp.common.dto.ad.Ad;
import cz.lp.enumerate.ProcessorCondition;
import cz.lp.exception.ProcessorExtractDataException;
import cz.lp.processor.Processor;
import cz.lp.processor.ProcessorFilter;
import cz.lp.processor.Result;
import cz.lp.processor.custom.advertisement.bazoscz.adapter.BazosCzAdAdapter;
import cz.lp.processor.resolver.ProcessorConditionResolver;
import cz.lp.processor.service.ProcessorRepositoryService;
import cz.lp.processor.service.ProcessorService;
import cz.lp.common.repository.AdRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BazosCzProcessor extends Processor<Ad> {

    private final AdRepository adRepository;


    public BazosCzProcessor(String className, Long id, Long crawlerId, ProcessorCondition condition, String urlPattern, ProcessorService processorService, ProcessorConditionResolver processorConditionResolver, ProcessorRepositoryService processorRepositoryService, ProcessorFilter processorFilter) {
        super(className, id, crawlerId, condition, urlPattern, processorService, processorConditionResolver, processorFilter);
        this.adRepository = processorRepositoryService.getAdRepository();
    }

    @Override
    protected List<Ad> extractData(Result result) throws ProcessorExtractDataException {

        String sourceName = processorService.getSourceName(this.crawlerId).orElseThrow(ProcessorExtractDataException::new);
        Optional<Ad> ad = new BazosCzAdAdapter(result.getContentAsString(), result.getUrl(), sourceName).getAd();
        if (!ad.isPresent()) {
            throw new ProcessorExtractDataException();
        }
        return Lists.newArrayList(ad.get());
    }

    @Override
    protected void setDataAsRemoved(Result result, LocalDateTime removedOn) {

    }

    @Override
    protected void saveData(List<Ad> data, Long crawlerResultId, LocalDateTime lastUpdatedOn) {

        data.forEach(ad -> {

            AdEntity adEntity = new AdEntity(ad.getUrl(), ad.getSource(), ad.getTitle(), ad.getContent(), ad.getPublishedOn());
            List<AdImageEntity> imageEntities = ad.getImages().stream().map(adImage -> new AdImageEntity(adImage.getUrl(), adImage.getFileName(), adImage.getContentType(), adImage.getData(), adEntity)).collect(Collectors.toList());
            adEntity.setImages(imageEntities);
            adRepository.save(adEntity);
        });
    }
}
