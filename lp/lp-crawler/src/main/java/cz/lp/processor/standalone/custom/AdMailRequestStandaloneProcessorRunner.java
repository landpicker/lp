package cz.lp.processor.standalone.custom;

import cz.lp.CrawlerRunner;
import cz.lp.CrawlerScheduler;
import cz.lp.StandaloneProcessorScheduler;
import cz.lp.exception.ProcessorClassException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

@ComponentScan(basePackages = "cz.lp", excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {CrawlerRunner.class, CrawlerScheduler.class, StandaloneProcessorScheduler.class}))
public class AdMailRequestStandaloneProcessorRunner {

    private static final Logger log = LoggerFactory.getLogger(AdMailRequestStandaloneProcessorRunner.class);

    public static void main(String[] args) throws ProcessorClassException {

        log.info("Starting Ad-mail request standalone runner ...");
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(AdMailRequestStandaloneProcessorRunner.class);
        AdMailRequestStandaloneProcessor processor = ctx.getBean(AdMailRequestStandaloneProcessor.class);
        processor.execute();
    }
}
