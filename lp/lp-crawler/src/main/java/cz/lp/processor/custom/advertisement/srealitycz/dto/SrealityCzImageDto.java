package cz.lp.processor.custom.advertisement.srealitycz.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class SrealityCzImageDto {

    private final Long id;
    private final SrealityCzImageLinksDto links;

    @JsonCreator
    public SrealityCzImageDto(
            @JsonProperty("id") Long id,
            @JsonProperty("_links") SrealityCzImageLinksDto links) {
        this.id = id;
        this.links = links;
    }
}
