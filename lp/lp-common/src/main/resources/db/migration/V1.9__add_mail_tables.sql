CREATE SEQUENCE mail_template_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.mail_template
(
  id bigint NOT NULL DEFAULT nextval('mail_template_id_seq'::regclass),
  subject_pattern character varying(255) NOT NULL,
  content_pattern character varying(255) NOT NULL,
  CONSTRAINT mail_template_id_pk PRIMARY KEY (id)
);

CREATE SEQUENCE mail_request_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.mail_request
(
  id bigint NOT NULL DEFAULT nextval('mail_request_id_seq'::regclass),
  status character varying(50) NOT NULL,
  subject character varying(255) NOT NULL,
  content text NOT NULL,
  subscribers json NOT NULL DEFAULT '[]',
  created_on timestamp without time zone NOT NULL,
  processed_on timestamp without time zone,
  CONSTRAINT mail_request_id_pk PRIMARY KEY (id)
);

CREATE SEQUENCE mail_request_attachment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.mail_request_attachment
(
  id bigint NOT NULL DEFAULT nextval('mail_request_attachment_id_seq'::regclass),
  mail_request_id bigint NOT NULL,
  file_name character varying(255) NOT NULL,
  content_type character varying(255) NOT NULL,
  data bytea NOT NULL,
  CONSTRAINT mail_request_attachment_id_pk PRIMARY KEY (id),
  CONSTRAINT mail_request_id_fk FOREIGN KEY (mail_request_id)
        REFERENCES public.mail_request (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION
);