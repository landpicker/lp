package cz.lp.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class DownloadUtil {

    private DownloadUtil() {
    }

    public static DownloadedData getData(String url) throws IOException {

        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpGet httpGet = new HttpGet(url);
            CloseableHttpResponse response = httpClient.execute(httpGet);
            try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                response.getEntity().writeTo(baos);
                baos.flush();
                return new DownloadedData(baos.toByteArray(), response.getFirstHeader("content-type").getValue());
            }
        }
    }

    @AllArgsConstructor
    @Getter
    public static class DownloadedData {
        private final byte[] data;
        private final String contentType;
    }
}
