package cz.lp.crawler;

import cz.lp.client.CrawlerHttpClient;
import cz.lp.client.CrawlerHttpResponse;
import cz.lp.crawler.context.ResultContext;
import cz.lp.crawler.resolver.CrawlerConditionResolver;
import cz.lp.crawler.service.CrawlerService;
import cz.lp.enumerate.CrawlerCondition;
import cz.lp.enumerate.RecrawlStrategy;
import cz.lp.enumerate.ResultStatus;
import cz.lp.exception.*;
import org.apache.http.client.methods.HttpGet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

import static java.util.Objects.isNull;

/**
 * Crawler abstract class provides unified methods for calling some custom crawler.
 */
public abstract class Crawler {

    protected static Logger log = LoggerFactory.getLogger(Crawler.class);

    private CrawlerService crawlerService;
    private CrawlerConditionResolver crawlerConditionResolver;
    protected CrawlerFilter crawlerFilter;

    protected final String className;
    protected final Long id;
    protected final CrawlerCondition condition;
    protected final Integer delay;
    protected final RecrawlStrategy recrawlStrategy;
    protected final String recrawlUrlPattern;
    protected final Set<SeedUrl> seedUrls;

    protected final LocalDateTime newRecrawlOn;
    protected final LocalDateTime newLastCrawledOn;
    protected final CrawlerHttpClient httpClient;

    /**
     * Creates instance of crawler.
     *
     * @param className                class name
     * @param id                       crawler identifier
     * @param condition                crawler condition
     * @param delay                    delay between
     * @param recrawlStrategy          recrawl strategy
     * @param recrawlUrlPattern        recrawl URL pattern
     * @param recrawlInterval          recrawl interval (in minutes)
     * @param recrawlOn                recrawl on
     * @param seedUrls                 seed URLs
     * @param crawlerService           crawler service
     * @param crawlerConditionResolver crawler condition resolver
     * @param crawlerFilter            crawler filter
     */
    public Crawler(String className, Long id, CrawlerCondition condition, Integer delay, RecrawlStrategy recrawlStrategy, String recrawlUrlPattern, Long recrawlInterval, LocalDateTime recrawlOn, Set<SeedUrl> seedUrls, CrawlerService crawlerService, CrawlerConditionResolver crawlerConditionResolver, CrawlerFilter crawlerFilter) {
        this.className = className;
        this.id = id;
        this.condition = condition;
        this.delay = delay;
        this.recrawlStrategy = recrawlStrategy;
        this.recrawlUrlPattern = recrawlUrlPattern;
        this.newLastCrawledOn = LocalDateTime.now();
        this.newRecrawlOn = (!isNull(recrawlOn)) ? recrawlOn.plusMinutes(recrawlInterval) : LocalDateTime.now().plusMinutes(recrawlInterval);
        this.seedUrls = seedUrls;
        this.httpClient = CrawlerHttpClient.builder().build();
        this.crawlerService = crawlerService;
        this.crawlerConditionResolver = crawlerConditionResolver;
        this.crawlerFilter = crawlerFilter;
    }

    /**
     * Gets class name.
     *
     * @return class name
     */
    public String getClassName() {
        return className;
    }

    /**
     * Gets crawler identifier.
     *
     * @return crawler identifier
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets information about result of condition.
     *
     * @return true if the condition is true, otherwise false
     */
    public final boolean checkCondition() {

        boolean result = crawlerConditionResolver.meetsCondition(condition);
        if (!result) {
            log.warn("{} - Crawler does not meet condition", className);
        }
        return result;
    }

    /**
     * Runs crawler.
     *
     * @return true for successful crawling, otherwise false
     */
    public final boolean crawl() {

        log.info("{} - Starting crawling...", className);
        initSeedUrls();
        Optional<ResultContext> optionalContext;

        try {
            while ((optionalContext = getFollowingResultForFetch(id, recrawlStrategy, recrawlUrlPattern, newLastCrawledOn)).isPresent()) {

                ResultContext context = optionalContext.get();
                try {
                    fetch(context);
                    extractLinks(context);
                } catch (CrawlerFetchException ex) {
                    setFailedResult(context, ex);
                    log.warn("{} - Failed to fetch URL: {}, {}", className, context.getUrl(), ex.getMessage());
                } catch (CrawlerExtractLinksException ex) {
                    log.error("{} - Unable to extract links from URL: {}", className, context.getUrl());
                    setFailedResult(context, ex);
                    updateCrawlerAfterFailed();
                    return false;
                } finally {
                    save(context);
                }
            }
            log.info("{} - No crawler URLs for fetching.", className);
        } catch (CrawlerUnknownRecrawlStrategyException ex) {
            log.error(ex.getMessage());
            updateCrawlerAfterFailed();
            return false;
        }
        updateCrawlerAfterSuccessFinish();
        return true;
    }


    /**
     * Initializes crawler seed URLs (creates NOT_FETCHED crawler results).
     */
    private final void initSeedUrls() {

        crawlerService.createNotFetchedResultFromSeedUrls(id, seedUrls);
    }

    /**
     * Gets following result for fetch.
     *
     * @param crawlerId        crawler identifier.
     * @param recrawlStrategy  recrawl strategy
     * @param recrawlUrlPattern  recrawl URL pattern
     * @param newLastCrawledOn new last crawled on
     * @return result context
     * @throws CrawlerUnknownRecrawlStrategyException if recrawl strategy does not exist
     */
    private Optional<ResultContext> getFollowingResultForFetch(Long crawlerId, RecrawlStrategy recrawlStrategy, String recrawlUrlPattern, LocalDateTime newLastCrawledOn) throws CrawlerUnknownRecrawlStrategyException {

        Optional<ResultContext> resultContext;
        switch (recrawlStrategy) {

            case RECRAWL_ALL:
                resultContext = crawlerService.getFollowingResultByRecrawlAllStrategy(crawlerId, newLastCrawledOn);
                break;
            case RECRAWL_BY_PATTERN:
                resultContext = crawlerService.getFollowingResultByRecrawlByUrlPatternStrategy(crawlerId, recrawlUrlPattern, newLastCrawledOn);
                break;
            default:
                throw new CrawlerUnknownRecrawlStrategyException(String.format("{} - Unknown recrawl strategy: %s", className, recrawlStrategy.name()));
        }
        return resultContext;
    }

    /**
     * Fetches URL from context.
     *
     * @param context result context
     * @throws CrawlerFetchException when something during fetching URL failed
     */
    protected void fetch(ResultContext context) throws CrawlerFetchException {

        log.info("{} - Fetching URL: {}", className, context.getUrl());
        try {

            long randomDelay = getRandomDelay(delay);
            Thread.sleep(randomDelay);

            CrawlerHttpResponse response = fetchUrl(context.getUrl());
            context.setStatus(ResultStatus.SUCCESS);
            context.setContent(response.getContent());
            context.setContentType(response.getContentType());
            context.setHash(generateContentHash(response.getContent(), context.getUrl()));
        } catch (CrawlerHttpClientParameterException | InterruptedException | CrawlerHttpClientCommunicationException ex) {
            throw new CrawlerFetchException(ex);
        }
    }

    /**
     * Fetches URL.
     * <p>
     * By default sends GET request without additional headers and parameters, or can be overridden by extended crawler.
     * <p/>
     *
     * @param url URL
     * @return result
     * @throws CrawlerHttpClientParameterException
     * @throws CrawlerHttpClientCommunicationException
     */
    protected CrawlerHttpResponse fetchUrl(String url) throws CrawlerHttpClientParameterException, CrawlerHttpClientCommunicationException {

        return httpClient.executeRequest(HttpGet.METHOD_NAME, url, null, null);
    }

    /**
     * Extracts and saves new links from result.
     */
    private void extractLinks(ResultContext context) throws CrawlerExtractLinksException {

        Optional<Set<String>> links = parseLinks(context.getContentAsString(), context.getUrl());
        if (!links.isPresent()) {
            throw new CrawlerExtractLinksException();
        }
        context.setExtractedLinks(links.get());
    }

    /**
     * Gets random delay between requests.
     *
     * @param maxDelay may delay in seconds
     * @return random delay in milliseconds
     */
    protected long getRandomDelay(int maxDelay) {

        Random rand = new Random();
        long randomDelay = rand.nextInt(maxDelay + 1);
        log.debug("Generated random delay between requests: {}", randomDelay);
        return randomDelay;
    }

    /**
     * Parses links from fetched content.
     * <p>
     * This method must be implemented in extended custom crawler class.
     * </p>
     *
     * @param content fetched content
     * @param url     URL
     * @return links
     */
    protected abstract Optional<Set<String>> parseLinks(String content, String url);

    /**
     * Generates content hash.
     * <p>
     * This method must be implemented in extended crawler class.
     * </p>
     *
     * @param content content as bytes
     * @param url     URL
     * @return hash
     */
    protected abstract String generateContentHash(byte[] content, String url);

    /**
     * Saves result and extracted links.
     *
     * @param context result context
     */
    private void save(ResultContext context) {

        crawlerService.saveResult(context);
        crawlerService.saveExtractedLinks(context, id, newLastCrawledOn);
    }

    /**
     * Sets failed result.
     *
     * @param ex exception
     */
    private void setFailedResult(ResultContext context, Throwable ex) {

        context.setStatus(ResultStatus.FAILED);
        context.setContent(null);
        context.setContentType(null);
        context.setHash(null);
        context.setMessage(ex.getMessage());
    }

    /**
     * Updates crawler after success finish.
     */
    private void updateCrawlerAfterSuccessFinish() {

        crawlerService.updateCrawlerAfterSuccessFinish(id, newRecrawlOn, newLastCrawledOn);
    }

    /**
     * Updates crawler after failed.
     */
    private void updateCrawlerAfterFailed() {

        crawlerService.updateCrawlerAfterFailed(id, newLastCrawledOn);
    }
}