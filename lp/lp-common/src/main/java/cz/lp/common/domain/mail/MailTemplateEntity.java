package cz.lp.common.domain.mail;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Mail template entity provides patterns for subject and content of specific email.
 */

@Entity
@Table(name = "mail_template")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MailTemplateEntity {

    @Id
    @SequenceGenerator(name = "id", sequenceName = "mail_template_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "subject_pattern")
    private String subjectPattern;

    @Column(name = "content_pattern")
    private String contentPattern;
}
