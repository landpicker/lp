package cz.lp.crawler.custom.vdparea;

import cz.lp.CrawlerRunner;
import cz.lp.CrawlerScheduler;
import cz.lp.CrawlerWorkerService;
import cz.lp.StandaloneProcessorScheduler;
import cz.lp.exception.CrawlerClassException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

/**
 * This class run only VdpAreaCrawler {@link VdpAreaCrawler}
 */

@ComponentScan(basePackages = "cz.lp", excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {CrawlerRunner.class, CrawlerScheduler.class, StandaloneProcessorScheduler.class}))
public class VdpAreaCrawlerRunner {

    private static final Logger log = LoggerFactory.getLogger(VdpAreaCrawlerRunner.class);

    private static final Long ID = 1l;

    public static void main(String[] args) throws CrawlerClassException {

        log.info("Starting VDP area crawler runner ...");
        ApplicationContext ctx = new AnnotationConfigApplicationContext(VdpAreaCrawlerRunner.class);
        CrawlerWorkerService crawlerWorkerService = ctx.getBean(CrawlerWorkerService.class);
        VdpAreaCrawler crawler = (VdpAreaCrawler) crawlerWorkerService.getCrawler(ID);
        if (crawler.checkCondition()) {
            crawler.crawl();
        } else {
            log.warn("VDP area crawler does not meet startup conditions");
        }

    }
}
