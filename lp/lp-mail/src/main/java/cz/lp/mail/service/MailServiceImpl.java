package cz.lp.mail.service;


import cz.lp.common.dto.mail.MailRequest;
import cz.lp.common.dto.mail.MailRequestAttachment;
import cz.lp.common.enumerate.MailRequestStatus;
import cz.lp.common.service.mail.MailRequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class MailServiceImpl implements MailService {

    private static final Logger log = LoggerFactory.getLogger(MailServiceImpl.class);
    private final JavaMailSender mailSender;
    private final Environment env;
    private final MailRequestService mailRequestService;

    @Autowired
    public MailServiceImpl(JavaMailSender mailSender, Environment env, MailRequestService mailRequestService) {
        this.mailSender = mailSender;
        this.env = env;
        this.mailRequestService = mailRequestService;
    }

    @Override
    public void sendAllPreparedEmails() {

        Optional<MailRequest> mailRequestOptional;
        while ((mailRequestOptional = mailRequestService.getNextMailRequestReadyToExecute()).isPresent()) {

            MailRequest mailRequest = mailRequestOptional.get();
            try {
                log.info("Sending mail with id: {}", mailRequestOptional.get().getId());
                send(mailRequest.getSubject(), mailRequest.getContent(), mailRequest.getAttachments(), mailRequest.getSubscribers());
                mailRequestService.changeStatus(mailRequest.getId(), MailRequestStatus.SENT);
            } catch (MessagingException ex) {
                log.error("Failed to send email with id: {}", mailRequest.getId(), ex);
                mailRequestService.changeStatus(mailRequest.getId(), MailRequestStatus.FAILED);
            }
        }
    }

    /**
     * Sends email.
     *
     * @param subject         subject of email.
     * @param content         content of email.
     * @param attachments     attachments
     * @param recipientEmails recipients emails
     * @throws MessagingException throws exception if sending failed
     */
    private void send(String subject, String content, List<MailRequestAttachment> attachments, Set<String> recipientEmails) throws MessagingException {

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setFrom(env.getProperty("mail.sender"));
        helper.setTo(recipientEmails.stream().toArray(String[]::new));
        helper.setSubject(subject);
        helper.setText(content, true);
        for (MailRequestAttachment attachment : attachments) {
            DataSource ds = new ByteArrayDataSource(attachment.getData(), attachment.getContentType());
            helper.addAttachment(attachment.getFileName(), ds);
        }
        mailSender.send(message);
    }
}
