package cz.lp.enumerate;

/**
 * Result status provides enumerate of status.
 */
public enum ResultStatus {

    NOT_FETCHED, SUCCESS, FAILED
}
