package cz.lp.common.domain;

import com.google.common.collect.Lists;
import cz.lp.common.dto.Epsg5514Coordinate;
import cz.lp.common.dto.Wgs84Coordinate;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;

public class AreaBorderEntityTest {

    private static final Long ID = 1l;
    private static final List<Epsg5514Coordinate> EPSG_5514_COORDINATES = Lists.newArrayList(Epsg5514Coordinate.of(-510752.25f, -1141672.44f));
    private static final List<Wgs84Coordinate> WGS84_COORDINATES = Lists.newArrayList(Wgs84Coordinate.of(17.6518000f, 49.2232000f));
    private static final AreaEntity AREA = Mockito.mock(AreaEntity.class);


    @Test
    public void constructor_empty() throws Exception {

        AreaBorderEntity areaBorderEntity = new AreaBorderEntity();
        Assert.assertNotNull(areaBorderEntity);
        Assert.assertNull(areaBorderEntity.getId());
        Assert.assertNull(areaBorderEntity.getEpsg5514Coordinates());
        Assert.assertNull(areaBorderEntity.getWgs84Coordinates());
        Assert.assertNull(areaBorderEntity.getArea());
    }

    @Test
    public void constructor() throws Exception {

        AreaBorderEntity areaBorderEntity = new AreaBorderEntity(ID, EPSG_5514_COORDINATES, WGS84_COORDINATES, AREA);
        Assert.assertNotNull(areaBorderEntity);
        Assert.assertEquals(ID, areaBorderEntity.getId());
        Assert.assertEquals(EPSG_5514_COORDINATES, areaBorderEntity.getEpsg5514Coordinates());
        Assert.assertEquals(WGS84_COORDINATES, areaBorderEntity.getWgs84Coordinates());
        Assert.assertEquals(AREA, areaBorderEntity.getArea());
    }

    @Test
    public void constructor_withoutId() throws Exception {

        AreaBorderEntity areaBorderEntity = new AreaBorderEntity(EPSG_5514_COORDINATES, WGS84_COORDINATES, AREA);
        Assert.assertNotNull(areaBorderEntity);
        Assert.assertNull(areaBorderEntity.getId());
        Assert.assertEquals(EPSG_5514_COORDINATES, areaBorderEntity.getEpsg5514Coordinates());
        Assert.assertEquals(WGS84_COORDINATES, areaBorderEntity.getWgs84Coordinates());
        Assert.assertEquals(AREA, areaBorderEntity.getArea());
    }

    @Test
    public void getId1() throws Exception {

        AreaBorderEntity areaBorderEntity = new AreaBorderEntity(ID, EPSG_5514_COORDINATES, WGS84_COORDINATES, AREA);
        Assert.assertNotNull(areaBorderEntity);
        Assert.assertEquals(ID, areaBorderEntity.getId());
    }

    @Test
    public void getEpsg5514Coordinates() throws Exception {

        AreaBorderEntity areaBorderEntity = new AreaBorderEntity(ID, EPSG_5514_COORDINATES, WGS84_COORDINATES, AREA);
        Assert.assertNotNull(areaBorderEntity);
        Assert.assertEquals(EPSG_5514_COORDINATES, areaBorderEntity.getEpsg5514Coordinates());
    }

    @Test
    public void getWgs841Coordinates() throws Exception {

        AreaBorderEntity areaBorderEntity = new AreaBorderEntity(ID, EPSG_5514_COORDINATES, WGS84_COORDINATES, AREA);
        Assert.assertNotNull(areaBorderEntity);
        Assert.assertEquals(ID, areaBorderEntity.getId());
        Assert.assertEquals(WGS84_COORDINATES, areaBorderEntity.getWgs84Coordinates());
    }

    @Test
    public void getArea1() throws Exception {

        AreaBorderEntity areaBorderEntity = new AreaBorderEntity(ID, EPSG_5514_COORDINATES, WGS84_COORDINATES, AREA);
        Assert.assertNotNull(areaBorderEntity);
        Assert.assertEquals(AREA, areaBorderEntity.getArea());
    }
}