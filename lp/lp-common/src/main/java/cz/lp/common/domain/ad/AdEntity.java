package cz.lp.common.domain.ad;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

/**
 * Ad entity represents advertisement of land or building  in database table.
 */

@Entity
@Table(name = "ad")
@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor()
public class AdEntity {

    @NonNull
    @Id
    @Column(name = "url", unique = true, nullable = false)
    private String url;

    @NonNull
    @Column(name = "source", nullable = false)
    private String source;

    @NonNull
    @Column(name = "title", nullable = false)
    private String title;

    @NonNull
    @Column(name = "content", nullable = false)
    private String content;

    @NonNull
    @Column(name = "published_on", nullable = false)
    private LocalDate publishedOn;

    @OneToMany(mappedBy = "ad", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<AdImageEntity> images;




}
