package cz.lp.resolver;

import cz.lp.crawler.resolver.CrawlerConditionResolver;
import cz.lp.enumerate.CrawlerCondition;
import cz.lp.crawler.service.CrawlerConditionService;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

public class CrawlerConditionResolverTest {

    private static CrawlerConditionResolver crawlerConditionResolver;

    @BeforeClass
    public static void setUp() throws Exception {

        CrawlerConditionService conditionService = Mockito.mock(CrawlerConditionService.class);
        crawlerConditionResolver = new CrawlerConditionResolver(conditionService);
    }

    @Test
    public void meetsCondition_whenConditionIsNone_shouldReturnTrue() throws Exception {

        Assert.assertTrue(crawlerConditionResolver.meetsCondition(CrawlerCondition.NONE));
    }

}