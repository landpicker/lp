package cz.lp.server.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.base.MoreObjects;
import com.google.common.collect.Lists;
import cz.lp.common.domain.AreaEntity;
import cz.lp.common.dto.Epsg5514Coordinate;
import cz.lp.common.enumerate.AreaType;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

/**
 * Area data transfer object represents region, district, city or cadastral territory.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Area {

    private final String code;
    private final AreaType type;
    private final String name;
    private final List<Epsg5514Coordinate> border;
    private final String parentAreaCode;
    private final List<Area> subAreas;

    /**
     * Creates instance of area data transfer object.
     *
     * @param code           area unique code
     * @param type           area type
     * @param name           area name
     * @param name           area name
     * @param border         area border in EPSG 5514 coordinates
     * @param parentAreaCode parent area code
     */
    public Area(String code, AreaType type, String name, List<Epsg5514Coordinate> border, String parentAreaCode) {
        this.code = code;
        this.type = type;
        this.name = name;
        this.border = border;
        this.subAreas = Lists.newArrayList();
        this.parentAreaCode = parentAreaCode;
    }

    /**
     * Creates instance of area data transfer object.
     *
     * @param code           area unique code
     * @param type           area type
     * @param name           area name
     * @param border         area border in EPSG 5514 coordinates
     * @param subAreas       sub areas
     * @param parentAreaCode parent area code
     */
    public Area(String code, AreaType type, String name, List<Epsg5514Coordinate> border, String parentAreaCode, List<Area> subAreas) {
        this.code = code;
        this.type = type;
        this.name = name;
        this.border = border;
        this.parentAreaCode = parentAreaCode;
        this.subAreas = subAreas;
    }

    /**
     * Creates instance of area data transfer object.
     *
     * @param areaEntity area entity
     */
    public static Area ofEntityWithoutSubAreas(AreaEntity areaEntity) {

        return new Area(areaEntity.getCode(),
                areaEntity.getType(),
                areaEntity.getName(),
                nonNull(areaEntity.getAreaBorder()) ? areaEntity.getAreaBorder().getEpsg5514Coordinates() : Lists.newArrayList(),
                null,
                Lists.newArrayList());
    }

    /**
     * Creates instance of area data transfer object.
     *
     * @param areaEntity area entity
     */
    public static Area ofEntityWithoutBorder(AreaEntity areaEntity) {


        return new Area(areaEntity.getCode(),
                areaEntity.getType(),
                areaEntity.getName(),
                Lists.newArrayList(),
                null,
                areaEntity.getSubAreas().stream().map(Area::ofEntityWithoutBorder).collect(Collectors.toList()));
    }

    /**
     * Creates instance of area from area without sub areas.
     *
     * @param area           area
     */
    public static Area ofWithoutSubAreas(Area area) {

        return new Area(area.getCode(), area.getType(), area.getName(), area.getBorder(), area.getParentAreaCode());
    }

    /**
     * Gets area unique code.
     *
     * @return area unique code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets area type.
     *
     * @return area type
     */
    public AreaType getType() {
        return type;
    }

    /**
     * Gets area name.
     *
     * @return area name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets area border in EPSG 5514 coordinates.
     *
     * @return area border in EPSG 5514 coordinates
     */
    public List<Epsg5514Coordinate> getBorder() {
        return border;
    }

    /**
     * Gets parent area code.
     *
     * @return parent area code
     */
    public String getParentAreaCode() {
        return parentAreaCode;
    }

    /**
     * Gets sub areas.
     *
     * @return sub areas
     */
    public List<Area> getSubAreas() {
        return subAreas;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("code", code)
                .add("type", type)
                .add("name", name)
                .add("parentAreaCode", parentAreaCode)
                .omitNullValues()
                .toString();
    }
}
