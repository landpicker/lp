package cz.lp.server.dto.filter;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import cz.lp.common.enumerate.LandType;

import java.util.Set;

/**
 * Land filter data transfer object provides information about required land types of searched lands.
 */
public class LandTypeFilter {

    private final Set<LandType> landTypes;

    /**
     * Creates instance of land type filter.
     *
     * @param landTypes land types
     */
    @JsonCreator
    public LandTypeFilter(@JsonProperty("landTypes") Set<LandType> landTypes) {
        this.landTypes = landTypes;
    }

    /**
     * Gets land types.
     *
     * @return land types
     */
    public Set<LandType> getLandTypes() {
        return landTypes;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("landTypes", landTypes)
                .omitNullValues()
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LandTypeFilter that = (LandTypeFilter) o;
        return Objects.equal(landTypes, that.landTypes);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(landTypes);
    }
}
