export interface AreaFilter {
	codes: string[];
}

export interface AcreageFilter {
	from: number;
	to: number;
}

export interface LandTypeFilter {
	landTypes: string[];
}

export interface LandFilter {
	acreageFilter: AcreageFilter;
	landTypeFilter: LandTypeFilter;
}

export class LandSearchFilter {
	areaFilter: AreaFilter;
	landFilter: LandFilter;

	constructor(areaCodes: string[], acreageFrom: number, acreageTo: number, landTypes: string[]) {
		this.areaFilter = <AreaFilter>{ codes: areaCodes };
		let acreageFilter = <AcreageFilter>{ from: acreageFrom, to: acreageTo };
		let landTypeFilter = <LandTypeFilter>{ landTypes: landTypes };
		this.landFilter = <LandFilter>{ acreageFilter: acreageFilter, landTypeFilter: landTypeFilter };
	}
}
