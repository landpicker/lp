package cz.lp.common.enumerate;

public enum MailRequestStatus {

    READY, SENT, FAILED
}
