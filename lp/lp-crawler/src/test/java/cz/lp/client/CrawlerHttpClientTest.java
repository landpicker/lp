package cz.lp.client;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import cz.lp.exception.CrawlerHttpClientCommunicationException;
import cz.lp.exception.CrawlerHttpClientParameterException;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.lang.reflect.UndeclaredThrowableException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

/**
 * Unit tests of crawler HTTP client test {@link CrawlerHttpClient}.
 */
public class CrawlerHttpClientTest {

    private static WireMockServer wireMockServer;
    private static final int PORT = 8888;
    private static final String HOST = "localhost";

    private static final int READ_TIMEOUT = 10000;
    private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36";
    private static final String ACCEPT_LANGUAGE = "cs-CZ,cs;q=0.9";
    private static final String REFERER = "https://www.google.cz/";

    private static final String CONTENT_TYPE = "text/html";
    private static final String GET_200_ENDPOINT_PATH = "/get/200";
    private static final String GET_301_ENDPOINT_PATH = "/get/301";
    private static final String GET_200_ENDPOINT_CONTENT = "GET 200 CONTENT";
    private static final String GET_404_ENDPOINT_PATH = "/get/404";
    private static final String POST_200_ENDPOINT_PATH = "/post/200";
    private static final String POST_200_ENDPOINT_CONTENT = "POST 200 CONTENT";
    private static final byte[] POST_DATA = "param_1=value_1&param_2=value_2".getBytes();
    private static final Map<String, String> FORM_PARAMS = ImmutableMap.of("param_1", "value_1", "param_2", "value_2");

    private static final String BASE_URL = "http://" + HOST + ":" + String.valueOf(PORT);

    @BeforeClass
    public static void setUp() throws Exception {

        wireMockServer = new WireMockServer(PORT);
        wireMockServer.start();
        WireMock.configureFor(HOST, wireMockServer.port());

    }

    @AfterClass
    public static void tearDown() throws Exception {

        wireMockServer.stop();
    }

    @Test
    public void builder_shouldReturnNewInstance() throws Exception {

        Assert.assertNotNull(CrawlerHttpClient.builder());
    }

    @Test
    public void builder_readTimeout_whenReadTimeOutIsGreaterThanZero_shouldBuilder() throws Exception {

        Assert.assertNotNull(CrawlerHttpClient.builder().readTimeout(READ_TIMEOUT));
    }

    @Test(expected = NullPointerException.class)
    public void builder_readTimeout_whenReadTimeOutIsLessThanZero_shouldThrowExceptions() throws Exception {

        Assert.assertNotNull(CrawlerHttpClient.builder().readTimeout(-1));
    }

    @Test
    public void builder_userAgent_whenUserAgentIsNotNull_shouldBuilder() throws Exception {

        Assert.assertNotNull(CrawlerHttpClient.builder().userAgent(USER_AGENT));
    }

    @Test(expected = NullPointerException.class)
    public void builder_userAgent_whenUserAgentIsNull_shouldThrowException() throws Exception {

        Assert.assertNotNull(CrawlerHttpClient.builder().userAgent(null));
    }

    @Test
    public void builder_acceptLanguage_whenAcceptLanguageIsNotNull_shouldBuilder() throws Exception {

        Assert.assertNotNull(CrawlerHttpClient.builder().acceptLanguage(ACCEPT_LANGUAGE));
    }

    @Test(expected = NullPointerException.class)
    public void builder_acceptLanguage_whenAcceptLanguageIsNull_shouldThrowException() throws Exception {

        Assert.assertNotNull(CrawlerHttpClient.builder().acceptLanguage(null));
    }

    @Test
    public void builder_referer_whenRefererIsNotNull_shouldBuilder() throws Exception {

        Assert.assertNotNull(CrawlerHttpClient.builder().referer(REFERER));
    }

    @Test(expected = NullPointerException.class)
    public void builder_referer_whenRefererIsNull_shouldThrowException() throws Exception {

        Assert.assertNotNull(CrawlerHttpClient.builder().referer(null));
    }


    //executeRequest
    @Test(expected = CrawlerHttpClientParameterException.class)
    public void executeRequest_whenMethodIsNotSupported_shouldThrowException() throws CrawlerHttpClientCommunicationException, CrawlerHttpClientParameterException {

        CrawlerHttpClient client = CrawlerHttpClient.builder().build();
        client.executeRequest(HttpDelete.METHOD_NAME, BASE_URL + GET_200_ENDPOINT_PATH, ImmutableMap.of(HttpHeaders.ACCEPT, CONTENT_TYPE), null);
    }

    @Test(expected = CrawlerHttpClientParameterException.class)
    public void executeRequest_whenUrlIsEmpty_shouldThrowException() throws CrawlerHttpClientCommunicationException, CrawlerHttpClientParameterException {

        CrawlerHttpClient client = CrawlerHttpClient.builder().build();
        client.executeRequest(HttpGet.METHOD_NAME, "", ImmutableMap.of(HttpHeaders.ACCEPT, CONTENT_TYPE), null);
    }

    @Test(expected = CrawlerHttpClientParameterException.class)
    public void executeRequest_whenUrlIsNull_shouldThrowException() throws CrawlerHttpClientCommunicationException, CrawlerHttpClientParameterException {

        CrawlerHttpClient client = CrawlerHttpClient.builder().build();
        client.executeRequest(HttpGet.METHOD_NAME, null, ImmutableMap.of(HttpHeaders.ACCEPT, CONTENT_TYPE), null);
    }

    @Test
    public void executeRequest_methodGet_whenRequestIsOk_shouldReturnResponse() throws CrawlerHttpClientCommunicationException, CrawlerHttpClientParameterException {

        stubFor(get(urlEqualTo(GET_200_ENDPOINT_PATH))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader(HttpHeaders.CONTENT_TYPE, CONTENT_TYPE)
                        .withBody(GET_200_ENDPOINT_CONTENT)));

        CrawlerHttpClient client = CrawlerHttpClient.builder().build();
        CrawlerHttpResponse response = client.executeRequest(HttpGet.METHOD_NAME, BASE_URL + GET_200_ENDPOINT_PATH, ImmutableMap.of(HttpHeaders.ACCEPT, CONTENT_TYPE), null);
        Assert.assertNotNull(response);
        Assert.assertEquals(HttpStatus.SC_OK, response.getStatus());
        Assert.assertEquals(GET_200_ENDPOINT_CONTENT, new String(response.getContent()));
        Assert.assertEquals(CONTENT_TYPE, response.getContentType());
    }

    @Test
    public void executeRequest_methodGet_whenAdditionalHeadersIsEmpty_shouldReturnResponse() throws CrawlerHttpClientCommunicationException, CrawlerHttpClientParameterException {

        CrawlerHttpClient client = CrawlerHttpClient.builder().build();
        stubFor(get(urlEqualTo(GET_200_ENDPOINT_PATH))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader(HttpHeaders.CONTENT_TYPE, CONTENT_TYPE)
                        .withBody(GET_200_ENDPOINT_CONTENT)));
        CrawlerHttpResponse response = client.executeRequest(HttpGet.METHOD_NAME, BASE_URL + GET_200_ENDPOINT_PATH, Maps.newHashMap(), null);
        Assert.assertNotNull(response);
        Assert.assertEquals(HttpStatus.SC_OK, response.getStatus());
        Assert.assertEquals(GET_200_ENDPOINT_CONTENT, new String(response.getContent()));
        Assert.assertEquals(CONTENT_TYPE, response.getContentType());
    }

    @Test
    public void executeRequest_methodGet_whenAdditionalHeadersIsNull_shouldReturnResponse() throws CrawlerHttpClientCommunicationException, CrawlerHttpClientParameterException {

        CrawlerHttpClient client = CrawlerHttpClient.builder().build();
        stubFor(get(urlEqualTo(GET_200_ENDPOINT_PATH))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader(HttpHeaders.CONTENT_TYPE, CONTENT_TYPE)
                        .withBody(GET_200_ENDPOINT_CONTENT)));
        CrawlerHttpResponse response = client.executeRequest("GET", BASE_URL + GET_200_ENDPOINT_PATH, null, null);
        Assert.assertNotNull(response);
        Assert.assertEquals(HttpStatus.SC_OK, response.getStatus());
        Assert.assertEquals(GET_200_ENDPOINT_CONTENT, new String(response.getContent()));
        Assert.assertEquals(CONTENT_TYPE, response.getContentType());
    }

    @Test(expected = CrawlerHttpClientCommunicationException.class)
    public void executeRequest_methodGet_whenServerDoesNotExist_shouldThrowException() throws CrawlerHttpClientCommunicationException, CrawlerHttpClientParameterException {

        stubFor(get(urlEqualTo(GET_200_ENDPOINT_PATH))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader(HttpHeaders.CONTENT_TYPE, CONTENT_TYPE)
                        .withBody(GET_200_ENDPOINT_CONTENT)));
        CrawlerHttpClient client = CrawlerHttpClient.builder().build();
        client.executeRequest(HttpGet.METHOD_NAME, "http://localhost:9999", ImmutableMap.of(HttpHeaders.ACCEPT, CONTENT_TYPE), null);
    }

    @Test(expected = CrawlerHttpClientCommunicationException.class)
    public void executeRequest_methodGet_whenUrlIsInvalid_shouldThrowException() throws CrawlerHttpClientCommunicationException, CrawlerHttpClientParameterException {

        stubFor(get(urlEqualTo(GET_200_ENDPOINT_PATH))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader(HttpHeaders.CONTENT_TYPE, CONTENT_TYPE)
                        .withBody(GET_200_ENDPOINT_CONTENT)));
        CrawlerHttpClient client = CrawlerHttpClient.builder().build();
        client.executeRequest(HttpGet.METHOD_NAME, "bad_url", Maps.newHashMap(), null);
    }

    @Test(expected = CrawlerHttpClientCommunicationException.class)
    public void executeRequest_methodGet_whenUrlDoesNotExist_shouldThowException() throws CrawlerHttpClientCommunicationException, CrawlerHttpClientParameterException {

        stubFor(get(urlEqualTo(GET_200_ENDPOINT_PATH))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader(HttpHeaders.CONTENT_TYPE, CONTENT_TYPE)
                        .withBody(GET_200_ENDPOINT_CONTENT)));
        CrawlerHttpClient client = CrawlerHttpClient.builder().build();
        client.executeRequest(HttpGet.METHOD_NAME, BASE_URL + GET_404_ENDPOINT_PATH, Maps.newHashMap(), null);
    }

    @Test
    public void executeRequest_methodPost_whenRequestIsOk_shouldReturnResponse() throws CrawlerHttpClientCommunicationException, CrawlerHttpClientParameterException {

        stubFor(post(urlEqualTo(POST_200_ENDPOINT_PATH))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader(HttpHeaders.CONTENT_TYPE, CONTENT_TYPE)
                        .withBody(POST_200_ENDPOINT_CONTENT)));

        CrawlerHttpClient client = CrawlerHttpClient.builder().build();
        CrawlerHttpResponse response = client.executeRequest(HttpPost.METHOD_NAME, BASE_URL + POST_200_ENDPOINT_PATH, ImmutableMap.of(HttpHeaders.ACCEPT, CONTENT_TYPE), FORM_PARAMS);
        Assert.assertNotNull(response);
        Assert.assertEquals(HttpStatus.SC_OK, response.getStatus());
        Assert.assertEquals(POST_200_ENDPOINT_CONTENT, new String(response.getContent()));
        Assert.assertEquals(CONTENT_TYPE, response.getContentType());
    }

    // getUrl
    @Test
    public void getUrl_whenUrlIsOK_shouldReturnMalformedUrl() throws Throwable {

        try {
            URL url = ReflectionTestUtils.invokeMethod(CrawlerHttpClient.builder().build(), "getUrl", BASE_URL);
            Assert.assertNotNull(url);
            Assert.assertEquals(BASE_URL, url.toString());
        } catch (UndeclaredThrowableException ex) {
            throw ex.getUndeclaredThrowable();
        }
    }

    @Test(expected = CrawlerHttpClientCommunicationException.class)
    public void getUrl_whenUrlIsIncorrect_shouldThrowException() throws Throwable {

        try {
            ReflectionTestUtils.invokeMethod(CrawlerHttpClient.builder().build(), "getUrl", "google");
        } catch (UndeclaredThrowableException ex) {
            throw ex.getUndeclaredThrowable();
        }
    }

    // openConnection
    @Test
    public void openConnection_whenUrlIsOk_shouldReturnConnection() throws Throwable {

        stubFor(get(urlEqualTo(GET_200_ENDPOINT_PATH))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader(HttpHeaders.CONTENT_TYPE, CONTENT_TYPE)
                        .withBody(GET_200_ENDPOINT_CONTENT)));

        try {
            HttpURLConnection urlConnection = ReflectionTestUtils.invokeMethod(CrawlerHttpClient.builder().build(), "openConnection", new URL(BASE_URL + GET_200_ENDPOINT_PATH));
            Assert.assertNotNull(urlConnection);
            urlConnection.disconnect();
        } catch (UndeclaredThrowableException ex) {
            throw ex.getUndeclaredThrowable();
        }
    }

    // getPostData
    @Test
    public void getPostData_whenFormParamsIsOk_shouldReturnBytes() throws Throwable {


        try {
            byte[] postData = ReflectionTestUtils.invokeMethod(CrawlerHttpClient.builder().build(), "getPostData", FORM_PARAMS);
            Assert.assertNotNull(postData);
            Assert.assertEquals(new String(POST_DATA), new String(postData, StandardCharsets.UTF_8.name()));
        } catch (UndeclaredThrowableException ex) {
            throw ex.getUndeclaredThrowable();
        }
    }

    @Test
    public void getPostData_whenFormParamsIsEmpty_shouldReturnBytes() throws Throwable {

        try {
            byte[] postData = ReflectionTestUtils.invokeMethod(CrawlerHttpClient.builder().build(), "getPostData", Maps.newHashMap());
            Assert.assertNotNull(postData);
            Assert.assertTrue(postData.length == 0);
        } catch (UndeclaredThrowableException ex) {
            throw ex.getUndeclaredThrowable();
        }
    }

    // writePostData
    @Test
    public void writePostData_whenPostDataIsOk_shouldWriteData() throws Throwable {

        stubFor(post(urlEqualTo(POST_200_ENDPOINT_PATH))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader(HttpHeaders.CONTENT_TYPE, CONTENT_TYPE)
                        .withBody(POST_200_ENDPOINT_CONTENT)));
        HttpURLConnection connection = (HttpURLConnection) new URL(BASE_URL + POST_200_ENDPOINT_PATH).openConnection();
        connection.setRequestMethod(HttpPost.METHOD_NAME);
        connection.setDoOutput(true);
        connection.setInstanceFollowRedirects(true);
        connection.setRequestProperty(HttpHeaders.CONTENT_LENGTH, Integer.toString(POST_DATA.length));
        try {
            ReflectionTestUtils.invokeMethod(CrawlerHttpClient.builder().build(), "writePostData", connection, POST_DATA);
        } catch (UndeclaredThrowableException ex) {
            throw ex.getUndeclaredThrowable();
        }
    }

    @Test(expected = CrawlerHttpClientCommunicationException.class)
    public void writePostData_whenOutputIsNotSet_shouldWriteData() throws Throwable {

        stubFor(post(urlEqualTo(POST_200_ENDPOINT_PATH))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader(HttpHeaders.CONTENT_TYPE, CONTENT_TYPE)
                        .withBody(POST_200_ENDPOINT_CONTENT)));

        HttpURLConnection connection = (HttpURLConnection) new URL(BASE_URL + POST_200_ENDPOINT_PATH).openConnection();
        connection.setRequestMethod(HttpPost.METHOD_NAME);
        connection.setInstanceFollowRedirects(true);
        connection.setRequestProperty(HttpHeaders.CONTENT_LENGTH, Integer.toString(POST_DATA.length));
        try {
            ReflectionTestUtils.invokeMethod(CrawlerHttpClient.builder().build(), "writePostData", connection, POST_DATA);
        } catch (UndeclaredThrowableException ex) {
            throw ex.getUndeclaredThrowable();
        }
    }

    // getResponse
    @Test
    public void getResponse_whenResponseIsOk_shouldReturnResponse() throws Throwable {

        stubFor(get(urlEqualTo(GET_200_ENDPOINT_PATH))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader(HttpHeaders.CONTENT_TYPE, CONTENT_TYPE)
                        .withBody(GET_200_ENDPOINT_CONTENT)));

        HttpURLConnection connection = (HttpURLConnection) new URL(BASE_URL + GET_200_ENDPOINT_PATH).openConnection();
        try {
            CrawlerHttpResponse response = ReflectionTestUtils.invokeMethod(CrawlerHttpClient.builder().build(), "getResponse", connection);
            Assert.assertNotNull(response);
            Assert.assertEquals(HttpStatus.SC_OK, response.getStatus());
            Assert.assertEquals(GET_200_ENDPOINT_CONTENT, new String(response.getContent()));
            Assert.assertEquals(CONTENT_TYPE, response.getContentType());
        } catch (UndeclaredThrowableException ex) {
            throw ex.getUndeclaredThrowable();
        }
    }

    @Test(expected = CrawlerHttpClientCommunicationException.class)
    public void getResponse_whenResponseIsEmpty_shouldThrowException() throws Throwable {

        stubFor(get(urlEqualTo(GET_200_ENDPOINT_PATH))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader(HttpHeaders.CONTENT_TYPE, CONTENT_TYPE)));
        HttpURLConnection connection = (HttpURLConnection) new URL(BASE_URL + GET_200_ENDPOINT_PATH).openConnection();
        try {
            ReflectionTestUtils.invokeMethod(CrawlerHttpClient.builder().build(), "getResponse", connection);
        } catch (UndeclaredThrowableException ex) {
            throw ex.getUndeclaredThrowable();
        }
    }

    @Test(expected = CrawlerHttpClientCommunicationException.class)
    public void getResponse_whenResponseIsNull_shouldThrowException() throws Throwable {

        stubFor(get(urlEqualTo(GET_200_ENDPOINT_PATH))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader(HttpHeaders.CONTENT_TYPE, CONTENT_TYPE)
                        .withBody(GET_200_ENDPOINT_CONTENT)));
        HttpURLConnection connection = (HttpURLConnection) new URL(BASE_URL + GET_404_ENDPOINT_PATH).openConnection();
        try {
            ReflectionTestUtils.invokeMethod(CrawlerHttpClient.builder().build(), "getResponse", connection);
        } catch (UndeclaredThrowableException ex) {
            throw ex.getUndeclaredThrowable();
        }
    }

    // closeConnection
    @Test
    public void closeConnection_whenConnectionIsOk_shouldCloseConnection() throws Throwable {

        stubFor(get(urlEqualTo(GET_200_ENDPOINT_PATH))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader(HttpHeaders.CONTENT_TYPE, CONTENT_TYPE)
                        .withBody(GET_200_ENDPOINT_CONTENT)));
        HttpURLConnection connection = (HttpURLConnection) new URL(BASE_URL + GET_200_ENDPOINT_PATH).openConnection();
        try {
            ReflectionTestUtils.invokeMethod(CrawlerHttpClient.builder().build(), "closeConnection", connection);
        } catch (UndeclaredThrowableException ex) {
            throw ex.getUndeclaredThrowable();
        }
    }

    @Test
    public void closeConnection_whenConnectionIsClosed_shouldCloseConnection() throws Throwable {

        stubFor(get(urlEqualTo(GET_200_ENDPOINT_PATH))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader(HttpHeaders.CONTENT_TYPE, CONTENT_TYPE)
                        .withBody(GET_200_ENDPOINT_CONTENT)));
        HttpURLConnection connection = (HttpURLConnection) new URL(BASE_URL + GET_200_ENDPOINT_PATH).openConnection();
        connection.disconnect();
        try {
            ReflectionTestUtils.invokeMethod(CrawlerHttpClient.builder().build(), "closeConnection", connection);
        } catch (UndeclaredThrowableException ex) {
            throw ex.getUndeclaredThrowable();
        }
    }

    @Test
    public void closeConnection_whenConnectionIsNull_shouldCloseConnection() throws Throwable {

        try {
            ReflectionTestUtils.invokeMethod(CrawlerHttpClient.builder().build(), "closeConnection", (Object) null);
        } catch (UndeclaredThrowableException ex) {
            throw ex.getUndeclaredThrowable();
        }
    }
}