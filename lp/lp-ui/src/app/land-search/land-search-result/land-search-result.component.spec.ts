import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandSearchResultComponent } from './land-search-result.component';
import { TranslateModule } from '@ngx-translate/core';
import { MatIconModule, MatToolbarModule, MatProgressSpinner, MatProgressSpinnerModule, MatTableModule, MatPaginatorModule, MatTooltipModule, MatSnackBarModule, MatSnackBarRef, MAT_SNACK_BAR_DATA } from '@angular/material';
import { LandSearchTableComponent } from './land-search-table/land-search-table.component';

describe('LandSearchResultComponent', () => {
	let component: LandSearchResultComponent;
	let fixture: ComponentFixture<LandSearchResultComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [TranslateModule.forRoot(), MatToolbarModule, MatIconModule, MatProgressSpinnerModule, MatTableModule, MatPaginatorModule, MatTooltipModule, MatSnackBarModule],
			providers: [
				{
					provide: MatSnackBarRef,
					useValue: {}
				},
				{
					provide: MAT_SNACK_BAR_DATA,
					useValue: {}
				}
			],
			declarations: [LandSearchResultComponent, LandSearchTableComponent]
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(LandSearchResultComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
