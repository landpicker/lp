package cz.lp.exception;

/**
 * Processor extract data exception is generated when data extraction failed.
 */
public class ProcessorExtractDataException extends Exception {

    public ProcessorExtractDataException() {
    }

    public ProcessorExtractDataException(String message) {
        super(message);
    }

    public ProcessorExtractDataException(String message, Throwable cause) {
        super(message, cause);
    }
}
