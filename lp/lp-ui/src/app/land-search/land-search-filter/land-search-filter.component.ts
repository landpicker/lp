import { Component, Output, EventEmitter, Input, OnInit } from '@angular/core';
import { FormGroup } from '../../../../node_modules/@angular/forms';
import { createAcreageFilterFormGroup, getFilterAcreageFrom, getFilterAcreageTo } from '../../shared/acreage-filter/acreage-filter.component';
import { createLandTypeFilterFormGroup, getSelectedLandTypes } from '../../shared/land-type-filter/land-type-filter.component';
import { LandType } from '../../shared/land-type-filter/land-type.interface';
import { createAreaFilterFormGroup, getFilterAreaCodes } from '../../shared/area-filter/area-filter.component';
import { LandSearchFilter } from './land-search-filter.interface';
import { LandSearchService } from '../land-search.service';
import { Subscription } from 'rxjs';

const DEFAULT_ACREAGE_FROM_VALUE: number = 500;
const DEFAULT_ACREAGE_TO_VALUE: number = 1000;
const DEFAULT_LAND_TYPES: LandType[] = [LandType.GARDEN, LandType.ORCHARD];

export interface FilterChangeEvent {
	filter: LandSearchFilter;
}

@Component({
	selector: 'lp-land-search-filter',
	templateUrl: './land-search-filter.component.html',
	styleUrls: ['./land-search-filter.component.scss']
})
export class LandSearchFilterComponent implements OnInit {
	@Input()
	filter: LandSearchFilter;

	@Output()
	submitFilter = new EventEmitter<FilterChangeEvent>();
	landSearchForm: FormGroup;
	landSearchFormChanged: boolean;

	expectedCountOfLands: number;
	private _expectedCountOfLandsSubscription: Subscription;

	constructor(private _landSearchService: LandSearchService) {}

	ngOnInit() {
		if (this.filter) {
      this._initFilterFromFilter(this.filter);
      this._refreshExpectedCountOfLands();
		} else {
			this._initFilterWithDefaultValues();
		}

		this.landSearchForm.valueChanges.subscribe((val) => {
			this.landSearchFormChanged = true;
			this.expectedCountOfLands = undefined;
			if (this.landSearchForm.valid) {
				this._refreshExpectedCountOfLands();
			}
		});
	}

	get areaForm(): FormGroup {
		return <FormGroup>this.landSearchForm.get('areaForm');
	}

	get acreageForm(): FormGroup {
		return <FormGroup>this.landSearchForm.get('acreageForm');
	}

	get landTypeForm(): FormGroup {
		return <FormGroup>this.landSearchForm.get('landTypeForm');
	}

	get landSearchFilter(): LandSearchFilter {
		const areaCodes = getFilterAreaCodes(this.areaForm);
		const acreageFrom = getFilterAcreageFrom(this.acreageForm);
		const acreageTo = getFilterAcreageTo(this.acreageForm);
		const landTypes = getSelectedLandTypes(this.landTypeForm);
		return new LandSearchFilter(areaCodes, acreageFrom, acreageTo, landTypes);
	}

	private _initFilterFromFilter(filter: LandSearchFilter) {
		let areaForm: FormGroup = createAreaFilterFormGroup(filter.areaFilter.codes);
		let acreageForm: FormGroup = createAcreageFilterFormGroup(filter.landFilter.acreageFilter.from, filter.landFilter.acreageFilter.to);
		let landTypeForm: FormGroup = createLandTypeFilterFormGroup(filter.landFilter.landTypeFilter.landTypes);
		this.landSearchForm = new FormGroup({
			areaForm,
			acreageForm,
			landTypeForm
		});
	}

	private _initFilterWithDefaultValues() {
		let areaForm: FormGroup = createAreaFilterFormGroup();
		let acreageForm: FormGroup = createAcreageFilterFormGroup(DEFAULT_ACREAGE_FROM_VALUE, DEFAULT_ACREAGE_TO_VALUE);
		let landTypeForm: FormGroup = createLandTypeFilterFormGroup(DEFAULT_LAND_TYPES);
		this.landSearchForm = new FormGroup({
			areaForm,
			acreageForm,
			landTypeForm
		});
	}

	isValidAndUpdated(): boolean {
		return this.landSearchForm.valid && this.landSearchFormChanged;
	}

	private _refreshExpectedCountOfLands() {
		if (this._expectedCountOfLandsSubscription) this._expectedCountOfLandsSubscription.unsubscribe();
		this._expectedCountOfLandsSubscription = this._landSearchService.geExpectedCountOftLands(this.landSearchFilter).subscribe((expectedCount) => {
			this.expectedCountOfLands = expectedCount;
		});
	}

	onSubmit() {
		this.landSearchFormChanged = false;
		this.submitFilter.emit({ filter: this.landSearchFilter } as FilterChangeEvent);
	}
}
