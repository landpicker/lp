package cz.lp.repository;

import cz.lp.domain.CrawlerResultEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Set;

/**
 * Crawler result repository provides access to crawler result table.
 */
public interface CrawlerResultRepository extends CrudRepository<CrawlerResultEntity, Long> {

    /**
     * Check if result with specific URL already exists.
     *
     * @param url URL
     * @return true if result with specific URL exists, otherwise false
     */
    @Query(value = "SELECT CASE WHEN COUNT(id) = 1 THEN true ELSE false END FROM crawler_result WHERE url = :url LIMIT 1", nativeQuery = true)
    boolean existByUrl(@Param("url") String url);

    /**
     * Check if results with specific crawler already exist.
     *
     * @param crawlerId crawler identifier
     * @return true if results exist, otherwise false
     */
    @Query(value = "SELECT CASE WHEN COUNT(id) > 0 THEN TRUE ELSE FALSE END AS exist FROM (SELECT id FROM crawler_result WHERE crawler_id = :crawler_id) AS result_select", nativeQuery = true)
    Boolean existResultsOfCrawler(@Param("crawler_id") Long crawlerId);

    /**
     * Finds all identifiers by crawler identifier, processor identifier and processing order to process.
     * It used by processor.
     *
     * @param crawlerId       crawler identifier
     * @param processorId     processor identifier
     * @param processingOrder processing order
     * @return crawler result identifiers
     */
    @Query(value = "SELECT cr.id FROM crawler_result AS cr " +
            "LEFT JOIN processor_crawler_result AS pcr ON cr.id = pcr.crawler_result_id " +
            "WHERE cr.crawler_id = :crawler_id AND (pcr.processor_id = :processor_id OR pcr.processor_id IS NULL) AND (pcr.last_processed_on < cr.last_modified_on OR pcr.last_processed_on IS NULL) AND cr.status = 'SUCCESS' AND cr.processing_order = :processing_order", nativeQuery = true)
    Set<BigInteger> findAllIdentifiersByCrawlerIdAndProcessingOrder(@Param("crawler_id") Long crawlerId, @Param("processor_id") Long processorId, @Param("processing_order") Integer processingOrder);

    /**
     * Finds only one result by crawler identifier and last crawled on by processing order.
     * It used by crawler (RECRAWL ALL STRATEGY).
     *
     * @param crawlerId        crawler identifier
     * @param newLastCrawledOn new last crawled on
     * @return crawler result entity
     */
    @Query(value = "SELECT * FROM crawler_result WHERE crawler_id = :crawler_id AND (last_crawled_on IS NULL OR last_crawled_on <> :new_last_crawled_on) ORDER BY processing_order ASC LIMIT 1", nativeQuery = true)
    CrawlerResultEntity findOneByCrawlerIdAndLastRecrawledOnOrderByProcessingOrder(@Param("crawler_id") Long crawlerId, @Param("new_last_crawled_on") Timestamp newLastCrawledOn);

    /**
     * Finds only one result by crawler identifier and recrawl URL pattern and last recrawled on or not fetched by processing order.
     * It used by crawler (RECRAWL BY PATTERN STRATEGY).
     *
     * @param crawlerId         crawler identifier
     * @param recrawlUrlPattern recrawl URL pattern
     * @param newLastCrawledOn  new last crawled on
     * @return crawler result entity
     */
    @Query(value = "SELECT * FROM crawler_result WHERE crawler_id = :crawler_id AND ((url ~* :recrawl_url_pattern AND last_crawled_on <> :new_last_crawled_on) OR status = 'NOT_FETCHED') ORDER BY processing_order ASC LIMIT 1", nativeQuery = true)
    CrawlerResultEntity findOneByCrawlerIdByRecrawlUrlPatternAndLastRecrawledOnOrNotFetchedOrderByProcessingOrder(@Param("crawler_id") Long crawlerId, @Param("recrawl_url_pattern") String recrawlUrlPattern, @Param("new_last_crawled_on") Timestamp newLastCrawledOn);


}
