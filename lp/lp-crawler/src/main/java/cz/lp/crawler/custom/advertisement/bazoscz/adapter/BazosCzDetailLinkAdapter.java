package cz.lp.crawler.custom.advertisement.bazoscz.adapter;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Bazos CZ detail link adapter provides method for converting content to links.
 */
public class BazosCzDetailLinkAdapter {

    private static final String DETAIL_URL_SELECTOR = "table[class=inzeraty] a";

    private LinkedHashSet<String> links;

    public BazosCzDetailLinkAdapter(String content, String url) {

        Document document = Jsoup.parse(content, url, Parser.xmlParser());
        extractLinks(document);
    }

    /**
     * Extracts links.
     *
     * @param document document
     */
    private void extractLinks(Document document) {
        links = document.select(DETAIL_URL_SELECTOR).stream()
                .map(element -> element.attr("abs:href"))
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    /**
     * Gets links.
     *
     * @return links
     */
    public Optional<Set<String>> getLinks() {

        if (Objects.isNull(links) || links.isEmpty()) {
            Optional.empty();
        }
        return Optional.of(links);
    }
}
