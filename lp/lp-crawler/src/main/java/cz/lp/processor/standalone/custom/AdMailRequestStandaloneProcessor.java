package cz.lp.processor.standalone.custom;

import cz.lp.common.dto.ad.Ad;
import cz.lp.common.service.ad.AdService;
import cz.lp.common.service.mail.MailRequestService;
import cz.lp.processor.standalone.StandaloneProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static java.util.Objects.isNull;

/**
 * Ad-mail request standalone processor provides transforming ads to mail requests.
 */

@Component
public class AdMailRequestStandaloneProcessor implements StandaloneProcessor {

    private static final Logger log = LoggerFactory.getLogger(AdMailRequestStandaloneProcessor.class);

    private final MailRequestService mailRequestService;
    private final AdService adService;

    @Autowired
    public AdMailRequestStandaloneProcessor(MailRequestService mailRequestService, AdService adService) {
        this.mailRequestService = mailRequestService;
        this.adService = adService;
    }

    @Override
    public void execute() {

        Optional<Ad> ad;
        while ((ad = adService.getNextAdReadyToCreateMailRequest()).isPresent()) {
            Long mailRequestId = mailRequestService.createMailFromAd(ad.get());
            if (!isNull(mailRequestId)) {
                log.info("Processing ad with url: {}", ad.get().getUrl());
                adService.markAdWithCreatedMailRequest(ad.get().getUrl(), mailRequestId);
            }else {
                log.info("Creating mail from ad with url: {} failed, stopping mail service...", ad.get().getUrl());
                System.exit(1);
            }
        }
    }

}
