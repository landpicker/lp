import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from 'src/app/app-routing-module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { PageNotFoundComponent } from 'src/app/app-config/page-not-found/page-not-found.component';
import { LandSearchModule } from './land-search/land-search.module';
import { NotificationModule } from './shared/notification/notification.module';
import { GlobalErrorHandler } from './app-config/global-error-handler/global-error-handler';

export function createTranslateLoader(http: HttpClient) {
	return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
	declarations: [AppComponent, PageNotFoundComponent],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		AppRoutingModule,
		HttpClientModule,
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: createTranslateLoader,
				deps: [HttpClient]
			}
    }),
    NotificationModule,
    LandSearchModule
	],
	providers: [

    GlobalErrorHandler,
		{
			provide: ErrorHandler,
			useClass: GlobalErrorHandler
		}
	],
	bootstrap: [AppComponent]
})
export class AppModule {}
