package cz.lp.crawler.custom.vdparea.adapter;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;

public class VdpCityAreaDocumentAdapterTest {

    private static final String URL = "http://vdp.cuzk.cz/vdp/ruian/obce/export?vc.kod=&op.kod=&ok.kod=1234&pu.kod=&ob.nazev=&ob.statusKod=&ob.kod=&ohrada.id=&obg.sort=KOD&export=XML";

    @Test
    public void getLinks() throws Exception {

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("crawler/vdparea/city_valid_sample.xml");
        String content = IOUtils.toString(inputStream, StandardCharsets.UTF_8.name());

        Optional<Set<String>> links = new VdpCityAreaDocumentLinkAdapter(content, URL, null).getLinks();
        Assert.assertTrue(links.isPresent());
        Assert.assertNotNull(links);
        Assert.assertFalse(links.get().isEmpty());
        Assert.assertEquals(89, links.get().size());
        Iterator<String> iterator = links.get().iterator();
        Assert.assertTrue(iterator.hasNext());
        Assert.assertEquals("http://vdp.cuzk.cz/vdp/ruian/katastralniuzemi/export?ob.kod=500011&ku.nazev=&ku.kod=&ohrada.id=&kug.sort=KOD&export=XML", iterator.next());
    }

    @Test
    public void getLinks_whenDistrictElementsMissing_shouldReturnEmptySet() throws Exception {

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("crawler/vdparea/city_missing_city_code_elements_sample.xml");
        String content = IOUtils.toString(inputStream, StandardCharsets.UTF_8.name());
        Optional<Set<String>> links = new VdpCityAreaDocumentLinkAdapter(content, URL, null).getLinks();
        Assert.assertTrue(links.isPresent());
        Assert.assertNotNull(links);
        Assert.assertTrue(links.get().isEmpty());
    }

    @Test
    public void getLinks_whenCodeElementsMissing_shouldReturnEmptySet() throws Exception {

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("crawler/vdparea/city_missing_city_elements_samples.xml");
        String content = IOUtils.toString(inputStream, StandardCharsets.UTF_8.name());
        Optional<Set<String>> links = new VdpCityAreaDocumentLinkAdapter(content, URL, null).getLinks();
        Assert.assertTrue(links.isPresent());
        Assert.assertNotNull(links);
        Assert.assertTrue(links.get().isEmpty());
    }
}