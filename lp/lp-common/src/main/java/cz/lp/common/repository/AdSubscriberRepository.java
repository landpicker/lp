package cz.lp.common.repository;

import cz.lp.common.domain.ad.AdSubscriberEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Set;

public interface AdSubscriberRepository extends CrudRepository<AdSubscriberEntity, AdSubscriberEntity.AdSubscriberId>, JpaRepository<AdSubscriberEntity, AdSubscriberEntity.AdSubscriberId> {


    @Query(value = "SELECT subscriber_email FROM ad_subscriber WHERE :url ~* ad_url_pattern", nativeQuery = true)
    Set<String> findAllSubsriberEmailsByUrl(@Param("url") String url);
}
