package cz.lp.crawler.custom.advertisement.bazoscz.adapter;

import cz.lp.crawler.custom.vdparea.filter.VdpAreaCrawlerFilter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

/**
 * Bazos CZ page link adapter provides method for converting content to links.
 */
public class BazosCzPageLinkAdapter {

    private static final String PAGE_URL_SELECTOR = "div[class=strankovani] a";

    private LinkedHashSet<String> links;

    public BazosCzPageLinkAdapter(String content, String url) {

        Document document = Jsoup.parse(content, url, Parser.xmlParser());
        extractLinks(document);
    }

    /**
     * Extracts links.
     *
     * @param document document
     */
    private void extractLinks(Document document) {
        links = document.select(PAGE_URL_SELECTOR).stream()
                .map(element -> element.attr("abs:href"))
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    /**
     * Gets links.
     *
     * @return links
     */
    public Optional<Set<String>> getLinks() {

        if (Objects.isNull(links) || links.isEmpty()) {
            Optional.empty();
        }
        return Optional.of(links);
    }
}
