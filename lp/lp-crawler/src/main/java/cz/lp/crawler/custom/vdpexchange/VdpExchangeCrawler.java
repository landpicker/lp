package cz.lp.crawler.custom.vdpexchange;

import com.google.common.collect.Sets;
import cz.lp.crawler.Crawler;
import cz.lp.crawler.CrawlerFilter;
import cz.lp.crawler.SeedUrl;
import cz.lp.crawler.custom.vdpexchange.adapter.VdpExchangeGzLinkAdapter;
import cz.lp.crawler.resolver.CrawlerConditionResolver;
import cz.lp.crawler.service.CrawlerService;
import cz.lp.enumerate.CrawlerCondition;
import cz.lp.enumerate.RecrawlStrategy;
import cz.lp.util.GzipUtil;
import org.apache.commons.codec.digest.DigestUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;

/**
 * VDP exchange crawler provides crawling informations about cities, cadastral territories and lands from http://vdp.cuzk.cz/vdp/ruian/vymennyformat/vyhledej.
 */
public class VdpExchangeCrawler extends Crawler {

    private static final String EXCHANGE_SEED_URL_PATTERN = "(?i)https?://(www.)?vdp\\.cuzk\\.cz/vdp/ruian/vymennyformat/seznamlinku\\?vf.pu=S&_vf.pu=on&_vf.pu=on&vf.cr=U&vf.up=OB&vf.ds=K&_vf.vu=on&_vf.vu=on&_vf.vu=on&_vf.vu=on&vf.uo=O&ob.kod=\\d+&search=Vyhledat";
    private static final String GZ_URL_PATTERN = "https?://(?:www.)?vdp\\.cuzk\\.cz/vymenny_format/soucasna/\\d+_OB_\\d+\\w+\\.xml\\.gz";
    private static final String HASH_SUB_CONTENT_SELECTOR = "vf|Data";

    public VdpExchangeCrawler(String className, Long id, CrawlerCondition condition, Integer delay, RecrawlStrategy recrawlStrategy, String recrawlUrlPattern, Long recrawlInterval, LocalDateTime recrawlOn, Set<SeedUrl> seedUrls, CrawlerService crawlerService, CrawlerConditionResolver crawlerConditionResolver, CrawlerFilter crawlerFilter) {
        super(className, id, condition, delay, recrawlStrategy, recrawlUrlPattern, recrawlInterval, recrawlOn, seedUrls, crawlerService, crawlerConditionResolver, crawlerFilter);
    }

    @Override
    protected Optional<Set<String>> parseLinks(String content, String url) {

        if (url.matches(EXCHANGE_SEED_URL_PATTERN)) {

            return new VdpExchangeGzLinkAdapter(content, url).getLinks();

        } else {
            return Optional.of(Sets.newHashSet());
        }
    }

    @Override
    protected String generateContentHash(byte[] content, String url) {

        if (url.matches(GZ_URL_PATTERN)) {

            try {
                String unzippedContent = new String(GzipUtil.decompressGZIP(content));
                Document document = Jsoup.parse(new String(unzippedContent), url, Parser.xmlParser());
                Elements subContentElements = document.select(HASH_SUB_CONTENT_SELECTOR);
                if (subContentElements.isEmpty()) {
                    return null;
                }
                String subContent = document.select(HASH_SUB_CONTENT_SELECTOR).html();
                return DigestUtils.md5Hex(subContent).toUpperCase();
            } catch (IOException e) {
                return null;
            }

        } else {
            return DigestUtils.md5Hex(content).toUpperCase();
        }
    }
}
