import { Injectable } from "@angular/core";
import { ErrorHandler } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";
import { Router } from "@angular/router";
import { NgZone } from "@angular/core";
import { Injector } from "@angular/core";
import { LoggingService } from "src/app/app-config/global-error-handler/logging.service";

@Injectable({
	providedIn: "root"
})
export class GlobalErrorHandler implements ErrorHandler {

	defaultErrorHandler: ErrorHandler; 
	constructor(private log: LoggingService) {

		this.defaultErrorHandler = new ErrorHandler();
	}

	handleError(error) {
		this.log.error(error);
		this.defaultErrorHandler.handleError(error);
	}
}
