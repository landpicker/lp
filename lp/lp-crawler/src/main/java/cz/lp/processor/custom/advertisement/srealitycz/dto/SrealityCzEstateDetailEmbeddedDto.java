package cz.lp.processor.custom.advertisement.srealitycz.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.util.List;

@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class SrealityCzEstateDetailEmbeddedDto {

    private final List<SrealityCzImageDto> images;

    public SrealityCzEstateDetailEmbeddedDto(
            @JsonProperty("images") List<SrealityCzImageDto> images) {
        this.images = images;
    }
}
