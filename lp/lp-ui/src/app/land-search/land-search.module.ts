import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandSearchComponent } from './land-search.component';
import { MatSidenavModule, MatIconModule, MatButtonModule, MatFormFieldModule, MatDividerModule, MatToolbarModule, MatProgressSpinnerModule, MatTableModule, MatPaginatorModule, MatCardModule, MatPaginatorIntl, MatTooltipModule, MatSnackBarModule } from '../../../node_modules/@angular/material';
import { TranslateModule } from '../../../node_modules/@ngx-translate/core';
import { LandSearchFilterComponent } from './land-search-filter/land-search-filter.component';
import { AcreageFilterModule } from '../shared/acreage-filter/acreage-filter.module';
import { ReactiveFormsModule, FormsModule } from '../../../node_modules/@angular/forms';
import { LandSearchMapComponent } from './land-search-map/land-search-map.component';
import { MapModule } from '../shared/map/map.module';
import { LandTypeFilterModule } from '../shared/land-type-filter/land-type-filter.module';
import { AreaFilterModule } from '../shared/area-filter/area-filter.module';
import { LandSearchResultComponent } from './land-search-result/land-search-result.component';
import { LandSearchTablePaginationService } from './land-search-result/land-search-table/land-search-table-pagination.service';
import { LandSearchTableComponent } from './land-search-result/land-search-table/land-search-table.component';
import { LandSearchInfoComponent } from './land-search-info/land-search-info.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    AreaFilterModule,
    AcreageFilterModule,
    LandTypeFilterModule,
    MapModule,
    FormsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatSidenavModule,
    MatButtonModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatPaginatorModule,
    MatCardModule,
    MatTooltipModule
  ],
  providers: [{ provide: MatPaginatorIntl, useClass: LandSearchTablePaginationService }],

  declarations: [LandSearchComponent, LandSearchFilterComponent, LandSearchMapComponent, LandSearchTableComponent, LandSearchResultComponent, LandSearchInfoComponent]
})
export class LandSearchModule { }
