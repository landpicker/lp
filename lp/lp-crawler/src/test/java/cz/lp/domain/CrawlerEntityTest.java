package cz.lp.domain;

import com.google.common.collect.Lists;
import cz.lp.enumerate.CrawlerCondition;
import cz.lp.enumerate.RecrawlStrategy;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.sql.Timestamp;
import java.util.List;

public class CrawlerEntityTest {

    private static final Long ID = 1l;
    private static final SourceEntity SOURCE = Mockito.mock(SourceEntity.class);
    private static final String CRAWLER_CLASS = "Class";
    private static final CrawlerCondition CRAWLER_CONDITION = CrawlerCondition.NONE;
    private static final Integer DELAY = 1000;
    private static final Boolean ENABLED = Boolean.TRUE;
    private static final RecrawlStrategy RECRAWL_STRATEGY = RecrawlStrategy.RECRAWL_ALL;
    private static final String RECRAWL_URL_PATTERN = ".*";
    private static final Long RECRAWL_INTERVAL = 1000l;
    private static final Timestamp RECRAWL_ON = Mockito.mock(Timestamp.class);
    private static final Timestamp LAST_RECRAWLED_ON = Mockito.mock(Timestamp.class);
    private static final List<CrawlerSeedUrlEntity> CRAWLER_SEED_URLS = Lists.newArrayList(Mockito.mock(CrawlerSeedUrlEntity.class));
    private static final List<CrawlerResultEntity> CRAWLER_RESULTS = Lists.newArrayList(Mockito.mock(CrawlerResultEntity.class));
    private static final List<ProcessorEntity> PROCESSORS = Lists.newArrayList(Mockito.mock(ProcessorEntity.class));

    @Test
    public void constructor_empty(){

        CrawlerEntity crawler = new CrawlerEntity();
        Assert.assertNotNull(crawler);
        Assert.assertNull(crawler.getId());
        Assert.assertNull(crawler.getSource());
        Assert.assertNull(crawler.getCrawlerClass());
        Assert.assertNull(crawler.getCondition());
        Assert.assertNull(crawler.getDelay());
        Assert.assertNull(crawler.isEnabled());
        Assert.assertNull(crawler.getRecrawlStrategy());
        Assert.assertNull(crawler.getRecrawlInterval());
        Assert.assertNull(crawler.getRecrawlOn());
        Assert.assertNull(crawler.getLastRecrawledOn());
        Assert.assertNull(crawler.getCrawlerSeedUrls());
        Assert.assertNull(crawler.getCrawlerResults());
        Assert.assertNull(crawler.getProcessors());
    }

    @Test
    public void constructor(){

        CrawlerEntity crawler = new CrawlerEntity(ID, SOURCE, CRAWLER_CLASS, CRAWLER_CONDITION, DELAY, ENABLED, RECRAWL_STRATEGY, RECRAWL_URL_PATTERN, RECRAWL_INTERVAL, RECRAWL_ON, LAST_RECRAWLED_ON, CRAWLER_SEED_URLS, CRAWLER_RESULTS, PROCESSORS);
        Assert.assertNotNull(crawler);
        Assert.assertEquals(ID, crawler.getId());
        Assert.assertEquals(SOURCE, crawler.getSource());
        Assert.assertEquals(CRAWLER_CLASS, crawler.getCrawlerClass());
        Assert.assertEquals(CRAWLER_CONDITION, crawler.getCondition());
        Assert.assertEquals(DELAY, crawler.getDelay());
        Assert.assertEquals(ENABLED, crawler.isEnabled());
        Assert.assertEquals(RECRAWL_STRATEGY, crawler.getRecrawlStrategy());
        Assert.assertEquals(RECRAWL_URL_PATTERN, crawler.getRecrawlUrlPattern());
        Assert.assertEquals(RECRAWL_INTERVAL, crawler.getRecrawlInterval());
        Assert.assertEquals(RECRAWL_ON, crawler.getRecrawlOn());
        Assert.assertEquals(LAST_RECRAWLED_ON, crawler.getLastRecrawledOn());
        Assert.assertEquals(CRAWLER_SEED_URLS, crawler.getCrawlerSeedUrls());
        Assert.assertEquals(CRAWLER_RESULTS, crawler.getCrawlerResults());
        Assert.assertEquals(PROCESSORS, crawler.getProcessors());
    }

    @Test
    public void getId() throws Exception {

        CrawlerEntity crawler = new CrawlerEntity(ID, SOURCE, CRAWLER_CLASS, CRAWLER_CONDITION, DELAY, ENABLED, RECRAWL_STRATEGY, RECRAWL_URL_PATTERN, RECRAWL_INTERVAL, RECRAWL_ON, LAST_RECRAWLED_ON, CRAWLER_SEED_URLS, CRAWLER_RESULTS, PROCESSORS);
        Assert.assertNotNull(crawler);
        Assert.assertEquals(ID, crawler.getId());
    }

    @Test
    public void getSource() throws Exception {

        CrawlerEntity crawler = new CrawlerEntity(ID, SOURCE, CRAWLER_CLASS, CRAWLER_CONDITION, DELAY, ENABLED, RECRAWL_STRATEGY, RECRAWL_URL_PATTERN, RECRAWL_INTERVAL, RECRAWL_ON, LAST_RECRAWLED_ON, CRAWLER_SEED_URLS, CRAWLER_RESULTS, PROCESSORS);
        Assert.assertNotNull(crawler);
        Assert.assertEquals(SOURCE, crawler.getSource());
    }

    @Test
    public void getCrawlerClass() throws Exception {

        CrawlerEntity crawler = new CrawlerEntity(ID, SOURCE, CRAWLER_CLASS, CRAWLER_CONDITION, DELAY, ENABLED, RECRAWL_STRATEGY, RECRAWL_URL_PATTERN, RECRAWL_INTERVAL, RECRAWL_ON, LAST_RECRAWLED_ON, CRAWLER_SEED_URLS, CRAWLER_RESULTS, PROCESSORS);
        Assert.assertNotNull(crawler);
        Assert.assertEquals(CRAWLER_CLASS, crawler.getCrawlerClass());
    }

    @Test
    public void getCondition() throws Exception {

        CrawlerEntity crawler = new CrawlerEntity(ID, SOURCE, CRAWLER_CLASS, CRAWLER_CONDITION, DELAY, ENABLED, RECRAWL_STRATEGY, RECRAWL_URL_PATTERN, RECRAWL_INTERVAL, RECRAWL_ON, LAST_RECRAWLED_ON, CRAWLER_SEED_URLS, CRAWLER_RESULTS, PROCESSORS);
        Assert.assertNotNull(crawler);
        Assert.assertEquals(CRAWLER_CONDITION, crawler.getCondition());
    }

    @Test
    public void getDelay() throws Exception {

        CrawlerEntity crawler = new CrawlerEntity(ID, SOURCE, CRAWLER_CLASS, CRAWLER_CONDITION, DELAY, ENABLED, RECRAWL_STRATEGY, RECRAWL_URL_PATTERN, RECRAWL_INTERVAL, RECRAWL_ON, LAST_RECRAWLED_ON, CRAWLER_SEED_URLS, CRAWLER_RESULTS, PROCESSORS);
        Assert.assertNotNull(crawler);
        Assert.assertEquals(DELAY, crawler.getDelay());
    }

    @Test
    public void isEnabled() throws Exception {

        CrawlerEntity crawler = new CrawlerEntity(ID, SOURCE, CRAWLER_CLASS, CRAWLER_CONDITION, DELAY, ENABLED, RECRAWL_STRATEGY, RECRAWL_URL_PATTERN, RECRAWL_INTERVAL, RECRAWL_ON, LAST_RECRAWLED_ON, CRAWLER_SEED_URLS, CRAWLER_RESULTS, PROCESSORS);
        Assert.assertNotNull(crawler);
        Assert.assertEquals(ENABLED, crawler.isEnabled());
    }

    @Test
    public void setEnabled() throws Exception {

        CrawlerEntity crawler = new CrawlerEntity();
        Assert.assertNotNull(crawler);
        Assert.assertNull(crawler.isEnabled());
        crawler.setEnabled(ENABLED);
        Assert.assertEquals(ENABLED, crawler.isEnabled());
    }

    @Test
    public void getRecrawlStrategy() throws Exception {

        CrawlerEntity crawler = new CrawlerEntity(ID, SOURCE, CRAWLER_CLASS, CRAWLER_CONDITION, DELAY, ENABLED, RECRAWL_STRATEGY, RECRAWL_URL_PATTERN, RECRAWL_INTERVAL, RECRAWL_ON, LAST_RECRAWLED_ON, CRAWLER_SEED_URLS, CRAWLER_RESULTS, PROCESSORS);
        Assert.assertNotNull(crawler);
        Assert.assertEquals(RECRAWL_STRATEGY, crawler.getRecrawlStrategy());
    }

    @Test
    public void getRecrawlUrlPattern() throws Exception {

        CrawlerEntity crawler = new CrawlerEntity(ID, SOURCE, CRAWLER_CLASS, CRAWLER_CONDITION, DELAY, ENABLED, RECRAWL_STRATEGY, RECRAWL_URL_PATTERN, RECRAWL_INTERVAL, RECRAWL_ON, LAST_RECRAWLED_ON, CRAWLER_SEED_URLS, CRAWLER_RESULTS, PROCESSORS);
        Assert.assertNotNull(crawler);
        Assert.assertEquals(RECRAWL_URL_PATTERN, crawler.getRecrawlUrlPattern());
    }

    @Test
    public void getRecrawlInterval() throws Exception {

        CrawlerEntity crawler = new CrawlerEntity(ID, SOURCE, CRAWLER_CLASS, CRAWLER_CONDITION, DELAY, ENABLED, RECRAWL_STRATEGY, RECRAWL_URL_PATTERN, RECRAWL_INTERVAL, RECRAWL_ON, LAST_RECRAWLED_ON, CRAWLER_SEED_URLS, CRAWLER_RESULTS, PROCESSORS);
        Assert.assertNotNull(crawler);
        Assert.assertEquals(RECRAWL_INTERVAL, crawler.getRecrawlInterval());
    }

    @Test
    public void getRecrawlOn() throws Exception {

        CrawlerEntity crawler = new CrawlerEntity(ID, SOURCE, CRAWLER_CLASS, CRAWLER_CONDITION, DELAY, ENABLED, RECRAWL_STRATEGY, RECRAWL_URL_PATTERN, RECRAWL_INTERVAL, RECRAWL_ON, LAST_RECRAWLED_ON, CRAWLER_SEED_URLS, CRAWLER_RESULTS, PROCESSORS);
        Assert.assertNotNull(crawler);
        Assert.assertEquals(RECRAWL_ON, crawler.getRecrawlOn());
    }

    @Test
    public void setRecrawlOn() throws Exception {

        CrawlerEntity crawler = new CrawlerEntity();
        Assert.assertNotNull(crawler);
        Assert.assertNull(crawler.getRecrawlOn());
        crawler.setRecrawlOn(RECRAWL_ON);
        Assert.assertEquals(RECRAWL_ON, crawler.getRecrawlOn());
    }

    @Test
    public void getLastRecrawledOn() throws Exception {

        CrawlerEntity crawler = new CrawlerEntity(ID, SOURCE, CRAWLER_CLASS, CRAWLER_CONDITION, DELAY, ENABLED, RECRAWL_STRATEGY, RECRAWL_URL_PATTERN, RECRAWL_INTERVAL, RECRAWL_ON, LAST_RECRAWLED_ON, CRAWLER_SEED_URLS, CRAWLER_RESULTS, PROCESSORS);
        Assert.assertNotNull(crawler);
        Assert.assertEquals(LAST_RECRAWLED_ON, crawler.getLastRecrawledOn());
    }

    @Test
    public void setLastRecrawledOn() throws Exception {

        CrawlerEntity crawler = new CrawlerEntity();
        Assert.assertNotNull(crawler);
        Assert.assertNull(crawler.getLastRecrawledOn());
        crawler.setLastRecrawledOn(LAST_RECRAWLED_ON);
        Assert.assertEquals(LAST_RECRAWLED_ON, crawler.getLastRecrawledOn());
    }

    @Test
    public void getCrawlerSeedUrls() throws Exception {

        CrawlerEntity crawler = new CrawlerEntity(ID, SOURCE, CRAWLER_CLASS, CRAWLER_CONDITION, DELAY, ENABLED, RECRAWL_STRATEGY, RECRAWL_URL_PATTERN, RECRAWL_INTERVAL, RECRAWL_ON, LAST_RECRAWLED_ON, CRAWLER_SEED_URLS, CRAWLER_RESULTS, PROCESSORS);
        Assert.assertNotNull(crawler);
        Assert.assertEquals(CRAWLER_SEED_URLS, crawler.getCrawlerSeedUrls());
    }

    @Test
    public void getCrawlerResults() throws Exception {

        CrawlerEntity crawler = new CrawlerEntity(ID, SOURCE, CRAWLER_CLASS, CRAWLER_CONDITION, DELAY, ENABLED, RECRAWL_STRATEGY, RECRAWL_URL_PATTERN, RECRAWL_INTERVAL, RECRAWL_ON, LAST_RECRAWLED_ON, CRAWLER_SEED_URLS, CRAWLER_RESULTS, PROCESSORS);
        Assert.assertNotNull(crawler);
        Assert.assertEquals(CRAWLER_RESULTS, crawler.getCrawlerResults());
    }

    @Test
    public void getProcessors() throws Exception {

        CrawlerEntity crawler = new CrawlerEntity(ID, SOURCE, CRAWLER_CLASS, CRAWLER_CONDITION, DELAY, ENABLED, RECRAWL_STRATEGY, RECRAWL_URL_PATTERN, RECRAWL_INTERVAL, RECRAWL_ON, LAST_RECRAWLED_ON, CRAWLER_SEED_URLS, CRAWLER_RESULTS, PROCESSORS);
        Assert.assertNotNull(crawler);
        Assert.assertEquals(PROCESSORS, crawler.getProcessors());
    }
}