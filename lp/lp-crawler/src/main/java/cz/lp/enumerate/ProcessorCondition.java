package cz.lp.enumerate;

/**
 * ProcessorEntity condition provides enumerate of conditions for execute processor.
 */

public enum ProcessorCondition {

    NONE,
    EXIST_VDP_AREA_CRAWLER_RESULTS,
    EXIST_VDP_EXCHANGE_CRAWLER_RESULTS,
    EXIST_BAZOS_CZ_CRAWLER_RESULTS,
    EXIST_SREALITY_CZ_CRAWLER_RESULTS
}
