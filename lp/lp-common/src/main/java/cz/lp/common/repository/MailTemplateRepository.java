package cz.lp.common.repository;

import cz.lp.common.domain.mail.MailRequestEntity;
import cz.lp.common.domain.mail.MailTemplateEntity;
import cz.lp.common.enumerate.MailRequestStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MailTemplateRepository extends CrudRepository<MailTemplateEntity, Long>, JpaRepository<MailTemplateEntity, Long> {

}
