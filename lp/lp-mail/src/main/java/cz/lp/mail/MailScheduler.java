package cz.lp.mail;

import cz.lp.mail.service.MailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Mail scheduler defines when mail requests ready to send will be executed.
 */
@Component
public class MailScheduler {

    private static final Logger log = LoggerFactory.getLogger(MailScheduler.class);

    private MailService mailService;

    @Autowired
    public MailScheduler(MailService mailService) {
        this.mailService = mailService;
    }

    /**
     * Check every 5 minutes for emails ready to send.
     */
    @Scheduled(fixedDelay = 300000)
    public void executeEmailRequests() {

        log.info("Executing mail requests...");
        this.mailService.sendAllPreparedEmails();
        log.info("Executing mail requests is done");
    }
}
