package cz.lp.domain;

import cz.lp.enumerate.ResultStatus;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Crawler result entity represents object in database table and contains results of crawling.
 */
@Entity
@Table(name = "crawler_result")
public class CrawlerResultEntity implements Serializable {

    @Id
    @SequenceGenerator(name = "id", sequenceName = "crawler_result_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "crawler_id", nullable = false)
    private CrawlerEntity crawler;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false, length = 50)
    private ResultStatus status;

    @Column(name = "url", unique = true, nullable = false)
    private String url;

    @Column(name = "content_type", length = 50)
    private String contentType;

    @Column(name = "content")
    private byte[] content;

    @Column(name = "hash", length = 255)
    private String hash;

    @Column(name = "message")
    private String message;

    @Column(name = "processing_order")
    private Integer processingOrder;

    @Column(name = "created_on")
    private Timestamp createdOn;

    @Column(name = "last_crawled_on")
    private Timestamp lastCrawledOn;

    @Column(name = "last_modified_on")
    private Timestamp lastModifiedOn;

    /**
     * Creates default constructor.
     */
    public CrawlerResultEntity() {
    }

    /**
     * Creates instance of crawler result entity.
     *
     * @param id              identifier
     * @param crawler         crawler
     * @param status          status
     * @param url             URL
     * @param contentType     content type
     * @param content         content
     * @param hash            hash
     * @param message         message
     * @param processingOrder processing order
     * @param createdOn       created on
     * @param lastCrawledOn   last crawled on
     */
    public CrawlerResultEntity(Long id, CrawlerEntity crawler, ResultStatus status, String url, String contentType,
                               byte[] content, String hash, String message, Integer processingOrder, Timestamp createdOn, Timestamp lastCrawledOn, Timestamp lastModifiedOn) {
        this.id = id;
        this.crawler = crawler;
        this.status = status;
        this.url = url;
        this.contentType = contentType;
        this.content = content;
        this.hash = hash;
        this.message = message;
        this.createdOn = createdOn;
        this.processingOrder = processingOrder;
        this.lastCrawledOn = lastCrawledOn;
        this.lastModifiedOn = lastModifiedOn;
    }

    /**
     * Creates instance of crawler result entity.
     *
     * @param crawler         crawler
     * @param status          status
     * @param url             URL
     * @param createdOn       created on
     * @param processingOrder processing order
     */
    public CrawlerResultEntity(CrawlerEntity crawler, ResultStatus status, String url, Integer processingOrder, Timestamp createdOn) {
        this.crawler = crawler;
        this.status = status;
        this.url = url;
        this.processingOrder = processingOrder;
        this.createdOn = createdOn;
    }

    /**
     * Gets identifier.
     *
     * @return identifier
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets crawler.
     *
     * @return crawler
     */
    public CrawlerEntity getCrawler() {
        return crawler;
    }

    /**
     * Gets status.
     *
     * @return status
     */
    public ResultStatus getStatus() {
        return status;
    }


    /**
     * Sets status.
     *
     * @param status status
     */
    public void setStatus(ResultStatus status) {
        this.status = status;
    }

    /**
     * Gets URL.
     *
     * @return URL
     */
    public String getUrl() {
        return url;
    }

    /**
     * Gets content type.
     *
     * @return content type
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * Sets content type.
     *
     * @param contentType content type
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /**
     * Gets content.
     *
     * @return content
     */
    public byte[] getContent() {
        return content;
    }

    /**
     * Sets content.
     *
     * @param content content
     */
    public void setContent(byte[] content) {
        this.content = content;
    }

    /**
     * Gets hash.
     *
     * @return hash
     */
    public String getHash() {
        return hash;
    }

    /**
     * Sets hash.
     *
     * @param hash hash
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * Gets message.
     *
     * @return message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets message.
     *
     * @param message message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Gets processing order.
     *
     * @return processing order
     */
    public Integer getProcessingOrder() {
        return processingOrder;
    }

    /**
     * Gets created on.
     *
     * @return created on
     */
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    /**
     * Gets last crawled on.
     *
     * @return last crawled on
     */
    public Timestamp getLastCrawledOn() {
        return lastCrawledOn;
    }

    /**
     * Sets last crawled on.
     *
     * @param lastCrawledOn last crawled on
     */
    public void setLastCrawledOn(Timestamp lastCrawledOn) {
        this.lastCrawledOn = lastCrawledOn;
    }

    /**
     * Gets last modified on.
     *
     * @return last modified on
     */
    public Timestamp getLastModifiedOn() {
        return lastModifiedOn;
    }

    /**
     * Sets last modified on.
     *
     * @param lastModifiedOn last modified on
     */
    public void setLastModifiedOn(Timestamp lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }
}
