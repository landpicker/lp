package cz.lp.crawler.context;

import cz.lp.domain.CrawlerResultEntity;
import cz.lp.enumerate.ResultStatus;

import java.time.LocalDateTime;
import java.util.Set;

/**
 * Result context represents shared context between all steps of crawling.
 */
public class ResultContext {

    private final Long resultId;
    private final String url;
    private final Integer processingOrder;
    private final LocalDateTime createdOn;

    private ResultStatus status;
    private byte[] content;
    private String contentType;
    private String hash;
    private String message;
    private Set<String> extractedLinks;
    private final LocalDateTime lastCrawledOn;

    /**
     * Creates instance of result context.
     *
     * @param resultId        result identifier
     * @param url             URL
     * @param processingOrder processing order
     * @param createdOn       created on
     * @param lastCrawledOn   last crawled on
     */
    private ResultContext(Long resultId, String url, Integer processingOrder, LocalDateTime createdOn, LocalDateTime lastCrawledOn) {
        this.resultId = resultId;
        this.url = url;
        this.processingOrder = processingOrder;
        this.createdOn = createdOn;
        this.lastCrawledOn = lastCrawledOn;
    }

    /**
     * Creates instance of result context from result entity.
     *
     * @param resultEntity crawler result entity
     * @return result context
     */
    public static ResultContext of(CrawlerResultEntity resultEntity, LocalDateTime lastCrawledOn) {

        return new ResultContext(resultEntity.getId(),
                resultEntity.getUrl(),
                resultEntity.getProcessingOrder(),
                resultEntity.getCreatedOn().toLocalDateTime(),
                lastCrawledOn);

    }

    /**
     * Gets result identifier.
     *
     * @return result identifier
     */
    public Long getResultId() {
        return resultId;
    }

    /**
     * Gets URL.
     *
     * @return URL
     */
    public String getUrl() {
        return url;
    }

    /**
     * Gets created on.
     *
     * @return created on
     */
    public LocalDateTime getCreatedOn() {
        return createdOn;
    }


    /**
     * Gets processing order.
     *
     * @return processing order
     */
    public Integer getProcessingOrder() {
        return processingOrder;
    }

    /**
     * Gets last crawled on.
     *
     * @return last crawled on
     */
    public LocalDateTime getLastCrawledOn() {
        return lastCrawledOn;
    }

    /**
     * Gets status.
     *
     * @return status
     */
    public ResultStatus getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status status
     */
    public void setStatus(ResultStatus status) {
        this.status = status;
    }

    /**
     * Gets content as bytes.
     *
     * @return content
     */
    public byte[] getContent() {
        return content;
    }

    /**
     * Gets content as string.
     *
     * @return content
     */
    public String getContentAsString() {
        return new String(content);
    }

    /**
     * Sets content.
     *
     * @param content content
     */
    public void setContent(byte[] content) {
        this.content = content;
    }

    /**
     * Gets content type.
     *
     * @return content type
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * Sets content type.
     *
     * @param contentType content type
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /**
     * Gets hash.
     *
     * @return hash
     */
    public String getHash() {
        return hash;
    }

    /**
     * Sets hash.
     *
     * @param hash hash
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * Gets message.
     *
     * @return message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets message.
     *
     * @param message message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Gets extracted links.
     *
     * @return links
     */
    public Set<String> getExtractedLinks() {
        return extractedLinks;
    }

    /**
     * Sets extracted links
     *
     * @param extractedLinks link
     */
    public void setExtractedLinks(Set<String> extractedLinks) {
        this.extractedLinks = extractedLinks;
    }
}



