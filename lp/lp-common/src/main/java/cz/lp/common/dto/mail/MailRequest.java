package cz.lp.common.dto.mail;

import cz.lp.common.domain.mail.MailRequestEntity;
import lombok.*;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@AllArgsConstructor
@Builder(builderMethodName = "custom")
@Value
@Getter
@ToString
public class MailRequest {

    private final Long id;
    private final String subject;
    private final String content;
    private final Set<String> subscribers;
    private final List<MailRequestAttachment> attachments;

    public static MailRequestBuilder builder(String subject, String content, Set<String> subscribers) {
        return custom()
                .subject(subject)
                .content(content)
                .subscribers(subscribers);
    }

    public static MailRequest from(MailRequestEntity mailRequestEntity){

        return new MailRequest(
                mailRequestEntity.getId(),
                mailRequestEntity.getSubject(),
                mailRequestEntity.getContent(),
                mailRequestEntity.getSubscribers(),
                mailRequestEntity.getAttachments().stream().map(MailRequestAttachment::from).collect(Collectors.toList()));
    }
}
