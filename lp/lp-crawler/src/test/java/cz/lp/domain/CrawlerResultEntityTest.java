package cz.lp.domain;

import cz.lp.enumerate.ResultStatus;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.sql.Timestamp;

public class CrawlerResultEntityTest {

    private static final Long ID = 1l;
    private static final CrawlerEntity CRAWLER = Mockito.mock(CrawlerEntity.class);
    private static final ResultStatus RESULT_STATUS = ResultStatus.SUCCESS;
    private static final String URL = "http://";
    private static final String CONTENT_TYPE = "text/html";
    private static final byte[] CONTENT = "<html></html>".getBytes();
    private static final String HASH = "65as4er68dvf24sdc6sgsed";
    private static final String MESSAGE = "";
    private static final Integer PROCESSING_ORDER = 1;
    private static final Timestamp CREATED_ON = Mockito.mock(Timestamp.class);
    private static final Timestamp LAST_CRAWLED_ON = Mockito.mock(Timestamp.class);
    private static final Timestamp LAST_MODIFIED_ON = Mockito.mock(Timestamp.class);

    @Test
    public void constructor_empty() throws Exception {

        CrawlerResultEntity crawlerResult = new CrawlerResultEntity();
        Assert.assertNotNull(crawlerResult);
        Assert.assertNull(crawlerResult.getId());
        Assert.assertNull(crawlerResult.getCrawler());
        Assert.assertNull(crawlerResult.getStatus());
        Assert.assertNull(crawlerResult.getUrl());
        Assert.assertNull(crawlerResult.getContentType());
        Assert.assertNull(crawlerResult.getContent());
        Assert.assertNull(crawlerResult.getHash());
        Assert.assertNull(crawlerResult.getMessage());
        Assert.assertNull(crawlerResult.getProcessingOrder());
        Assert.assertNull(crawlerResult.getCreatedOn());
        Assert.assertNull(crawlerResult.getLastCrawledOn());
        Assert.assertNull(crawlerResult.getLastModifiedOn());
    }


    @Test
    public void constructor() throws Exception {

        CrawlerResultEntity crawlerResult = new CrawlerResultEntity(ID, CRAWLER, RESULT_STATUS, URL, CONTENT_TYPE, CONTENT, HASH, MESSAGE, PROCESSING_ORDER, CREATED_ON, LAST_CRAWLED_ON, LAST_MODIFIED_ON);
        Assert.assertNotNull(crawlerResult);
        Assert.assertEquals(ID, crawlerResult.getId());
        Assert.assertEquals(CRAWLER, crawlerResult.getCrawler());
        Assert.assertEquals(RESULT_STATUS, crawlerResult.getStatus());
        Assert.assertEquals(URL, crawlerResult.getUrl());
        Assert.assertEquals(CONTENT_TYPE, crawlerResult.getContentType());
        Assert.assertEquals(CONTENT, crawlerResult.getContent());
        Assert.assertEquals(HASH, crawlerResult.getHash());
        Assert.assertEquals(MESSAGE, crawlerResult.getMessage());
        Assert.assertEquals(PROCESSING_ORDER, crawlerResult.getProcessingOrder());
        Assert.assertEquals(CREATED_ON, crawlerResult.getCreatedOn());
        Assert.assertEquals(LAST_CRAWLED_ON, crawlerResult.getLastCrawledOn());
        Assert.assertEquals(LAST_MODIFIED_ON, crawlerResult.getLastModifiedOn());
    }

    @Test
    public void constructor_newResult() throws Exception {

        CrawlerResultEntity crawlerResult = new CrawlerResultEntity(CRAWLER, RESULT_STATUS, URL, PROCESSING_ORDER, CREATED_ON);
        Assert.assertNotNull(crawlerResult);
        Assert.assertEquals(CRAWLER, crawlerResult.getCrawler());
        Assert.assertEquals(RESULT_STATUS, crawlerResult.getStatus());
        Assert.assertEquals(URL, crawlerResult.getUrl());
        Assert.assertEquals(PROCESSING_ORDER, crawlerResult.getProcessingOrder());
        Assert.assertEquals(CREATED_ON, crawlerResult.getCreatedOn());
    }

    @Test
    public void getId() throws Exception {

        CrawlerResultEntity crawlerResult = new CrawlerResultEntity(ID, CRAWLER, RESULT_STATUS, URL, CONTENT_TYPE, CONTENT, HASH, MESSAGE, PROCESSING_ORDER, CREATED_ON, LAST_CRAWLED_ON, LAST_MODIFIED_ON);
        Assert.assertNotNull(crawlerResult);
        Assert.assertEquals(ID, crawlerResult.getId());
    }

    @Test
    public void getCrawlerSeedUrl() throws Exception {

        CrawlerResultEntity crawlerResult = new CrawlerResultEntity(ID, CRAWLER, RESULT_STATUS, URL, CONTENT_TYPE, CONTENT, HASH, MESSAGE, PROCESSING_ORDER, CREATED_ON, LAST_CRAWLED_ON, LAST_MODIFIED_ON);
        Assert.assertNotNull(crawlerResult);
        Assert.assertEquals(CRAWLER, crawlerResult.getCrawler());
    }

    @Test
    public void getStatus() throws Exception {

        CrawlerResultEntity crawlerResult = new CrawlerResultEntity(ID, CRAWLER, RESULT_STATUS, URL, CONTENT_TYPE, CONTENT, HASH, MESSAGE, PROCESSING_ORDER, CREATED_ON, LAST_CRAWLED_ON, LAST_MODIFIED_ON);
        Assert.assertNotNull(crawlerResult);
        Assert.assertEquals(RESULT_STATUS, crawlerResult.getStatus());
    }

    @Test
    public void setStatus() throws Exception {

        CrawlerResultEntity crawlerResult = new CrawlerResultEntity();
        Assert.assertNotNull(crawlerResult);
        Assert.assertNull(crawlerResult.getStatus());
        crawlerResult.setStatus(RESULT_STATUS);
        Assert.assertEquals(RESULT_STATUS, crawlerResult.getStatus());
    }

    @Test
    public void getUrl() throws Exception {

        CrawlerResultEntity crawlerResult = new CrawlerResultEntity(ID, CRAWLER, RESULT_STATUS, URL, CONTENT_TYPE, CONTENT, HASH, MESSAGE, PROCESSING_ORDER, CREATED_ON, LAST_CRAWLED_ON, LAST_MODIFIED_ON);
        Assert.assertNotNull(crawlerResult);
        Assert.assertEquals(URL, crawlerResult.getUrl());
    }

    @Test
    public void getContentType() throws Exception {

        CrawlerResultEntity crawlerResult = new CrawlerResultEntity(ID, CRAWLER, RESULT_STATUS, URL, CONTENT_TYPE, CONTENT, HASH, MESSAGE, PROCESSING_ORDER, CREATED_ON, LAST_CRAWLED_ON, LAST_MODIFIED_ON);
        Assert.assertNotNull(crawlerResult);
        Assert.assertEquals(CONTENT_TYPE, crawlerResult.getContentType());
    }

    @Test
    public void setContentType() throws Exception {

        CrawlerResultEntity crawlerResult = new CrawlerResultEntity();
        Assert.assertNotNull(crawlerResult);
        Assert.assertNull(crawlerResult.getContentType());
        crawlerResult.setContentType(CONTENT_TYPE);
        Assert.assertEquals(CONTENT_TYPE, crawlerResult.getContentType());
    }

    @Test
    public void getContent() throws Exception {

        CrawlerResultEntity crawlerResult = new CrawlerResultEntity(ID, CRAWLER, RESULT_STATUS, URL, CONTENT_TYPE, CONTENT, HASH, MESSAGE, PROCESSING_ORDER, CREATED_ON, LAST_CRAWLED_ON, LAST_MODIFIED_ON);
        Assert.assertNotNull(crawlerResult);
        Assert.assertEquals(CONTENT, crawlerResult.getContent());
    }

    @Test
    public void setContent() throws Exception {

        CrawlerResultEntity crawlerResult = new CrawlerResultEntity();
        Assert.assertNotNull(crawlerResult);
        Assert.assertNull(crawlerResult.getContent());
        crawlerResult.setContent(CONTENT);
        Assert.assertEquals(CONTENT, crawlerResult.getContent());
    }

    @Test
    public void getHash() throws Exception {

        CrawlerResultEntity crawlerResult = new CrawlerResultEntity(ID, CRAWLER, RESULT_STATUS, URL, CONTENT_TYPE, CONTENT, HASH, MESSAGE, PROCESSING_ORDER, CREATED_ON, LAST_CRAWLED_ON, LAST_MODIFIED_ON);
        Assert.assertNotNull(crawlerResult);
        Assert.assertEquals(HASH, crawlerResult.getHash());
    }

    @Test
    public void setHash() throws Exception {

        CrawlerResultEntity crawlerResult = new CrawlerResultEntity();
        Assert.assertNotNull(crawlerResult);
        Assert.assertNull(crawlerResult.getHash());
        crawlerResult.setHash(HASH);
        Assert.assertEquals(HASH, crawlerResult.getHash());
    }

    @Test
    public void getMessage() throws Exception {

        CrawlerResultEntity crawlerResult = new CrawlerResultEntity(ID, CRAWLER, RESULT_STATUS, URL, CONTENT_TYPE, CONTENT, HASH, MESSAGE, PROCESSING_ORDER, CREATED_ON, LAST_CRAWLED_ON, LAST_MODIFIED_ON);
        Assert.assertNotNull(crawlerResult);
        Assert.assertEquals(MESSAGE, crawlerResult.getMessage());
    }

    @Test
    public void setMessage() throws Exception {

        CrawlerResultEntity crawlerResult = new CrawlerResultEntity();
        Assert.assertNotNull(crawlerResult);
        Assert.assertNull(crawlerResult.getMessage());
        crawlerResult.setMessage(MESSAGE);
        Assert.assertEquals(MESSAGE, crawlerResult.getMessage());
    }

    @Test
    public void getProcessingOrder() throws Exception {

        CrawlerResultEntity crawlerResult = new CrawlerResultEntity(ID, CRAWLER, RESULT_STATUS, URL, CONTENT_TYPE, CONTENT, HASH, MESSAGE, PROCESSING_ORDER, CREATED_ON, LAST_CRAWLED_ON, LAST_MODIFIED_ON);
        Assert.assertNotNull(crawlerResult);
        Assert.assertEquals(PROCESSING_ORDER, crawlerResult.getProcessingOrder());
    }

    @Test
    public void getCreatedOn() throws Exception {

        CrawlerResultEntity crawlerResult = new CrawlerResultEntity(ID, CRAWLER, RESULT_STATUS, URL, CONTENT_TYPE, CONTENT, HASH, MESSAGE, PROCESSING_ORDER, CREATED_ON, LAST_CRAWLED_ON, LAST_MODIFIED_ON);
        Assert.assertNotNull(crawlerResult);
        Assert.assertEquals(CREATED_ON, crawlerResult.getCreatedOn());
    }

    @Test
    public void getLastCrawledOn() throws Exception {

        CrawlerResultEntity crawlerResult = new CrawlerResultEntity(ID, CRAWLER, RESULT_STATUS, URL, CONTENT_TYPE, CONTENT, HASH, MESSAGE, PROCESSING_ORDER, CREATED_ON, LAST_CRAWLED_ON, LAST_MODIFIED_ON);
        Assert.assertNotNull(crawlerResult);
        Assert.assertEquals(LAST_CRAWLED_ON, crawlerResult.getLastCrawledOn());
    }

    @Test
    public void setLastCrawledOn() throws Exception {

        CrawlerResultEntity crawlerResult = new CrawlerResultEntity();
        Assert.assertNotNull(crawlerResult);
        Assert.assertNull(crawlerResult.getLastCrawledOn());
        crawlerResult.setLastCrawledOn(LAST_CRAWLED_ON);
        Assert.assertEquals(LAST_CRAWLED_ON, crawlerResult.getLastCrawledOn());
    }

    @Test
    public void getLastModifiedOn() throws Exception {

        CrawlerResultEntity crawlerResult = new CrawlerResultEntity(ID, CRAWLER, RESULT_STATUS, URL, CONTENT_TYPE, CONTENT, HASH, MESSAGE, PROCESSING_ORDER, CREATED_ON, LAST_CRAWLED_ON, LAST_MODIFIED_ON);
        Assert.assertNotNull(crawlerResult);
        Assert.assertEquals(LAST_MODIFIED_ON, crawlerResult.getLastModifiedOn());
    }

    @Test
    public void setLastModifiedOn() throws Exception {

        CrawlerResultEntity crawlerResult = new CrawlerResultEntity();
        Assert.assertNotNull(crawlerResult);
        Assert.assertNull(crawlerResult.getLastModifiedOn());
        crawlerResult.setLastModifiedOn(LAST_MODIFIED_ON);
        Assert.assertEquals(LAST_MODIFIED_ON, crawlerResult.getLastModifiedOn());
    }
}