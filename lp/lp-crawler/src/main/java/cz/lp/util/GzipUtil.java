package cz.lp.util;

import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class GzipUtil {

    public static byte[] decompressGZIP(byte[] gzip) throws IOException {

        try (GzipCompressorInputStream in = new GzipCompressorInputStream(new ByteArrayInputStream(gzip))) {

            try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {

                IOUtils.copy(in, os);
                os.flush();
                return os.toByteArray();
            }
        }
    }
}
