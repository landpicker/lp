package cz.lp.crawler;

import cz.lp.enumerate.SeedUrlCondition;

/**
 * Seed URL object represents wrapper around crawler seed URL.
 */
public class SeedUrl {

    private final String url;
    private final boolean enabled;
    private final SeedUrlCondition condition;

    /**
     * Creates instance of seed URL.
     *
     * @param url       URL
     * @param enabled   enabled
     * @param condition condition
     */
    private SeedUrl(String url, Boolean enabled, SeedUrlCondition condition) {
        this.url = url;
        this.enabled = enabled;
        this.condition = condition;
    }

    /**
     * Creates instance of seed URL.
     *
     * @param url       URL
     * @param enabled   enabled
     * @param condition condition
     * @return seed URL
     */
    public static SeedUrl of(String url, Boolean enabled, SeedUrlCondition condition) {
        return new SeedUrl(url, enabled, condition);
    }

    /**
     * Gets URL.
     *
     * @return URL
     */
    public String getUrl() {
        return url;
    }

    /**
     * Gets enabled.
     *
     * @return enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Gets condition.
     *
     * @return condition
     */
    public SeedUrlCondition getCondition() {
        return condition;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SeedUrl seedUrl = (SeedUrl) o;

        return url != null ? url.equals(seedUrl.url) : seedUrl.url == null;
    }

    @Override
    public int hashCode() {
        return url != null ? url.hashCode() : 0;
    }
}
