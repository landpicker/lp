package cz.lp.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Processor crawler result keeps information if crawler result was processed by specific processor with time information.
 */
@Entity
@Table(name = "processor_crawler_result")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class ProcessorCrawlerResultEntity implements Serializable {

    @EmbeddedId
    private ProcessorCrawlerResultId processorCrawlerResultId;

    @NonNull
    @Column(name = "last_processed_on", nullable = false)
    private LocalDateTime lastProcessedOn;

    @Embeddable
    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    public static class ProcessorCrawlerResultId implements Serializable {

        private static final long serialVersionUID = 1L;

        @Column(name = "processor_id")
        private Long processorId;

        @Column(name = "crawler_result_id")
        private Long crawlerResultId;
    }
}
