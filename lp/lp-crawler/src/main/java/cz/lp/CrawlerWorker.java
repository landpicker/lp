package cz.lp;

import cz.lp.crawler.Crawler;
import cz.lp.processor.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Crawler worker provides only one method that runs executables crawlers and their processors.
 */
@Component
public class CrawlerWorker {

    private static final Logger log = LoggerFactory.getLogger(CrawlerWorker.class);

    private final CrawlerWorkerService crawlerWorkerService;

    @Autowired
    public CrawlerWorker(CrawlerWorkerService crawlerWorkerService) {
        this.crawlerWorkerService = crawlerWorkerService;
    }

    /**
     * Runs crawling executable crawlers and their processors.
     * <p>
     * <strong>Crawler is executable when:</strong>
     * <ul>
     * <li>crawler.enabled = true</li>
     * <li>crawler.recrawl_on < NOW() OR recrawl_on IS NULL</li>
     * <li>crawler.condition = true</li>
     * </ul>
     * <strong>Processor is executable when:</strong>
     * <ul>
     * <li>crawler.enabled = true</li>
     * <li>processor.condition = true</li>
     * </ul>
     * </p>
     */
    public void runCrawlers() {

        List<Crawler> crawlers = crawlerWorkerService.getExecutableCrawlers();
        if (crawlers.isEmpty()) {
            log.info("No executables crawlers");
            return;
        }

        crawlers.forEach(crawler -> {

            if (crawler.crawl()) {

                List<Processor> processors = crawlerWorkerService.getExecutableProcessors(crawler.getId());
                if (processors.isEmpty()) {
                    log.info("{} - No processors", crawler.getClassName());
                    return;
                }
                processors.forEach(Processor::process);
            } else {
                log.warn("{} - No processors for executing, because crawler was disabled", crawler.getClassName());
            }
        });
    }
}
