import { TestBed } from '@angular/core/testing';

import { LandSearchTablePaginationService } from './land-search-table-pagination.service';
import { TranslateModule } from '@ngx-translate/core';

describe('LandSearchTablePaginationService', () => {
	beforeEach(() =>
		TestBed.configureTestingModule({
			imports: [TranslateModule.forRoot()]
		}));

	it('should be created', () => {
		const service: LandSearchTablePaginationService = TestBed.get(LandSearchTablePaginationService);
		expect(service).toBeTruthy();
	});
});
