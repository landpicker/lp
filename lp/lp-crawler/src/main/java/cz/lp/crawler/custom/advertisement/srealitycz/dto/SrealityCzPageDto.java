package cz.lp.crawler.custom.advertisement.srealitycz.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class SrealityCzPageDto {

    private final Integer resultSize;
    private final Integer page;
    private final Integer perPage;
    private final SrealityCzEmbeddedDto embedded;

    @JsonCreator
    public SrealityCzPageDto(
            @JsonProperty("result_size") Integer resultSize,
            @JsonProperty("page") Integer page,
            @JsonProperty("per_page") Integer perPage,
            @JsonProperty("_embedded") SrealityCzEmbeddedDto embedded) {
        this.resultSize = resultSize;
        this.page = page;
        this.perPage = perPage;
        this.embedded = embedded;
    }
}
