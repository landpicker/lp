package cz.lp.processor.custom.areabase.adapter;

import cz.lp.common.enumerate.AreaType;
import cz.lp.processor.custom.areabase.Area;
import cz.lp.processor.custom.areabase.filter.AreaBaseProcessorFilter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

public class AreaBaseCityAdapter {

    private final AreaBaseProcessorFilter filter;

    private static final String CITY_ELEMENT_SELECTOR = "Obec";
    private static final String CODE_ELEMENT_SELECTOR = "Kod";
    private static final String NAME_ELEMENT_SELECTOR = "Nazev";
    private static final String PARENT_CODE_ELEMENT_SELECTOR = "OkresKod";

    private List<Area> cities;

    public AreaBaseCityAdapter(String content, String url, AreaBaseProcessorFilter filter) {

        this.filter = filter;
        Document document = Jsoup.parse(content, url, Parser.xmlParser());
        extractCities(document);
    }

    public void extractCities(Document document) {

        cities = document.select(CITY_ELEMENT_SELECTOR).stream()
                .map(element -> {

                    Element codeElement = element.select(CODE_ELEMENT_SELECTOR).first();
                    Element nameElement = element.select(NAME_ELEMENT_SELECTOR).first();
                    Element parentAreaCodeElement = element.select(PARENT_CODE_ELEMENT_SELECTOR).first();
                    if (isNull(codeElement) || isNull(nameElement) || isNull(parentAreaCodeElement)) {
                        return null;
                    }
                    return new Area(codeElement.text(), AreaType.CITY, nameElement.text(), parentAreaCodeElement.text());
                })
                .filter(Objects::nonNull)
                .filter(area -> filter.isCityAreaAllowed(area.getCode()))
                .collect(Collectors.toList());
    }

    /**
     * Gets cities.
     *
     * @return cities
     */
    public Optional<List<Area>> getCities() {

        if (isNull(cities) || cities.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(cities);
    }
}
