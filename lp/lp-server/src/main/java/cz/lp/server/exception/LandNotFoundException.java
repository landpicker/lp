package cz.lp.server.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class LandNotFoundException extends RuntimeException {

    public LandNotFoundException(String message) {
        super(message);
    }
}
