package cz.lp.common.repository;


import cz.lp.common.domain.AreaEntity;
import cz.lp.common.domain.LandEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

/**
 * Area repository provides methods for access to area table and other related tables in database.
 */
public interface AreaRepository extends CrudRepository<AreaEntity, String>, JpaRepository<AreaEntity, String> {

    /**
     * Check if first level areas already exist.
     *
     * @return true if first level areas exist, otherwise false
     */
    @Query(value = "SELECT CASE WHEN COUNT(code) > 0 THEN TRUE ELSE FALSE END AS exist FROM (SELECT code FROM area WHERE parent_area_code IS NULL LIMIT 1) AS area_select", nativeQuery = true)
    Boolean existFirstLevelAreas();

    /**
     * Gets all areas from first level.
     *
     * @return first level areas
     */
    @Query(value = "SELECT * FROM area WHERE parent_area_code IS NULL", nativeQuery = true)
    List<AreaEntity> getAllFirstLevelAreas();

    /**
     * Check if city areas already exist.
     *
     * @return true if city areas exist, otherwise false
     */
    @Query(value = "SELECT CASE WHEN COUNT(code) > 0 THEN TRUE ELSE FALSE END AS exist FROM (SELECT code FROM area WHERE type LIKE 'CITY' LIMIT 1) AS area_select", nativeQuery = true)
    Boolean existCityAreas();

    /**
     * Gets all areas by type.
     *
     * @return areas
     */
    @Query(value = "SELECT * FROM area WHERE type LIKE :type", nativeQuery = true)
    List<AreaEntity> getAllAreasByType(@Param("type") String type);

    /**
     * Updated all areas as removed by crawler result identifier.
     *
     * @param removedOn       removed on
     * @param crawlerResultId crawler result identifier
     * @return count of updated rows
     */
    @Modifying
    @Transactional
    @Query(value = "UPDATE area SET removed_on = :removed_on WHERE crawler_result_id = :crawler_result_id", nativeQuery = true)
    int updateAllAreasAsRemovedByCrawlerResultId(@Param("removed_on") Timestamp removedOn, @Param("crawler_result_id") Long crawlerResultId);

    /**
     * Check if area with specific code already exists.
     *
     * @param code code
     * @return true if area with specific code exists, otherwise false
     */
    @Query(value = "SELECT CASE WHEN COUNT(code) = 1 THEN true ELSE false END FROM area WHERE code = :code LIMIT 1", nativeQuery = true)
    boolean existByCode(@Param("code") String code);

    @Query("SELECT a FROM AreaEntity AS a WHERE a.code IN :codes")
    List<AreaEntity> getAreasByCodes(@Param("codes") Set<String> codes);

}
