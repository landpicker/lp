import { LandType } from '../shared/land-type-filter/land-type.interface';
import { MapEpsg5514Coordinate } from '../shared/map/map.interface';
import { LandSearchFilter } from './land-search-filter/land-search-filter.interface';

export interface LandSearchResult {
	id: string;
	landSearchFilter: LandSearchFilter;
	lands: Land[];
	page: number;
	totalPages: number;
	totalElements: number;
}

export interface Land {
  id: string;
  cityArea: string;
  cadastralTerritoryArea: string;
	type: LandType;
	trunkNumber: number;
	underDivisionNumber: number;
  acreage: number;
  definitionPointCoordinate: MapEpsg5514Coordinate;
	borderCoordinates: MapEpsg5514Coordinate[];
}
