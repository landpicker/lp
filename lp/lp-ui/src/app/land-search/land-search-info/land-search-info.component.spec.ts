import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandSearchInfoComponent } from './land-search-info.component';
import { TranslateModule } from '@ngx-translate/core';

describe('LandSearchInfoComponent', () => {
  let component: LandSearchInfoComponent;
  let fixture: ComponentFixture<LandSearchInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [ LandSearchInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandSearchInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
