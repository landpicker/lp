export enum MapType {
	BASE = 'BASE',
	PHOTO = 'PHOTO'
}

export interface MapPolygon {
  id: string;
  definitionPoint:{
    coordinate: MapEpsg5514Coordinate;
  },
  border: {
    coordinates: MapEpsg5514Coordinate[];
  }
}

export interface MapEpsg5514Coordinate {
	x: number;
	y: number;
}

export const MAP_DEFAULT_POLYGON_OPTIONS = {
	color: '#990000',
	opacity: 0.2,
	outlineColor: '#990000',
	outlineOpacity: 0.7,
	outlineWidth: 2,
	curvature: 0
};
export const MAP_FOCUSED_POLYGON_OPTIONS = {
	color: '#64ABFF',
	opacity: 0.2,
	outlineColor: '#64ABFF',
	outlineOpacity: 0.7,
	outlineWidth: 2,
	curvature: 0
};
