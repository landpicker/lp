package cz.lp.crawler.custom.advertisement.srealitycz.adapter;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.*;

public class SrealityCzPageLinkAdapterTest {

    private static final String URL = "https://www.sreality.cz/api/cs/v2/estates?category_main_cb=3&category_type_cb=1&distance=10&per_page=10&region=obec+Z%C3%A1b%C5%99eh&region_entity_id=461&region_entity_type=municipality&page=1";

    @Test
    public void constructorWithExtractLinks() throws IOException {

        try(InputStream is = getClass().getClassLoader().getResourceAsStream("crawler/srealitycz/page.json")){

            byte[] content = IOUtils.toByteArray(is);
            SrealityCzPageLinkAdapter adapter = new SrealityCzPageLinkAdapter(new String(content, Charset.forName("utf-8")), URL);
            Optional<Set<String>> links = adapter.getLinks();
            assertTrue(links.isPresent());
            assertEquals(15, links.get().size());
        }
    }
}