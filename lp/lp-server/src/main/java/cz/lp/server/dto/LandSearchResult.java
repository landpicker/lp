package cz.lp.server.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.base.MoreObjects;
import cz.lp.server.dto.filter.LandSearchFilter;

import java.util.List;

/**
 * Land data transfer object represents result of searched land with search identifier.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LandSearchResult {

    private final String id;
    private final LandSearchFilter landSearchFilter;
    private final List<Land> lands;
    private final Integer page;
    private final Integer totalPages;
    private final Long totalElements;

    /**
     * Creates instance of land search result.
     *
     * @param id               unique search identifier
     * @param landSearchFilter land search filter
     * @param lands            searched lands
     * @param page             page number of land list
     * @param totalPages       total pages
     * @param totalElements    total elements
     */
    public LandSearchResult(String id, LandSearchFilter landSearchFilter, List<Land> lands, Integer page, Integer totalPages, Long totalElements) {
        this.id = id;
        this.landSearchFilter = landSearchFilter;
        this.lands = lands;
        this.page = page;
        this.totalPages = totalPages;
        this.totalElements = totalElements;
    }

    /**
     * Creates instance of land search result.
     *
     * @param id               unique search identifier
     * @param landSearchFilter land search filter
     * @param lands            searched lands
     * @param page             page number of land list
     * @param totalPages       total pages
     * @param totalElements    total elements
     */
    public static LandSearchResult of(String id, LandSearchFilter landSearchFilter, List<Land> lands, Integer page, Integer totalPages, Long totalElements) {
        return new LandSearchResult(id, landSearchFilter, lands, page, totalPages, totalElements);
    }

    /**
     * Gets unique search identifier.
     *
     * @return unique search identifier
     */
    public String getId() {
        return id;
    }

    /**
     * Gets land filter.
     *
     * @return land filter
     */
    public LandSearchFilter getLandSearchFilter() {
        return landSearchFilter;
    }

    /**
     * Gets searched lands.
     *
     * @return searched lands
     */
    public List<Land> getLands() {
        return lands;
    }

    /**
     * Gets page number of land list.
     *
     * @return page number
     */
    public Integer getPage() {
        return page;
    }

    /**
     * Gets total pages.
     *
     * @return total pages
     */
    public Integer getTotalPages() {
        return totalPages;
    }

    /**
     * Gets total elements.
     * @return total elements
     */
    public Long getTotalElements() {
        return totalElements;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("landSearchFilter", landSearchFilter)
                .add("lands", lands)
                .add("page", page)
                .add("totalPages", totalPages)
                .add("totalElements", totalElements)
                .toString();
    }
}
