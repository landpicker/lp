package cz.lp.common.service.mail;

import cz.lp.common.dto.mail.MailTemplate;

import java.util.Optional;

public interface MailTemplateService {

    Optional<MailTemplate> getAdTemplate();

    Optional<MailTemplate> getTemplate(long id);
}
