package cz.lp.repository;

import com.google.common.collect.Lists;
import cz.lp.domain.*;
import cz.lp.enumerate.CrawlerCondition;
import cz.lp.enumerate.RecrawlStrategy;
import cz.lp.repository.configuration.RepositoryTemplate;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

public class CrawlerRepositoryIT extends RepositoryTemplate {

    @Autowired
    private CrawlerRepository crawlerRepository;

    @Autowired
    private SourceRepository sourceRepository;

    private final static Long SOURCE_ID = 1l;
    private final static String CRAWLER_CLASS = "CrawlerClass";
    private final static CrawlerCondition CRAWLER_CONDITION = CrawlerCondition.EXIST_FIRST_LEVEL_AREA;
    private final static Integer DELAY = 1000;
    private final static Boolean ENABLED = Boolean.TRUE;
    private final static RecrawlStrategy RECRAWL_STRATEGY = RecrawlStrategy.RECRAWL_ALL;
    private static final String RECRAWL_URL_PATTERN = ".*";
    private final static Long RECRAWL_INTERVAL = 1000l;
    private final static Timestamp RECRAWL_ON = null;
    private final static Timestamp LAST_RECRAWLED_ON = null;
    private final static List<CrawlerSeedUrlEntity> SEED_URL_ENTITIES = null;
    private final static List<CrawlerResultEntity> RESULT_ENTITIES = null;
    private final static List<ProcessorEntity> PROCESSORS = null;

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void insert() {

        SourceEntity sourceEntity = sourceRepository.findOne(SOURCE_ID);
        CrawlerEntity crawlerEntity = new CrawlerEntity(null, sourceEntity, CRAWLER_CLASS, CRAWLER_CONDITION, DELAY,
                ENABLED, RECRAWL_STRATEGY, RECRAWL_URL_PATTERN, RECRAWL_INTERVAL, RECRAWL_ON, LAST_RECRAWLED_ON, SEED_URL_ENTITIES, RESULT_ENTITIES, PROCESSORS);
        Long id = crawlerRepository.save(crawlerEntity).getId();
        CrawlerEntity savedCrawlerEntity = crawlerRepository.findOne(id);
        Assert.assertNotNull(savedCrawlerEntity);
        Assert.assertEquals(id, savedCrawlerEntity.getId());
        Assert.assertEquals(SOURCE_ID, savedCrawlerEntity.getSource().getId());
        Assert.assertEquals(CRAWLER_CLASS, savedCrawlerEntity.getCrawlerClass());
        Assert.assertEquals(CRAWLER_CONDITION, savedCrawlerEntity.getCondition());
        Assert.assertEquals(DELAY, savedCrawlerEntity.getDelay());
        Assert.assertEquals(ENABLED, savedCrawlerEntity.isEnabled());
        Assert.assertEquals(RECRAWL_STRATEGY, savedCrawlerEntity.getRecrawlStrategy());
        Assert.assertEquals(RECRAWL_INTERVAL, savedCrawlerEntity.getRecrawlInterval());
        Assert.assertEquals(RECRAWL_URL_PATTERN, savedCrawlerEntity.getRecrawlUrlPattern());
        Assert.assertEquals(RECRAWL_ON, savedCrawlerEntity.getRecrawlOn());
        Assert.assertEquals(LAST_RECRAWLED_ON, savedCrawlerEntity.getLastRecrawledOn());
        Assert.assertEquals(SEED_URL_ENTITIES, savedCrawlerEntity.getCrawlerSeedUrls());
        Assert.assertEquals(RESULT_ENTITIES, savedCrawlerEntity.getCrawlerResults());
        Assert.assertEquals(PROCESSORS, savedCrawlerEntity.getProcessors());
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler/INSERT_crawler_WITH_id_=_1_AND_recrawl_on_=_NULL_AND_ENABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void update() {

        CrawlerEntity crawlerEntity = crawlerRepository.findOne(1l);
        Assert.assertNotNull(crawlerEntity);
        Assert.assertTrue(crawlerEntity.isEnabled());
        Assert.assertNull(crawlerEntity.getRecrawlOn());
        Assert.assertNull(crawlerEntity.getLastRecrawledOn());

        Boolean newEnabled = false;
        Timestamp newRecrawlOn = Timestamp.valueOf("2018-01-01 00:00:01.000000");
        Timestamp newLastRecrawledOn = Timestamp.valueOf("2018-03-03 00:00:01.000000");
        crawlerEntity.setEnabled(newEnabled);
        crawlerEntity.setRecrawlOn(newRecrawlOn);
        crawlerEntity.setLastRecrawledOn(newLastRecrawledOn);
        crawlerRepository.save(crawlerEntity);
        CrawlerEntity updatedCrawlerEntity = crawlerRepository.findOne(1l);
        Assert.assertEquals(newEnabled, updatedCrawlerEntity.isEnabled());
        Assert.assertEquals(newRecrawlOn, updatedCrawlerEntity.getRecrawlOn());
        Assert.assertEquals(newLastRecrawledOn, updatedCrawlerEntity.getLastRecrawledOn());
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler/INSERT_crawler_WITH_id_=_1_AND_recrawl_on_=_NULL_AND_ENABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void findOne() {

        CrawlerEntity crawlerEntity = crawlerRepository.findOne(1l);
        Assert.assertNotNull(crawlerEntity);
        Assert.assertEquals(1l, crawlerEntity.getId().longValue());
        Assert.assertEquals(SOURCE_ID, crawlerEntity.getSource().getId());
        Assert.assertEquals("CrawlerClass 1", crawlerEntity.getCrawlerClass());
        Assert.assertEquals(CrawlerCondition.EXIST_FIRST_LEVEL_AREA, crawlerEntity.getCondition());
        Assert.assertEquals(1, crawlerEntity.getDelay().intValue());
        Assert.assertEquals(true, crawlerEntity.isEnabled());
        Assert.assertEquals(RecrawlStrategy.RECRAWL_ALL, crawlerEntity.getRecrawlStrategy());
        Assert.assertEquals(1000, crawlerEntity.getRecrawlInterval().longValue());
        Assert.assertEquals(null, crawlerEntity.getRecrawlOn());
        Assert.assertEquals(null, crawlerEntity.getLastRecrawledOn());
        Assert.assertEquals(Lists.newArrayList(), crawlerEntity.getCrawlerSeedUrls());
        Assert.assertEquals(Lists.newArrayList(), crawlerEntity.getCrawlerResults());
        Assert.assertEquals(Lists.newArrayList(), crawlerEntity.getProcessors());
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler/INSERT_crawler_WITH_id_=_1_AND_recrawl_on_=_NULL_AND_ENABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void findAllEnabled_whenRecrawlOnIsNullAndEnabled_shouldReturnCrawl() throws SQLException {

        List<CrawlerEntity> crawlerEntities = crawlerRepository.findAllEnabled();
        Assert.assertTrue(crawlerEntities.size() == 1);
        Assert.assertEquals(new Long(1l), crawlerEntities.get(0).getId());
        Assert.assertEquals("CrawlerClass 1", crawlerEntities.get(0).getCrawlerClass());
        Assert.assertEquals(CrawlerCondition.EXIST_FIRST_LEVEL_AREA, crawlerEntities.get(0).getCondition());
        Assert.assertEquals(new Integer(1), crawlerEntities.get(0).getDelay());
        Assert.assertTrue(crawlerEntities.get(0).isEnabled());
        Assert.assertEquals(RecrawlStrategy.RECRAWL_ALL, crawlerEntities.get(0).getRecrawlStrategy());
        Assert.assertEquals(new Long(1000), crawlerEntities.get(0).getRecrawlInterval());
        Assert.assertNull(crawlerEntities.get(0).getRecrawlOn());
        Assert.assertNull(crawlerEntities.get(0).getLastRecrawledOn());
        Assert.assertTrue(crawlerEntities.get(0).getCrawlerSeedUrls().isEmpty());
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler/INSERT_crawler_WITH_id_=_2_AND_recrawl_on_=_NULL_AND_DISABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void findAllEnabled_whenRecrawlOnIsNullAndDisabled_shouldReturnCrawl() throws SQLException {

        List<CrawlerEntity> crawlerEntities = crawlerRepository.findAllEnabled();
        Assert.assertTrue(crawlerEntities.isEmpty());
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler/INSERT_crawler_WITH_id_=_3_AND_recrawl_on_less_than_NOW_AND_ENABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void findAllEnabled_whenRecrawlOnIsLessThanNowAndEnabled_shouldReturnCrawl() throws SQLException {

        List<CrawlerEntity> crawlerEntities = crawlerRepository.findAllEnabled();
        Assert.assertTrue(crawlerEntities.size() == 1);
        Assert.assertEquals(new Long(3l), crawlerEntities.get(0).getId());
        Assert.assertEquals("CrawlerClass 3", crawlerEntities.get(0).getCrawlerClass());
        Assert.assertEquals(CrawlerCondition.EXIST_FIRST_LEVEL_AREA, crawlerEntities.get(0).getCondition());
        Assert.assertEquals(new Integer(1), crawlerEntities.get(0).getDelay());
        Assert.assertTrue(crawlerEntities.get(0).isEnabled());
        Assert.assertEquals(RecrawlStrategy.RECRAWL_ALL, crawlerEntities.get(0).getRecrawlStrategy());
        Assert.assertEquals(new Long(1000), crawlerEntities.get(0).getRecrawlInterval());
        Assert.assertEquals(Timestamp.valueOf("2000-01-01 00:00:01.000000"), crawlerEntities.get(0).getRecrawlOn());
        Assert.assertEquals(Timestamp.valueOf("2000-01-01 00:00:01.000000"), crawlerEntities.get(0).getLastRecrawledOn());
        Assert.assertTrue(crawlerEntities.get(0).getCrawlerSeedUrls().isEmpty());
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler/INSERT_crawler_WITH_id_=_4_AND_recrawl_on_greater_than_NOW_AND_ENABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void findAllEnabled_whenRecrawlOnIsGreaterThanNowAndEnabled_shouldReturnCrawl() throws SQLException {

        List<CrawlerEntity> crawlerEntities = crawlerRepository.findAllEnabled();
        Assert.assertTrue(crawlerEntities.isEmpty());
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler/INSERT_crawler_WITH_id_=_1_AND_recrawl_on_=_NULL_AND_ENABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
    })
    public void findAllEnabled_whenWithSource_shouldReturnSource() throws SQLException {

        List<CrawlerEntity> crawlerEntities = crawlerRepository.findAllEnabled();
        Assert.assertTrue(crawlerEntities.size() == 1);
        Assert.assertEquals(new Long(1l), crawlerEntities.get(0).getId());
        Assert.assertEquals("CrawlerClass 1", crawlerEntities.get(0).getCrawlerClass());
        Assert.assertEquals(CrawlerCondition.EXIST_FIRST_LEVEL_AREA, crawlerEntities.get(0).getCondition());
        Assert.assertEquals(new Integer(1), crawlerEntities.get(0).getDelay());
        Assert.assertTrue(crawlerEntities.get(0).isEnabled());
        Assert.assertEquals(RecrawlStrategy.RECRAWL_ALL, crawlerEntities.get(0).getRecrawlStrategy());
        Assert.assertEquals(new Long(1000), crawlerEntities.get(0).getRecrawlInterval());
        Assert.assertNull(crawlerEntities.get(0).getRecrawlOn());
        Assert.assertNull(crawlerEntities.get(0).getLastRecrawledOn());
        Assert.assertTrue(crawlerEntities.get(0).getCrawlerSeedUrls().isEmpty());
        SourceEntity sourceEntity = crawlerEntities.get(0).getSource();
        Assert.assertNotNull(sourceEntity);
        Assert.assertEquals(new Long(1), sourceEntity.getId());
        Assert.assertEquals("CUZK", sourceEntity.getProvider());
        Assert.assertEquals("http://cuzk.cz", sourceEntity.getWebsiteUrl());
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler/INSERT_crawler_WITH_id_=_1_AND_recrawl_on_=_NULL_AND_ENABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
    })
    public void findAllEnabled_whenCrawlerSeedUrlsIsEmpty_shouldReturnNoUrl() throws SQLException {

        List<CrawlerEntity> crawlerEntities = crawlerRepository.findAllEnabled();
        Assert.assertTrue(crawlerEntities.size() == 1);
        Assert.assertEquals(new Long(1l), crawlerEntities.get(0).getId());
        Assert.assertEquals("CrawlerClass 1", crawlerEntities.get(0).getCrawlerClass());
        Assert.assertEquals(CrawlerCondition.EXIST_FIRST_LEVEL_AREA, crawlerEntities.get(0).getCondition());
        Assert.assertEquals(new Integer(1), crawlerEntities.get(0).getDelay());
        Assert.assertTrue(crawlerEntities.get(0).isEnabled());
        Assert.assertEquals(RecrawlStrategy.RECRAWL_ALL, crawlerEntities.get(0).getRecrawlStrategy());
        Assert.assertEquals(new Long(1000), crawlerEntities.get(0).getRecrawlInterval());
        Assert.assertNull(crawlerEntities.get(0).getRecrawlOn());
        Assert.assertNull(crawlerEntities.get(0).getLastRecrawledOn());
        Assert.assertTrue(crawlerEntities.get(0).getCrawlerSeedUrls().isEmpty());
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler/INSERT_crawler_WITH_id_=_1_AND_recrawl_on_=_NULL_AND_ENABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler-seed-url/INSERT_crawler_seed_url_WITH_id_=_1_AND_crawler_id_=_1_ENABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler-seed-url/INSERT_crawler_seed_url_WITH_id_=_2_AND_crawler_id_=_1_DISABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void findAllEnabled_whenCrawlerSeedUrlsIsNotEmpty_shouldReturnUrls() throws SQLException {

        List<CrawlerEntity> crawlerEntities = crawlerRepository.findAllEnabled();
        Assert.assertTrue(crawlerEntities.size() == 1);
        Assert.assertEquals(new Long(1l), crawlerEntities.get(0).getId());
        Assert.assertEquals("CrawlerClass 1", crawlerEntities.get(0).getCrawlerClass());
        Assert.assertEquals(CrawlerCondition.EXIST_FIRST_LEVEL_AREA, crawlerEntities.get(0).getCondition());
        Assert.assertEquals(new Integer(1), crawlerEntities.get(0).getDelay());
        Assert.assertTrue(crawlerEntities.get(0).isEnabled());
        Assert.assertEquals(RecrawlStrategy.RECRAWL_ALL, crawlerEntities.get(0).getRecrawlStrategy());
        Assert.assertEquals(new Long(1000), crawlerEntities.get(0).getRecrawlInterval());
        Assert.assertNull(crawlerEntities.get(0).getRecrawlOn());
        Assert.assertNull(crawlerEntities.get(0).getLastRecrawledOn());

        List<CrawlerSeedUrlEntity> crawlerSeedUrlEntities = crawlerEntities.get(0).getCrawlerSeedUrls();
        Assert.assertTrue(crawlerSeedUrlEntities.size() == 2);
        Assert.assertEquals(new Long(1), crawlerSeedUrlEntities.get(0).getId());
        Assert.assertEquals("http://cuzk.cz/seed_1", crawlerSeedUrlEntities.get(0).getUrl());
        Assert.assertTrue(crawlerSeedUrlEntities.get(0).isEnabled());
        Assert.assertEquals(new Long(2), crawlerSeedUrlEntities.get(1).getId());
        Assert.assertEquals("http://cuzk.cz/seed_2", crawlerSeedUrlEntities.get(1).getUrl());
        Assert.assertFalse(crawlerSeedUrlEntities.get(1).isEnabled());
    }

    @Test(expected = DataAccessException.class)
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler/INSERT_crawler_WITH_id_=_5_AND_condition_=_INVALID_AND_ENABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void findAllEnabled_whenConditionIsInvalid_shouldThrowException() throws SQLException {

        crawlerRepository.findAllEnabled();
    }

    @Test(expected = DataAccessException.class)
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler/INSERT_crawler_WITH_id_=_6_AND_recrawl_strategy_=_INVALID_AND_ENABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void findAllEnabled_whenRecrawlStrategyIsInvalid_shouldThrowException() throws SQLException {

        crawlerRepository.findAllEnabled();
    }

}