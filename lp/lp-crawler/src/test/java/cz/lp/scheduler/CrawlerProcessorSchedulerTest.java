package cz.lp.scheduler;

import cz.lp.CrawlerScheduler;
import cz.lp.CrawlerWorker;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CrawlerProcessorSchedulerTest {

    @Mock
    private static CrawlerWorker crawlerWorker;

    @Test
    public void runCrawlersAndProcessors() throws Exception {

        Mockito.doNothing().when(crawlerWorker).runCrawlers();
        CrawlerScheduler scheduler = new CrawlerScheduler(crawlerWorker);
        scheduler.runCrawlers();
    }
}