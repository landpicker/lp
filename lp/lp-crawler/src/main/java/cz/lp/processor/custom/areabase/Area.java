package cz.lp.processor.custom.areabase;

import cz.lp.common.enumerate.AreaType;

public class Area {

    private final String code;
    private final AreaType type;
    private final String name;
    private final String parentAreaCode;

    /**
     * Creates instance of area.
     *
     * @param code           code
     * @param type           type
     * @param name           name
     * @param parentAreaCode parent area code
     */
    public Area(String code, AreaType type, String name, String parentAreaCode) {
        this.code = code;
        this.type = type;
        this.name = name;
        this.parentAreaCode = parentAreaCode;
    }

    /**
     * Gets code.
     *
     * @return code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets type.
     *
     * @return type
     */
    public AreaType getType() {
        return type;
    }

    /**
     * Gets name.
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets parent area code.
     *
     * @return parent area code
     */
    public String getParentAreaCode() {
        return parentAreaCode;
    }
}
