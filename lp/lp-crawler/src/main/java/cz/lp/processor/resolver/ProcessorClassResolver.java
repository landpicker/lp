package cz.lp.processor.resolver;

import cz.lp.common.repository.AreaBorderRepository;
import cz.lp.common.repository.AreaRepository;
import cz.lp.common.repository.LandBorderRepository;
import cz.lp.common.repository.LandRepository;
import cz.lp.domain.ProcessorEntity;
import cz.lp.enumerate.ProcessorCondition;
import cz.lp.exception.ProcessorClassException;
import cz.lp.processor.Processor;
import cz.lp.processor.ProcessorFilter;
import cz.lp.processor.ProcessorFilterContext;
import cz.lp.processor.service.ProcessorRepositoryService;
import cz.lp.processor.service.ProcessorService;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class ProcessorClassResolver {

    /**
     * Creates instance of processor from processor entity by class name (reflections)
     *
     * @param processorEntity processor entity
     * @return processor
     * @throws ProcessorClassException when something during creating instance of processor by reflection failed
     */
    public static Processor createProcessorInstance(ProcessorEntity processorEntity, ProcessorService processorService, ProcessorConditionResolver processorConditionResolver,
                                                    ProcessorRepositoryService processorRepositoryService, ProcessorFilterContext processorFilterContext) throws ProcessorClassException {

        try {
            Class<?> type = Class.forName(processorEntity.getProcessorClass());
            Constructor<?> constructor = type.getConstructor(String.class, Long.class, Long.class, ProcessorCondition.class, String.class, ProcessorService.class, ProcessorConditionResolver.class, ProcessorRepositoryService.class, ProcessorFilter.class);
            return (Processor) constructor.newInstance(type.getSimpleName(), processorEntity.getId(), processorEntity.getCrawler().getId(), processorEntity.getCondition(), processorEntity.getUrlPattern(), processorService, processorConditionResolver, processorRepositoryService, processorFilterContext.getFilter(type.getSimpleName()));
        } catch (ClassNotFoundException ex) {
            throw new ProcessorClassException(String.format("Unable to find processor class: %s", processorEntity.getProcessorClass()), ex);
        } catch (NoSuchMethodException ex) {
            throw new ProcessorClassException(String.format("Unable to find constructor with defined arguments for class: %s", processorEntity.getProcessorClass()), ex);
        } catch (IllegalAccessException ex) {
            throw new ProcessorClassException(String.format("Invalid arguments for creating class: %s", processorEntity.getProcessorClass()), ex);
        } catch (InstantiationException ex) {
            throw new ProcessorClassException(String.format("Unable to create instance of constructor of abstract class for class: %s", processorEntity.getProcessorClass()), ex);
        } catch (InvocationTargetException ex) {
            throw new ProcessorClassException(String.format("Constructor exception for class: %s", processorEntity.getProcessorClass()), ex);
        }
    }
}
