package cz.lp.server.dto.filter;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;

/**
 * LandSearchFilter data transfer object provides data for filtering land search query.
 */
public class LandSearchFilter {

    private AreaFilter areaFilter;
    private LandFilter landFilter;

    /**
     * Creates instance of filter.
     *
     * @param areaFilter     area filter
     * @param landFilter     land filter
     */
    @JsonCreator
    public LandSearchFilter(@JsonProperty("areaFilter") AreaFilter areaFilter, @JsonProperty("landFilter") LandFilter landFilter) {
        this.areaFilter = areaFilter;
        this.landFilter = landFilter;
    }

    /**
     * Gets area filter.
     *
     * @return area filter
     */
    public AreaFilter getAreaFilter() {
        return areaFilter;
    }

    /**
     * Gets land filter.
     *
     * @return land filter
     */
    public LandFilter getLandFilter() {
        return landFilter;
    }

    /**
     * Validates filter.
     */
    public void validate(){
        // TODO
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("areaFilter", areaFilter)
                .add("landFilter", landFilter)
                .omitNullValues()
                .toString();
    }
}
