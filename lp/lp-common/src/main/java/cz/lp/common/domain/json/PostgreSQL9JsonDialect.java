package cz.lp.common.domain.json;

import org.hibernate.dialect.PostgreSQL9Dialect;

import java.sql.Types;

public class PostgreSQL9JsonDialect extends PostgreSQL9Dialect {

    public PostgreSQL9JsonDialect() {
        registerColumnType(Types.JAVA_OBJECT - 1, "json");
    }

}