package cz.lp.processor.standalone;

/**
 * Global interface for standalone processor.
 */
public interface StandaloneProcessor {

    /**
     * Method for executing standalone processor.
     */
    void execute();
}
