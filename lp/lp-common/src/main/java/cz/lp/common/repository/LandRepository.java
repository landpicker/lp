package cz.lp.common.repository;

import cz.lp.common.domain.LandEntity;
import cz.lp.common.enumerate.LandType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Set;

/**
 * Land repository provides methods for access to land table and other related tables in database.
 */
public interface LandRepository extends CrudRepository<LandEntity, String>, JpaRepository<LandEntity, String> {


    @Query("SELECT COUNT(l) FROM LandEntity l WHERE l.area.code IN :cadastralTerritoryAreaCodes " +
            "AND l.type IN :landTypes " +
            "AND (:acreageFrom IS NULL OR l.acreage > :acreageFrom) " +
            "AND (:acreageTo IS NULL OR l.acreage < :acreageTo)")
    Long getCountOfLandsByFilter(@Param("cadastralTerritoryAreaCodes") Set<String> cadastralTerritoryAreaCodes,
                                 @Param("landTypes") Set<LandType> landTypes,
                                 @Param("acreageFrom") Integer acreageFrom,
                                 @Param("acreageTo") Integer acreageTo);

    @Query("SELECT l FROM LandEntity l WHERE l.area.code IN :cadastralTerritoryAreaCodes " +
            "AND l.type IN :landTypes " +
            "AND (:acreageFrom IS NULL OR l.acreage > :acreageFrom) " +
            "AND (:acreageTo IS NULL OR l.acreage < :acreageTo)")
    Page<LandEntity> getLandsByFilter(@Param("cadastralTerritoryAreaCodes") Set<String> cadastralTerritoryAreaCodes,
                                      @Param("landTypes") Set<LandType> landTypes,
                                      @Param("acreageFrom") Integer acreageFrom,
                                      @Param("acreageTo") Integer acreageTo, Pageable pageable);
}
