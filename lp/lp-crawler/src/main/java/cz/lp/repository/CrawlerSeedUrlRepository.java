package cz.lp.repository;

import cz.lp.domain.CrawlerSeedUrlEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Crawler seed URL repository provides access to crawler seed url table.
 */
public interface CrawlerSeedUrlRepository extends CrudRepository<CrawlerSeedUrlEntity, Long> {
}
