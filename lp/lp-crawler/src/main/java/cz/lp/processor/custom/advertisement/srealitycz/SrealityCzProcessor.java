package cz.lp.processor.custom.advertisement.srealitycz;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import cz.lp.common.domain.ad.AdEntity;
import cz.lp.common.domain.ad.AdImageEntity;
import cz.lp.common.dto.ad.Ad;
import cz.lp.common.repository.AdRepository;
import cz.lp.enumerate.ProcessorCondition;
import cz.lp.exception.ProcessorExtractDataException;
import cz.lp.processor.Processor;
import cz.lp.processor.ProcessorFilter;
import cz.lp.processor.Result;
import cz.lp.processor.custom.advertisement.srealitycz.adapter.SrealityCzAdAdapter;
import cz.lp.processor.resolver.ProcessorConditionResolver;
import cz.lp.processor.service.ProcessorRepositoryService;
import cz.lp.processor.service.ProcessorService;
import cz.lp.util.DownloadUtil;
import cz.lp.util.GzipUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class SrealityCzProcessor extends Processor<Ad> {

    private static Logger log = LoggerFactory.getLogger(SrealityCzProcessor.class);

    private static final String SREALITY_SITEMAP_LIST_URL = "https://www.sreality.cz/sitemap.xml";
    private static final Pattern SREALITY_DETAIL_URL_PATTERN = Pattern.compile("(?i)https?://(www.)?sreality\\.cz/detail/(prodej|drazby)/(dum|pozemek)/.*/\\d+$");


    private final AdRepository adRepository;
    private Set<String> siteMap;


    public SrealityCzProcessor(String className, Long id, Long crawlerId, ProcessorCondition condition, String urlPattern, ProcessorService processorService, ProcessorConditionResolver processorConditionResolver, ProcessorRepositoryService processorRepositoryService, ProcessorFilter processorFilter) {
        super(className, id, crawlerId, condition, urlPattern, processorService, processorConditionResolver, processorFilter);
        this.adRepository = processorRepositoryService.getAdRepository();
        try {
            this.siteMap = downloadSitemap();
        } catch (IOException e) {
            log.error("Unable to download Sreality CZ sitemap list");
            this.siteMap = Sets.newHashSet();
        }
    }

    @Override
    protected List<Ad> extractData(Result result) throws ProcessorExtractDataException {

        String sourceName = processorService.getSourceName(this.crawlerId).orElseThrow(ProcessorExtractDataException::new);
        Optional<Ad> ad = new SrealityCzAdAdapter(result.getContentAsString(), result.getUrl(), sourceName, siteMap).getAd();
        if (!ad.isPresent()) {
            throw new ProcessorExtractDataException();
        }
        return Lists.newArrayList(ad.get());
    }

    @Override
    protected void setDataAsRemoved(Result result, LocalDateTime removedOn) {

    }

    @Override
    protected void saveData(List<Ad> data, Long crawlerResultId, LocalDateTime lastUpdatedOn) {

        data.forEach(ad -> {

            AdEntity adEntity = new AdEntity(ad.getUrl(), ad.getSource(), ad.getTitle(), ad.getContent(), ad.getPublishedOn());
            List<AdImageEntity> imageEntities = ad.getImages().stream().map(adImage -> new AdImageEntity(adImage.getUrl(), adImage.getFileName(), adImage.getContentType(), adImage.getData(), adEntity)).collect(Collectors.toList());
            adEntity.setImages(imageEntities);
            adRepository.save(adEntity);
        });
    }

    private Set<String> downloadSitemap() throws IOException {

        log.info("Downloading Sreality site map list: {}", SREALITY_SITEMAP_LIST_URL);
        DownloadUtil.DownloadedData siteMapListData = DownloadUtil.getData(SREALITY_SITEMAP_LIST_URL);
        Document siteMapListDocument = Jsoup.parse(new String(siteMapListData.getData(), "utf-8"), SREALITY_SITEMAP_LIST_URL, Parser.xmlParser());
        return siteMapListDocument.select("loc").stream()
                .map(element -> {
                    try {
                        String siteMapUrl = element.text();
                        log.info("Downloading Sreality site map: {}", siteMapUrl);
                        DownloadUtil.DownloadedData siteMapData = DownloadUtil.getData(siteMapUrl);
                        String unzippedContent = new String(GzipUtil.decompressGZIP(siteMapData.getData()));
                        return  Jsoup.parse(unzippedContent, siteMapUrl, Parser.xmlParser());

                    } catch (IOException e) {
                        log.error("Unable to download Sreality CZ sitemap");
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .flatMap(document -> document.select("loc").stream()
                        .map(Element::text)
                        .filter(SREALITY_DETAIL_URL_PATTERN.asPredicate()))
                .collect(Collectors.toSet());
    }

}
