package cz.lp.server.dto.filter;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * Land filter data transfer object provides parameters for searched lands.
 */
public class LandFilter {

    private AcreageFilter acreageFilter;
    private LandTypeFilter landTypeFilter;

    /**
     * Creates instance of acreage filter.
     *
     * @param acreageFilter acreage filter
     */
    @JsonCreator
    public LandFilter(@JsonProperty("acreageFilter") AcreageFilter acreageFilter, @JsonProperty("landTypeFilter") LandTypeFilter landTypeFilter) {
        this.acreageFilter = acreageFilter;
        this.landTypeFilter = landTypeFilter;
    }

    /**
     * Gets acreage filter.
     *
     * @return acreage filter
     */
    public AcreageFilter getAcreageFilter() {
        return acreageFilter;
    }

    /**
     * Gets land type filter.
     *
     * @return land type filter
     */
    public LandTypeFilter getLandTypeFilter() {
        return landTypeFilter;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("acreageFilter", acreageFilter)
                .add("landTypeFilter", landTypeFilter)
                .omitNullValues()
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LandFilter that = (LandFilter) o;
        return Objects.equal(acreageFilter, that.acreageFilter) &&
                Objects.equal(landTypeFilter, that.landTypeFilter);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(acreageFilter, landTypeFilter);
    }
}
