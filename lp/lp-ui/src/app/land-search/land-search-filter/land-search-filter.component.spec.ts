import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandSearchFilterComponent } from './land-search-filter.component';
import { TranslateModule } from '../../../../node_modules/@ngx-translate/core';
import { AcreageFilterModule } from '../../shared/acreage-filter/acreage-filter.module';
import { FormsModule, ReactiveFormsModule } from '../../../../node_modules/@angular/forms';
import { LandTypeFilterModule } from '../../shared/land-type-filter/land-type-filter.module';
import { AreaFilterModule } from 'src/app/shared/area-filter/area-filter.module';
import { HttpClientModule } from '@angular/common/http';
import { MatProgressSpinnerModule } from '@angular/material';

describe('LandSearchFilterComponent', () => {
  let component: LandSearchFilterComponent;
  let fixture: ComponentFixture<LandSearchFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        HttpClientModule,
        ReactiveFormsModule,
        AreaFilterModule,
        AcreageFilterModule,
        LandTypeFilterModule,
        MatProgressSpinnerModule
      ],
      declarations: [ LandSearchFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandSearchFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
