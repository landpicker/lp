INSERT INTO public.source (id, provider, website_url) VALUES (3, 'Bazos.cz', 'https://bazos.cz');

INSERT INTO public.crawler (id, source_id, crawler_class, condition, delay, enabled, recrawl_strategy, recrawl_url_pattern, recrawl_interval, recrawl_on, last_recrawled_on)
VALUES (3, 3, 'cz.lp.crawler.custom.advertisement.bazoscz.BazosCzCrawler', 'NONE', 200, false, 'RECRAWL_BY_PATTERN', 'https?://(www.)?reality\.bazos\.cz/prodam/.*', 60, null, null);


INSERT INTO public.crawler_seed_url (crawler_id, url, enabled, condition) VALUES (3, 'https://reality.bazos.cz/prodam/dum/?rubriky=reality&hlokalita=78901&humkreis=15', true, 'NONE');
INSERT INTO public.crawler_seed_url (crawler_id, url, enabled, condition) VALUES (3, 'https://reality.bazos.cz/prodam/pozemek/?rubriky=reality&hlokalita=78901&humkreis=15', true, 'NONE');

