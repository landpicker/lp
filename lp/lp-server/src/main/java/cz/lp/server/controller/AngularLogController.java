package cz.lp.server.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Angular log controller provides REST method for obtaining error from angular application.
 */
@RestController
@RequestMapping("/api/log")
public class AngularLogController {

    private static final Logger LOG = LoggerFactory.getLogger(AngularLogController.class);

    @RequestMapping(value = "/error", method = RequestMethod.POST)
    public void getAllAreas(@RequestBody Map error) {

        LOG.error("Angular error: {} \n{}", error.get("message"), error.get("stack"));
    }
}
