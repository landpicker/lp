import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { LandSearchResult } from '../land-search.interface';
import { PaginationChangeEvent, LandSearchTableComponent, ShowOnMapEvent } from './land-search-table/land-search-table.component';
import { NotificationService } from 'src/app/shared/notification/notification.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'lp-land-search-result',
	templateUrl: './land-search-result.component.html',
	styleUrls: ['./land-search-result.component.scss']
})
export class LandSearchResultComponent implements OnInit {
	@Input()
	landSearchResult: LandSearchResult;
	@Output()
	changePagination = new EventEmitter<PaginationChangeEvent>();
	@Output()
	showOnMap = new EventEmitter();
	@ViewChild(LandSearchTableComponent)
	landSearchTableComponent: LandSearchTableComponent;
	constructor(private _translateService: TranslateService, private _notificationService: NotificationService) {}

	ngOnInit() {}

	startLoading() {
		if (!!this.landSearchTableComponent) {
			this.landSearchTableComponent.startLoading();
		}
	}

	stopLoading() {
		if (!!this.landSearchTableComponent) {
			this.landSearchTableComponent.stopLoading();
		}
	}

	onPaginationChange(event: PaginationChangeEvent) {
		this.changePagination.emit(event);
	}

	onShowOnMap(event: ShowOnMapEvent) {
		this.showOnMap.emit(event);
	}

	onShare() {
		const urlWithShareId = window.location.href;
		this._copyUrlToClipboard(urlWithShareId, (success) => {
			if (success) {
				this._translateService.get('land-search.result.notification.snackbar.share-to-clipboard', { url: urlWithShareId }).subscribe((res) => {
					this._notificationService.showSnackbarNotification(res);
				});
			}
		});
	}

	private _copyUrlToClipboard(text: string, cb: Function): void {
		const textArea = document.createElement('textarea');

		textArea.style.position = 'fixed';
		textArea.style.top = '0';
		textArea.style.left = '0';
		textArea.style.width = '2em';
		textArea.style.height = '2em';
		textArea.style.padding = '0';
		textArea.style.border = 'none';
		textArea.style.outline = 'none';
		textArea.style.boxShadow = 'none';
		textArea.style.background = 'transparent';
		textArea.value = text;
		document.body.appendChild(textArea);
		textArea.select();
		try {
			cb(document.execCommand('copy') ? true : false);
		} catch (err) {
			cb(false);
		}
		document.body.removeChild(textArea);
	}
}
