import { Component, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material';

@Component({
  selector: 'lp-notification-snackbar-view',
  templateUrl: './notification-snackbar-view.component.html',
  styleUrls: ['./notification-snackbar-view.component.scss']
})
export class NotificationSnackbarViewComponent {

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: any) { }
}
