package cz.lp.processor.custom.areaborder.adapter;

import com.google.common.collect.Lists;
import cz.lp.common.dto.Epsg5514Coordinate;
import cz.lp.processor.custom.areaborder.AreaBorder;
import cz.lp.util.CoordinateUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import java.util.List;
import java.util.Optional;

import static java.util.Objects.isNull;

public class AreaBorderCadastralTerritoryAdapter {

    private final static String CADSTRAL_TERRITORY_SELECTOR = "vf|KatastralniUzemi[gml:id]";
    private final static String CADASTRAL_TERRITORY_CODE_SELECTOR = "kui|Kod";
    private final static String BORDER_POLYGON_SELECTOR = "kui|Geometrie kui|OriginalniHranice gml|posList";

    private List<AreaBorder> cadastralTerritoryBorders;

    public AreaBorderCadastralTerritoryAdapter(String content, String url) {

        Document document = Jsoup.parse(content, url, Parser.xmlParser());
        extractcadastralTerritoryBorders(document);
    }

    private void extractcadastralTerritoryBorders(Document document) {

        cadastralTerritoryBorders = Lists.newArrayList();
        Elements cadastralTerritoryElements = document.select(CADSTRAL_TERRITORY_SELECTOR);
        cadastralTerritoryElements.forEach(cadastralTerritoryElement -> {

            Element cadastralTerritoryCode = cadastralTerritoryElement.selectFirst(CADASTRAL_TERRITORY_CODE_SELECTOR);
            if (cadastralTerritoryCode == null) return;
            String areaCode = cadastralTerritoryCode.text();

            Element posListElement = document.selectFirst(BORDER_POLYGON_SELECTOR);
            if (posListElement == null) return;

            List<Epsg5514Coordinate> epsg5514Coordinates = CoordinateUtil.extractEpsg5514Coordinates(posListElement.text());
            if (epsg5514Coordinates.isEmpty()) return;
            this.cadastralTerritoryBorders.add(new AreaBorder(areaCode, epsg5514Coordinates));

        });
    }

    /**
     * Gets cadastral territory borders.
     *
     * @return cadastral territory borders
     */
    public Optional<List<AreaBorder>> getCadastralTerritoryBorders() {

        if (isNull(cadastralTerritoryBorders) || cadastralTerritoryBorders.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(cadastralTerritoryBorders);
    }
}
