package cz.lp.server.domain;

import cz.lp.server.dto.filter.LandSearchFilter;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Land search entity represents land search filter as search query for land searching in database table.
 */

@Entity
@Table(name = "land_search")
public class LandSearchEntity {

    @Id
    @Column(name = "id", unique = true, nullable = false, length = 36)
    private String id;

    @Type(type = "Json", parameters = {
            @org.hibernate.annotations.Parameter(name = "type", value = "cz.lp.server.dto.filter.LandSearchFilter")
    })
    @Column(name = "land_search_filter")
    private LandSearchFilter landSearchFilter;

    @Column(name = "land_search_filter_hash")
    private Integer landSearchFilterHash;

    /**
     * Creates instance of default constructor.
     */
    public LandSearchEntity() {
    }

    /**
     * Creates instance of land search entity.
     *
     * @param id                   unique search identifier
     * @param landSearchFilter     land search filter
     * @param landSearchFilterHash land search filter hash
     */
    public LandSearchEntity(String id, LandSearchFilter landSearchFilter, Integer landSearchFilterHash) {
        this.id = id;
        this.landSearchFilter = landSearchFilter;
        this.landSearchFilterHash = landSearchFilterHash;
    }

    /**
     * Gets unique search identifier.
     *
     * @return unique search identifier
     */
    public String getId() {
        return id;
    }

    /**
     * Sets unique search identifier.
     *
     * @param id unique search identifier
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets land search filter.
     *
     * @return land search filter
     */
    public LandSearchFilter getLandSearchFilter() {
        return landSearchFilter;
    }

    /**
     * Sets land search filter.
     *
     * @param landSearchFilter land search filter
     */
    public void setLandSearchFilter(LandSearchFilter landSearchFilter) {
        this.landSearchFilter = landSearchFilter;
    }

    /**
     * Gets hash code of land search filter.
     *
     * @return hash code of land search filter
     */
    public Integer getLandSearchFilterHash() {
        return landSearchFilterHash;
    }

    /**
     * Sets hash code of land search filter.
     *
     * @param landSearchFilterHash hash code of land search filter
     */
    public void setLandSearchFilterHash(Integer landSearchFilterHash) {
        this.landSearchFilterHash = landSearchFilterHash;
    }
}
