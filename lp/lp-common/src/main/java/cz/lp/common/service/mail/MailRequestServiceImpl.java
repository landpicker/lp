package cz.lp.common.service.mail;

import cz.lp.common.domain.mail.MailRequestAttachmentEntity;
import cz.lp.common.domain.mail.MailRequestEntity;
import cz.lp.common.dto.ad.Ad;
import cz.lp.common.dto.mail.MailRequest;
import cz.lp.common.dto.mail.MailRequestAttachment;
import cz.lp.common.dto.mail.MailTemplate;
import cz.lp.common.enumerate.MailRequestStatus;
import cz.lp.common.repository.AdSubscriberRepository;
import cz.lp.common.repository.MailRequestRepository;
import org.apache.commons.text.StringSubstitutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

@Service
@Transactional
public class MailRequestServiceImpl implements MailRequestService {

    protected static Logger log = LoggerFactory.getLogger(MailRequestServiceImpl.class);

    private final MailRequestRepository mailRequestRepository;
    private final AdSubscriberRepository adSubscriberRepository;
    private final MailTemplateService mailTemplateService;

    @Autowired
    public MailRequestServiceImpl(MailRequestRepository mailRequestRepository,
                                  AdSubscriberRepository adSubscriberRepository,
                                  MailTemplateService mailTemplateService) {
        this.mailRequestRepository = mailRequestRepository;
        this.adSubscriberRepository = adSubscriberRepository;
        this.mailTemplateService = mailTemplateService;
    }

    @Override
    public Long createMailFromAd(Ad ad) {

        Set<String> subscriberEmails = adSubscriberRepository.findAllSubsriberEmailsByUrl(ad.getUrl());
        Optional<MailTemplate> mailTemplateOptional = mailTemplateService.getAdTemplate();

        String subject = "";
        String content = "";
        if (mailTemplateOptional.isPresent()) {
            MailTemplate mailTemplate = mailTemplateOptional.get();
            Map<String, String> subjectData = new HashMap<>();
            subjectData.put("source", ad.getSource());
            subject = StringSubstitutor.replace(mailTemplate.getSubjectPattern(), subjectData);

            Map<String, String> contentData = new HashMap<>();
            contentData.put("title", ad.getTitle());
            contentData.put("published_on", ad.getPublishedOn().format(DateTimeFormatter.ofPattern("dd. MM. yyyy")));
            contentData.put("source", ad.getSource());
            contentData.put("url", ad.getUrl());
            contentData.put("content", ad.getContent());
            content = StringSubstitutor.replace(mailTemplate.getContentPattern(), contentData);
        }

        MailRequest mailRequest = MailRequest
                .builder(subject, content, subscriberEmails)
                .attachments(
                        ad.getImages().stream()
                                .map(adImage -> new MailRequestAttachment(adImage.getFileName(), adImage.getContentType(), adImage.getData()))
                                .collect(Collectors.toList())
                ).build();
        return createMailRequest(mailRequest);
    }

    @Override
    public Long createMailRequest(MailRequest mailRequest) {

        MailRequestEntity mailRequestEntity = new MailRequestEntity();
        mailRequestEntity.setStatus(MailRequestStatus.READY);
        mailRequestEntity.setSubject(mailRequest.getSubject());
        mailRequestEntity.setContent(mailRequest.getContent());
        mailRequestEntity.setSubscribers(mailRequest.getSubscribers());
        mailRequestEntity.setCreatedOn(Timestamp.valueOf(LocalDateTime.now()));
        mailRequestEntity.setAttachments(mailRequest.getAttachments().stream()
                .map(mailRequestAttachment -> {
                    MailRequestAttachmentEntity attachmentEntity = new MailRequestAttachmentEntity();
                    attachmentEntity.setFileName(mailRequestAttachment.getFileName());
                    attachmentEntity.setContentType(mailRequestAttachment.getContentType());
                    attachmentEntity.setData(mailRequestAttachment.getData());
                    attachmentEntity.setRequest(mailRequestEntity);
                    return attachmentEntity;
                }).collect(Collectors.toList()));
        mailRequestRepository.save(mailRequestEntity);
        return mailRequestEntity.getId();
    }

    @Override
    public Optional<MailRequest> getNextMailRequestReadyToExecute() {

        MailRequestEntity mailRequestEntity = mailRequestRepository.findOneReadyToSendOrderByCreatedOn();
        if (isNull(mailRequestEntity)) {
            return Optional.empty();
        }

        return Optional.of(MailRequest.from(mailRequestEntity));
    }

    @Override
    public void changeStatus(Long mailRequestId, MailRequestStatus malRequestStatus) {

        MailRequestEntity mailRequestEntity = mailRequestRepository.findOne(mailRequestId);
        mailRequestEntity.setStatus(malRequestStatus);
        mailRequestEntity.setProcessedOn(Timestamp.valueOf(LocalDateTime.now()));
    }
}
