import { Component, Input } from '@angular/core';
import { FormGroup, FormControl } from '../../../../node_modules/@angular/forms';
import { LandType } from './land-type.interface';
import { MatCheckboxChange } from '../../../../node_modules/@angular/material';

@Component({
	selector: 'lp-land-type-filter',
	templateUrl: './land-type-filter.component.html',
	styleUrls: ['./land-type-filter.component.scss']
})
export class LandTypeFilterComponent {
	@Input()
	landTypeForm: FormGroup;

	constructor() {}

	checkAll() {
		Object.keys(this.landTypeForm.controls).forEach((formControlName) => {
			let value = this.landTypeForm.get(formControlName).value;
			value.checked = true;
			this.landTypeForm.get(formControlName).setValue(value);
		});
	}

	uncheckAll() {
		Object.keys(this.landTypeForm.controls).forEach((formControlName) => {
			let value = this.landTypeForm.get(formControlName).value;
			value.checked = false;
			this.landTypeForm.get(formControlName).setValue(value);
		});
	}

	onChange(event: MatCheckboxChange, formControlName: string) {
		let value = this.landTypeForm.get(formControlName).value;
		value.checked = event.checked;
		this.landTypeForm.get(formControlName).setValue(value);
	}
}

export function createLandTypeFilterFormGroup(checkedLandTypes?: string[]): FormGroup {
	return new FormGroup(
		{
			typeArableLand: new FormControl({
				key: LandType.ARABLE_LAND,
				checked: checkedLandTypes ? containsLandType(LandType.ARABLE_LAND, checkedLandTypes) : false
			}),
			typeHopper: new FormControl({
				key: LandType.HOPPER,
				checked: checkedLandTypes ? containsLandType(LandType.HOPPER, checkedLandTypes) : false
			}),
			typeVineyard: new FormControl({
				key: LandType.VINEYARD,
				checked: checkedLandTypes ? containsLandType(LandType.VINEYARD, checkedLandTypes) : false
			}),
			typeGarden: new FormControl({
				key: LandType.GARDEN,
				checked: checkedLandTypes ? containsLandType(LandType.GARDEN, checkedLandTypes) : false
			}),
			typeOrchard: new FormControl({
				key: LandType.ORCHARD,
				checked: checkedLandTypes ? containsLandType(LandType.ORCHARD, checkedLandTypes) : false
			}),
			typePermanentGrassland: new FormControl({
				key: LandType.PERMANENT_GRASSLAND,
				checked: checkedLandTypes ? containsLandType(LandType.PERMANENT_GRASSLAND, checkedLandTypes) : false
			}),
			typeForestLand: new FormControl({
				key: LandType.FOREST_LAND,
				checked: checkedLandTypes ? containsLandType(LandType.FOREST_LAND, checkedLandTypes) : false
			}),
			typeWaterArea: new FormControl({
				key: LandType.WATER_AREA,
				checked: checkedLandTypes ? containsLandType(LandType.WATER_AREA, checkedLandTypes) : false
			}),
			typeBuiltAreaAndCourtyard: new FormControl({
				key: LandType.BUILT_AREA_AND_COURTYARD,
				checked: checkedLandTypes ? containsLandType(LandType.BUILT_AREA_AND_COURTYARD, checkedLandTypes) : false
			}),
			typeOtherAreas: new FormControl({
				key: LandType.OTHER_AREAS,
				checked: checkedLandTypes ? containsLandType(LandType.OTHER_AREAS, checkedLandTypes) : false
			})
		},
		validateLandType
	);
}

export function getSelectedLandTypes(landTypeForm: FormGroup): string[] {
	return Object.keys(landTypeForm.controls)
		.filter((formControlName) => landTypeForm.get(formControlName).value.checked)
		.map((formControlName) => landTypeForm.get(formControlName).value.key);
}

function containsLandType(landType: LandType, landTypeKeys: string[]) {
	return landTypeKeys.findIndex((key) => LandType[key] === landType) !== -1;
}

function validateLandType(landTypeForm: FormGroup) {
	return Object.keys(landTypeForm.controls).filter((formControlName) => landTypeForm.get(formControlName).value.checked).length > 0 ? null : { invalid: true };
}
