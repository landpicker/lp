package cz.lp.processor.custom.advertisement.bazoscz;

import cz.lp.CrawlerRunner;
import cz.lp.CrawlerScheduler;
import cz.lp.CrawlerWorkerService;
import cz.lp.StandaloneProcessorScheduler;
import cz.lp.exception.ProcessorClassException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

@ComponentScan(basePackages = "cz.lp", excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {CrawlerRunner.class, CrawlerScheduler.class, StandaloneProcessorScheduler.class}))
public class BazosCzProcessorRunner {

    private static final Logger log = LoggerFactory.getLogger(BazosCzProcessorRunner.class);

    private static final Long ID = 4l;

    public static void main(String[] args) throws ProcessorClassException {

        log.info("Starting Bazos CZ processor runner ...");
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(BazosCzProcessorRunner.class);
        CrawlerWorkerService crawlerWorkerService = ctx.getBean(CrawlerWorkerService.class);
        BazosCzProcessor processor = (BazosCzProcessor) crawlerWorkerService.getProcessor(ID);
        if (processor.checkCondition()) {
            processor.process();
        } else {
            log.warn("Bazos CZ processor does not meet startup conditions");
        }
    }
}
