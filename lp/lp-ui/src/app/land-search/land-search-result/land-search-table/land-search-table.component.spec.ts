import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandSearchTableComponent } from './land-search-table.component';
import { MatPaginatorModule, MatProgressSpinnerModule, MatTableModule, MatIconModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('LandSearchTableComponent', () => {
  let component: LandSearchTableComponent;
  let fixture: ComponentFixture<LandSearchTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BrowserAnimationsModule, TranslateModule.forRoot(), MatTableModule, MatPaginatorModule, MatProgressSpinnerModule, MatIconModule],
      declarations: [ LandSearchTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandSearchTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
