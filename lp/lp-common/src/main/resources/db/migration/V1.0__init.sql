-- Crawler tables

CREATE SEQUENCE source_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.source
(
  id bigint NOT NULL DEFAULT nextval('source_id_seq'::regclass),
  provider character varying(100) NOT NULL,
  website_url text NOT NULL,
  CONSTRAINT source_id_pk PRIMARY KEY (id)
);

CREATE SEQUENCE crawler_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.crawler
(
  id bigint NOT NULL DEFAULT nextval('crawler_id_seq'::regclass),
  source_id bigint NOT NULL,
  crawler_class character varying(100) UNIQUE NOT NULL,
  condition character varying(100) NOT NULL,
  delay integer NOT NULL,
  enabled boolean NOT NULL,
  recrawl_strategy character varying(100) NOT NULL,
  recrawl_interval bigint,
  recrawl_on timestamp without time zone,
  last_recrawled_on timestamp without time zone,
  CONSTRAINT crawler_id_pk PRIMARY KEY (id),
  CONSTRAINT crawler_source_id_fk FOREIGN KEY (source_id)
      REFERENCES public.source (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE SEQUENCE crawler_seed_url_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.crawler_seed_url
(
  id bigint NOT NULL DEFAULT nextval('crawler_seed_url_id_seq'::regclass),
  crawler_id bigint NOT NULL,
  url text UNIQUE NOT NULL,
  enabled boolean NOT NULL,
  condition character varying(100) NOT NULL,
  CONSTRAINT crawler_seed_url_id_pk PRIMARY KEY (id),
  CONSTRAINT crawler_seed_url_crawler_id_fk FOREIGN KEY (crawler_id)
      REFERENCES public.crawler (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE SEQUENCE crawler_result_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.crawler_result
(
  id bigint NOT NULL DEFAULT nextval('crawler_result_id_seq'::regclass),
  crawler_id bigint NOT NULL,
  status character varying(50) NOT NULL,
  url text UNIQUE NOT NULL,
  content_type character varying(50),
  content bytea,
  hash character varying(255),
  message text,
  processing_order integer,
  created_on timestamp without time zone,
  last_crawled_on timestamp without time zone,
  last_modified_on timestamp without time zone,
  CONSTRAINT crawler_result_id_pk PRIMARY KEY (id),
  CONSTRAINT crawler_result_crawler_id_fk FOREIGN KEY (crawler_id)
      REFERENCES public.crawler (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE SEQUENCE processor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.processor
(
  id bigint NOT NULL DEFAULT nextval('processor_id_seq'::regclass),
  crawler_id bigint NOT NULL,
  processor_class character varying(100) UNIQUE NOT NULL,
  condition character varying(100) NOT NULL,
  url_pattern text NOT NULL,
  enabled boolean NOT NULL,
  last_processed_on timestamp without time zone,
  CONSTRAINT processor_id_pk PRIMARY KEY (id),
  CONSTRAINT processor_cralwer_id_fk FOREIGN KEY (crawler_id)
      REFERENCES public.crawler (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE public.area
(
  code character varying(10) UNIQUE NOT NULL,
  type character varying(50) NOT NULL,
  name character varying(50) NOT NULL,
  parent_area_code character varying(10),
  crawler_result_id bigint,
  created_on timestamp without time zone NOT NULL,
  last_updated_on timestamp without time zone,
  removed_on timestamp without time zone,
  CONSTRAINT area_code_pk PRIMARY KEY (code),
  CONSTRAINT area_parent_area_code_fk FOREIGN KEY (parent_area_code)
      REFERENCES public.area (code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE SEQUENCE area_border_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.area_border
(
  id bigint NOT NULL DEFAULT nextval('area_border_id_seq'::regclass),
  epsg5514 json NOT NULL,
  wgs84 json,
  area_code character varying(10) NOT NULL,
  CONSTRAINT area_border_id_pk PRIMARY KEY (id),
  CONSTRAINT area_border_area_code_fk FOREIGN KEY (area_code)
      REFERENCES public.area (code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE SEQUENCE land_border_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.land_border
(
  id bigint NOT NULL DEFAULT nextval('land_border_id_seq'::regclass),
  epsg5514 json NOT NULL,
  wgs84 json,
  CONSTRAINT land_border_id_pk PRIMARY KEY (id)
);

CREATE TABLE public.land
(
  id character varying(30) UNIQUE NOT NULL,
  type character varying(50) NOT NULL,
  trunk_number integer NOT NULL,
  under_division_number integer,
  acreage integer NOT NULL,
  contains_building boolean,
  definition_point json NOT NULL,
  created_on timestamp without time zone NOT NULL,
  last_updated_on timestamp without time zone,
  removed_on timestamp without time zone,
  crawler_result_id bigint,
  area_code character varying(10) NOT NULL,
  land_border_id bigint NOT NULL,
  CONSTRAINT land_id_pk PRIMARY KEY (id),
  CONSTRAINT land_area_code_fk FOREIGN KEY (area_code)
      REFERENCES public.area (code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT land_land_border_id_fk FOREIGN KEY (land_border_id)
      REFERENCES public.land_border (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE public.land_search
(
    id character varying(36) UNIQUE NOT NULL,
    land_search_filter json NOT NULL,
    land_search_filter_hash integer UNIQUE NOT NULL,
    CONSTRAINT land_search_id_pk PRIMARY KEY (id)
)

