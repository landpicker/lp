package cz.lp.common.enumerate;

/**
 * Area type provides enumerate of type.
 */
public enum AreaType {

    REGION, // Kraj
    DISTRICT, // Okres
    CITY, // Mesto (obec)
    CADASTRAL_TERRITORY // Katastralni uzemi

}
