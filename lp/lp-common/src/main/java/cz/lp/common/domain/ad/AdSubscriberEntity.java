package cz.lp.common.domain.ad;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Ad image subscriber represents relation between subscriber and ad.
 */

@Entity
@Table(name = "ad_subscriber")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class AdSubscriberEntity implements Serializable{

    @EmbeddedId
    private AdSubscriberId adSubscriberId;

    @Embeddable
    @Data
    public class AdSubscriberId implements Serializable{

        private static final long serialVersionUID = 1L;

        @Column(name = "subscriber_email")
        private String subscriberEmail;

        @Column(name = "ad_url_pattern")
        private String adUrlPattern;
    }

}
