import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class LoggingService {
	constructor(private http: HttpClient) {}

	error(error: any): void {
		let body = { message: error.message, stack: error.stack };
		this.http.post('/api/log/error', body).subscribe();
	}
}
