package cz.lp.client;

import java.util.Objects;

public final class CrawlerHttpResponse {

    private int status;
    private byte[] content;
    private String contentType;

    /**
     * Creates instance of crawler response.
     *
     * @param status      HTTP status code
     * @param content     content
     * @param contentType content type
     * @throws NullPointerException when response or content type is null
     */
    public CrawlerHttpResponse(int status, byte[] content, String contentType) {
        this.status = status;
        this.content = Objects.requireNonNull(content);
        this.contentType = Objects.requireNonNull(contentType);
    }

    /**
     * Creates instance of crawler response.
     *
     * @param status      HTTP status code
     * @param content     content
     * @param contentType content type
     * @return crawler response
     * @throws NullPointerException when response or content type is null
     */
    public static final CrawlerHttpResponse of(int status, byte[] content, String contentType) {

        return new CrawlerHttpResponse(status, content, contentType);

    }

    /**
     * Gets HTTP status code.
     *
     * @return HTTP status code
     */
    public int getStatus() {
        return status;
    }

    /**
     * Gets content.
     *
     * @return content
     */
    public byte[] getContent() {
        return content;
    }

    /**
     * Gets content type
     *
     * @return
     */
    public String getContentType() {
        return contentType;
    }
}
