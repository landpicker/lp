CREATE TABLE public.processor_crawler_result
(
  processor_id bigint NOT NULL,
  crawler_result_id bigint NOT NULL,
  last_processed_on timestamp without time zone NOT NULL,
  CONSTRAINT processor_crawler_result_pk PRIMARY KEY (processor_id, crawler_result_id)
);
-- All crawled results will be processed again (we don't know what crawler result was processed at the moment]