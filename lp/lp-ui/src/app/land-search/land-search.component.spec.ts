import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandSearchComponent } from './land-search.component';
import { TranslateModule } from '../../../node_modules/@ngx-translate/core';
import { MatButtonModule, MatSidenavModule, MatIconModule, MatToolbarModule, MatProgressSpinnerModule, MatCardModule } from '../../../node_modules/@angular/material';
import { AcreageFilterModule } from '../shared/acreage-filter/acreage-filter.module';
import { LandTypeFilterModule } from '../shared/land-type-filter/land-type-filter.module';
import { MapModule } from '../shared/map/map.module';
import { LandSearchFilterComponent } from './land-search-filter/land-search-filter.component';
import { LandSearchMapComponent } from './land-search-map/land-search-map.component';
import { NO_ERRORS_SCHEMA } from '../../../node_modules/@angular/core';
import { AreaFilterModule } from '../shared/area-filter/area-filter.module';
import { HttpClientModule } from '@angular/common/http';
import { LandSearchService } from './land-search.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LandSearchInfoComponent } from './land-search-info/land-search-info.component';
import { LandSearchResultComponent } from './land-search-result/land-search-result.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('LandSearchComponent', () => {
  let component: LandSearchComponent;
  let fixture: ComponentFixture<LandSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        HttpClientModule,
        RouterTestingModule,
        AreaFilterModule,
        AcreageFilterModule,
        LandTypeFilterModule,
        FormsModule,
        ReactiveFormsModule,
        MapModule,
        MatSidenavModule,
        MatButtonModule,
        MatIconModule,
        MatToolbarModule,
        MatProgressSpinnerModule,
        MatCardModule,
        MatToolbarModule
      ],
      providers: [ LandSearchService ],
      declarations: [ LandSearchComponent, LandSearchFilterComponent, LandSearchMapComponent, LandSearchInfoComponent, LandSearchResultComponent ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
