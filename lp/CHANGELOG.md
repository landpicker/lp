# 1.2.0 (21.06.2019)
### Features

* **lp-crawler:** crawling and processing ads from Sreality.cz

# 1.1.0 (11.06.2019)
### Features

* **lp:** new object "ad" ("advertisement")
* **lp-crawler:** add service for creating mail requests
* **lp-mail:** new service (module) that provides sending created mail requests 
* **lp-crawler:** new type of processor - standalone that is independent on crawler - it has own scheduler
* **lp-crawler:** new standalone processor for creating mail request from ads
* **lp-crawler:** crawling and processing ads from Bazos.cz


# 1.0.1 (11.06.2019)
### Bug fixes

* **lp-ui:** Change http protocol to https for mapy.cz API

# 1.0.0 (03.11.2019)
### First version

* **TBD:**