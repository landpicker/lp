INSERT INTO public.source (id, provider, website_url) VALUES (4, 'Sreality.cz', 'https://sreality.cz');

INSERT INTO public.crawler (id, source_id, crawler_class, condition, delay, enabled, recrawl_strategy, recrawl_url_pattern, recrawl_interval, recrawl_on, last_recrawled_on)
VALUES (4, 4, 'cz.lp.crawler.custom.advertisement.srealitycz.SrealityCzCrawler', 'NONE', 200, false, 'RECRAWL_BY_PATTERN', '(?i)https?://(www.)?sreality\.cz/api/cs/v2/estates\?.*&page=.*', 60, null, null);


INSERT INTO public.crawler_seed_url (crawler_id, url, enabled, condition) VALUES (4, 'https://www.sreality.cz/api/cs/v2/estates?category_main_cb=3&category_type_cb=1&distance=10&per_page=100&region=obec+Z%C3%A1b%C5%99eh&region_entity_id=461&region_entity_type=municipality&page=1', true, 'NONE');
INSERT INTO public.crawler_seed_url (crawler_id, url, enabled, condition) VALUES (4, 'https://www.sreality.cz/api/cs/v2/estates?category_main_cb=2&category_type_cb=1&distance=10&per_page=100&region=obec+Z%C3%A1b%C5%99eh&region_entity_id=461&region_entity_type=municipality&page=1', true, 'NONE');

