import { Component, OnInit, ViewChild, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { MatTableDataSource, MatPaginator, PageEvent } from '@angular/material';
import { tap } from 'rxjs/operators';
import { LandSearchResult, Land } from '../../land-search.interface';

export interface PaginationChangeEvent {
	page: number;
	size: number;
}

export interface ShowOnMapEvent {
	landId: string;
}

export const DEFAULT_PAGE_SIZE: number = 10;

@Component({
	selector: 'lp-land-search-table',
	templateUrl: './land-search-table.component.html',
	styleUrls: ['./land-search-table.component.scss']
})
export class LandSearchTableComponent implements OnInit, AfterViewInit {
	@Input()
	set landSearchResult(landSearchResult: LandSearchResult) {
		if (landSearchResult) {
			this.lands = landSearchResult.lands;
			this.dataSource = new MatTableDataSource<Land>(this.lands);
			this.totalLands = landSearchResult.totalElements;
			this.paginator.pageIndex = landSearchResult.page;
		}
	}
	@Output()
	changePagination = new EventEmitter<PaginationChangeEvent>();
	@Output()
	showOnMap = new EventEmitter();

	defaultPageSize: number = DEFAULT_PAGE_SIZE;
	displayedColumns: string[] = ['landNumber', 'cityArea', 'cadstralTerritoryArea', 'type', 'acreage', 'action'];

	dataSource: MatTableDataSource<Land>;
	isLoading: boolean = false;
	lands: Land[];
	totalLands: number;

	@ViewChild(MatPaginator)
	paginator: MatPaginator;

	ngOnInit() {
		this.dataSource = new MatTableDataSource<Land>(this.lands);
	}

	ngAfterViewInit(): void {
		this.paginator.page
			.pipe(
				tap((event: PageEvent) => {
					this.changePagination.emit({ page: event.pageIndex, size: event.pageSize } as PaginationChangeEvent);
				})
			)
			.subscribe();
	}

	startLoading() {
		this.isLoading = true;
	}

	stopLoading() {
		this.isLoading = false;
	}
}
