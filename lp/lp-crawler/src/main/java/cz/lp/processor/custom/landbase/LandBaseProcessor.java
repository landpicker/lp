package cz.lp.processor.custom.landbase;

import com.google.common.collect.Lists;
import cz.lp.common.domain.AreaEntity;
import cz.lp.common.domain.LandBorderEntity;
import cz.lp.common.domain.LandEntity;
import cz.lp.common.repository.AreaBorderRepository;
import cz.lp.common.repository.AreaRepository;
import cz.lp.common.repository.LandBorderRepository;
import cz.lp.common.repository.LandRepository;
import cz.lp.enumerate.ProcessorCondition;
import cz.lp.exception.ProcessorExtractDataException;
import cz.lp.processor.Processor;
import cz.lp.processor.ProcessorFilter;
import cz.lp.processor.Result;
import cz.lp.processor.custom.landbase.adapter.LandBaseDataAdapter;
import cz.lp.processor.resolver.ProcessorConditionResolver;
import cz.lp.processor.service.ProcessorRepositoryService;
import cz.lp.processor.service.ProcessorService;
import cz.lp.util.GzipUtil;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class LandBaseProcessor extends Processor<Land> {

    private AreaRepository areaRepository;
    private AreaBorderRepository areaBorderRepository;
    private LandRepository landRepository;
    private LandBorderRepository landBorderRepository;

    public LandBaseProcessor(String className, Long id, Long crawlerId, ProcessorCondition condition, String urlPattern, ProcessorService processorService, ProcessorConditionResolver processorConditionResolver, ProcessorRepositoryService processorRepositoryService, ProcessorFilter processorFilter) {
        super(className, id, crawlerId, condition, urlPattern, processorService, processorConditionResolver, processorFilter);
        this.areaRepository = processorRepositoryService.getAreaRepository();
        this.areaBorderRepository = processorRepositoryService.getAreaBorderRepository();
        this.landRepository = processorRepositoryService.getLandRepository();
        this.landBorderRepository = processorRepositoryService.getLandBorderRepository();
    }

    @Override
    protected List<Land> extractData(Result result) throws ProcessorExtractDataException {

        try {
            String unzippedContent = new String(GzipUtil.decompressGZIP(result.getContent()));
            LandBaseDataAdapter landBaseDataAdapter = new LandBaseDataAdapter(unzippedContent, result.getUrl());
            Optional<List<Land>> lands = landBaseDataAdapter.getLands();
            if (!lands.isPresent()) {
                throw new ProcessorExtractDataException();
            }
            return lands.get();
        } catch (IOException ex) {
            throw new ProcessorExtractDataException();
        }
    }

    @Override
    protected void setDataAsRemoved(Result result, LocalDateTime removedOn) {

    }

    @Override
    protected void saveData(List<Land> data, Long crawlerResultId, LocalDateTime lastUpdatedOn) {

        List<LandEntity> landEntities = data.stream().map(land -> {
            AreaEntity areaEntity = new AreaEntity(land.getCadastralTerritoryAreaCode());
            LandBorderEntity landBorderEntity = new LandBorderEntity(land.getBorderEpsg5514Coordinates(), null);
            LandEntity landEntity = new LandEntity(land.getId(), land.getType(), land.getTrunkNumber(), land.getUnderDivisionNumber(), land.getAcreage(), null, land.getDefinitionPointEpsg5514(), Timestamp.valueOf(lastUpdatedOn), null, null, crawlerResultId, areaEntity, landBorderEntity);
            return landEntity;
        }).collect(Collectors.toList());

        List<List<LandEntity>> landEntityChunks = Lists.partition(landEntities, 100);

        landEntityChunks.forEach(lands -> {
            landRepository.save(lands);
        });
    }
}
