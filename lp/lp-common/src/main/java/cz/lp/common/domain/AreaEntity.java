package cz.lp.common.domain;

import cz.lp.common.enumerate.AreaType;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * Area entity represents area object in database table.
 */

@Entity
@Table(name = "area")
public class AreaEntity {

    @Id
    @Column(name = "code", unique = true, nullable = false)
    private String code;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false, length = 50)
    private AreaType type;

    @Column(name = "name", nullable = false, length = 50)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_area_code")
    private AreaEntity parentArea;

    @OneToMany(mappedBy = "parentArea", fetch = FetchType.LAZY)
    private List<AreaEntity> subAreas;

    @Column(name = "crawler_result_id")
    private Long crawlerResultId;

    @Column(name = "created_on", nullable = false)
    private Timestamp createdOn;

    @Column(name = "last_updated_on")
    private Timestamp lastUpdatedOn;

    @Column(name = "removed_on")
    private Timestamp removedOn;

    @OneToOne(mappedBy = "area", fetch = FetchType.LAZY)
    private AreaBorderEntity areaBorder;

    /**
     * Creates instance of default constructor.
     */
    public AreaEntity() {
    }

    /**
     * Creates instance of default constructor.
     *
     * @param code            code
     */
    public AreaEntity(String code) {
        this.code = code;
    }

    /**
     * Creates instance of area entity.
     *
     * @param code            code
     * @param type            type
     * @param name            name
     * @param parentArea      parent area
     * @param subAreas        sub areas
     * @param crawlerResultId crawler result identifier
     * @param createdOn       created on
     * @param lastUpdatedOn   last updated on
     */
    public AreaEntity(String code, AreaType type, String name, AreaEntity parentArea, List<AreaEntity> subAreas, Long crawlerResultId, Timestamp createdOn, Timestamp lastUpdatedOn) {
        this.code = code;
        this.type = type;
        this.name = name;
        this.parentArea = parentArea;
        this.subAreas = subAreas;
        this.crawlerResultId = crawlerResultId;
        this.createdOn = createdOn;
        this.lastUpdatedOn = lastUpdatedOn;
    }

    /**
     * Creates instance of area entity.
     *
     * @param code            code
     * @param type            type
     * @param name            name
     * @param parentArea      parent are
     * @param subAreas        sub areas
     * @param crawlerResultId crawler result identifier
     * @param createdOn       created on
     * @param lastUpdatedOn   last updated on
     * @param removedOn       removed on
     */
    public AreaEntity(String code, AreaType type, String name, AreaEntity parentArea, List<AreaEntity> subAreas, Long crawlerResultId, Timestamp createdOn, Timestamp lastUpdatedOn, Timestamp removedOn) {
        this.code = code;
        this.type = type;
        this.name = name;
        this.parentArea = parentArea;
        this.subAreas = subAreas;
        this.crawlerResultId = crawlerResultId;
        this.createdOn = createdOn;
        this.lastUpdatedOn = lastUpdatedOn;
        this.removedOn = removedOn;
    }

    /**
     * Creates instance of area entity.
     *
     * @param code            code
     * @param type            type
     * @param name            name
     * @param parentArea      parent are
     * @param subAreas        sub areas
     * @param crawlerResultId crawler result identifier
     * @param createdOn       created on
     * @param lastUpdatedOn   last updated on
     * @param removedOn       removed on
     * @param areaBorder      area border
     */
    public AreaEntity(String code, AreaType type, String name, AreaEntity parentArea, List<AreaEntity> subAreas, Long crawlerResultId, Timestamp createdOn, Timestamp lastUpdatedOn, Timestamp removedOn, AreaBorderEntity areaBorder) {
        this.code = code;
        this.type = type;
        this.name = name;
        this.parentArea = parentArea;
        this.subAreas = subAreas;
        this.crawlerResultId = crawlerResultId;
        this.createdOn = createdOn;
        this.lastUpdatedOn = lastUpdatedOn;
        this.removedOn = removedOn;
        this.areaBorder = areaBorder;
    }

    /**
     * Gets code.
     *
     * @return code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets type.
     *
     * @return type
     */
    public AreaType getType() {
        return type;
    }

    /**
     * Gets name.
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets parent area.
     *
     * @return parent area
     */
    public AreaEntity getParentArea() {
        return parentArea;
    }

    /**
     * Gets sub areas.
     *
     * @return sub areas
     */
    public List<AreaEntity> getSubAreas() {
        return subAreas;
    }

    /**
     * Gets crawler result identifier.
     *
     * @return crawler result identifier
     */
    public Long getCrawlerResultId() {
        return crawlerResultId;
    }

    /**
     * Gets created on.
     *
     * @return created on
     */
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    /**
     * Gets last updated on.
     *
     * @return last updated on
     */
    public Timestamp getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    /**
     * Sets last updated on.
     *
     * @param lastUpdatedOn last updated on
     */
    public void setLastUpdatedOn(Timestamp lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    /**
     * Gets removed on.
     *
     * @return removed on
     */
    public Timestamp getRemovedOn() {
        return removedOn;
    }

    /**
     * Sets removed on.
     *
     * @param removedOn removed on
     */
    public void setRemovedOn(Timestamp removedOn) {
        this.removedOn = removedOn;
    }

    /**
     * Gets area border.
     *
     * @return area border
     */
    public AreaBorderEntity getAreaBorder() {
        return areaBorder;
    }
}
