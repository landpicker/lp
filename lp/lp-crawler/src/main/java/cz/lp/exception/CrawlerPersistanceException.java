package cz.lp.exception;

public class CrawlerPersistanceException extends Exception {

    public CrawlerPersistanceException(String message) {
        super(message);
    }

    public CrawlerPersistanceException(String message, Throwable cause) {
        super(message, cause);
    }
}
