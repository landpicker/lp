import { Injectable, Component } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { NotificationSnackbarViewComponent } from './notification-snackbar-view/notification-snackbar-view.component';

@Injectable({
	providedIn: 'root'
})
export class NotificationService {
	constructor(private _snackBar: MatSnackBar) {}

	showSnackbarNotification(text: string) {
    this._snackBar.openFromComponent(NotificationSnackbarViewComponent, {
      duration: 3000,
      data: {
        text: text
      }
    });
	}
}
