import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { RouterModule } from '../../node_modules/@angular/router';
import { TranslateModule } from '../../node_modules/@ngx-translate/core';
import { NO_ERRORS_SCHEMA } from '../../node_modules/@angular/core';
import { APP_BASE_HREF } from '../../node_modules/@angular/common';
import { AppRoutingModule } from './app-routing-module';
import { LandSearchModule } from './land-search/land-search.module';
import { PageNotFoundComponent } from './app-config/page-not-found/page-not-found.component';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterModule,
        TranslateModule.forRoot(),
        AppRoutingModule,
        LandSearchModule
      ],
      providers: [{provide: APP_BASE_HREF, useValue: '/'}],
      declarations: [
        AppComponent, PageNotFoundComponent
      ],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
