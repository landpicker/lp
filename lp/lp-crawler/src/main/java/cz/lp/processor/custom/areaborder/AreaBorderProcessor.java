package cz.lp.processor.custom.areaborder;

import cz.lp.common.domain.AreaBorderEntity;
import cz.lp.common.domain.AreaEntity;
import cz.lp.common.repository.AreaBorderRepository;
import cz.lp.common.repository.AreaRepository;
import cz.lp.common.repository.LandBorderRepository;
import cz.lp.common.repository.LandRepository;
import cz.lp.enumerate.ProcessorCondition;
import cz.lp.exception.ProcessorExtractDataException;
import cz.lp.processor.Processor;
import cz.lp.processor.ProcessorFilter;
import cz.lp.processor.Result;
import cz.lp.processor.custom.areaborder.adapter.AreaBorderDataAdapter;
import cz.lp.processor.resolver.ProcessorConditionResolver;
import cz.lp.processor.service.ProcessorRepositoryService;
import cz.lp.processor.service.ProcessorService;
import cz.lp.util.GzipUtil;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.isNull;

public class AreaBorderProcessor extends Processor<AreaBorder> {


    private AreaRepository areaRepository;
    private AreaBorderRepository areaBorderRepository;

    public AreaBorderProcessor(String className, Long id, Long crawlerId, ProcessorCondition condition, String urlPattern, ProcessorService processorService, ProcessorConditionResolver processorConditionResolver,
                               ProcessorRepositoryService processorRepositoryService, ProcessorFilter processorFilter) {
        super(className, id, crawlerId, condition, urlPattern, processorService, processorConditionResolver, processorFilter);
        this.areaRepository = processorRepositoryService.getAreaRepository();
        this.areaBorderRepository = processorRepositoryService.getAreaBorderRepository();
    }

    @Override
    protected List<AreaBorder> extractData(Result result) throws ProcessorExtractDataException {

        List<AreaBorder> areaBorders = new ArrayList<>();
        try {
            String unzippedContent = new String(GzipUtil.decompressGZIP(result.getContent()));
            AreaBorderDataAdapter areaBorderDataAdapter = new AreaBorderDataAdapter(unzippedContent, result.getUrl());

            Optional<AreaBorder> cityAreaBorder = areaBorderDataAdapter.getCityAreaBorder();
            Optional<List<AreaBorder>> cadastralTerritoryBorders = areaBorderDataAdapter.getCadastralTerritoryBorders();
            if (!cityAreaBorder.isPresent() || !cadastralTerritoryBorders.isPresent()) {
                throw new ProcessorExtractDataException();
            }
            areaBorders.add(cityAreaBorder.get());
            areaBorders.addAll(cadastralTerritoryBorders.get());
        } catch (IOException ex) {
            throw new ProcessorExtractDataException();
        }
        return areaBorders;
    }

    @Override
    protected void saveData(List<AreaBorder> data, Long crawlerResultId, LocalDateTime lastUpdatedOn) {

        data.forEach(areaBorder -> {

            AreaBorderEntity areaBorderEntity = areaBorderRepository.findOneByAreaCode(areaBorder.getCode());
            if (isNull(areaBorderEntity)) {
                createAreaBorder(areaBorder);
            } else {
                updateAreaBorder(areaBorderEntity, areaBorder);
            }
        });
    }

    @Override
    protected void setDataAsRemoved(Result result, LocalDateTime removedOn) {

    }

    /**
     * Creates area border.
     *
     * @param area border           area border
     */
    private void createAreaBorder(AreaBorder area) {

        AreaEntity areaEntity = areaRepository.findOne(area.getCode());
        AreaBorderEntity areaBorderEntity = new AreaBorderEntity(area.getEpsg5514Coordinates(), area.getWgs84Coordinates(), areaEntity);
        areaBorderRepository.save(areaBorderEntity);
    }

    /**
     * Updated area border.
     *
     * @param areaBorderEntity area border entity
     * @param areaBorder       area border
     */
    private void updateAreaBorder(AreaBorderEntity areaBorderEntity, AreaBorder areaBorder) {

        areaBorderEntity.setEpsg5514Coordinates(areaBorder.getEpsg5514Coordinates());
        areaBorderEntity.setWgs84Coordinates(areaBorder.getWgs84Coordinates());
        areaBorderRepository.save(areaBorderEntity);
    }
}
