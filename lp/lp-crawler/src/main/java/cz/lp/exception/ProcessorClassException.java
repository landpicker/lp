package cz.lp.exception;

/**
 * Crawler class exception is generated if something during creating instance of crawler by reflection failed.
 */
public class ProcessorClassException extends Exception {

    public ProcessorClassException(String message) {
        super(message);
    }

    public ProcessorClassException(String message, Throwable cause) {
        super(message, cause);
    }
}
