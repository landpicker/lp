package cz.lp.server.controller;

import cz.lp.server.dto.Land;
import cz.lp.server.dto.LandSearchResult;
import cz.lp.server.dto.filter.LandSearchFilter;
import cz.lp.server.service.LandService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

import static cz.lp.server.controller.ControllerUtil.setJsonResponseHeaders;

/**
 * Land REST API controller provides data about lands.
 */
@RestController
@RequestMapping("/api/lands")
public class LandController {

    private static final Logger LOG = LoggerFactory.getLogger(LandController.class);

    private final LandService landService;

    @Autowired
    public LandController(LandService landService) {
        this.landService = landService;
    }

    @RequestMapping(value = "/count", method = RequestMethod.POST)
    public Long getExpectedCountOfLands(@RequestBody LandSearchFilter landSearchFilter, HttpServletResponse response) {

        LOG.info("Request: Get expected count of lands with filter: {}.", landSearchFilter);
        setJsonResponseHeaders(response);
        final long expectedCountOfLands = landService.getExpectedCountOfLands(landSearchFilter);
        LOG.info("Response: Get expected count of lands: {}.", expectedCountOfLands);
        return expectedCountOfLands;
    }

    /**
     * This request is used in case if the user shares a link with result (lands).
     *
     * @param searchId search identifier
     * @param page     page of land list (can be null => default value is 1)
     * @param size     size of page (can be null => default value is 25)
     * @param response http response
     * @return land search result (contains lands without border and filter information)
     */
    @RequestMapping(value = "/search/{id}", method = RequestMethod.POST)
    public LandSearchResult searchLandsBySearchId(@PathVariable("id") String searchId, @RequestParam(name = "page", required = false, defaultValue = "0") Integer page, @RequestParam(name = "size", required = false, defaultValue = "10") Integer size, HttpServletResponse response) {

        LOG.info("Request: Search land with search id: {}.", searchId);
        setJsonResponseHeaders(response);
        final LandSearchResult landSearchResult = landService.getLands(searchId, page, size);
        LOG.info("Response: Search land with filter: {}: {} lands were found.", landSearchResult.getLandSearchFilter(), landSearchResult.getLands().size());
        return landSearchResult;
    }

    /**
     * This request is used in case if the user changes the filter.
     *
     * @param landSearchFilter land search filter
     * @param page             page of land list (can be null => default value is 1)
     * @param size             size of page (can be null => default value is 25)
     * @param response         http response
     * @return land search result (contains lands without border and filter information)
     */
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public LandSearchResult searchLandsByFilter(@RequestBody LandSearchFilter landSearchFilter, @RequestParam(name = "page", required = false, defaultValue = "0") Integer page, @RequestParam(name = "size", required = false, defaultValue = "10") Integer size, HttpServletResponse response) {

        LOG.info("Request: Search land with filter: {}.", landSearchFilter);
        landSearchFilter.validate();
        setJsonResponseHeaders(response);
        final LandSearchResult landSearchResult = landService.getLands(landSearchFilter, page, size);
        LOG.info("Response: Search land with filter: {}: {} lands were found.", landSearchResult.getLandSearchFilter(), landSearchResult.getLands().size());
        return landSearchResult;
    }

    /**
     * This request is used in case if the user can display detail information about land.
     *
     * @param id       land identifier
     * @param response http response
     * @return land (also with border)
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Land getLand(@PathVariable("id") String id, HttpServletResponse response) {

        LOG.info("Request: Get land with id: {}.", id);
        setJsonResponseHeaders(response);
        final Land land = landService.getLand(id);
        LOG.info("Response: Get land with id: {}: {}.", id, land);
        return land;
    }
}
