package cz.lp.domain;

import cz.lp.enumerate.CrawlerCondition;
import cz.lp.enumerate.RecrawlStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * Crawler entity represents crawler object in database table and contains important settings of crawling.
 */
@Entity
@Table(name = "crawler")
public class CrawlerEntity implements Serializable {

    @Id
    @SequenceGenerator(name = "id", sequenceName = "crawler_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "source_id", nullable = false)
    private SourceEntity source;

    @Column(name = "crawler_class", unique = true, nullable = false, length = 100)
    private String crawlerClass;

    @Enumerated(EnumType.STRING)
    @Column(name = "condition", nullable = false, length = 100)
    private CrawlerCondition condition;

    @Column(name = "delay", nullable = false)
    private Integer delay;

    @Column(name = "enabled", nullable = false)
    private Boolean enabled;

    @Enumerated(EnumType.STRING)
    @Column(name = "recrawl_strategy", nullable = false, length = 100)
    private RecrawlStrategy recrawlStrategy;

    @Column(name = "recrawl_url_pattern")
    private String recrawlUrlPattern;

    @Column(name = "recrawl_interval")
    private Long recrawlInterval;

    @Column(name = "recrawl_on")
    private Timestamp recrawlOn;

    @Column(name = "last_recrawled_on")
    private Timestamp lastRecrawledOn;

    @OneToMany(mappedBy = "crawler", fetch = FetchType.LAZY)
    private List<CrawlerSeedUrlEntity> crawlerSeedUrls;

    @OneToMany(mappedBy = "crawler", fetch = FetchType.LAZY)
    private List<CrawlerResultEntity> crawlerResults;

    @OneToMany(mappedBy = "crawler", fetch = FetchType.LAZY)
    private List<ProcessorEntity> processors;

    /**
     * Creates instance of default constructor.
     */
    public CrawlerEntity() {
    }

    /**
     * Creates instance of crawler entity.
     *
     * @param id              identifier
     * @param source          source
     * @param crawlerClass    crawler class
     * @param condition       condition
     * @param delay           delay
     * @param enabled         enabled
     * @param recrawlStrategy recrawl strategy
     * @param recrawlPattern  recrawl pattern
     * @param recrawlInterval recrawl interval
     * @param recrawlOn       recrawl on
     * @param lastRecrawledOn last recrawled on
     * @param crawlerSeedUrls crawler seed URL
     * @param crawlerResults  crawler results
     * @param processors      processors
     */
    public CrawlerEntity(Long id, SourceEntity source, String crawlerClass, CrawlerCondition condition, Integer delay, Boolean enabled,
                         RecrawlStrategy recrawlStrategy, String recrawlPattern, Long recrawlInterval, Timestamp recrawlOn, Timestamp lastRecrawledOn,
                         List<CrawlerSeedUrlEntity> crawlerSeedUrls, List<CrawlerResultEntity> crawlerResults, List<ProcessorEntity> processors) {
        this.id = id;
        this.source = source;
        this.crawlerClass = crawlerClass;
        this.condition = condition;
        this.delay = delay;
        this.enabled = enabled;
        this.recrawlStrategy = recrawlStrategy;
        this.recrawlUrlPattern = recrawlPattern;
        this.recrawlInterval = recrawlInterval;
        this.recrawlOn = recrawlOn;
        this.lastRecrawledOn = lastRecrawledOn;
        this.crawlerSeedUrls = crawlerSeedUrls;
        this.crawlerResults = crawlerResults;
        this.processors = processors;
    }

    /**
     * Gets identifier.
     *
     * @return identifier
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets source.
     *
     * @return source
     */
    public SourceEntity getSource() {
        return source;
    }

    /**
     * Gets crawler class.
     *
     * @return crawler class
     */
    public String getCrawlerClass() {
        return crawlerClass;
    }

    /**
     * Gets condition.
     *
     * @return condition
     */
    public CrawlerCondition getCondition() {
        return condition;
    }

    /**
     * Gets delay.
     *
     * @return delay
     */
    public Integer getDelay() {
        return delay;
    }

    /**
     * Checks if is enabled.
     *
     * @return enabled
     */
    public Boolean isEnabled() {
        return enabled;
    }

    /**
     * Sets enabled.
     *
     * @param enabled enabled
     */
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * Gets recrawl strategy.
     *
     * @return recrawl strategy
     */
    public RecrawlStrategy getRecrawlStrategy() {
        return recrawlStrategy;
    }

    /**
     * Get recrawl URL pattern (It is used only for RECRAWL_BY_PATTERN strategy)
     * @return recrawl pattern
     */
    public String getRecrawlUrlPattern() {
        return recrawlUrlPattern;
    }

    /**
     * Gets recrawl interval.
     *
     * @return recrawl interval
     */
    public Long getRecrawlInterval() {
        return recrawlInterval;
    }

    /**
     * Gets recrawl on.
     *
     * @return recrawl on
     */
    public Timestamp getRecrawlOn() {
        return recrawlOn;
    }

    /**
     * Sets recrawl on.
     *
     * @param recrawlOn recrawl on
     */
    public void setRecrawlOn(Timestamp recrawlOn) {
        this.recrawlOn = recrawlOn;
    }

    /**
     * Gets last recrawled on
     *
     * @return last recrawled on
     */
    public Timestamp getLastRecrawledOn() {
        return lastRecrawledOn;
    }

    /**
     * Sets last recrawled on.
     *
     * @param lastRecrawledOn last recrawled on
     */
    public void setLastRecrawledOn(Timestamp lastRecrawledOn) {
        this.lastRecrawledOn = lastRecrawledOn;
    }

    /**
     * Gets crawler SEED URLs.
     *
     * @return crawler SEED URLs.
     */
    public List<CrawlerSeedUrlEntity> getCrawlerSeedUrls() {
        return crawlerSeedUrls;
    }

    /**
     * Gets crawler results.
     *
     * @return crawler results
     */
    public List<CrawlerResultEntity> getCrawlerResults() {
        return crawlerResults;
    }

    /**
     * Gets processors.
     *
     * @return processor
     */
    public List<ProcessorEntity> getProcessors() {
        return processors;
    }
}
