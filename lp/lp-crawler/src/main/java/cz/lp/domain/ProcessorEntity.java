package cz.lp.domain;

import cz.lp.enumerate.ProcessorCondition;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Processor entity represents processor object in database table and contains important settings of processing.
 */
@Entity
@Table(name = "processor")
public class ProcessorEntity implements Serializable {

    @Id
    @SequenceGenerator(name = "id", sequenceName = "processor_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "crawler_id", nullable = false)
    private CrawlerEntity crawler;

    @Column(name = "processor_class", unique = true, nullable = false, length = 100)
    private String processorClass;

    @Enumerated(EnumType.STRING)
    @Column(name = "condition", nullable = false, length = 100)
    private ProcessorCondition condition;

    @Column(name = "url_pattern", nullable = false)
    private String urlPattern;

    @Column(name = "enabled")
    private Boolean enabled;

    @Column(name = "last_processed_on")
    private Timestamp lastProcessedOn;

    /**
     * Creates default constructor.
     */
    public ProcessorEntity() {
    }

    /**
     * Creates instance of processor entity.
     *
     * @param id              identifier
     * @param crawler         crawler
     * @param processorClass  processor class
     * @param condition       condition
     * @param urlPattern      URL pattern
     * @param enabled         enabled
     * @param lastProcessedOn last processed on
     */
    public ProcessorEntity(Long id, CrawlerEntity crawler, String processorClass, ProcessorCondition condition, String urlPattern, Boolean enabled, Timestamp lastProcessedOn) {
        this.id = id;
        this.crawler = crawler;
        this.processorClass = processorClass;
        this.condition = condition;
        this.urlPattern = urlPattern;
        this.enabled = enabled;
        this.lastProcessedOn = lastProcessedOn;
    }

    /**
     * Gets identifier.
     *
     * @return identifier
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets crawler.
     *
     * @return crawler
     */
    public CrawlerEntity getCrawler() {
        return crawler;
    }

    /**
     * Gets processor class.
     *
     * @return processor class
     */
    public String getProcessorClass() {
        return processorClass;
    }

    /**
     * Gets condition.
     *
     * @return condition
     */
    public ProcessorCondition getCondition() {
        return condition;
    }

    /**
     * Gets URL pattern.
     * @return URL pattern
     */
    public String getUrlPattern() {
        return urlPattern;
    }

    /**
     * Gets enabled.
     *
     * @return enabled
     */
    public Boolean isEnabled() {
        return enabled;
    }

    /**
     * Sets enabled.
     *
     * @param enabled enabled
     */
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * Gets last processed on.
     *
     * @return last processed on
     */
    public Timestamp getLastProcessedOn() {
        return lastProcessedOn;
    }

    /**
     * Sets last processed on.
     *
     * @param lastProcessedOn last processed on
     */
    public void setLastProcessedOn(Timestamp lastProcessedOn) {
        this.lastProcessedOn = lastProcessedOn;
    }
}
