package cz.lp.mail.service;

/**
 * Mail service provides methods for obtaining and executing e-mail requests to send.
 */
public interface MailService {

    /**
     * Gets all mail requests ready to execute.
     */
    void sendAllPreparedEmails();
}
