package cz.lp.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 * Main class for scheduling and executing mail requests.
 */

@ComponentScan(basePackages = {"cz.lp.mail", "cz.lp.common.service.mail"})
public class MailRunner {

    private static final Logger log = LoggerFactory.getLogger(MailRunner.class);

    public static void main(String[] args) {

        log.info("Starting Mail Runner...");
        new AnnotationConfigApplicationContext(MailRunner.class);
    }
}
