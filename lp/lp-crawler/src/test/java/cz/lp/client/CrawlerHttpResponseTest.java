package cz.lp.client;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit tests of crawler http response DTO {@link CrawlerHttpResponse}
 */
public class CrawlerHttpResponseTest {

    private static final int STATUS_CODE = 200;
    private static final byte[] CONTENT = "CONTENT".getBytes();
    private static final String CONTENT_TYPE = "text/html";

    @Test
    public void constructor_shouldReturnObject() throws Exception {

        CrawlerHttpResponse response = new CrawlerHttpResponse(STATUS_CODE, CONTENT, CONTENT_TYPE);
        Assert.assertNotNull(response);
        Assert.assertEquals(STATUS_CODE, response.getStatus());
        Assert.assertEquals(CONTENT, response.getContent());
        Assert.assertEquals(CONTENT_TYPE, response.getContentType());
    }

    @Test(expected = NullPointerException.class)
    public void constructor_whenContentIsNull_shouldThrowException() throws Exception {

        new CrawlerHttpResponse(STATUS_CODE, null, CONTENT_TYPE);
    }

    @Test(expected = NullPointerException.class)
    public void constructor_whenContentTypeIsNull_shouldThrowException() throws Exception {

        new CrawlerHttpResponse(STATUS_CODE, CONTENT, null);
    }

    @Test
    public void of_shouldReturnObject() throws Exception {

        CrawlerHttpResponse response = new CrawlerHttpResponse(STATUS_CODE, CONTENT, CONTENT_TYPE);
        Assert.assertNotNull(response);
        Assert.assertEquals(STATUS_CODE, response.getStatus());
        Assert.assertEquals(CONTENT, response.getContent());
        Assert.assertEquals(CONTENT_TYPE, response.getContentType());
    }

    @Test(expected = NullPointerException.class)
    public void of_whenContentIsNull_shouldThrowException() throws Exception {

        new CrawlerHttpResponse(STATUS_CODE, null, CONTENT_TYPE);
    }

    @Test(expected = NullPointerException.class)
    public void of_whenContentTypeIsNull_shouldThrowException() throws Exception {

        new CrawlerHttpResponse(STATUS_CODE, CONTENT, null);
    }

    @Test
    public void getStatus() throws Exception {
        CrawlerHttpResponse response = new CrawlerHttpResponse(STATUS_CODE, CONTENT, CONTENT_TYPE);
        Assert.assertNotNull(response);
        Assert.assertEquals(STATUS_CODE, response.getStatus());
    }

    @Test
    public void getContent() throws Exception {
        CrawlerHttpResponse response = new CrawlerHttpResponse(STATUS_CODE, CONTENT, CONTENT_TYPE);
        Assert.assertNotNull(response);
        Assert.assertEquals(CONTENT, response.getContent());
    }

    @Test
    public void getContentType() throws Exception {
        CrawlerHttpResponse response = new CrawlerHttpResponse(STATUS_CODE, CONTENT, CONTENT_TYPE);
        Assert.assertNotNull(response);
        Assert.assertEquals(CONTENT_TYPE, response.getContentType());
    }
}