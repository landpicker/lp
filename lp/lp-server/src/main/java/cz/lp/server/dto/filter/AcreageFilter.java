package cz.lp.server.dto.filter;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * Acreage filter data transfer object provides information about required acreage of searched lands.
 */
public class AcreageFilter {

    private final Integer from;
    private final Integer to;

    /**
     * Creates instance of acreage filter.
     *
     * @param from acreage from [m2]
     * @param to   acreage to [m2]
     */
    @JsonCreator
    public AcreageFilter(@JsonProperty("from") Integer from, @JsonProperty("to") Integer to) {
        this.from = from;
        this.to = to;
    }

    /**
     * Gets acreage from [m2].
     *
     * @return acreage from [m2].
     */
    public Integer getFrom() {
        return from;
    }

    /**
     * Gets acreage to [m2].
     *
     * @return acreage to [m2].
     */
    public Integer getTo() {
        return to;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("from", from)
                .add("to", to)
                .omitNullValues()
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AcreageFilter that = (AcreageFilter) o;
        return Objects.equal(from, that.from) &&
                Objects.equal(to, that.to);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(from, to);
    }
}
