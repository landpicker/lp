package cz.lp.processor.custom.landbase;

import cz.lp.CrawlerRunner;
import cz.lp.CrawlerScheduler;
import cz.lp.CrawlerWorkerService;
import cz.lp.StandaloneProcessorScheduler;
import cz.lp.exception.ProcessorClassException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

/**
 * This class run only LandBaseProcessor {@link LandBaseProcessor}
 */
@ComponentScan(basePackages = "cz.lp", excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {CrawlerRunner.class, CrawlerScheduler.class, StandaloneProcessorScheduler.class}))
public class LandBaseProcessorRunner {

    private static final Logger log = LoggerFactory.getLogger(LandBaseProcessorRunner.class);

    private static final Long ID = 3l;

    public static void main(String[] args) throws ProcessorClassException {

        log.info("Starting Land base processor runner ...");
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(LandBaseProcessorRunner.class);
        CrawlerWorkerService crawlerWorkerService = ctx.getBean(CrawlerWorkerService.class);
        LandBaseProcessor processor = (LandBaseProcessor) crawlerWorkerService.getProcessor(ID);
        if (processor.checkCondition()) {
            processor.process();
        } else {
            log.warn("Land base processor does not meet startup conditions");
        }
    }
}
