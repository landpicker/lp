export const environment = {
	production: true,
	baseUrl: "/",
	version: require('../../package.json').version
};
