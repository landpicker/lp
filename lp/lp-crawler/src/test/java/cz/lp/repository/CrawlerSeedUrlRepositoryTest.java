package cz.lp.repository;

import cz.lp.domain.CrawlerEntity;
import cz.lp.domain.CrawlerSeedUrlEntity;
import cz.lp.enumerate.SeedUrlCondition;
import cz.lp.repository.configuration.RepositoryTemplate;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

public class CrawlerSeedUrlRepositoryTest extends RepositoryTemplate {

    @Autowired
    private CrawlerRepository crawlerRepository;

    @Autowired
    private CrawlerSeedUrlRepository crawlerSeedUrlRepository;

    private static final Long CRAWLER_ID = 1l;
    private static final Long ID = 1l;
    private static final String URL = "http://cuzk.cz/seed_1";
    private static final Boolean ENABLED = true;
    private static final SeedUrlCondition CONDITION = SeedUrlCondition.NONE;

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler/INSERT_crawler_WITH_id_=_1_AND_recrawl_on_=_NULL_AND_ENABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void insert() {

        CrawlerEntity crawlerEntity = crawlerRepository.findOne(CRAWLER_ID);
        CrawlerSeedUrlEntity crawlerSeedUrlEntity = new CrawlerSeedUrlEntity(ID, crawlerEntity, URL, ENABLED, CONDITION);
        Long id = crawlerSeedUrlRepository.save(crawlerSeedUrlEntity).getId();
        CrawlerSeedUrlEntity savedCrawlerSeedUrlEntity = crawlerSeedUrlRepository.findOne(id);
        Assert.assertNotNull(savedCrawlerSeedUrlEntity);
        Assert.assertEquals(id, savedCrawlerSeedUrlEntity.getId());
        Assert.assertEquals(URL, savedCrawlerSeedUrlEntity.getUrl());
        Assert.assertEquals(ENABLED, savedCrawlerSeedUrlEntity.isEnabled());
        Assert.assertEquals(CONDITION, savedCrawlerSeedUrlEntity.getCondition());
        Assert.assertEquals(CRAWLER_ID, savedCrawlerSeedUrlEntity.getCrawler().getId());
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler/INSERT_crawler_WITH_id_=_1_AND_recrawl_on_=_NULL_AND_ENABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler-seed-url/INSERT_crawler_seed_url_WITH_id_=_1_AND_crawler_id_=_1_ENABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void findOne() {

        CrawlerSeedUrlEntity crawlerSeedUrlEntity = crawlerSeedUrlRepository.findOne(ID);
        Assert.assertNotNull(crawlerSeedUrlEntity);
        Assert.assertEquals(ID, crawlerSeedUrlEntity.getId());
        Assert.assertEquals(URL, crawlerSeedUrlEntity.getUrl());
        Assert.assertEquals(ENABLED, crawlerSeedUrlEntity.isEnabled());
        Assert.assertEquals(CONDITION, crawlerSeedUrlEntity.getCondition());
        Assert.assertEquals(CRAWLER_ID, crawlerSeedUrlEntity.getCrawler().getId());
    }

}