package cz.lp.repository;

import cz.lp.domain.ProcessorCrawlerResultEntity;
import org.springframework.data.repository.CrudRepository;

public interface ProcessorCrawlerResultRepository extends CrudRepository<ProcessorCrawlerResultEntity, ProcessorCrawlerResultEntity.ProcessorCrawlerResultId> {

}
