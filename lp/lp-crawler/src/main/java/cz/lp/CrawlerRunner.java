package cz.lp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 * Main class for scheduling and executing crawlers.
 */

@ComponentScan
public class CrawlerRunner {

    private static final Logger log = LoggerFactory.getLogger(CrawlerRunner.class);

    public static void main(String[] args) {

        log.info("Starting Crawler Runner...");
        new AnnotationConfigApplicationContext(CrawlerRunner.class);
    }
}
