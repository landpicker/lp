import { Injectable } from '@angular/core';
import { HttpClient } from '../../../../node_modules/@angular/common/http';
import { Area } from './area.interface';
import { Observable } from '../../../../node_modules/rxjs';

@Injectable({
	providedIn: 'root'
})
export class AreaFilterService {
	constructor(private http: HttpClient) {}

	getAreaTree(): Observable<Area[]> {
		return this.http.get<Area[]>('api/areas/tree');
	}
}
