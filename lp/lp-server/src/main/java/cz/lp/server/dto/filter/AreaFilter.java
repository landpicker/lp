package cz.lp.server.dto.filter;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;

import java.util.Set;

/**
 * Area filter data transfer object provides information about required areas of searched lands.
 */
public class AreaFilter {

    private final Set<String> codes;

    /**
     * Creates instance of area filter.
     *
     * @param codes area codes
     */
    @JsonCreator
    public AreaFilter(@JsonProperty("codes") Set<String> codes) {
        this.codes = codes;
    }

    /**
     * Gets area codes.
     *
     * @return area codes
     */
    public Set<String> getCodes() {
        return codes;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("codes", codes)
                .omitNullValues()
                .toString();
    }
}
