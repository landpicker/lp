package cz.lp.processor.custom.landbase;

import cz.lp.common.dto.Epsg5514Coordinate;
import cz.lp.common.enumerate.LandType;

import java.util.List;

public class Land {

    private String id;
    private LandType type;
    private Integer trunkNumber;
    private Integer underDivisionNumber;
    private int acreage;
    private Epsg5514Coordinate definitionPointEpsg5514;
    private String cadastralTerritoryAreaCode;
    private List<Epsg5514Coordinate> borderEpsg5514Coordinates;

    /**
     * Creates instance of land.
     *
     * @param id                         land unique identifier
     * @param type                       type
     * @param trunkNumber                trunk number
     * @param underDivisionNumber        under division number
     * @param acreage                    acreage
     * @param definitionPointEpsg5514    definition point EPSG5514
     * @param cadastralTerritoryAreaCode cadastral territory area code
     * @param borderEpsg5514Coordinates  border coordinates EPSG5514
     */
    public Land(String id, LandType type, Integer trunkNumber, Integer underDivisionNumber, int acreage, Epsg5514Coordinate definitionPointEpsg5514, String cadastralTerritoryAreaCode, List<Epsg5514Coordinate> borderEpsg5514Coordinates) {
        this.id = id;
        this.type = type;
        this.trunkNumber = trunkNumber;
        this.underDivisionNumber = underDivisionNumber;
        this.acreage = acreage;
        this.definitionPointEpsg5514 = definitionPointEpsg5514;
        this.cadastralTerritoryAreaCode = cadastralTerritoryAreaCode;
        this.borderEpsg5514Coordinates = borderEpsg5514Coordinates;
    }

    /**
     * Gets land unique identifier.
     *
     * @return land unique identifier
     */
    public String getId() {
        return id;
    }

    /**
     * Gets land type.
     *
     * @return land type
     */
    public LandType getType() {
        return type;
    }

    /**
     * Gets trunk number.
     *
     * @return trunk number
     */
    public Integer getTrunkNumber() {
        return trunkNumber;
    }

    /**
     * Gets under division number.
     *
     * @return under division number
     */
    public Integer getUnderDivisionNumber() {
        return underDivisionNumber;
    }

    /**
     * Gets acreage.
     *
     * @return acreage
     */
    public int getAcreage() {
        return acreage;
    }

    /**
     * Gets definition point EPSG5514.
     *
     * @return definition point EPSG5514
     */
    public Epsg5514Coordinate getDefinitionPointEpsg5514() {
        return definitionPointEpsg5514;
    }

    /**
     * Gets cadastral territory area code.
     *
     * @return cadastral territory area code
     */
    public String getCadastralTerritoryAreaCode() {
        return cadastralTerritoryAreaCode;
    }

    /**
     * Gets border ESG5514 coordinates.
     *
     * @return border ESG5514 coordinates
     */
    public List<Epsg5514Coordinate> getBorderEpsg5514Coordinates() {
        return borderEpsg5514Coordinates;
    }
}
