package cz.lp.processor.custom.areabase.adapter;

import cz.lp.common.enumerate.AreaType;
import cz.lp.processor.custom.areabase.Area;
import cz.lp.processor.custom.areabase.filter.AreaBaseProcessorFilter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

public class AreaBaseRegionAdapter {

    private final AreaBaseProcessorFilter filter;

    private static final String REGION_ELEMENT_SELECTOR = "Vusc";
    private static final String CODE_ELEMENT_SELECTOR = "Kod";
    private static final String NAME_ELEMENT_SELECTOR = "Nazev";

    private List<Area> regions;

    public AreaBaseRegionAdapter(String content, String url, AreaBaseProcessorFilter filter) {

        this.filter = filter;
        Document document = Jsoup.parse(content, url, Parser.xmlParser());
        extractRegions(document);
    }

    public void extractRegions(Document document) {

        regions = document.select(REGION_ELEMENT_SELECTOR).stream()
                .map(element -> {

                    Element codeElement = element.select(CODE_ELEMENT_SELECTOR).first();
                    Element nameElement = element.select(NAME_ELEMENT_SELECTOR).first();
                    if (isNull(codeElement) || isNull(nameElement)) {
                        return null;
                    }
                    return new Area(codeElement.text(), AreaType.REGION, nameElement.text(),null);
                })
                .filter(Objects::nonNull)
                .filter(area -> filter.isRegionAreaAllowed(area.getCode()))
                .collect(Collectors.toList());
    }

    /**
     * Gets regions.
     *
     * @return regions
     */
    public Optional<List<Area>> getRegions() {

        if (isNull(regions) || regions.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(regions);
    }
}
