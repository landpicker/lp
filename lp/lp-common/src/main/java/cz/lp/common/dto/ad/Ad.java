package cz.lp.common.dto.ad;

import cz.lp.common.domain.ad.AdEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
public class Ad {

    private final String url;
    private final String source;
    private final String title;
    private final String content;
    private final LocalDate publishedOn;
    private final List<AdImage> images;

    public static Ad from(AdEntity adEntity) {
        return new Ad(adEntity.getUrl(), adEntity.getSource(), adEntity.getTitle(), adEntity.getContent(), adEntity.getPublishedOn(),
                adEntity.getImages().stream().map(AdImage::from).collect(Collectors.toList())
        );
    }
}
