package cz.lp.common.domain.ad;

import lombok.*;

import javax.persistence.*;

/**
 * Ad image entity represents image of advertisement in database table.
 */

@Entity
@Table(name = "ad_image")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AdImageEntity {

    @Id
    @Column(name = "url", unique = true, nullable = false)
    private String url;

    @Column(name = "file_name", nullable = false)
    private String fileName;

    @Column(name = "content_type", nullable = false)
    private String contentType;

    @Column(name = "data", nullable = false)
    private byte[] data;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "ad_id")
    private AdEntity ad;
}
