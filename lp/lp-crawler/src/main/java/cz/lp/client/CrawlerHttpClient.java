package cz.lp.client;

import com.google.common.net.HttpHeaders;
import cz.lp.exception.CrawlerHttpClientCommunicationException;
import cz.lp.exception.CrawlerHttpClientParameterException;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Objects;

import static java.lang.String.format;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;


/**
 * CrawlerEntity HTTP client provides methods for sending HTTP requests (GET and POST)
 */

public class CrawlerHttpClient {

    private static final Logger log = LoggerFactory.getLogger(CrawlerHttpClient.class);

    public static final int DEFAULT_READ_TIMEOUT = 30000;
    public static final String DEFAULT_USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36";
    public static final String DEFAULT_ACCEPT_LANGUAGE = "cs-CZ,cs;q=0.9";

    private int readTimeout;
    private String userAgent;
    private String acceptLanguage;
    private String referer;

    /**
     * Creates instance of crawler HTTP client
     *
     * @param readTimeout    read timeout
     * @param userAgent      user agent
     * @param acceptLanguage accept language
     * @param referer        referer
     */
    private CrawlerHttpClient(int readTimeout, String userAgent, String acceptLanguage, String referer) {

        this.readTimeout = (readTimeout != -1) ? readTimeout : DEFAULT_READ_TIMEOUT;
        this.userAgent = (!isNull(userAgent)) ? userAgent : DEFAULT_USER_AGENT;
        this.acceptLanguage = (!isNull(acceptLanguage)) ? acceptLanguage : DEFAULT_ACCEPT_LANGUAGE;
        this.referer = referer;
    }

    /**
     * Creates instance of crawler HTTP client builder.
     */
    public static final class Builder {

        private Integer readTimeout = -1;
        private String userAgent;
        private String acceptLanguage;
        private String referer;

        /**
         * Sets read timeout.
         *
         * @param readTimeout read timeout
         * @return builder
         */
        public Builder readTimeout(int readTimeout) {
            if (readTimeout <= 0) {
                throw new NullPointerException("Read timeout can not be less than 0");
            }
            this.readTimeout = readTimeout;
            return this;
        }

        /**
         * Sets user agent.
         *
         * @param userAgent user agent
         * @return builder
         */
        public Builder userAgent(String userAgent) {
            this.userAgent = Objects.requireNonNull(userAgent);
            return this;
        }

        /**
         * Sets accept language.
         *
         * @param acceptLanguage accept language
         * @return builder
         */
        public Builder acceptLanguage(String acceptLanguage) {
            this.acceptLanguage = Objects.requireNonNull(acceptLanguage);
            return this;
        }

        /**
         * Sets referer.
         *
         * @param referer referer
         * @return builder
         */
        public Builder referer(String referer) {
            this.referer = Objects.requireNonNull(referer);
            return this;
        }

        /**
         * Builds crawler HTTP client from builder.
         *
         * @return crawler HTTP client
         */
        public CrawlerHttpClient build() {

            return new CrawlerHttpClient(readTimeout, userAgent, acceptLanguage, referer);
        }
    }

    /**
     * Gets builder.
     *
     * @return builder
     */
    public static Builder builder() {

        return new Builder();
    }

    /**
     * Executes HTTP request.
     *
     * @param method            HTTP method (GET or POST)
     * @param url               URL
     * @param additionalHeaders additional headers
     * @param formParams        POST form parameters
     * @return crawler HTTP response
     * @throws CrawlerHttpClientCommunicationException when something during executing request failed
     */
    public CrawlerHttpResponse executeRequest(String method, String url, Map<String, String> additionalHeaders, Map<String, String> formParams) throws CrawlerHttpClientParameterException, CrawlerHttpClientCommunicationException {

        if (!method.equalsIgnoreCase(HttpGet.METHOD_NAME) && !method.equalsIgnoreCase(HttpPost.METHOD_NAME)) {
            final String msg = String.format("Invalid method: %s", method);
            log.error(msg);
            throw new CrawlerHttpClientParameterException(msg);
        }

        if (isBlank(url)) {
            final String msg = "URL can not be null or empty";
            log.error(msg);
            throw new CrawlerHttpClientParameterException(msg);

        }

        URL malformedUrl = getUrl(url);
        HttpURLConnection connection = openConnection(malformedUrl);

        if (!isNull(additionalHeaders)) {
            additionalHeaders.entrySet().stream().forEach(additionalHeader -> connection.setRequestProperty(additionalHeader.getKey(), additionalHeader.getValue()));
        }
        try {
            connection.setRequestMethod(method);
        } catch (ProtocolException ex) {
            closeConnection(connection);
            throw new CrawlerHttpClientCommunicationException("Failed to set request method GET", ex);
        }

        if (HttpPost.METHOD_NAME.equals(method)) {

            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("charset", StandardCharsets.UTF_8.name());
            connection.setUseCaches(false);

            if (nonNull(formParams) && !formParams.isEmpty()) {
                byte[] postData = getPostData(formParams);
                connection.setDoOutput(true);
                connection.setInstanceFollowRedirects(true);
                connection.setRequestProperty("Content-Length", Integer.toString(postData.length));
                writePostData(connection, postData);
            }
        }

        int status;
        try {
            status = connection.getResponseCode();
        } catch (IOException ex) {
            closeConnection(connection);
            throw new CrawlerHttpClientCommunicationException("Unable to get a response code", ex);
        }

        if (status == HttpStatus.SC_OK || status == HttpStatus.SC_NOT_MODIFIED) {
            return getResponse(connection);
        }

        if (status == HttpStatus.SC_TEMPORARY_REDIRECT
                || status == HttpStatus.SC_MOVED_PERMANENTLY
                || status == HttpStatus.SC_MOVED_TEMPORARILY
                || status == HttpStatus.SC_SEE_OTHER) {

            String redirectToUrl = connection.getHeaderField(HttpHeaders.LOCATION);
            return executeRequest(method, redirectToUrl, additionalHeaders, formParams);
        }

        throw new CrawlerHttpClientCommunicationException(format("Could not get an OK response from %s", url), status);
    }

    /**
     * Gets malformed URL.
     *
     * @param url URL
     * @return malformed URL
     * @throws CrawlerHttpClientCommunicationException when creation of malformed URL failed
     */
    private URL getUrl(String url) throws CrawlerHttpClientCommunicationException {

        try {
            return new URL(url);
        } catch (MalformedURLException ex) {
            final String msg = format("Failed to create URL from: %s", url);
            log.error(msg, ex);
            throw new CrawlerHttpClientCommunicationException(msg);
        }
    }

    /**
     * Opens HTTP URL connection.
     *
     * @param url URL
     * @return HTTP URL connection
     * @throws CrawlerHttpClientCommunicationException when open connection failed
     */
    private HttpURLConnection openConnection(URL url) throws CrawlerHttpClientCommunicationException {

        URLConnection connection;
        try {
            connection = url.openConnection();
        } catch (IOException ex) {
            final String msg = String.format("Failed to open HTTP URL connection: %s", url.toString());
            log.error(msg, ex);
            throw new CrawlerHttpClientCommunicationException(msg, ex);
        }

        connection.setReadTimeout(readTimeout);
        connection.addRequestProperty(HttpHeaders.ACCEPT_LANGUAGE, acceptLanguage);
        connection.addRequestProperty(HttpHeaders.USER_AGENT, userAgent);

        if (isNotBlank(referer)) {
            connection.addRequestProperty(HttpHeaders.REFERER, referer);
        }

        return (HttpURLConnection) connection;
    }

    /**
     * Gets POST data.
     *
     * @param formParams form parameters
     * @return post data as bytes
     * @throws CrawlerHttpClientCommunicationException when defined encoding is not supported
     */
    private byte[] getPostData(Map<String, String> formParams) throws CrawlerHttpClientCommunicationException {

        StringBuilder postData = new StringBuilder();
        try {
            for (Map.Entry<String, String> param : formParams.entrySet()) {

                if (postData.length() != 0) postData.append('&');
                postData.append(URLEncoder.encode(param.getKey(), StandardCharsets.UTF_8.name()));
                postData.append('=');
                postData.append(URLEncoder.encode(param.getValue(), StandardCharsets.UTF_8.name()));
            }
            return postData.toString().getBytes(StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException ex) {
            final String msg = "Unable to get POST data";
            log.error(msg);
            throw new CrawlerHttpClientCommunicationException(msg, ex);
        }
    }

    /**
     * Writes POST data to connection.
     *
     * @param connection HTTP URL connection
     * @param postData   POST data
     * @throws CrawlerHttpClientCommunicationException when writing POST data to connection failed
     */
    private void writePostData(HttpURLConnection connection, byte[] postData) throws CrawlerHttpClientCommunicationException {

        try {
            try (DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream())) {
                outputStream.write(postData);
            }
        } catch (IOException ex) {
            final String msg = String.format("Failed to write POST data for URL: %s", connection.getURL().toString());
            log.error(msg);
            throw new CrawlerHttpClientCommunicationException(msg, ex);
        }
    }


    /**
     * Gets response from HTTP URL connection.
     *
     * @param connection HTTP URL connection
     * @return crawler HTTP response
     * @throws CrawlerHttpClientCommunicationException when reading response failed or response is empty
     */
    private CrawlerHttpResponse getResponse(HttpURLConnection connection) throws CrawlerHttpClientCommunicationException {

        try {
            byte[] responseBytes = IOUtils.toByteArray(connection.getInputStream());

            if (responseBytes.length == 0) {
                final String msg = String.format("Response is empty for URL: %s", connection.getURL().toString());
                throw new CrawlerHttpClientCommunicationException(msg);
            }

            return CrawlerHttpResponse.of(connection.getResponseCode(), responseBytes, connection.getContentType());
        } catch (IOException ex) {
            final String msg = String.format("Unable get response from: %s", connection.getURL().toString());
            throw new CrawlerHttpClientCommunicationException(msg, ex);
        } finally {
            closeConnection(connection);
        }

        /*try (BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
            String inputLine;
            StringBuilder content = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }

            if (isBlank(content.toString())) {
                final String msg = String.format("Response is empty for URL: %s", connection.getURL().toString());
                throw new CrawlerHttpClientCommunicationException(msg);
            }

            return CrawlerHttpResponse.of(connection.getResponseCode(), content.toString(), connection.getContentType());

        } catch (IOException ex) {
            final String msg = String.format("Unable get response from: %s", connection.getURL().toString());
            throw new CrawlerHttpClientCommunicationException(msg, ex);

        } finally {
            closeConnection(connection);
        }*/
    }

    /**
     * Closes HTTP URL connection
     *
     * @param connection connection
     */
    private void closeConnection(HttpURLConnection connection) {

        if (!isNull(connection)) {
            connection.disconnect();
        }
    }
}
