package cz.lp.common.repository;

import com.google.common.collect.Lists;
import cz.lp.common.domain.AreaBorderEntity;
import cz.lp.common.domain.AreaEntity;
import cz.lp.common.dto.Epsg5514Coordinate;
import cz.lp.common.dto.Wgs84Coordinate;
import cz.lp.common.repository.configuration.RepositoryTemplate;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import java.util.List;

public class AreaBorderRepositoryIT extends RepositoryTemplate {


    @Autowired
    private AreaRepository areaRepository;

    @Autowired
    private AreaBorderRepository areaBorderRepository;

    private static final Long ID = 1l;
    private static final List<Epsg5514Coordinate> EPSG_5514_COORDINATES = Lists.newArrayList(Epsg5514Coordinate.of(-510752.25f, -1141672.44f), Epsg5514Coordinate.of(-410752.25f, -2141672.44f));
    private static final List<Wgs84Coordinate> WGS84_COORDINATES = Lists.newArrayList(Wgs84Coordinate.of(17.6518000f, 49.2232000f),Wgs84Coordinate.of(18.6518000f, 48.2232000f));


    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/area/INSERT_area_WITH_code_=_1_AND_parent_area_code_=_NULL_REGION.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/area/INSERT_area_WITH_code_=_4_AND_parent_area_code_=_1_DISTRICT.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/area/INSERT_area_WITH_code_=_6_AND_parent_area_code_=_4_CITY.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void insert() {

        AreaEntity areaEntity = areaRepository.findOne("4");
        AreaBorderEntity areaBorderEntity = new AreaBorderEntity(EPSG_5514_COORDINATES, WGS84_COORDINATES, areaEntity);
        areaBorderRepository.save(areaBorderEntity);

        AreaBorderEntity savedAreaBorderEntity = areaBorderRepository.findOne(areaBorderEntity.getId());
        Assert.assertNotNull(savedAreaBorderEntity);
    }

}