package cz.lp.resolver;

import com.google.common.collect.Lists;
import cz.lp.crawler.Crawler;
import cz.lp.crawler.CrawlerFilterContext;
import cz.lp.crawler.custom.vdparea.VdpAreaCrawler;
import cz.lp.crawler.resolver.CrawlerClassResolver;
import cz.lp.domain.*;
import cz.lp.enumerate.CrawlerCondition;
import cz.lp.enumerate.RecrawlStrategy;
import cz.lp.exception.CrawlerClassException;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.sql.Timestamp;
import java.util.List;

public class CrawlerClassResolverTest {

    private static final Long CRAWLER_ID = new Long(1);
    private static final String CRAWLER_VALID_CLASS = "cz.lp.crawler.custom.vdparea.VdpAreaCrawler";
    private static final String CRAWLER_INVALID_CLASS = "cz.lp.crawler.custom.vdparea.InvalidCrawler";
    private static final CrawlerCondition CRAWLER_CONDITION = CrawlerCondition.NONE;
    private static final Integer CRAWLER_DELAY = 1;
    private static final Boolean CRAWLER_ENABLED = Boolean.TRUE;
    private static final RecrawlStrategy CRAWLER_RECRAWL_STRATEGY = RecrawlStrategy.RECRAWL_ALL;
    private static final String CRAWLER_RECRAWL_URL_PATTERN = ".*";
    private static final Long CRAWLER_RECRAWL_INTERVAL = new Long(1000);
    private static final Timestamp CRAWLER_RECRAWL_ON = Mockito.mock(Timestamp.class);
    private static final Timestamp CRAWLER_LAST_RECRAWLED_ON = Mockito.mock(Timestamp.class);
    private static final List<CrawlerSeedUrlEntity> CRAWLER_SEED_URLS = Lists.newArrayList(Mockito.mock(CrawlerSeedUrlEntity.class));
    private static final List<CrawlerResultEntity> CRAWLER_RESULTS = Lists.newArrayList(Mockito.mock(CrawlerResultEntity.class));
    private static final List<ProcessorEntity> CRAWLER_PROCESSORS = Lists.newArrayList(Mockito.mock(ProcessorEntity.class));
    private static final SourceEntity SOURCE = Mockito.mock(SourceEntity.class);

    @Test
    public void createCrawlerInstance_whenClassNameIsOk_shouldReturnNewInstance() throws Exception {

        CrawlerEntity crawlerEntity = new CrawlerEntity(CRAWLER_ID, SOURCE, CRAWLER_VALID_CLASS, CRAWLER_CONDITION, CRAWLER_DELAY, CRAWLER_ENABLED, CRAWLER_RECRAWL_STRATEGY, CRAWLER_RECRAWL_URL_PATTERN, CRAWLER_RECRAWL_INTERVAL, CRAWLER_RECRAWL_ON, CRAWLER_LAST_RECRAWLED_ON, CRAWLER_SEED_URLS, CRAWLER_RESULTS, CRAWLER_PROCESSORS);
        CrawlerFilterContext crawlerFilterContext = Mockito.mock(CrawlerFilterContext.class);
        Mockito.when(crawlerFilterContext.getFilter(Mockito.anyString())).thenReturn(null);
        Crawler crawler = CrawlerClassResolver.createCrawlerInstance(crawlerEntity, null, null, crawlerFilterContext);
        Assert.assertNotNull(crawler);
        Assert.assertTrue(crawler instanceof VdpAreaCrawler);
    }

    @Test(expected = CrawlerClassException.class)
    public void createCrawlerInstance_whenClassNameIsInvalid_shouldThrowException() throws Exception {

        CrawlerEntity crawlerEntity = new CrawlerEntity(CRAWLER_ID, SOURCE, CRAWLER_INVALID_CLASS, CRAWLER_CONDITION, CRAWLER_DELAY, CRAWLER_ENABLED, CRAWLER_RECRAWL_STRATEGY, CRAWLER_RECRAWL_URL_PATTERN, CRAWLER_RECRAWL_INTERVAL, CRAWLER_RECRAWL_ON, CRAWLER_LAST_RECRAWLED_ON, CRAWLER_SEED_URLS, CRAWLER_RESULTS, CRAWLER_PROCESSORS);
        CrawlerClassResolver.createCrawlerInstance(crawlerEntity, null, null, null);
    }
}