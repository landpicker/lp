package cz.lp.domain;

import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;

public class SourceEntityTest {

    private static final Long ID = 1l;
    private static final String PROVIDER = "CUZK";
    private static final String WEBSITE_URL = "http://czuk.cz";
    private static final List<CrawlerEntity> CRAWLERS = Lists.newArrayList(Mockito.mock(CrawlerEntity.class));

    @Test
    public void constructor_empty() throws Exception {

        SourceEntity source = new SourceEntity();
        Assert.assertNotNull(source);
        Assert.assertNull(source.getId());
        Assert.assertNull(source.getProvider());
        Assert.assertNull(source.getWebsiteUrl());
        Assert.assertNull(source.getCrawlers());
    }

    @Test
    public void constructor() throws Exception {

        SourceEntity source = new SourceEntity(ID, PROVIDER, WEBSITE_URL, CRAWLERS);
        Assert.assertNotNull(source);
        Assert.assertEquals(ID, source.getId());
        Assert.assertEquals(PROVIDER, source.getProvider());
        Assert.assertEquals(WEBSITE_URL, source.getWebsiteUrl());
        Assert.assertEquals(CRAWLERS, source.getCrawlers());
    }

    @Test
    public void getId() throws Exception {

        SourceEntity source = new SourceEntity(ID, PROVIDER, WEBSITE_URL, CRAWLERS);
        Assert.assertNotNull(source);
        Assert.assertEquals(ID, source.getId());
    }

    @Test
    public void getProvider() throws Exception {

        SourceEntity source = new SourceEntity(ID, PROVIDER, WEBSITE_URL, CRAWLERS);
        Assert.assertNotNull(source);
        Assert.assertEquals(PROVIDER, source.getProvider());
    }

    @Test
    public void getWebsiteUrl() throws Exception {

        SourceEntity source = new SourceEntity(ID, PROVIDER, WEBSITE_URL, CRAWLERS);
        Assert.assertNotNull(source);
        Assert.assertEquals(WEBSITE_URL, source.getWebsiteUrl());
    }

    @Test
    public void getCrawlers() throws Exception {

        SourceEntity source = new SourceEntity(ID, PROVIDER, WEBSITE_URL, CRAWLERS);
        Assert.assertNotNull(source);
        Assert.assertEquals(ID, source.getId());
        Assert.assertEquals(CRAWLERS, source.getCrawlers());
    }
}