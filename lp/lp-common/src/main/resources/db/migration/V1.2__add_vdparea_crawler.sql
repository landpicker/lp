INSERT INTO public.source (id, provider, website_url) VALUES (1, 'ČÚZK', 'http://vdp.cuzk.cz/vdp/ruian/vusc/vyhledej');

INSERT INTO public.crawler (id, source_id, crawler_class, condition, delay, enabled, recrawl_strategy, recrawl_interval, recrawl_on, last_recrawled_on)
VALUES (1, 1, 'cz.lp.crawler.custom.vdparea.VdpAreaCrawler', 'NONE', 200, false, 'RECRAWL_ALL', 60, null, null);

--INSERT INTO public.crawler_seed_url (crawler_id, url, enabled, condition) VALUES (1, 'http://vdp.cuzk.cz/vdp/ruian/okresy/export?vc.kod=19&kr.kod=&ok.nazev=&ok.kod=&ohrada.id=&okg.sort=KOD&export=XML', false, 'NONE');
--INSERT INTO public.crawler_seed_url (crawler_id, url, enabled, condition) VALUES (1, 'http://vdp.cuzk.cz/vdp/ruian/okresy/export?vc.kod=27&kr.kod=&ok.nazev=&ok.kod=&ohrada.id=&okg.sort=KOD&export=XML', false, 'NONE');
--INSERT INTO public.crawler_seed_url (crawler_id, url, enabled, condition) VALUES (1, 'http://vdp.cuzk.cz/vdp/ruian/okresy/export?vc.kod=35&kr.kod=&ok.nazev=&ok.kod=&ohrada.id=&okg.sort=KOD&export=XML', false, 'NONE');
--INSERT INTO public.crawler_seed_url (crawler_id, url, enabled, condition) VALUES (1, 'http://vdp.cuzk.cz/vdp/ruian/okresy/export?vc.kod=43&kr.kod=&ok.nazev=&ok.kod=&ohrada.id=&okg.sort=KOD&export=XML', false, 'NONE');
--INSERT INTO public.crawler_seed_url (crawler_id, url, enabled, condition) VALUES (1, 'http://vdp.cuzk.cz/vdp/ruian/okresy/export?vc.kod=51&kr.kod=&ok.nazev=&ok.kod=&ohrada.id=&okg.sort=KOD&export=XML', false, 'NONE');
--INSERT INTO public.crawler_seed_url (crawler_id, url, enabled, condition) VALUES (1, 'http://vdp.cuzk.cz/vdp/ruian/okresy/export?vc.kod=60&kr.kod=&ok.nazev=&ok.kod=&ohrada.id=&okg.sort=KOD&export=XML', false, 'NONE');
--INSERT INTO public.crawler_seed_url (crawler_id, url, enabled, condition) VALUES (1, 'http://vdp.cuzk.cz/vdp/ruian/okresy/export?vc.kod=78&kr.kod=&ok.nazev=&ok.kod=&ohrada.id=&okg.sort=KOD&export=XML', false, 'NONE');
--INSERT INTO public.crawler_seed_url (crawler_id, url, enabled, condition) VALUES (1, 'http://vdp.cuzk.cz/vdp/ruian/okresy/export?vc.kod=86&kr.kod=&ok.nazev=&ok.kod=&ohrada.id=&okg.sort=KOD&export=XML', false, 'NONE');
--INSERT INTO public.crawler_seed_url (crawler_id, url, enabled, condition) VALUES (1, 'http://vdp.cuzk.cz/vdp/ruian/okresy/export?vc.kod=94&kr.kod=&ok.nazev=&ok.kod=&ohrada.id=&okg.sort=KOD&export=XML', false, 'NONE');
--INSERT INTO public.crawler_seed_url (crawler_id, url, enabled, condition) VALUES (1, 'http://vdp.cuzk.cz/vdp/ruian/okresy/export?vc.kod=108&kr.kod=&ok.nazev=&ok.kod=&ohrada.id=&okg.sort=KOD&export=XML', false, 'NONE');
--INSERT INTO public.crawler_seed_url (crawler_id, url, enabled, condition) VALUES (1, 'http://vdp.cuzk.cz/vdp/ruian/okresy/export?vc.kod=116&kr.kod=&ok.nazev=&ok.kod=&ohrada.id=&okg.sort=KOD&export=XML', false, 'NONE');
--INSERT INTO public.crawler_seed_url (crawler_id, url, enabled, condition) VALUES (1, 'http://vdp.cuzk.cz/vdp/ruian/okresy/export?vc.kod=124&kr.kod=&ok.nazev=&ok.kod=&ohrada.id=&okg.sort=KOD&export=XML', true, 'NONE');
--INSERT INTO public.crawler_seed_url (crawler_id, url, enabled, condition) VALUES (1, 'http://vdp.cuzk.cz/vdp/ruian/okresy/export?vc.kod=132&kr.kod=&ok.nazev=&ok.kod=&ohrada.id=&okg.sort=KOD&export=XML', false, 'NONE');
--INSERT INTO public.crawler_seed_url (crawler_id, url, enabled, condition) VALUES (1, 'http://vdp.cuzk.cz/vdp/ruian/okresy/export?vc.kod=141&kr.kod=&ok.nazev=&ok.kod=&ohrada.id=&okg.sort=KOD&export=XML', false, 'NONE');
INSERT INTO public.crawler_seed_url (crawler_id, url, enabled, condition) VALUES (1, 'https://vdp.cuzk.cz/vdp/ruian/vusc/export?rs.kod=&vc.nazev=&vc.kod=&ohrada.id=&vcg.sort=NAZEV&export=XML', true, 'NONE');

