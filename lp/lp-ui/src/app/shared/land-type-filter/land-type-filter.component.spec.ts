import { async, ComponentFixture, TestBed, flush } from '@angular/core/testing';

import { LandTypeFilterComponent, createLandTypeFilterFormGroup } from './land-type-filter.component';
import { TranslateModule } from '../../../../node_modules/@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '../../../../node_modules/@angular/forms';
import { MatFormFieldModule, MatButtonModule, MatCheckboxModule } from '../../../../node_modules/@angular/material';
import { LandType } from './land-type.interface';
import { By } from '../../../../node_modules/@angular/platform-browser';

describe('LandTypeFilterComponent', () => {
	let component: LandTypeFilterComponent;
	let fixture: ComponentFixture<LandTypeFilterComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [TranslateModule.forRoot(), FormsModule, ReactiveFormsModule, MatFormFieldModule, MatButtonModule, MatCheckboxModule],
			declarations: [LandTypeFilterComponent]
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(LandTypeFilterComponent);
		component = fixture.componentInstance;
	});

	it('init without form group as input - should create instance', () => {
		expect(component).toBeTruthy();
		expect(component.landTypeForm).toBeUndefined();
	});

	it('init with form group as input - should create instance', () => {
		component.landTypeForm = createLandTypeFilterFormGroup([LandType.ARABLE_LAND, LandType.ORCHARD, LandType.VINEYARD]);

		expect(component).toBeTruthy();
		expect(component.landTypeForm).toBeTruthy();
		expect(component.landTypeForm.get('typeArableLand').value.key).toBe(LandType.ARABLE_LAND);
		expect(component.landTypeForm.get('typeArableLand').value.checked).toBeTruthy();
		expect(component.landTypeForm.get('typeHopper').value.key).toBe(LandType.HOPPER);
		expect(component.landTypeForm.get('typeHopper').value.checked).toBeFalsy();
		expect(component.landTypeForm.get('typeVineyard').value.key).toBe(LandType.VINEYARD);
		expect(component.landTypeForm.get('typeVineyard').value.checked).toBeTruthy();
		expect(component.landTypeForm.get('typeGarden').value.key).toBe(LandType.GARDEN);
		expect(component.landTypeForm.get('typeGarden').value.checked).toBeFalsy();
		expect(component.landTypeForm.get('typeOrchard').value.key).toBe(LandType.ORCHARD);
		expect(component.landTypeForm.get('typeOrchard').value.checked).toBeTruthy();
		expect(component.landTypeForm.get('typePermanentGrassland').value.key).toBe(LandType.PERMANENT_GRASSLAND);
		expect(component.landTypeForm.get('typePermanentGrassland').value.checked).toBeFalsy();
		expect(component.landTypeForm.get('typeForestLand').value.key).toBe(LandType.FOREST_LAND);
		expect(component.landTypeForm.get('typeForestLand').value.checked).toBeFalsy();
		expect(component.landTypeForm.get('typeWaterArea').value.key).toBe(LandType.WATER_AREA);
		expect(component.landTypeForm.get('typeWaterArea').value.checked).toBeFalsy();
		expect(component.landTypeForm.get('typeBuiltAreaAndCourtyard').value.key).toBe(LandType.BUILT_AREA_AND_COURTYARD);
		expect(component.landTypeForm.get('typeBuiltAreaAndCourtyard').value.checked).toBeFalsy();
		expect(component.landTypeForm.get('typeOtherAreas').value.key).toBe(LandType.OTHER_AREAS);
		expect(component.landTypeForm.get('typeOtherAreas').value.checked).toBeFalsy();
	});

	it('check first type - should add type', () => {
		component.landTypeForm = createLandTypeFilterFormGroup([]);
		let checkBoxInputElement = fixture.debugElement.query(By.css('mat-checkbox:first-child input'));

		expect(component).toBeTruthy();
		expect(component.landTypeForm).toBeTruthy();
		expect(component.landTypeForm.get('typeArableLand').value.key).toBe(LandType.ARABLE_LAND);
		expect(component.landTypeForm.get('typeArableLand').value.checked).toBeFalsy();
		expect(component.landTypeForm.invalid).toBeTruthy();
		expect(checkBoxInputElement.nativeElement.checked).toBeFalsy();

		const spyOnChange = spyOn(component, 'onChange').and.callThrough();
		checkBoxInputElement.nativeElement.click();

		expect(spyOnChange).toHaveBeenCalled();
		expect(component.landTypeForm.get('typeArableLand').value.key).toBe(LandType.ARABLE_LAND);
		expect(component.landTypeForm.get('typeArableLand').value.checked).toBeTruthy();
		expect(component.landTypeForm.valid).toBeTruthy();
		expect(checkBoxInputElement.nativeElement.checked).toBeTruthy();
	});

	it('uncheck before last type - should remove type', () => {
		component.landTypeForm = createLandTypeFilterFormGroup([LandType.ARABLE_LAND, LandType.BUILT_AREA_AND_COURTYARD]);
		let checkBoxInputElement = fixture.debugElement.query(By.css('mat-checkbox:first-child input'));
		fixture.detectChanges();

		expect(component).toBeTruthy();
		expect(component.landTypeForm).toBeTruthy();
		expect(component.landTypeForm.get('typeArableLand').value.key).toBe(LandType.ARABLE_LAND);
		expect(component.landTypeForm.get('typeArableLand').value.checked).toBeTruthy();
		expect(component.landTypeForm.valid).toBeTruthy();
		expect(checkBoxInputElement.nativeElement.checked).toBeTruthy();

		const spyOnChange = spyOn(component, 'onChange').and.callThrough();
		checkBoxInputElement.nativeElement.click();

		expect(spyOnChange).toHaveBeenCalled();
		expect(component.landTypeForm.get('typeArableLand').value.key).toBe(LandType.ARABLE_LAND);
		expect(component.landTypeForm.get('typeArableLand').value.checked).toBeFalsy();
		expect(component.landTypeForm.valid).toBeTruthy();
		expect(checkBoxInputElement.nativeElement.checked).toBeFalsy();
	});

	it('uncheck last type - should remove type and return invalid error', () => {
		component.landTypeForm = createLandTypeFilterFormGroup([LandType.ARABLE_LAND]);
		let checkBoxInputElement = fixture.debugElement.query(By.css('mat-checkbox:first-child input'));
		fixture.detectChanges();

		expect(component).toBeTruthy();
		expect(component.landTypeForm).toBeTruthy();
		expect(component.landTypeForm.get('typeArableLand').value.key).toBe(LandType.ARABLE_LAND);
		expect(component.landTypeForm.get('typeArableLand').value.checked).toBeTruthy();
		expect(component.landTypeForm.valid).toBeTruthy();
		expect(checkBoxInputElement.nativeElement.checked).toBeTruthy();

		const spyOnChange = spyOn(component, 'onChange').and.callThrough();
		checkBoxInputElement.nativeElement.click();

		expect(spyOnChange).toHaveBeenCalled();
		expect(component.landTypeForm.get('typeArableLand').value.key).toBe(LandType.ARABLE_LAND);
		expect(component.landTypeForm.get('typeArableLand').value.checked).toBeFalsy();
		expect(component.landTypeForm.invalid).toBeTruthy();
		expect(checkBoxInputElement.nativeElement.checked).toBeFalsy();
	});

	it('check all - should select all types into form', () => {
		component.landTypeForm = createLandTypeFilterFormGroup([]);

		expect(component).toBeTruthy();
		expect(component.landTypeForm).toBeTruthy();
		expect(component.landTypeForm.get('typeArableLand').value.key).toBe(LandType.ARABLE_LAND);
		expect(component.landTypeForm.get('typeArableLand').value.checked).toBeFalsy();
		expect(component.landTypeForm.get('typeHopper').value.key).toBe(LandType.HOPPER);
		expect(component.landTypeForm.get('typeHopper').value.checked).toBeFalsy();
		expect(component.landTypeForm.get('typeVineyard').value.key).toBe(LandType.VINEYARD);
		expect(component.landTypeForm.get('typeVineyard').value.checked).toBeFalsy();
		expect(component.landTypeForm.get('typeGarden').value.key).toBe(LandType.GARDEN);
		expect(component.landTypeForm.get('typeGarden').value.checked).toBeFalsy();
		expect(component.landTypeForm.get('typeOrchard').value.key).toBe(LandType.ORCHARD);
		expect(component.landTypeForm.get('typeOrchard').value.checked).toBeFalsy();
		expect(component.landTypeForm.get('typePermanentGrassland').value.key).toBe(LandType.PERMANENT_GRASSLAND);
		expect(component.landTypeForm.get('typePermanentGrassland').value.checked).toBeFalsy();
		expect(component.landTypeForm.get('typeForestLand').value.key).toBe(LandType.FOREST_LAND);
		expect(component.landTypeForm.get('typeForestLand').value.checked).toBeFalsy();
		expect(component.landTypeForm.get('typeWaterArea').value.key).toBe(LandType.WATER_AREA);
		expect(component.landTypeForm.get('typeWaterArea').value.checked).toBeFalsy();
		expect(component.landTypeForm.get('typeBuiltAreaAndCourtyard').value.key).toBe(LandType.BUILT_AREA_AND_COURTYARD);
		expect(component.landTypeForm.get('typeBuiltAreaAndCourtyard').value.checked).toBeFalsy();
		expect(component.landTypeForm.get('typeOtherAreas').value.key).toBe(LandType.OTHER_AREAS);
		expect(component.landTypeForm.get('typeOtherAreas').value.checked).toBeFalsy();

		const spyCheckAll = spyOn(<any>component, 'checkAll').and.callThrough();
		fixture.debugElement.query(By.css('button:first-child')).nativeElement.click();
		expect(spyCheckAll).toHaveBeenCalled();

		expect(component.landTypeForm.get('typeArableLand').value.key).toBe(LandType.ARABLE_LAND);
		expect(component.landTypeForm.get('typeArableLand').value.checked).toBeTruthy();
		expect(component.landTypeForm.get('typeHopper').value.key).toBe(LandType.HOPPER);
		expect(component.landTypeForm.get('typeHopper').value.checked).toBeTruthy();
		expect(component.landTypeForm.get('typeVineyard').value.key).toBe(LandType.VINEYARD);
		expect(component.landTypeForm.get('typeVineyard').value.checked).toBeTruthy();
		expect(component.landTypeForm.get('typeGarden').value.key).toBe(LandType.GARDEN);
		expect(component.landTypeForm.get('typeGarden').value.checked).toBeTruthy();
		expect(component.landTypeForm.get('typeOrchard').value.key).toBe(LandType.ORCHARD);
		expect(component.landTypeForm.get('typeOrchard').value.checked).toBeTruthy();
		expect(component.landTypeForm.get('typePermanentGrassland').value.key).toBe(LandType.PERMANENT_GRASSLAND);
		expect(component.landTypeForm.get('typePermanentGrassland').value.checked).toBeTruthy();
		expect(component.landTypeForm.get('typeForestLand').value.key).toBe(LandType.FOREST_LAND);
		expect(component.landTypeForm.get('typeForestLand').value.checked).toBeTruthy();
		expect(component.landTypeForm.get('typeWaterArea').value.key).toBe(LandType.WATER_AREA);
		expect(component.landTypeForm.get('typeWaterArea').value.checked).toBeTruthy();
		expect(component.landTypeForm.get('typeBuiltAreaAndCourtyard').value.key).toBe(LandType.BUILT_AREA_AND_COURTYARD);
		expect(component.landTypeForm.get('typeBuiltAreaAndCourtyard').value.checked).toBeTruthy();
		expect(component.landTypeForm.get('typeOtherAreas').value.key).toBe(LandType.OTHER_AREAS);
		expect(component.landTypeForm.get('typeOtherAreas').value.checked).toBeTruthy();
		expect(component.landTypeForm.valid).toBeTruthy();
	});

	it('uncheck all - should clear form and return invalid error', () => {
		component.landTypeForm = createLandTypeFilterFormGroup([
			LandType.ARABLE_LAND,
			LandType.HOPPER,
			LandType.VINEYARD,
			LandType.GARDEN,
			LandType.ORCHARD,
			LandType.PERMANENT_GRASSLAND,
			LandType.FOREST_LAND,
			LandType.WATER_AREA,
			LandType.BUILT_AREA_AND_COURTYARD,
			LandType.OTHER_AREAS
		]);

		expect(component).toBeTruthy();
		expect(component.landTypeForm).toBeTruthy();
		expect(component.landTypeForm.get('typeArableLand').value.key).toBe(LandType.ARABLE_LAND);
		expect(component.landTypeForm.get('typeArableLand').value.checked).toBeTruthy();
		expect(component.landTypeForm.get('typeHopper').value.key).toBe(LandType.HOPPER);
		expect(component.landTypeForm.get('typeHopper').value.checked).toBeTruthy();
		expect(component.landTypeForm.get('typeVineyard').value.key).toBe(LandType.VINEYARD);
		expect(component.landTypeForm.get('typeVineyard').value.checked).toBeTruthy();
		expect(component.landTypeForm.get('typeGarden').value.key).toBe(LandType.GARDEN);
		expect(component.landTypeForm.get('typeGarden').value.checked).toBeTruthy();
		expect(component.landTypeForm.get('typeOrchard').value.key).toBe(LandType.ORCHARD);
		expect(component.landTypeForm.get('typeOrchard').value.checked).toBeTruthy();
		expect(component.landTypeForm.get('typePermanentGrassland').value.key).toBe(LandType.PERMANENT_GRASSLAND);
		expect(component.landTypeForm.get('typePermanentGrassland').value.checked).toBeTruthy();
		expect(component.landTypeForm.get('typeForestLand').value.key).toBe(LandType.FOREST_LAND);
		expect(component.landTypeForm.get('typeForestLand').value.checked).toBeTruthy();
		expect(component.landTypeForm.get('typeWaterArea').value.key).toBe(LandType.WATER_AREA);
		expect(component.landTypeForm.get('typeWaterArea').value.checked).toBeTruthy();
		expect(component.landTypeForm.get('typeBuiltAreaAndCourtyard').value.key).toBe(LandType.BUILT_AREA_AND_COURTYARD);
		expect(component.landTypeForm.get('typeBuiltAreaAndCourtyard').value.checked).toBeTruthy();
		expect(component.landTypeForm.get('typeOtherAreas').value.key).toBe(LandType.OTHER_AREAS);
		expect(component.landTypeForm.get('typeOtherAreas').value.checked).toBeTruthy();

		const spyUncheckAll = spyOn(<any>component, 'uncheckAll').and.callThrough();
		fixture.debugElement.query(By.css('button:last-child')).nativeElement.click();
		expect(spyUncheckAll).toHaveBeenCalled();

		expect(component.landTypeForm.get('typeArableLand').value.key).toBe(LandType.ARABLE_LAND);
		expect(component.landTypeForm.get('typeArableLand').value.checked).toBeFalsy();
		expect(component.landTypeForm.get('typeHopper').value.key).toBe(LandType.HOPPER);
		expect(component.landTypeForm.get('typeHopper').value.checked).toBeFalsy();
		expect(component.landTypeForm.get('typeVineyard').value.key).toBe(LandType.VINEYARD);
		expect(component.landTypeForm.get('typeVineyard').value.checked).toBeFalsy();
		expect(component.landTypeForm.get('typeGarden').value.key).toBe(LandType.GARDEN);
		expect(component.landTypeForm.get('typeGarden').value.checked).toBeFalsy();
		expect(component.landTypeForm.get('typeOrchard').value.key).toBe(LandType.ORCHARD);
		expect(component.landTypeForm.get('typeOrchard').value.checked).toBeFalsy();
		expect(component.landTypeForm.get('typePermanentGrassland').value.key).toBe(LandType.PERMANENT_GRASSLAND);
		expect(component.landTypeForm.get('typePermanentGrassland').value.checked).toBeFalsy();
		expect(component.landTypeForm.get('typeForestLand').value.key).toBe(LandType.FOREST_LAND);
		expect(component.landTypeForm.get('typeForestLand').value.checked).toBeFalsy();
		expect(component.landTypeForm.get('typeWaterArea').value.key).toBe(LandType.WATER_AREA);
		expect(component.landTypeForm.get('typeWaterArea').value.checked).toBeFalsy();
		expect(component.landTypeForm.get('typeBuiltAreaAndCourtyard').value.key).toBe(LandType.BUILT_AREA_AND_COURTYARD);
		expect(component.landTypeForm.get('typeBuiltAreaAndCourtyard').value.checked).toBeFalsy();
		expect(component.landTypeForm.get('typeOtherAreas').value.key).toBe(LandType.OTHER_AREAS);
		expect(component.landTypeForm.get('typeOtherAreas').value.checked).toBeFalsy();
		expect(component.landTypeForm.invalid).toBeTruthy();
	});
});
