package cz.lp.common.domain;

import cz.lp.common.dto.Epsg5514Coordinate;
import cz.lp.common.dto.Wgs84Coordinate;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;

/**
 * Area border entity represents area border with EPSG5514 and WGS84 coordinates in database table.
 */

@Entity
@Table(name = "area_border")
public class AreaBorderEntity {

    @Id
    @SequenceGenerator(name = "id", sequenceName = "area_border_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id")
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Type(type = "JsonList", parameters = {
            @Parameter(name = "type", value = "cz.lp.common.dto.Epsg5514Coordinate")
    })
    @Column(name = "epsg5514", nullable = false)
    private List<Epsg5514Coordinate> epsg5514Coordinates;

    @Type(type = "JsonList", parameters = {
            @Parameter(name = "type", value = "cz.lp.common.dto.Wgs84Coordinate")
    })
    @Column(name = "wgs84", nullable = true)
    private List<Wgs84Coordinate> wgs84Coordinates;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "area_code")
    private AreaEntity area;


    /**
     * Creates instance of area border entity.
     */
    public AreaBorderEntity() {
    }

    /**
     * Creates instance of area border entity
     *
     * @param id                  identifier
     * @param epsg5514Coordinates WPSG5514 coordinates in JSON
     * @param wgs84Coordinates    WGS84 coordinates in JSON
     * @param area                area
     */
    public AreaBorderEntity(Long id, List<Epsg5514Coordinate> epsg5514Coordinates, List<Wgs84Coordinate> wgs84Coordinates, AreaEntity area) {
        this.id = id;
        this.epsg5514Coordinates = epsg5514Coordinates;
        this.wgs84Coordinates = wgs84Coordinates;
        this.area = area;
    }

    /**
     * Creates instance of area border entity
     *
     * @param epsg5514Coordinates WPSG5514 coordinates in JSON
     * @param wgs84Coordinates    WGS84 coordinates in JSON
     * @param area                area
     */
    public AreaBorderEntity(List<Epsg5514Coordinate> epsg5514Coordinates, List<Wgs84Coordinate> wgs84Coordinates, AreaEntity area) {
        this.epsg5514Coordinates = epsg5514Coordinates;
        this.wgs84Coordinates = wgs84Coordinates;
        this.area = area;
    }

    /**
     * Gets identifier.
     *
     * @return identifier
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets EPSG5514 coordinates in JSON format.
     *
     * @return EPSG5514 coordinates
     */
    public List<Epsg5514Coordinate> getEpsg5514Coordinates() {
        return epsg5514Coordinates;
    }

    /**
     * Sets EPSG 5514 coordinates in JSON format.
     *
     * @param epsg5514Coordinates EPSG 5514 coordinates
     */
    public void setEpsg5514Coordinates(List<Epsg5514Coordinate> epsg5514Coordinates) {
        this.epsg5514Coordinates = epsg5514Coordinates;
    }

    /**
     * Gets WGS84 coordinates inf JSON format.
     *
     * @return WGS84 coordinates
     */
    public List<Wgs84Coordinate> getWgs84Coordinates() {
        return wgs84Coordinates;
    }

    /**
     * Sets WGS84 coordinates.
     *
     * @param wgs84Coordinates WGS84 coordinates
     */
    public void setWgs84Coordinates(List<Wgs84Coordinate> wgs84Coordinates) {
        this.wgs84Coordinates = wgs84Coordinates;
    }

    /**
     * Gets area.
     *
     * @return area
     */
    public AreaEntity getArea() {
        return area;
    }
}
