package cz.lp.processor.custom.advertisement.srealitycz.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class SrealityCzImageLinksDto {

    private final SrealityCzGalleryViewDto view;

    @JsonCreator
    public SrealityCzImageLinksDto(
            @JsonProperty("view") SrealityCzGalleryViewDto view) {
        this.view = view;
    }
}
