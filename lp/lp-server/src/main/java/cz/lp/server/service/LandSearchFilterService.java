package cz.lp.server.service;

import cz.lp.server.domain.LandSearchEntity;
import cz.lp.server.dto.filter.LandSearchFilter;
import cz.lp.server.repository.LandSearchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

/**
 * Land search filter service provides operations about land search filters.
 */
@Service
public class LandSearchFilterService {

    private final LandSearchRepository landSearchRepository;

    @Autowired
    public LandSearchFilterService(LandSearchRepository landSearchRepository) {
        this.landSearchRepository = landSearchRepository;
    }

    /**
     * Gets land search filter by search identifier.
     *
     * @param searchId search identifier
     * @return land search filter
     */
    public Optional<LandSearchFilter> getLandSearchFilter(String searchId) {

        final LandSearchEntity landSearch = landSearchRepository.findOne(searchId);
        return Optional.ofNullable(landSearch.getLandSearchFilter());
    }

    /**
     * Get search id by land search filter (by hash code in database or save filter and generate new one).
     *
     * @param landSearchFilter land search filter
     * @return search identifier
     */
    public String getSearchId(LandSearchFilter landSearchFilter) {

        final LandSearchEntity landSearch = landSearchRepository
                .findOneByLandSearchFilterHash(landSearchFilter.hashCode())
                .orElseGet(() -> saveLandSearchFilter(landSearchFilter));
        return landSearch.getId();
    }

    /**
     * Save land search filter.
     *
     * @param landSearchFilter land search filter
     * @return land search entity
     */
    private LandSearchEntity saveLandSearchFilter(LandSearchFilter landSearchFilter) {
        LandSearchEntity landSearchEntity = new LandSearchEntity(generateSearchId(), landSearchFilter, landSearchFilter.hashCode());
        landSearchRepository.save(landSearchEntity);
        return landSearchEntity;
    }

    /**
     * Generates search identifier.
     *
     * @return search identifier
     */
    private String generateSearchId() {

        return UUID.randomUUID().toString();
    }
}
