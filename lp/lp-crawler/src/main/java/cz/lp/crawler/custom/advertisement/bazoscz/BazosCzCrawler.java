package cz.lp.crawler.custom.advertisement.bazoscz;

import com.google.common.collect.Sets;
import cz.lp.crawler.Crawler;
import cz.lp.crawler.CrawlerFilter;
import cz.lp.crawler.SeedUrl;
import cz.lp.crawler.custom.advertisement.bazoscz.adapter.BazosCzDetailLinkAdapter;
import cz.lp.crawler.custom.advertisement.bazoscz.adapter.BazosCzPageLinkAdapter;
import cz.lp.crawler.resolver.CrawlerConditionResolver;
import cz.lp.crawler.service.CrawlerService;
import cz.lp.enumerate.CrawlerCondition;
import cz.lp.enumerate.RecrawlStrategy;
import org.apache.commons.codec.digest.DigestUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;

/**
 * Bazos cz crawler provides crawling advertisement of lands and buildings from https://reality.bazos.cz.
 */
public class BazosCzCrawler extends Crawler {

    private static final String PAGE_URL_PATTERN = "(?i)https?://(www.)?reality\\.bazos\\.cz/prodam/\\w+/\\d*.*";
    private static final String HASH_PAGE_SUB_CONTENT_SELECTOR = "span[class=vypis]";
    private static final String HASH_DETAIL_SUB_CONTENT_SELECTOR = "div[class=popis]";


    public BazosCzCrawler(String className, Long id, CrawlerCondition condition, Integer delay, RecrawlStrategy recrawlStrategy, String recrawlUrlPattern, Long recrawlInterval, LocalDateTime recrawlOn, Set<SeedUrl> seedUrls, CrawlerService crawlerService, CrawlerConditionResolver crawlerConditionResolver, CrawlerFilter crawlerFilter) {
        super(className, id, condition, delay, recrawlStrategy, recrawlUrlPattern, recrawlInterval, recrawlOn, seedUrls, crawlerService, crawlerConditionResolver, crawlerFilter);
    }

    @Override
    protected Optional<Set<String>> parseLinks(String content, String url) {

        Set<String> links = Sets.newHashSet();
        // Extract links to next pages and details (only from page, not detail)
        if(url.matches(PAGE_URL_PATTERN)) {
            new BazosCzPageLinkAdapter(content, url).getLinks().ifPresent(pageLinks -> links.addAll(pageLinks));
            new BazosCzDetailLinkAdapter(content, url).getLinks().ifPresent(detailLinks -> links.addAll(detailLinks));
        }
        return Optional.of(links);
    }

    @Override
    protected String generateContentHash(byte[] content, String url) {
        Document document = Jsoup.parse(new String(content), url, Parser.xmlParser());

        String subContent;
        if(url.matches(PAGE_URL_PATTERN)){
            subContent = document.select(HASH_PAGE_SUB_CONTENT_SELECTOR).html();
        } else {
            subContent = document.select(HASH_DETAIL_SUB_CONTENT_SELECTOR).html();
        }
        return DigestUtils.md5Hex(subContent).toUpperCase();
    }


}
