package cz.lp.common.repository;

import com.google.common.collect.Lists;
import cz.lp.common.domain.AreaEntity;
import cz.lp.common.enumerate.AreaType;
import cz.lp.common.repository.configuration.RepositoryTemplate;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

public class AreaRepositoryIT extends RepositoryTemplate {

    @Autowired
    private AreaRepository areaRepository;

    private static final String CODE = "1234";
    private static final AreaType TYPE = AreaType.REGION;
    private static final String NAME = "Kraj Vysočina";
    private static final AreaEntity PARENT_AREA = null;
    private static final List<AreaEntity> EMPTY_SUB_AREAS = Lists.newArrayList();
    private static final Long CRAWLER_RESULT_ID = null;
    private static final Timestamp CREATED_ON = Timestamp.valueOf("2018-01-01 00:00:01.000000");
    private static final Timestamp LAST_UPDATED_ON = Timestamp.valueOf("2018-02-01 00:00:01.000000");


    @Test
    public void insert() {

        AreaEntity areaEntity = new AreaEntity(CODE, TYPE, NAME, PARENT_AREA, EMPTY_SUB_AREAS, CRAWLER_RESULT_ID, CREATED_ON, LAST_UPDATED_ON);
        String code = areaRepository.save(areaEntity).getCode();
        AreaEntity savedAreaEntity = areaRepository.findOne(code);
        Assert.assertNotNull(savedAreaEntity);
        Assert.assertEquals(code, savedAreaEntity.getCode());
        Assert.assertEquals(TYPE, savedAreaEntity.getType());
        Assert.assertEquals(NAME, savedAreaEntity.getName());
        Assert.assertEquals(PARENT_AREA, savedAreaEntity.getParentArea());
        Assert.assertEquals(EMPTY_SUB_AREAS, savedAreaEntity.getSubAreas());
        Assert.assertEquals(CRAWLER_RESULT_ID, savedAreaEntity.getCrawlerResultId());
        Assert.assertEquals(CREATED_ON, savedAreaEntity.getCreatedOn());
        Assert.assertEquals(LAST_UPDATED_ON, savedAreaEntity.getLastUpdatedOn());
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/area/INSERT_area_WITH_code_=_1_AND_parent_area_code_=_NULL_REGION.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
    })
    public void update(){

        AreaEntity regionAreaEntity = areaRepository.findOne("1");

        String newName = "Name";
        Timestamp newLastUpdatedOn = Timestamp.valueOf("2017-02-01 00:00:01.000000");
        Timestamp newRemovedOn = Timestamp.valueOf("2019-02-01 00:00:01.000000");
        regionAreaEntity.setName(newName);
        regionAreaEntity.setLastUpdatedOn(newLastUpdatedOn);
        regionAreaEntity.setRemovedOn(newRemovedOn);
        areaRepository.save(regionAreaEntity);

        AreaEntity updatedRegionAreaEntity = areaRepository.findOne("1");
        Assert.assertEquals(newName, updatedRegionAreaEntity.getName());
        Assert.assertEquals(newLastUpdatedOn, updatedRegionAreaEntity.getLastUpdatedOn());
        Assert.assertEquals(newRemovedOn, updatedRegionAreaEntity.getRemovedOn());
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/area/INSERT_area_WITH_code_=_1_AND_parent_area_code_=_NULL_REGION.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/area/INSERT_area_WITH_code_=_4_AND_parent_area_code_=_1_DISTRICT.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void findOne_whenRegiontAreaExist_shouldReturnRegionArea() {

        AreaEntity regionAreaEntity = areaRepository.findOne("1");
        Assert.assertNotNull(regionAreaEntity);
        Assert.assertEquals("1", regionAreaEntity.getCode());
        Assert.assertEquals(AreaType.REGION, regionAreaEntity.getType());
        Assert.assertEquals("Zlinsky", regionAreaEntity.getName());
        Assert.assertNull(regionAreaEntity.getCrawlerResultId());
        Assert.assertEquals(CREATED_ON, regionAreaEntity.getCreatedOn());
        Assert.assertEquals(LAST_UPDATED_ON, regionAreaEntity.getLastUpdatedOn());
        Assert.assertNull(regionAreaEntity.getParentArea());

        AreaEntity districtAreaEntity = regionAreaEntity.getSubAreas().get(0);
        Assert.assertNotNull(districtAreaEntity);
        Assert.assertEquals("4", districtAreaEntity.getCode());
        Assert.assertEquals(AreaType.DISTRICT, districtAreaEntity.getType());
        Assert.assertEquals("Kromeriz", districtAreaEntity.getName());
        Assert.assertEquals(EMPTY_SUB_AREAS, districtAreaEntity.getSubAreas());
        Assert.assertNull(regionAreaEntity.getCrawlerResultId());
        Assert.assertEquals(CREATED_ON, regionAreaEntity.getCreatedOn());
        Assert.assertEquals(LAST_UPDATED_ON, regionAreaEntity.getLastUpdatedOn());
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/area/INSERT_area_WITH_code_=_1_AND_parent_area_code_=_NULL_REGION.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/area/INSERT_area_WITH_code_=_4_AND_parent_area_code_=_1_DISTRICT.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void findOne_whenDistrictAreaExist_shouldReturnDistrictDistrictArea() {

        AreaEntity districtAreaEntity = areaRepository.findOne("4");
        Assert.assertNotNull(districtAreaEntity);
        Assert.assertEquals("4", districtAreaEntity.getCode());
        Assert.assertEquals(AreaType.DISTRICT, districtAreaEntity.getType());
        Assert.assertEquals(EMPTY_SUB_AREAS, districtAreaEntity.getSubAreas());
        Assert.assertNull(districtAreaEntity.getCrawlerResultId());
        Assert.assertEquals(CREATED_ON, districtAreaEntity.getCreatedOn());
        Assert.assertEquals(LAST_UPDATED_ON, districtAreaEntity.getLastUpdatedOn());

        AreaEntity areaEntity = districtAreaEntity.getParentArea();
        Assert.assertNotNull(areaEntity);
        Assert.assertEquals("1", areaEntity.getCode());
        Assert.assertEquals(AreaType.REGION, areaEntity.getType());
        Assert.assertEquals("Zlinsky", areaEntity.getName());
        Assert.assertNull(areaEntity.getParentArea());
        Assert.assertNull(areaEntity.getCrawlerResultId());
        Assert.assertEquals(CREATED_ON, areaEntity.getCreatedOn());
        Assert.assertEquals(LAST_UPDATED_ON, areaEntity.getLastUpdatedOn());
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void existFirstLevelAreas_whenAreaTableIsEmpty_shouldReturnFalse() throws Exception {

        Assert.assertFalse(areaRepository.existFirstLevelAreas());
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/area/INSERT_area_WITH_code_=_1_AND_parent_area_code_=_NULL_REGION.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/area/INSERT_area_WITH_code_=_2_AND_parent_area_code_=_NULL_REGION.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void existFirstLevelAreas_whenExistOnlyFirstLevel_shouldReturnTrue() throws Exception {

        Assert.assertTrue(areaRepository.existFirstLevelAreas());
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
    })
    public void getAllFirstLevelAreas_whenAreaTableIsEmpty_shouldReturnNothing() throws Exception {

        List<AreaEntity> areas = areaRepository.getAllFirstLevelAreas();
        Assert.assertNotNull(areas);
        Assert.assertTrue(areas.isEmpty());
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/area/INSERT_area_WITH_code_=_1_AND_parent_area_code_=_NULL_REGION.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/area/INSERT_area_WITH_code_=_2_AND_parent_area_code_=_NULL_REGION.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void getAllFirstLevelAreas_whenExistOnlyFirstLevelArea_shouldReturnFirstLevelArea() throws Exception {

        List<AreaEntity> areas = areaRepository.getAllFirstLevelAreas();
        Assert.assertNotNull(areas);
        Assert.assertEquals(2, areas.size());

        Assert.assertEquals("1", areas.get(0).getCode());
        Assert.assertEquals(AreaType.REGION, areas.get(0).getType());
        Assert.assertEquals("Zlinsky", areas.get(0).getName());
        Assert.assertNull(areas.get(0).getParentArea());
        Assert.assertTrue(areas.get(0).getSubAreas().isEmpty());
        Assert.assertNull(areas.get(0).getCrawlerResultId());
        Assert.assertEquals(CREATED_ON, areas.get(0).getCreatedOn());
        Assert.assertEquals(LAST_UPDATED_ON, areas.get(0).getLastUpdatedOn());

        Assert.assertEquals("2", areas.get(1).getCode());
        Assert.assertEquals(AreaType.REGION, areas.get(1).getType());
        Assert.assertEquals("Jihomoravsky", areas.get(1).getName());
        Assert.assertNull(areas.get(1).getParentArea());
        Assert.assertTrue(areas.get(1).getSubAreas().isEmpty());
        Assert.assertNull(areas.get(1).getCrawlerResultId());
        Assert.assertEquals(CREATED_ON, areas.get(1).getCreatedOn());
        Assert.assertEquals(LAST_UPDATED_ON, areas.get(1).getLastUpdatedOn());
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler/INSERT_crawler_WITH_id_=_1_AND_recrawl_on_=_NULL_AND_ENABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler-result/INSERT_crawler_result_WITH_id_=_1_AND_status_=_SUCCESS_AND_order_=_1_AND_last_crawled_on_IS_NOT_NULL.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/area/INSERT_area_WITH_code_=_1_AND_parent_area_code_=_NULL_REGION.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/area/INSERT_area_WITH_code_=_5_AND_parent_area_code_=_1_AND_crawler_result_id_=_1_DISTRICT.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void updateAllAreasAsRemovedByCrawlerResultId_whenAllAreInForce_shouldAllRemoved(){

        Timestamp removedOn = Timestamp.valueOf("2020-02-01 00:00:01.000000");
        areaRepository.updateAllAreasAsRemovedByCrawlerResultId(Timestamp.valueOf("2020-02-01 00:00:01.000000"), 1l);
        AreaEntity removedAreaEntity = areaRepository.findOne("5");
        Assert.assertEquals(removedOn, removedAreaEntity.getRemovedOn());
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/area/INSERT_area_WITH_code_=_1_AND_parent_area_code_=_NULL_REGION.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void existByCode_whenCodeExists_shouldReturnTrue(){

        Assert.assertTrue(areaRepository.existByCode("1"));
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/area/INSERT_area_WITH_code_=_1_AND_parent_area_code_=_NULL_REGION.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void existByCode_whenCodeDoesNotExist_shouldReturnFalse(){

        Assert.assertFalse(areaRepository.existByCode("2"));
    }
}