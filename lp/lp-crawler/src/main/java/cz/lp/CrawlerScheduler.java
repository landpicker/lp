package cz.lp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Crawler scheduler defines when crawlers will be launched.
 */
@Component
public class CrawlerScheduler {

    private static final Logger log = LoggerFactory.getLogger(CrawlerScheduler.class);

    private CrawlerWorker crawlerWorker;

    @Autowired
    public CrawlerScheduler(CrawlerWorker crawlerWorker) {
        this.crawlerWorker = crawlerWorker;
    }

    /**
     * Runs crawlers every 5 minutes (if are available - executable).
     */
    @Scheduled(fixedDelay = 300000)
    public void runCrawlers() {

        log.info("Running crawlers...");
        crawlerWorker.runCrawlers();
    }
}
