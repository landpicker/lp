package cz.lp.server.controller;

import javax.servlet.http.HttpServletResponse;

/**
 * Controller utility provides method for controllers.
 */
public class ControllerUtil {

    /**
     * Creates private instance.
     */
    private ControllerUtil() {
    }

    /**
     * Sets JSON response headers.
     *
     * @param response HTTP response
     */
    public static void setJsonResponseHeaders(HttpServletResponse response) {

        response.setHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "0");
    }
}
