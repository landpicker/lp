package cz.lp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Standalone processor scheduler defines when processors independent of crawler will be launched.
 */
@Component
public class StandaloneProcessorScheduler {

    private static final Logger log = LoggerFactory.getLogger(StandaloneProcessorScheduler.class);

    private final StandaloneProcessorService standaloneProcessorService;

    @Autowired
    public StandaloneProcessorScheduler(StandaloneProcessorService standaloneProcessorService) {
        this.standaloneProcessorService = standaloneProcessorService;
    }

    /**
     * Runs standalone processors every 5 minutes (if are available - executable).
     */
    @Scheduled(fixedDelay = 300000)
    public void runStandaloneProcessors() {

        log.info("Running standalone processors...");
        standaloneProcessorService.runStandaloneProcessors();
    }
}
