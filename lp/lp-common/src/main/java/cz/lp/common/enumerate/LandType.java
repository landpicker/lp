package cz.lp.common.enumerate;

import java.util.Arrays;
import java.util.Optional;

public enum LandType {

    ARABLE_LAND(2), // orná půda
    HOPPER(3), // chmelnice
    VINEYARD(4), // vinice
    GARDEN(5), // zahrada
    ORCHARD(6), // ovocný sad
    PERMANENT_GRASSLAND(7), // trvalý travní porost
    FOREST_LAND(10), // lesní pozemek
    WATER_AREA(11), // vodní plocha
    BUILT_AREA_AND_COURTYARD(13), // zastavěná plocha a nádvoří
    OTHER_AREAS(14); // ostatní plocha

    private final int code;

    LandType(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static Optional<LandType> valueOfCode(int code) {

        return Arrays.stream(values()).filter(landType -> landType.getCode() == code).findFirst();
    }
}
