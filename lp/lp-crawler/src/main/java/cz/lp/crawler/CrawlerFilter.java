package cz.lp.crawler;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * Crawler filter provides functions for filtering parsed links.
 */
public abstract class CrawlerFilter {

    protected List<String> allowedLinkRegexPatterns = Lists.newArrayList();

    /**
     * Gets information if the parsed link is allowed in case that the filter was defined.
     *
     * @param link parsed link
     * @return true if the link is allowed, otherwise false
     */
    public boolean isLinkAllowed(String link) {

        if (this.allowedLinkRegexPatterns.isEmpty()) return true;
        return allowedLinkRegexPatterns.stream().filter(linkRegexPattern -> link.matches(linkRegexPattern)).findFirst().isPresent();
    }
}
