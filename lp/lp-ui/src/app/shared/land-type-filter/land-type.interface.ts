export enum LandType {
	ARABLE_LAND = 'ARABLE_LAND', // orná půda
	HOPPER = 'HOPPER', // chmelnice
	VINEYARD = 'VINEYARD', // vinice
	GARDEN = 'GARDEN', // zahrada
	ORCHARD = 'ORCHARD', // ovocný sad
	PERMANENT_GRASSLAND = 'PERMANENT_GRASSLAND', // trvalý travní porost
	FOREST_LAND = 'FOREST_LAND', // lesní pozemek
	WATER_AREA = 'WATER_AREA', // vodní plocha
	BUILT_AREA_AND_COURTYARD = 'BUILT_AREA_AND_COURTYARD', // zastavěná plocha a nádvoří
	OTHER_AREAS = 'OTHER_AREAS' // ostatní plocha
}
