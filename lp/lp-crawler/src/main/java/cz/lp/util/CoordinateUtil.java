package cz.lp.util;

import com.google.common.collect.Lists;
import cz.lp.common.dto.Epsg5514Coordinate;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CoordinateUtil {

    private final static String COORDINATE_REGEX_PATTERN = "((-?\\d+\\.\\d{2})\\s(-?\\d+\\.\\d{2}))+";

    private CoordinateUtil() {
    }

    public static List<Epsg5514Coordinate> extractEpsg5514Coordinates(String text) {

        List<Epsg5514Coordinate> epsg5514Coordinates = Lists.newArrayList();
        Pattern coordinatePattern = Pattern.compile(COORDINATE_REGEX_PATTERN);
        Matcher coordinateMatcher = coordinatePattern.matcher(text);
        while (coordinateMatcher.find()) {
            String coordinateString = coordinateMatcher.group(0);
            String[] coordinateArray = coordinateString.split("\\s");
            try {
                float x = Float.valueOf(coordinateArray[0]);
                float y = Float.valueOf(coordinateArray[1]);
                epsg5514Coordinates.add(new Epsg5514Coordinate(x, y));
            } catch (NumberFormatException ex) {
                return Lists.newArrayList();
            }
        }
        return epsg5514Coordinates;
    }
}
