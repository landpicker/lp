INSERT INTO public.source (id, provider, website_url) VALUES (2, 'ČÚZK', 'http://vdp.cuzk.cz/vdp/ruian/vymennyformat/vyhledej');

INSERT INTO public.crawler (id, source_id, crawler_class, condition, delay, enabled, recrawl_strategy, recrawl_interval, recrawl_on, last_recrawled_on)
VALUES (2, 2, 'cz.lp.crawler.custom.vdpexchange.VdpExchangeCrawler', 'EXIST_CITY_LEVEL_AREAS', 200, false, 'RECRAWL_ALL', 60, null, null);

INSERT INTO public.crawler_seed_url (crawler_id, url, enabled, condition) VALUES (2, 'http://vdp.cuzk.cz/vdp/ruian/vymennyformat/seznamlinku?vf.pu=S&_vf.pu=on&_vf.pu=on&vf.cr=U&vf.up=OB&vf.ds=K&_vf.vu=on&_vf.vu=on&_vf.vu=on&_vf.vu=on&vf.uo=O&ob.kod=%s&search=Vyhledat', true, 'PUT_AREA_CITY_CODE');
