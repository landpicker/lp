package cz.lp.exception;

/**
 * CrawlerEntity HTTP client argument exception is generated if some request parameter is invalid.
 */
public class CrawlerHttpClientParameterException extends Exception {

    public CrawlerHttpClientParameterException(String message) {
        super(message);
    }

    public CrawlerHttpClientParameterException(String message, Throwable cause) {
        super(message, cause);
    }
}
