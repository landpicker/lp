import { async, ComponentFixture, TestBed, tick, fakeAsync, discardPeriodicTasks, flushMicrotasks, inject } from '@angular/core/testing';

import { AreaFilterComponent, createAreaFilterFormGroup, getFilterAreaCodes } from './area-filter.component';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule, MatAutocompleteModule, MatChipsModule, MatIconModule, MatAutocomplete, MatAutocompleteSelectedEvent } from '@angular/material';
import { AreaFilterService } from './area-filter.service';
import { Area, AreaType } from './area.interface';
import { Observable, of } from 'rxjs';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import { componentNeedsResolution } from '@angular/core/src/metadata/resource_loading';
import { OverlayContainer } from '@angular/cdk/overlay';

describe('AreaFilterComponent', () => {
	let component: AreaFilterComponent;
  let fixture: ComponentFixture<AreaFilterComponent>;


	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [BrowserAnimationsModule, TranslateModule.forRoot(), FormsModule, ReactiveFormsModule, MatFormFieldModule, MatInputModule, MatAutocompleteModule, MatChipsModule, MatIconModule],
			schemas: [NO_ERRORS_SCHEMA],
			providers: [{ provide: AreaFilterService, useClass: AreaFilterServiceMock }],
			declarations: [AreaFilterComponent]
		}).compileComponents();
  }));


	beforeEach(() => {
		fixture = TestBed.createComponent(AreaFilterComponent);
    component = fixture.componentInstance;
  });

	it('init without form group as input- should create', () => {
		fixture.detectChanges();
		expect(component).toBeTruthy();
		expect(component.areaInput.nativeElement.value).toEqual('');
		expect(component.areaForm.get('areas').value).toEqual([]);
		expect(component.areaAutocompleteControl.value).toBeNull();
		expect(component.areaTrees.length).toEqual(1);
		expect(component.selectedAreas).toEqual([]);
		expect(getFilterAreaCodes(component.areaForm).length).toEqual(0);
		expect(component.filteredAreaGroups).toBeDefined();
	});

	it('init with form group as input- should create', () => {
		component.areaForm = createAreaFilterFormGroup();

		fixture.detectChanges();
		expect(component).toBeTruthy();
		expect(component.areaInput.nativeElement.value).toEqual('');
		expect(component.areaForm.get('areas').value).toEqual([]);
		expect(component.areaAutocompleteControl.value).toBeNull();
		expect(component.areaTrees.length).toEqual(1);
		expect(component.selectedAreas).toEqual([]);
		expect(getFilterAreaCodes(component.areaForm).length).toEqual(0);
		expect(component.filteredAreaGroups).toBeDefined();
	});

	it('focus on empty autocomplete input - should do nothing', () => {
    // TODO
  });

	it('focus on autocomplete input which contains more than 2 characters - should return options in autocomplete', () => {
    // TODO
  });

	it('add first character into input - should display nothing', () => {
		component.areaForm = createAreaFilterFormGroup();

		fixture.detectChanges();
		let autocompleteInput = fixture.debugElement.query(By.css('input')).nativeElement;
		autocompleteInput.focus();
		autocompleteInput.value = 'h';
		component.filteredAreaGroups.subscribe((areaGroups) => {
			expect(areaGroups.length).toEqual(0);
		});
    autocompleteInput.dispatchEvent(new Event('input'));
    fixture.detectChanges();
		const optionElements = fixture.debugElement.queryAll(By.css('mat-option'));
		expect(optionElements.length).toBe(0);
	});

	it('add third character into input - should display options in autocomplete', () => {
		component.areaForm = createAreaFilterFormGroup();

		fixture.detectChanges();
		let autocompleteInput = fixture.debugElement.query(By.css('input')).nativeElement;
		autocompleteInput.focus();
		autocompleteInput.value = 'hyn';
		component.filteredAreaGroups.subscribe((areaGroups) => {
			expect(areaGroups.length).toEqual(2);
			expect(areaGroups[0].areas[0].name).toEqual('Hynčina - C');
			expect(areaGroups[1].areas[0].name).toEqual('Hynčina - CT');
		});
		autocompleteInput.dispatchEvent(new Event('input'));
		fixture.detectChanges();
		const optionElements = fixture.debugElement.queryAll(By.css('mat-option'));
		expect(optionElements.length).toBe(2);
		expect(optionElements[0].nativeElement.textContent.trim()).toBe('Hynčina - C');
		expect(optionElements[1].nativeElement.textContent.trim()).toBe('Hynčina - CT');
	});

	it('select option - should display chip with option and remove option from autocomplete', fakeAsync(() => {

    // TODO
    // component.areaForm = createAreaFilterFormGroup();
		// fixture.detectChanges();
		// let autocompleteInput = fixture.debugElement.query(By.css('input')).nativeElement;
		// autocompleteInput.focus();
		// autocompleteInput.value = 'hyn';
    // autocompleteInput.dispatchEvent(new Event('input'));

    // const autocompleteElement = fixture.debugElement.query(By.css('mat-autocomplete')).nativeElement;
    // console.log(autocompleteElement);
    // const firstOptionElement = fixture.debugElement.queryAll(By.css('mat-option'))[0].nativeElement;
    // firstOptionElement.focus();

    // firstOptionElement.click();
    // autocompleteElement.dispatchEvent(new MatAutocompleteSelectedEvent(autocompleteElement, firstOptionElement));

    // firstOptionElement.dispatchEvent(new Event('MatAutocompleteSelectedEvent'));
    // fixture.detectChanges();
    // console.log(firstOptionElement);
    // console.log(option);

    // const firstOptionElement = fixture.debugElement.queryAll(By.css('mat-option'))[0];
    // firstOptionElement.nativeElement.click();
    // fixture.detectChanges();
    // tick();
		// console.log(component.selectedAreas);
  }));

	it('select parent option - should remove child chip and return child option into autocomplete, should display parent chip with option and remove parent option from autocomplete', () => {
    // TODO
  });

	it('remove chip - should remove chip and return option into autocomplete', () => {
    // TODO
  });
});

class AreaFilterServiceMock {
	getAreaTree(): Observable<Area[]> {
		return of([
			<Area>{
				code: '1',
				type: AreaType.REGION,
				name: 'Olomoucký kraj - R',
				subAreas: [
					<Area>{
						code: '11',
						type: AreaType.DISTRICT,
						name: 'Šumperk - D',
						parentAreaCode: '1',
						subAreas: [
							<Area>{
								code: '111',
								type: AreaType.CITY,
								name: 'Hynčina - C',
								parentAreaCode: '11',
								subAreas: [<Area>{ code: '1111', type: AreaType.CADASTRAL_TERRITORY, name: 'Hynčina - CT', parentAreaCode: '111', subAreas: [] }]
							},
							<Area>{ code: '112', type: AreaType.CITY, name: 'Zábřeh - C', parentAreaCode: '11', subAreas: [] }
						]
					}
				]
			}
		]);
	}
}
