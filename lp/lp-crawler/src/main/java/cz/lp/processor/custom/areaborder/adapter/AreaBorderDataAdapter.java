package cz.lp.processor.custom.areaborder.adapter;

import cz.lp.processor.custom.areaborder.AreaBorder;

import java.util.List;
import java.util.Optional;

public class AreaBorderDataAdapter {

    private Optional<AreaBorder> cityAreaBorder;
    private Optional<List<AreaBorder>> cadastralTerritoryBorders;

    public AreaBorderDataAdapter(String content, String url) {

        AreaBorderCityAdapter cityAreaBorderAdapter = new AreaBorderCityAdapter(content, url);
        cityAreaBorder = cityAreaBorderAdapter.getCityAreaBorder();

        AreaBorderCadastralTerritoryAdapter cadastralTerritoryAdapter = new AreaBorderCadastralTerritoryAdapter(content, url);
        cadastralTerritoryBorders = cadastralTerritoryAdapter.getCadastralTerritoryBorders();
    }

    /**
     * Gets city area border.
     *
     * @return city area border
     */
    public Optional<AreaBorder> getCityAreaBorder() {
        return cityAreaBorder;
    }

    /**
     * Gets cadastral territory borders.
     *
     * @return cadastral territory borders
     */
    public Optional<List<AreaBorder>> getCadastralTerritoryBorders() {
        return cadastralTerritoryBorders;
    }
}
