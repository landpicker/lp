package cz.lp.crawler.custom.advertisement.srealitycz.adapter;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.*;

public class SrealityCzDetailLinkAdapterTest {

    @Test
    public void constructorWithExtractLinks() throws IOException {

        try(InputStream is = getClass().getClassLoader().getResourceAsStream("crawler/srealitycz/page.json")){

            byte[] content = IOUtils.toByteArray(is);
            SrealityCzDetailLinkAdapter adapter = new SrealityCzDetailLinkAdapter(new String(content, Charset.forName("utf-8")));
            Optional<Set<String>> links = adapter.getLinks();
            assertTrue(links.isPresent());
            assertEquals(10, links.get().size());
            assertTrue(links.get().contains("https://www.sreality.cz/api/cs/v2/estates/2428632668"));
        }
    }

}