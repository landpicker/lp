import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandSearchMapComponent } from './land-search-map.component';
import { TranslateModule } from '../../../../node_modules/@ngx-translate/core';
import { MapModule } from '../../shared/map/map.module';
import { MatCardModule } from '@angular/material';

describe('LandSearchMapComponent', () => {
  let component: LandSearchMapComponent;
  let fixture: ComponentFixture<LandSearchMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        MapModule,
        MatCardModule
      ],
      declarations: [ LandSearchMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandSearchMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
