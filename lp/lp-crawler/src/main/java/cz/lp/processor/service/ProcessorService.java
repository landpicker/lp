package cz.lp.processor.service;

import cz.lp.domain.CrawlerResultEntity;
import cz.lp.domain.ProcessorCrawlerResultEntity;
import cz.lp.domain.ProcessorEntity;
import cz.lp.domain.SourceEntity;
import cz.lp.processor.Result;
import cz.lp.repository.CrawlerResultRepository;
import cz.lp.repository.ProcessorCrawlerResultRepository;
import cz.lp.repository.ProcessorRepository;
import cz.lp.repository.SourceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

@Service
public class ProcessorService {

    private final ProcessorRepository processorRepository;
    private final CrawlerResultRepository crawlerResultRepository;
    private final ProcessorCrawlerResultRepository processorCrawlerResultRepository;
    private final SourceRepository sourceRepository;

    @Autowired
    public ProcessorService(ProcessorRepository processorRepository,
                            CrawlerResultRepository crawlerResultRepository,
                            ProcessorCrawlerResultRepository processorCrawlerResultRepository,
                            SourceRepository sourceRepository) {
        this.processorRepository = processorRepository;
        this.processorCrawlerResultRepository = processorCrawlerResultRepository;
        this.crawlerResultRepository = crawlerResultRepository;
        this.sourceRepository = sourceRepository;
    }

    /**
     * Gets crawler result identifiers by processing order.
     *
     * @param crawlerId       crawler identifier
     * @param processorId     processor identifier
     * @param processingOrder processing order
     * @return crawler result identifiers
     */
    public Optional<Set<Long>> getResultIdentifiersByProcessingOrder(Long crawlerId, Long processorId, Integer processingOrder) {

        Set<Long> crawlerResultIdentifiers = crawlerResultRepository.findAllIdentifiersByCrawlerIdAndProcessingOrder(crawlerId, processorId, processingOrder).stream()
                .map(bigInteger -> bigInteger.longValue())
                .collect(Collectors.toSet());
        if (isNull(crawlerResultIdentifiers) || crawlerResultIdentifiers.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(crawlerResultIdentifiers);
    }

    /**
     * Gets result for extract.
     *
     * @param crawlerResultId crawler result identifier
     * @return result
     */
    public Result getResultForExtract(Long crawlerResultId) {

        CrawlerResultEntity crawlerResultEntity = crawlerResultRepository.findOne(crawlerResultId);
        return new Result(crawlerResultEntity.getId(), crawlerResultEntity.getStatus(), crawlerResultEntity.getUrl(), crawlerResultEntity.getContent());
    }

    /**
     * Marks results as processed with time information
     *
     * @param processorId     processor identifier
     * @param crawlerResultId crawler result identifier
     */
    public void markResultAsProcessed(Long processorId, Long crawlerResultId) {

        ProcessorCrawlerResultEntity.ProcessorCrawlerResultId id = new ProcessorCrawlerResultEntity.ProcessorCrawlerResultId(processorId, crawlerResultId);
        ProcessorCrawlerResultEntity processorCrawlerResultEntity = processorCrawlerResultRepository.findOne(id);
        LocalDateTime lastProcessedOn = LocalDateTime.now();
        if (!isNull(processorCrawlerResultEntity)) {
            processorCrawlerResultEntity.setLastProcessedOn(lastProcessedOn);
        } else {
            processorCrawlerResultEntity = new ProcessorCrawlerResultEntity(id, lastProcessedOn);
            processorCrawlerResultRepository.save(processorCrawlerResultEntity);
        }
    }

    /**
     * Updates processor after success finish.
     *
     * @param processorId     processor identifier
     * @param lastProcessedOn last processed on
     */
    public void updateProcessorAfterSuccessFinish(Long processorId, LocalDateTime lastProcessedOn) {

        ProcessorEntity processorEntity = processorRepository.findOne(processorId);
        processorEntity.setLastProcessedOn(Timestamp.valueOf(lastProcessedOn));
        processorEntity.setEnabled(true);
        processorRepository.save(processorEntity);
    }

    /**
     * Updates processor after failed.
     *
     * @param processorId     processor identifier
     * @param lastProcessedOn last processed on
     */
    public void updateProcessorAfterFailed(Long processorId, LocalDateTime lastProcessedOn) {

        ProcessorEntity processorEntity = processorRepository.findOne(processorId);
        processorEntity.setLastProcessedOn(Timestamp.valueOf(lastProcessedOn));
        processorEntity.setEnabled(false);
        processorRepository.save(processorEntity);
    }

    /**
     * Get source name by crawler identifier.
     *
     * @param crawlerId crawler identifier
     * @return source name
     */
    public Optional<String> getSourceName(long crawlerId) {
        SourceEntity source = sourceRepository.findOneByCrawlerId(crawlerId);
        return !isNull(source) ? Optional.of(source.getProvider()) : Optional.empty();
    }
}
