package cz.lp.crawler.custom.advertisement.srealitycz.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.util.List;

@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class SrealityCzEmbeddedDto {

    private final List<SrealityCzEstateDto> estates;

    @JsonCreator
    public SrealityCzEmbeddedDto(@JsonProperty("estates") List<SrealityCzEstateDto> estates) {
        this.estates = estates;
    }
}
