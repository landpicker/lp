package cz.lp.processor.resolver;

import cz.lp.enumerate.ProcessorCondition;
import cz.lp.processor.service.ProcessorConditionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Processor condition resolver provides method for obtaining information about meeting of condition.
 */
@Component
public class ProcessorConditionResolver {

    private final ProcessorConditionService conditionService;

    @Autowired
    public ProcessorConditionResolver(ProcessorConditionService conditionService) {
        this.conditionService = conditionService;
    }

    /**
     * Gets information about meeting the condition.
     *
     * @param processorCondition processor condition
     * @param crawlerId          crawler identifier
     * @return true if the condition is met, otherwise false
     */
    public boolean meetsCondition(ProcessorCondition processorCondition, Long crawlerId) {

        boolean meetsCondition;

        switch (processorCondition) {
            case NONE:
                meetsCondition = true;
                break;
            case EXIST_VDP_AREA_CRAWLER_RESULTS:
                meetsCondition = conditionService.existResultsOfVdpAreaCrawler(crawlerId);
                break;
            case EXIST_VDP_EXCHANGE_CRAWLER_RESULTS:
                meetsCondition = conditionService.existResultsOfVdpExchangeCrawler(crawlerId);
                break;
            case EXIST_BAZOS_CZ_CRAWLER_RESULTS:
                meetsCondition = conditionService.existResultsOfBazosCzCrawler(crawlerId);
                break;
                case EXIST_SREALITY_CZ_CRAWLER_RESULTS:
                meetsCondition = conditionService.existResultsOfSrealityCzCrawler(crawlerId);
                break;
            default:
                meetsCondition = true;
                break;
        }

        return meetsCondition;
    }
}

