package cz.lp.exception;

/**
 * Crawler unknown recrawů strategy exception is generate when crawle using unknown recrawl strategy.
 */
public class CrawlerUnknownRecrawlStrategyException extends Exception {

    public CrawlerUnknownRecrawlStrategyException(String message) {
        super(message);
    }

    public CrawlerUnknownRecrawlStrategyException(String message, Throwable cause) {
        super(message, cause);
    }
}
