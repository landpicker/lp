package cz.lp.processor.custom.landbase.adapter;

import cz.lp.common.dto.Epsg5514Coordinate;
import cz.lp.common.enumerate.LandType;
import cz.lp.processor.custom.landbase.Land;
import cz.lp.util.CoordinateUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class LandBaseDataAdapter {

    private final static String LAND_SELECTOR = "vf|Parcela";
    private final static String LAND_ID_SELECTOR = "pai|Id";
    private final static String LAND_TRUNK_NUMBER_SELECTOR = "pai|KmenoveCislo";
    private final static String LAND_UNDER_DIVISION_NUMBER_SELECTOR = "pai|PododdeleniCisla";
    private final static String LAND_ACREAGE_SELECTOR = "pai|VymeraParcely";
    private final static String LAND_TYPE_SELECTOR = "pai|DruhPozemkuKod";
    private final static String LAND_DEFINITION_POINT_SELECTOR = "pai|Geometrie pai|DefinicniBod gml|pos";
    private final static String LAND_BORDER_COORDINATES_SELECTOR = "pai|Geometrie pai|OriginalniHranice gml|posList";
    private final static String CADASTRAL_TERRITORY_AREA_CODE_SELECTOR = "kui|Kod";

    private Optional<List<Land>> lands;

    public LandBaseDataAdapter(String content, String url) {


        Document document = Jsoup.parse(content, url, Parser.xmlParser());
        extractLands(document);

    }

    private void extractLands(Document document) {

        List<Land> lands = document.select(LAND_SELECTOR).stream().map(landElement -> {

            Element idElement = landElement.selectFirst(LAND_ID_SELECTOR);
            if (idElement == null) return null;
            String id = idElement.text();

            Element trunkNumberElement = landElement.selectFirst(LAND_TRUNK_NUMBER_SELECTOR);
            if (trunkNumberElement == null) return null;

            int trunkNumber;
            try {
                trunkNumber = Integer.valueOf(trunkNumberElement.text());
            } catch (NumberFormatException ex) {
                return null;
            }

            Element underDivisionNumberElement = landElement.selectFirst(LAND_UNDER_DIVISION_NUMBER_SELECTOR);
            Integer underDivisionNumber = null;
            try {
                if (underDivisionNumberElement != null && !underDivisionNumberElement.text().isEmpty()) {
                    underDivisionNumber = Integer.valueOf(underDivisionNumberElement.text());
                }
            } catch (NumberFormatException ex) {
                return null;
            }

            Element acreageNumberElement = landElement.selectFirst(LAND_ACREAGE_SELECTOR);
            if (acreageNumberElement == null) return null;
            int acreage;
            try {
                acreage = Integer.valueOf(acreageNumberElement.text());
            } catch (NumberFormatException ex) {
                return null;
            }

            Element typeElement = landElement.selectFirst(LAND_TYPE_SELECTOR);
            if (typeElement == null) return null;
            LandType type;
            try {
                int typeId = Integer.valueOf(typeElement.text());
                Optional<LandType> optionalType = LandType.valueOfCode(typeId);
                if (!optionalType.isPresent()) return null;
                type = optionalType.get();
            } catch (NumberFormatException ex) {
                return null;
            }

            Element definitionPointElement = landElement.selectFirst(LAND_DEFINITION_POINT_SELECTOR);
            if (definitionPointElement == null) return null;
            List<Epsg5514Coordinate> definitionPoint = CoordinateUtil.extractEpsg5514Coordinates(definitionPointElement.text());
            if (definitionPoint.size() != 1) return null;

            Element borderCoordinatesElement = landElement.selectFirst(LAND_BORDER_COORDINATES_SELECTOR);
            if (borderCoordinatesElement == null) return null;
            List<Epsg5514Coordinate> borderCoordinates = CoordinateUtil.extractEpsg5514Coordinates(borderCoordinatesElement.text());

            Element cadastralTerritoryAreaCodesElement = landElement.selectFirst(CADASTRAL_TERRITORY_AREA_CODE_SELECTOR);
            if (cadastralTerritoryAreaCodesElement == null) return null;
            String cadastralTerritoryAreaCode = cadastralTerritoryAreaCodesElement.text();
            return new Land(id, type, trunkNumber, underDivisionNumber, acreage, definitionPoint.get(0), cadastralTerritoryAreaCode, borderCoordinates);
        })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        if(lands.isEmpty()){
            this.lands = Optional.empty();
        } else {
            this.lands = Optional.of(lands);
        }
    }

    /**
     * Gets lands.
     *
     * @return land
     */
    public Optional<List<Land>> getLands() {

        return lands;
    }
}
