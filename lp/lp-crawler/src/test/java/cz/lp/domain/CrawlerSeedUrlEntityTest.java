package cz.lp.domain;

import cz.lp.enumerate.SeedUrlCondition;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class CrawlerSeedUrlEntityTest {

    private static final Long ID = 1l;
    private static final CrawlerEntity CRAWLER = Mockito.mock(CrawlerEntity.class);
    private static final String URL = "http://";
    private static final Boolean ENABLED = Boolean.TRUE;
    private static final SeedUrlCondition CONDITION = SeedUrlCondition.NONE;

    @Test
    public void constructor_empty() throws Exception {

        CrawlerSeedUrlEntity crawlerSeedUtl = new CrawlerSeedUrlEntity();
        Assert.assertNotNull(crawlerSeedUtl);
        Assert.assertNull(crawlerSeedUtl.getId());
        Assert.assertNull(crawlerSeedUtl.getCrawler());
        Assert.assertNull(crawlerSeedUtl.getUrl());
        Assert.assertNull(crawlerSeedUtl.isEnabled());
        Assert.assertNull(crawlerSeedUtl.getCondition());
    }

    @Test
    public void constructor() throws Exception {

        CrawlerSeedUrlEntity crawlerSeedUtl = new CrawlerSeedUrlEntity(ID, CRAWLER, URL, ENABLED, CONDITION);
        Assert.assertNotNull(crawlerSeedUtl);
        Assert.assertEquals(ID, crawlerSeedUtl.getId());
        Assert.assertEquals(CRAWLER, crawlerSeedUtl.getCrawler());
        Assert.assertEquals(URL, crawlerSeedUtl.getUrl());
        Assert.assertEquals(ENABLED, crawlerSeedUtl.isEnabled());
        Assert.assertEquals(CONDITION, crawlerSeedUtl.getCondition());
    }

    @Test
    public void getId() throws Exception {

        CrawlerSeedUrlEntity crawlerSeedUtl = new CrawlerSeedUrlEntity(ID, CRAWLER, URL, ENABLED, CONDITION);
        Assert.assertNotNull(crawlerSeedUtl);
        Assert.assertEquals(ID, crawlerSeedUtl.getId());
    }

    @Test
    public void getCrawler() throws Exception {

        CrawlerSeedUrlEntity crawlerSeedUtl = new CrawlerSeedUrlEntity(ID, CRAWLER, URL, ENABLED, CONDITION);
        Assert.assertNotNull(crawlerSeedUtl);
        Assert.assertEquals(CRAWLER, crawlerSeedUtl.getCrawler());
    }

    @Test
    public void getUrl() throws Exception {

        CrawlerSeedUrlEntity crawlerSeedUtl = new CrawlerSeedUrlEntity(ID, CRAWLER, URL, ENABLED, CONDITION);
        Assert.assertNotNull(crawlerSeedUtl);
        Assert.assertEquals(URL, crawlerSeedUtl.getUrl());
    }

    @Test
    public void isEnabled() throws Exception {

        CrawlerSeedUrlEntity crawlerSeedUtl = new CrawlerSeedUrlEntity(ID, CRAWLER, URL, ENABLED, CONDITION);
        Assert.assertNotNull(crawlerSeedUtl);
        Assert.assertEquals(ENABLED, crawlerSeedUtl.isEnabled());
    }

    @Test
    public void getCondition() throws Exception {

        CrawlerSeedUrlEntity crawlerSeedUtl = new CrawlerSeedUrlEntity(ID, CRAWLER, URL, ENABLED, CONDITION);
        Assert.assertNotNull(crawlerSeedUtl);
        Assert.assertEquals(CONDITION, crawlerSeedUtl.getCondition());
    }
}