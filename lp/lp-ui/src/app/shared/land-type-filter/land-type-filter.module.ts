import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandTypeFilterComponent } from './land-type-filter.component';
import { MatCheckboxModule, MatFormFieldModule, MatButtonModule } from '../../../../node_modules/@angular/material';
import { TranslateModule } from '../../../../node_modules/@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '../../../../node_modules/@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatButtonModule,
    MatCheckboxModule
  ],
  exports: [LandTypeFilterComponent],
  declarations: [LandTypeFilterComponent]
})
export class LandTypeFilterModule { }
