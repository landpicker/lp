package cz.lp.common.domain.ad;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ad_mail_request")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AdMailRequestEntity {

    @EmbeddedId
    private AdMailRequestId adMailRequestId;

    @Embeddable
    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    public static class AdMailRequestId implements Serializable {

        private static final long serialVersionUID = 1L;

        @Column(name = "ad_url", nullable = false)
        private String adUrl;

        @Column(name = "mail_request_id", nullable = false)
        private Long mailRequestId;
    }
}
