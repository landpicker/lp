package cz.lp.crawler.custom.advertisement.srealitycz;

import cz.lp.CrawlerRunner;
import cz.lp.CrawlerScheduler;
import cz.lp.CrawlerWorkerService;
import cz.lp.StandaloneProcessorScheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

/**
 * This class run only SrealityCzCrawlerRunner {@link SrealityCzCrawlerRunner}
 */

@ComponentScan(basePackages = "cz.lp", excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {CrawlerRunner.class, CrawlerScheduler.class, StandaloneProcessorScheduler.class}))
public class SrealityCzCrawlerRunner {

    private static final Logger log = LoggerFactory.getLogger(SrealityCzCrawlerRunner.class);

    private static final Long ID = 4l;

    public static void main(String[] args) {

        log.info("Starting Sreality CZ advertisements crawler runner ...");
        ApplicationContext ctx = new AnnotationConfigApplicationContext(SrealityCzCrawlerRunner.class);
        CrawlerWorkerService crawlerWorkerService = ctx.getBean(CrawlerWorkerService.class);
        SrealityCzCrawler crawler = (SrealityCzCrawler) crawlerWorkerService.getCrawler(ID);
        if (crawler.checkCondition()) {
            crawler.crawl();
        } else {
            log.warn("Sreality CZ advertisement crawler does not meet startup conditions");
        }

    }
}
