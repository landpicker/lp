import { Component, AfterViewInit, Input, ElementRef, Output, EventEmitter } from '@angular/core';
import { MapPolygon, MAP_FOCUSED_POLYGON_OPTIONS, MAP_DEFAULT_POLYGON_OPTIONS, MapEpsg5514Coordinate } from './map.interface';
import { TranslateService } from '../../../../node_modules/@ngx-translate/core';

declare var SMap: any;
declare var JAK: any;
declare var Loader: any;

const LAYER_POLYGON_ID = 'layer_polygon_id';
let component; // For event listener functions

@Component({
	selector: 'lp-map',
	templateUrl: './map.component.html',
	styleUrls: ['./map.component.scss']
})
export class MapComponent implements AfterViewInit {
	private _polygons: MapPolygon[];
	@Input()
	set polygons(polygons: MapPolygon[]) {
		this._polygons = polygons;
		if (this._isMapLoaded) {
			// If the array of polygons is changed, polygons is loaded (rendered) again.
			this._renderPolygons();
		}
	}
	get polygons() {
		return this._polygons;
  }

	@Output()
	selectPolygon = new EventEmitter<string>();

	private _map: any;
	private _isMapLoaded = false;
	private _layers = {};
	private _polygonGeometriesLayer;

	constructor(private _element: ElementRef, private _translate: TranslateService) {
		component = this;
	}

	ngAfterViewInit() {
		// Load external mapy.cz libraries and ini map.
		this._loadExternalLibrariesAndInitMap();
	}

	focusOnPolygon(id: string) {
		const layer = this._map.getLayer(LAYER_POLYGON_ID);
		let polygonGeometries: any[] = layer.getGeometries();
		this._setDefaultOptionsForAllPolygons(polygonGeometries);
		this._setFocusedOptionForPolygon(polygonGeometries[id]);
  }

  private _setDefaultOptionsForAllPolygons(polygons) {
		this.polygons.forEach((polygon) => {
			polygons[polygon.id].setOptions(MAP_DEFAULT_POLYGON_OPTIONS);
		});
	}

	private _setFocusedOptionForPolygon(polygon) {
		polygon.setOptions(MAP_FOCUSED_POLYGON_OPTIONS);
	}

	private _initMap() {
		this._map = new SMap(JAK.gel('map'));
		this._layers[SMap.DEF_BASE] = this._map.addDefaultLayer(SMap.DEF_BASE);
		this._layers[SMap.DEF_OPHOTO] = this._map.addDefaultLayer(SMap.DEF_OPHOTO);
    this._layers[SMap.DEF_BASE].enable();

    // TODO: add controls
    this._map.addDefaultControls();
    // this._addMouseControl();

		this._polygonGeometriesLayer = new SMap.Layer.Geometry(LAYER_POLYGON_ID);
		this._map.addLayer(this._polygonGeometriesLayer);
		this._polygonGeometriesLayer.enable();

		this._map.getSignals().addListener(window, 'geometry-click', this._handlePolygonClick);
		this._isMapLoaded = true;
  }

  private _addMouseControl() {
    const mouse = new SMap.Control.Mouse(SMap.MOUSE_PAN | SMap.MOUSE_WHEEL | SMap.MOUSE_ZOOM);
    this._map.addControl(mouse);
  }

	private _handlePolygonClick(e) {
    const selectedPolygonId = e.target.getId();
		component.focusOnPolygon(selectedPolygonId);
		component.selectPolygon.emit(selectedPolygonId);
	}

	private _renderPolygons() {
		if (this.polygons && this.polygons.length > 0) {

      // Remove all polygons
			this._polygonGeometriesLayer.removeAll();

      // Render geometry for each polygon
			this.polygons.forEach((polygon) => {
				let points = new Array();
				for (let coordinate of polygon.border.coordinates) {
					points.push(SMap.Coords.fromJTSK(coordinate.y * -1, coordinate.x * -1));
				}
				const polygonGeometry = new SMap.Geometry(SMap.GEOMETRY_POLYGON, polygon.id, points, MAP_DEFAULT_POLYGON_OPTIONS);
				this._polygonGeometriesLayer.addGeometry(polygonGeometry);
			});

			// Focus on the last polygon
			// Center and zoom map on the last polygon
			const lastPolygon = this.polygons[this.polygons.length - 1];
			this.focusOnPolygon(lastPolygon.id);
			this._centerAndZoomMapOnPolygon(lastPolygon.definitionPoint.coordinate);
		}
	}

	private _centerAndZoomMapOnPolygon(coordinate: MapEpsg5514Coordinate) {
		const center = SMap.Coords.fromJTSK(coordinate.y * -1, coordinate.x * -1);
		this._map.setCenterZoom(center, 15, true);
	}

	async _loadExternalLibrariesAndInitMap() {
		await this._loadScript('https://api.mapy.cz/loader.js');
		const version = Loader.version;
		const languageCode = this._translate.defaultLang;
		await this._loadScript('https://api.mapy.cz/js/api/v4/smap-jak.js?v=' + version);
		await this._loadScript('https://api.mapy.cz/config.js?key=&v=' + version);
		await this._loadScript('https://api.mapy.cz/js/lang/' + languageCode + '.js?' + version);
		await this._loadStyles('https://api.mapy.cz/css/api/v4/smap-jak.css?' + version);
		this._initMap();
		this._renderPolygons();
	}

	private _loadScript(scriptUrl: string) {
		return new Promise((resolve, reject) => {
			const scriptElement = document.createElement('script');
			scriptElement.src = scriptUrl;
			scriptElement.onload = resolve;
			this._element.nativeElement.prepend(scriptElement);
		});
	}

	private _loadStyles(styleUrl: string) {
		return new Promise((resolve, reject) => {
			const styleElement = document.createElement('link');
			styleElement.href = styleUrl;
			styleElement.rel = 'stylesheet';
			styleElement.onload = resolve;
			this._element.nativeElement.prepend(styleElement);
		});
	}
}
