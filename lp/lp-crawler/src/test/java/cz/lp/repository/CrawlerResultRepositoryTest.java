package cz.lp.repository;

import cz.lp.domain.CrawlerEntity;
import cz.lp.domain.CrawlerResultEntity;
import cz.lp.enumerate.ResultStatus;
import cz.lp.repository.configuration.RepositoryTemplate;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class CrawlerResultRepositoryTest extends RepositoryTemplate {

    @Autowired
    CrawlerRepository crawlerRepository;

    @Autowired
    CrawlerResultRepository crawlerResultRepository;

    private static final Long CRAWLER_ID = 1l;
    private static final Timestamp CREATED_ON = Timestamp.valueOf("2018-01-01 00:00:01.000000");
    private static final Timestamp LAST_CRAWLED_ON = Timestamp.valueOf("2018-03-01 00:00:01.000000");
    private static final Timestamp DIFFERENT_LAST_CRAWLED_ON = Timestamp.valueOf("2018-05-01 00:00:01.000000");

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler/INSERT_crawler_WITH_id_=_1_AND_recrawl_on_=_NULL_AND_ENABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void insert() throws Exception {

        CrawlerEntity crawlerEntity = crawlerRepository.findOne(1l);
        CrawlerResultEntity crawlerResultEntity = new CrawlerResultEntity(crawlerEntity, ResultStatus.SUCCESS, "http://result_1", 1, Timestamp.valueOf(LocalDateTime.now()));
        CrawlerResultEntity savedCrawlerResultEntity = crawlerResultRepository.save(crawlerResultEntity);
        Assert.assertNotNull(savedCrawlerResultEntity.getId());
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler/INSERT_crawler_WITH_id_=_1_AND_recrawl_on_=_NULL_AND_ENABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler-result/INSERT_crawler_result_WITH_id_=_1_AND_status_=_NOT_FETCHED_AND_order_=_0_AND_last_crawled_on_IS_NULL.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void update() throws Exception {

        ResultStatus newStatus = ResultStatus.SUCCESS;
        byte[] newContent = "<html>new content</html>".getBytes();
        String newContentType = "application/pdf";
        String newHash = "new hash";
        String newMessage = "new message";
        Timestamp newLastCrawledOn = Timestamp.valueOf("2018-05-05 00:00:01.000000");
        Timestamp newLastModifiedOn = Timestamp.valueOf("2018-06-06 00:00:01.000000");

        CrawlerResultEntity crawlerResultEntity = crawlerResultRepository.findOne(1l);
        crawlerResultEntity.setStatus(newStatus);
        crawlerResultEntity.setContent(newContent);
        crawlerResultEntity.setContentType(newContentType);
        crawlerResultEntity.setHash(newHash);
        crawlerResultEntity.setMessage(newMessage);
        crawlerResultEntity.setLastCrawledOn(newLastCrawledOn);
        crawlerResultEntity.setLastModifiedOn(newLastModifiedOn);
        crawlerResultRepository.save(crawlerResultEntity);

        CrawlerResultEntity updatedCrawlerResultEntity = crawlerResultRepository.findOne(1l);
        Assert.assertNotNull(updatedCrawlerResultEntity);
        Assert.assertEquals(1l, crawlerResultEntity.getId().longValue());
        Assert.assertEquals(newStatus, crawlerResultEntity.getStatus());
        Assert.assertEquals(newContentType, crawlerResultEntity.getContentType());
        Assert.assertEquals(newContent, crawlerResultEntity.getContent());
        Assert.assertEquals(newHash, crawlerResultEntity.getHash());
        Assert.assertEquals(newMessage, crawlerResultEntity.getMessage());
        Assert.assertEquals(newLastCrawledOn, crawlerResultEntity.getLastCrawledOn());
        Assert.assertEquals(newLastModifiedOn, crawlerResultEntity.getLastModifiedOn());
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler/INSERT_crawler_WITH_id_=_1_AND_recrawl_on_=_NULL_AND_ENABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler-result/INSERT_crawler_result_WITH_id_=_1_AND_status_=_NOT_FETCHED_AND_order_=_0_AND_last_crawled_on_IS_NULL.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void findOne() throws Exception {

        CrawlerResultEntity crawlerResultEntity = crawlerResultRepository.findOne(1l);
        Assert.assertNotNull(crawlerResultEntity);
        Assert.assertEquals(1l, crawlerResultEntity.getId().longValue());
        Assert.assertEquals(ResultStatus.NOT_FETCHED, crawlerResultEntity.getStatus());
        Assert.assertEquals("http://result_1", crawlerResultEntity.getUrl());
        Assert.assertEquals("text/html", crawlerResultEntity.getContentType());
        Assert.assertEquals("<html></html>", new String(crawlerResultEntity.getContent()));
        Assert.assertEquals("hash", crawlerResultEntity.getHash());
        Assert.assertEquals("", crawlerResultEntity.getMessage());
        Assert.assertEquals(0, crawlerResultEntity.getProcessingOrder().intValue());
        Assert.assertEquals(CREATED_ON, crawlerResultEntity.getCreatedOn());
        Assert.assertNull(crawlerResultEntity.getLastCrawledOn());
        Assert.assertNull(crawlerResultEntity.getLastModifiedOn());
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler/INSERT_crawler_WITH_id_=_1_AND_recrawl_on_=_NULL_AND_ENABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler-result/INSERT_crawler_result_WITH_id_=_1_AND_status_=_NOT_FETCHED_AND_order_=_0_AND_last_crawled_on_IS_NULL.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void existByUrl_whenUrlExist_shouldReturnTrue() throws Exception {

        Assert.assertTrue(crawlerResultRepository.existByUrl("http://result_1"));
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler/INSERT_crawler_WITH_id_=_1_AND_recrawl_on_=_NULL_AND_ENABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler-result/INSERT_crawler_result_WITH_id_=_1_AND_status_=_NOT_FETCHED_AND_order_=_0_AND_last_crawled_on_IS_NULL.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void existByUrl_whenUrlDoesNotExist_shouldReturnFalse() throws Exception {

        Assert.assertFalse(crawlerResultRepository.existByUrl("http://result_invalid"));
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler/INSERT_crawler_WITH_id_=_1_AND_recrawl_on_=_NULL_AND_ENABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void existByUrl_whenNoUrlExist_shouldReturnFalse() throws Exception {

        Assert.assertFalse(crawlerResultRepository.existByUrl("http://result_invalid"));
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler/INSERT_crawler_WITH_id_=_1_AND_recrawl_on_=_NULL_AND_ENABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void findOneByCrawlerIdAndLastRecrawledOnOrderByProcessingOrder_whenNotExistAnyUrlForCrawl_shouldReturnNull() throws Exception {

        CrawlerResultEntity crawlerResultEntity = crawlerResultRepository.findOneByCrawlerIdAndLastRecrawledOnOrderByProcessingOrder(CRAWLER_ID, LAST_CRAWLED_ON);
        Assert.assertNull(crawlerResultEntity);
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler/INSERT_crawler_WITH_id_=_1_AND_recrawl_on_=_NULL_AND_ENABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler-result/INSERT_crawler_result_WITH_id_=_1_AND_status_=_NOT_FETCHED_AND_order_=_0_AND_last_crawled_on_IS_NULL.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void findOneByCrawlerIdAndLastRecrawledOnOrderByProcessingOrder_whenLastCrawledOnIsNull_shouldReturnResult() throws Exception {

        CrawlerResultEntity crawlerResultEntity = crawlerResultRepository.findOneByCrawlerIdAndLastRecrawledOnOrderByProcessingOrder(CRAWLER_ID, LAST_CRAWLED_ON);
        Assert.assertNotNull(crawlerResultEntity);
        Assert.assertEquals(1l, crawlerResultEntity.getId().longValue());
        Assert.assertEquals(ResultStatus.NOT_FETCHED, crawlerResultEntity.getStatus());
        Assert.assertEquals("http://result_1", crawlerResultEntity.getUrl());
        Assert.assertEquals("text/html", crawlerResultEntity.getContentType());
        Assert.assertEquals("<html></html>", new String(crawlerResultEntity.getContent()));
        Assert.assertEquals("hash", crawlerResultEntity.getHash());
        Assert.assertEquals("", crawlerResultEntity.getMessage());
        Assert.assertEquals(0, crawlerResultEntity.getProcessingOrder().intValue());
        Assert.assertEquals(CREATED_ON, crawlerResultEntity.getCreatedOn());
        Assert.assertNull(crawlerResultEntity.getLastCrawledOn());
        Assert.assertNull(crawlerResultEntity.getLastModifiedOn());
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler/INSERT_crawler_WITH_id_=_1_AND_recrawl_on_=_NULL_AND_ENABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler-result/INSERT_crawler_result_WITH_id_=_2_AND_status_=_SUCCESS_AND_order_=_1_AND_last_crawled_on_IS_NOT_NULL.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void findOneByCrawlerIdAndLastRecrawledOnOrderByProcessingOrder_whenLastCrawledOnIsNotNullAndDifferent_shouldReturnResult() throws Exception {

        CrawlerResultEntity crawlerResultEntity = crawlerResultRepository.findOneByCrawlerIdAndLastRecrawledOnOrderByProcessingOrder(CRAWLER_ID, DIFFERENT_LAST_CRAWLED_ON);
        Assert.assertNotNull(crawlerResultEntity);
        Assert.assertEquals(2l, crawlerResultEntity.getId().longValue());
        Assert.assertEquals(ResultStatus.SUCCESS, crawlerResultEntity.getStatus());
        Assert.assertEquals("http://result_2", crawlerResultEntity.getUrl());
        Assert.assertEquals("text/html", crawlerResultEntity.getContentType());
        Assert.assertEquals("<html></html>", new String(crawlerResultEntity.getContent()));
        Assert.assertEquals("hash", crawlerResultEntity.getHash());
        Assert.assertEquals("", crawlerResultEntity.getMessage());
        Assert.assertEquals(1, crawlerResultEntity.getProcessingOrder().intValue());
        Assert.assertEquals(CREATED_ON, crawlerResultEntity.getCreatedOn());
        Assert.assertEquals(LAST_CRAWLED_ON, crawlerResultEntity.getLastCrawledOn());
        Assert.assertNull(crawlerResultEntity.getLastModifiedOn());
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler/INSERT_crawler_WITH_id_=_1_AND_recrawl_on_=_NULL_AND_ENABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler-result/INSERT_crawler_result_WITH_id_=_3_AND_status_=_FAILED_AND_order_=_2_AND_last_crawled_on_IS_NOT_NULL.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void findOneByCrawlerIdAndLastRecrawledOnOrderByProcessingOrder_whenLastCrawledOnIsNotNullAndSame_shouldReturnResult() throws Exception {

        CrawlerResultEntity crawlerResultEntity = crawlerResultRepository.findOneByCrawlerIdAndLastRecrawledOnOrderByProcessingOrder(CRAWLER_ID, LAST_CRAWLED_ON);
        Assert.assertNull(crawlerResultEntity);
    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"classpath:db/clear_db.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/source/INSERT_source_WITH_id_=_1.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler/INSERT_crawler_WITH_id_=_1_AND_recrawl_on_=_NULL_AND_ENABLED.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler-result/INSERT_crawler_result_WITH_id_=_1_AND_status_=_NOT_FETCHED_AND_order_=_0_AND_last_crawled_on_IS_NULL.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler-result/INSERT_crawler_result_WITH_id_=_2_AND_status_=_SUCCESS_AND_order_=_1_AND_last_crawled_on_IS_NOT_NULL.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(scripts = {"classpath:db/data/crawler-result/INSERT_crawler_result_WITH_id_=_3_AND_status_=_FAILED_AND_order_=_2_AND_last_crawled_on_IS_NOT_NULL.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    })
    public void findOneByCrawlerIdAndLastRecrawledOnOrderByProcessingOrder_whenExistManyUrl_shouldReturnResultWithLowestOrder() throws Exception {

        CrawlerResultEntity crawlerResultEntity = crawlerResultRepository.findOneByCrawlerIdAndLastRecrawledOnOrderByProcessingOrder(CRAWLER_ID, LAST_CRAWLED_ON);
        Assert.assertNotNull(crawlerResultEntity);
        Assert.assertEquals(1l, crawlerResultEntity.getId().longValue());
        Assert.assertEquals(0, crawlerResultEntity.getProcessingOrder().intValue());
    }
}